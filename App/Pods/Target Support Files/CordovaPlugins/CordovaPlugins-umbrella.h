#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "AppDelegate+nativepagetransitions.h"
#import "NativePageTransitions.h"
#import "ActionSheet.h"
#import "AppVersion.h"
#import "Calendar.h"
#import "CDVCamera.h"
#import "CDVExif.h"
#import "CDVJpegHeaderWriter.h"
#import "UIImage+CropScaleOrientation.h"
#import "CDVDevice.h"
#import "CDVNotification.h"
#import "CDVLocation.h"
#import "RequestLocationAccuracy.h"
#import "CFCallNumber.h"
#import "LaunchNavigatorPlugin.h"
#import "LN_LaunchNavigator.h"
#import "LN_Reachability.h"
#import "WE_CordovaLogger.h"
#import "WE_Logger.h"

FOUNDATION_EXPORT double CordovaPluginsVersionNumber;
FOUNDATION_EXPORT const unsigned char CordovaPluginsVersionString[];

