
  cordova.define('cordova/plugin_list', function(require, exports, module) {
    module.exports = [
      {
          "id": "cordova-plugin-calendar.Calendar",
          "file": "plugins/cordova-plugin-calendar/www/Calendar.js",
          "pluginId": "cordova-plugin-calendar",
        "clobbers": [
          "Calendar"
        ]
        },
      {
          "id": "call-number.CallNumber",
          "file": "plugins/call-number/www/CallNumber.js",
          "pluginId": "call-number",
        "clobbers": [
          "call"
        ]
        },
      {
          "id": "cordova-plugin-camera.Camera",
          "file": "plugins/cordova-plugin-camera/www/CameraConstants.js",
          "pluginId": "cordova-plugin-camera",
        "clobbers": [
          "Camera"
        ]
        },
      {
          "id": "cordova-plugin-camera.CameraPopoverHandle",
          "file": "plugins/cordova-plugin-camera/www/ios/CameraPopoverHandle.js",
          "pluginId": "cordova-plugin-camera",
        "clobbers": [
          "CameraPopoverHandle"
        ]
        },
      {
          "id": "cordova-plugin-camera.CameraPopoverOptions",
          "file": "plugins/cordova-plugin-camera/www/CameraPopoverOptions.js",
          "pluginId": "cordova-plugin-camera",
        "clobbers": [
          "CameraPopoverOptions"
        ]
        },
      {
          "id": "cordova-plugin-geolocation.Coordinates",
          "file": "plugins/cordova-plugin-geolocation/www/Coordinates.js",
          "pluginId": "cordova-plugin-geolocation",
        "clobbers": [
          "Coordinates"
        ]
        },
      {
          "id": "cordova-plugin-app-version.AppVersionPlugin",
          "file": "plugins/cordova-plugin-app-version/www/AppVersionPlugin.js",
          "pluginId": "cordova-plugin-app-version",
        "clobbers": [
          "cordova.getAppVersion"
        ]
        },
      {
          "id": "cordova-plugin-request-location-accuracy.RequestLocationAccuracy",
          "file": "plugins/cordova-plugin-request-location-accuracy/www/ios/RequestLocationAccuracy.js",
          "pluginId": "cordova-plugin-request-location-accuracy",
        "clobbers": [
          "cordova.plugins.locationAccuracy"
        ]
        },
      {
          "id": "cordova-plugin-android-permissions.Permissions",
          "file": "plugins/cordova-plugin-android-permissions/www/permissions-dummy.js",
          "pluginId": "cordova-plugin-android-permissions",
        "clobbers": [
          "cordova.plugins.permissions"
        ]
        },
      {
          "id": "cordova-plugin-device.device",
          "file": "plugins/cordova-plugin-device/www/device.js",
          "pluginId": "cordova-plugin-device",
        "clobbers": [
          "device"
        ]
        },
      {
          "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.Common",
          "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/common.js",
          "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
        "clobbers": [
          "launchnavigator"
        ]
        },
      {
          "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.LocalForage",
          "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/localforage.v1.5.0.min.js",
          "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
        "clobbers": [
          "localforage"
        ]
        },
      {
          "id": "cordova-plugin-camera.camera",
          "file": "plugins/cordova-plugin-camera/www/Camera.js",
          "pluginId": "cordova-plugin-camera",
        "clobbers": [
          "navigator.camera"
        ]
        },
      {
          "id": "cordova-plugin-geolocation.geolocation",
          "file": "plugins/cordova-plugin-geolocation/www/geolocation.js",
          "pluginId": "cordova-plugin-geolocation",
        "clobbers": [
          "navigator.geolocation"
        ]
        },
      {
          "id": "cordova-plugin-geolocation.Position",
          "file": "plugins/cordova-plugin-geolocation/www/Position.js",
          "pluginId": "cordova-plugin-geolocation",
        "clobbers": [
          "Position"
        ]
        },
      {
          "id": "cordova-plugin-geolocation.PositionError",
          "file": "plugins/cordova-plugin-geolocation/www/PositionError.js",
          "pluginId": "cordova-plugin-geolocation",
        "clobbers": [
          "PositionError"
        ]
        },
      {
          "id": "cordova-plugin-actionsheet.ActionSheet",
          "file": "plugins/cordova-plugin-actionsheet/www/ActionSheet.js",
          "pluginId": "cordova-plugin-actionsheet",
        "clobbers": [
          "window.plugins.actionsheet"
        ]
        },
      {
          "id": "com.telerik.plugins.nativepagetransitions.NativePageTransitions",
          "file": "plugins/com.telerik.plugins.nativepagetransitions/www/NativePageTransitions.js",
          "pluginId": "com.telerik.plugins.nativepagetransitions",
        "clobbers": [
          "window.plugins.nativepagetransitions"
        ]
        },
      {
          "id": "es6-promise-plugin.Promise",
          "file": "plugins/es6-promise-plugin/www/promise.js",
          "pluginId": "es6-promise-plugin",
        "runs": true
        },
      {
          "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.LaunchNavigator",
          "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/ios/launchnavigator.js",
          "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
        "merges": [
          "launchnavigator"
        ]
        },
      {
          "id": "cordova-plugin-dialogs.notification",
          "file": "plugins/cordova-plugin-dialogs/www/notification.js",
          "pluginId": "cordova-plugin-dialogs",
        "merges": [
          "navigator.notification"
        ]
        }
    ];
    module.exports.metadata =
    // TOP OF METADATA
    {
      "com.telerik.plugins.nativepagetransitions": "0.6.5",
      "cordova-plugin-actionsheet": "2.3.3",
      "cordova-plugin-android-permissions": "1.0.2",
      "cordova-plugin-app-version": "0.1.9",
      "cordova-plugin-calendar": "5.1.5",
      "cordova-plugin-camera": "4.1.0",
      "cordova-plugin-device": "2.0.3",
      "cordova-plugin-dialogs": "2.0.2",
      "cordova-plugin-geolocation": "4.0.2",
      "cordova-plugin-request-location-accuracy": "2.3.0",
      "es6-promise-plugin": "4.2.2",
      "call-number": "0.0.2",
      "uk.co.workingedge.phonegap.plugin.launchnavigator": "5.0.4"
    };
    // BOTTOM OF METADATA
    });
    