(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["settings-account-settings-account-settings-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/account-settings/account-settings.page.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/settings/account-settings/account-settings.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <!-- <ion-buttons slot=\"start\"> -->\n      <ion-back-button class=\"float-left\"></ion-back-button>\n    <!-- </ion-buttons> -->\n      <ion-title [size]=\"titleSize\">Account</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [scrollEvents]=\"true\" (ionScroll)=\"onContentScroll($event)\">\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-grid>\n    <form [formGroup]=\"accountForm\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"email\">\n              Email Address\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input type=\"text\" disabled formControlName=\"email\" id=\"email\">\n        </ion-col>\n      </ion-row>\n\n      <!-- <ion-row>\n        <ion-col size=\"12\" class=\"account-label ion-no-padding ion-text-uppercase\">\n          <label for=\"password\">\n            Password\n          </label>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input *ngIf=\"!showPw\" type=\"password\" placeholder=\"**************\" formControlName=\"password\" id=\"password\"\n            [ngClass]=\"{ 'is-invalid': (fc.password.dirty || fc.password.touched || submitted) && fc.password.errors, 'is-valid': (fc.password.dirty || fc.password.touched) && !fc.password.errors }\">\n          <input *ngIf=\"showPw\" type=\"text\" formControlName=\"password\" id=\"password\" [ngClass]=\"{ 'is-invalid': (fc.password.dirty || fc.password.touched || submitted) && fc.password.errors, 'is-valid': (fc.password.dirty || fc.password.touched) && !fc.password.errors }\">\n          <button ion-button clear color=\"dark\" type=\"button\" class=\"eye-btn\" (click)=\"showPw = !showPw\" item-right>\n            <ion-icon name=\"md-eye\"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row> -->\n\n      <ion-row>\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"firstName\">\n              First Name\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input type=\"text\" formControlName=\"firstName\" id=\"firstName\"\n            [ngClass]=\"{ 'is-invalid': (fc.firstName.dirty || fc.firstName.touched || submitted) && fc.firstName.errors, 'is-valid': (fc.firstName.dirty || fc.firstName.touched) && !fc.firstName.errors }\">\n        </ion-col>\n\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"middleName\">\n              Middle Name\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input type=\"text\" formControlName=\"middleName\" id=\"middleName\"\n          [ngClass]=\"{ 'is-invalid': (fc.middleName.dirty || fc.middleName.touched || submitted) && fc.middleName.errors, 'is-valid': (fc.middleName.dirty || fc.middleName.touched) && !fc.middleName.errors }\">\n        </ion-col>\n\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"lastName\">\n              Last Name\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input type=\"text\" formControlName=\"lastName\" id=\"lastName\"\n            [ngClass]=\"{ 'is-invalid': (fc.lastName.dirty || fc.lastName.touched || submitted) && fc.lastName.errors, 'is-valid': (fc.lastName.dirty || fc.lastName.touched) && !fc.lastName.errors }\">\n        </ion-col>\n\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"homePhone\">\n              Home Phone Number\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input type=\"number\" formControlName=\"homePhone\" id=\"homePhone\"\n            [ngClass]=\"{ 'is-invalid': (fc.homePhone.dirty || fc.homePhone.touched || submitted) && fc.homePhone.errors, 'is-valid': (fc.homePhone.dirty || fc.homePhone.touched) && !fc.homePhone.errors }\">\n        </ion-col>\n\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"mobilePhone\">\n              Mobile Phone Number\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input type=\"number\" formControlName=\"mobilePhone\" id=\"mobilePhone\"\n            [ngClass]=\"{ 'is-invalid': (fc.mobilePhone.dirty || fc.mobilePhone.touched || submitted) && fc.mobilePhone.errors, 'is-valid': (fc.mobilePhone.dirty || fc.mobilePhone.touched) && !fc.mobilePhone.errors }\">\n        </ion-col>\n\n        <!-- <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"vehicleType\">\n              Vehicle Type\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input type=\"text\" disabled formControlName=\"vehicleType\" id=\"vehicleType\" [ngClass]=\"{ 'is-invalid': (fc.vehicleType.dirty || fc.vehicleType.touched || submitted) && fc.vehicleType.errors, 'is-valid': (fc.vehicleType.dirty || fc.vehicleType.touched) && !fc.vehicleType.errors }\">\n        </ion-col>\n\n\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"plateNumber\">\n              Vehicle Plate Number\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input type=\"text\" disabled formControlName=\"plateNumber\" id=\"plateNumber\" [ngClass]=\"{ 'is-invalid': (fc.plateNumber.dirty || fc.plateNumber.touched || submitted) && fc.plateNumber.errors, 'is-valid': (fc.plateNumber.dirty || fc.plateNumber.touched) && !fc.plateNumber.errors }\">\n        </ion-col> -->\n      </ion-row>\n\n      <ion-row>\n        <ion-col size=\"12\" class=\"settings-button shadow\">\n          <button class=\"btn shadow-lg settings-btn save-btn\" icon-only round ion-button (click)=\"save()\">\n            Save Changes\n          </button>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size=\"12\" class=\"settings-button shadow\">\n          <button class=\"btn shadow-lg settings-btn\" icon-only round ion-button (click)=\"changePassword()\">\n            Change Password\n          </button>\n        </ion-col>\n      </ion-row>\n    </form>\n  </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/settings/account-settings/account-settings.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/settings/account-settings/account-settings.module.ts ***!
  \**********************************************************************/
/*! exports provided: AccountSettingsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountSettingsPageModule", function() { return AccountSettingsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _account_settings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./account-settings.page */ "./src/app/settings/account-settings/account-settings.page.ts");







const routes = [
    {
        path: '',
        component: _account_settings_page__WEBPACK_IMPORTED_MODULE_6__["AccountSettingsPage"]
    }
];
let AccountSettingsPageModule = class AccountSettingsPageModule {
};
AccountSettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"].forRoot({
                animated: true
            }),
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_account_settings_page__WEBPACK_IMPORTED_MODULE_6__["AccountSettingsPage"]]
    })
], AccountSettingsPageModule);



/***/ }),

/***/ "./src/app/settings/account-settings/account-settings.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/settings/account-settings/account-settings.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-title {\n  color: #003A58;\n  font-family: \"Montserrat-Bold\";\n  padding-top: 1rem;\n  margin-right: 5rem;\n}\n\n.float-left {\n  float: left;\n  margin-bottom: 3rem;\n}\n\nion-grid {\n  max-width: 350px;\n}\n\n.button-text {\n  color: #003A58 !important;\n}\n\n.account-label {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: left;\n          justify-content: left;\n  -webkit-box-align: left;\n          align-items: left;\n  height: 100%;\n  width: 100%;\n  color: #727272;\n  font-size: 0.75rem;\n  padding-top: 2rem;\n  font-family: \"Roboto-Medium\";\n}\n\n.account-input {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  height: 100%;\n  width: 100%;\n  color: #424242;\n  padding: 1rem 0 0 0;\n  font-family: \"Roboto-Bold\";\n  border-bottom: 0.001rem solid #a0a0a0;\n}\n\ninput {\n  background-color: transparent;\n  border: 0;\n  font-size: 1rem;\n  width: 100%;\n  font-weight: bold;\n  padding-bottom: 0.5rem;\n}\n\ninput[type=password] {\n  -webkit-text-stroke-width: 0.18rem;\n  letter-spacing: 0.18rem;\n}\n\n.settings-button {\n  padding-top: 1.5rem;\n}\n\n.eye-btn {\n  background-color: white;\n  color: #003A58;\n  font-size: 1rem;\n}\n\n.settings-btn {\n  background-color: #003A58;\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n  line-height: 1.3125rem;\n}\n\n.save-btn {\n  background: #F4773B 100% !important;\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036)) !important;\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%) !important;\n}\n\nspan {\n  color: white;\n  font-size: 0.75rem;\n  text-align: center;\n  width: 100%;\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL3NldHRpbmdzL2FjY291bnQtc2V0dGluZ3MvYWNjb3VudC1zZXR0aW5ncy5wYWdlLnNjc3MiLCIvVXNlcnMvYW50aG9ueS5icmlvbmVzL0RvY3VtZW50cy9DYXBEcml2ZXIvc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hcHAvc2V0dGluZ3MvYWNjb3VudC1zZXR0aW5ncy9hY2NvdW50LXNldHRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFPQTtFQUNJLHlCQUFBO0VBQ0EsZ0RBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkFBQTtBQ05KOztBRFNBO0VBQ0kseUJFbEJjO0VGbUJkLFdBQUE7RUFDQSxjRWhCSTtFRmlCSixtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtBQ05KOztBRFNBO0VBQ0ksK0JBQUE7RUFBQSx3QkFBQTtFQUNBLGdDQUFBO0VBQ0Esb0NBQUE7VUFBQSw4QkFBQTtBQ05KOztBRFNBO0VBQ0ksbUNBQUE7VUFBQSxrQ0FBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7VUFBQSx5QkFBQTtBQ05KOztBRFNBO0VBQ0ksd0JBQUE7VUFBQSx1QkFBQTtBQ05KOztBRFNBO0VBQ0ksWUFBQTtBQ05KOztBRS9DQTtFQUNJLGNERmM7RUNHZCw4QkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUZrREo7O0FFMUNBO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0FGNkNKOztBRTFDQTtFQUNJLGdCQUFBO0FGNkNKOztBRTFDQTtFQUNJLHlCQUFBO0FGNkNKOztBRXpDQTtFQUNJLG9CQUFBO0VBQUEsYUFBQTtFQUNBLHNCQUFBO1VBQUEscUJBQUE7RUFDQSx1QkFBQTtVQUFBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLDRCQUFBO0FGNENKOztBRXpDQTtFQUNJLG9CQUFBO0VBQUEsYUFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSwwQkFBQTtFQUNBLHFDQUFBO0FGNENKOztBRXpDQTtFQUNJLDZCQUFBO0VBQ0EsU0FBQTtFQUVBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBRjJDSjs7QUV4Q0E7RUFDSSxrQ0FBQTtFQUNBLHVCQUFBO0FGMkNKOztBRXhDQTtFQUNJLG1CQUFBO0FGMkNKOztBRXhDQTtFQUVJLHVCQUFBO0VBQ0EsY0R6RWM7RUMwRWQsZUFBQTtBRjBDSjs7QUV0Q0E7RUFDSSx5QkQvRWM7RUNpRmQsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0VBQ0Esc0JBQUE7QUZ3Q0o7O0FFckNBO0VBQ0ksbUNBQUE7RUFDQSx3R0FBQTtFQUFBLCtFQUFBO0FGd0NKOztBRXJDQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUZ3Q0oiLCJmaWxlIjoic3JjL2FwcC9zZXR0aW5ncy9hY2NvdW50LXNldHRpbmdzL2FjY291bnQtc2V0dGluZ3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2ZvbnRzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMnO1xuXG4vLyAuaXMtdmFsaWQge1xuLy8gICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWVcbi8vIH1cblxuLmlzLWludmFsaWQge1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNFRTQwMzY7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDNcbn1cblxuLmlzLWludmFsaWQtc2VsZWN0IHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6ICR3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgaGVpZ2h0OiAycmVtO1xufVxuXG4uYmctY29sb3ItMXtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgICBkaXNwbGF5OiBmbGV4IWltcG9ydGFudDtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXIhaW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5mbGV4LWFsaWduLWNlbnRlciB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgICBvcGFjaXR5OiAwLjU7XG59XG5cblxuXG5cbiIsIi5pcy1pbnZhbGlkIHtcbiAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICBib3JkZXI6IDFweCBzb2xpZCAjNUVCMDAzO1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWFsaWduLXJpZ2h0IHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgb3BhY2l0eTogMC41O1xufVxuXG5pb24tdGl0bGUge1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIHBhZGRpbmctdG9wOiAxcmVtO1xuICBtYXJnaW4tcmlnaHQ6IDVyZW07XG59XG5cbi5mbG9hdC1sZWZ0IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbi1ib3R0b206IDNyZW07XG59XG5cbmlvbi1ncmlkIHtcbiAgbWF4LXdpZHRoOiAzNTBweDtcbn1cblxuLmJ1dHRvbi10ZXh0IHtcbiAgY29sb3I6ICMwMDNBNTggIWltcG9ydGFudDtcbn1cblxuLmFjY291bnQtbGFiZWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGxlZnQ7XG4gIGFsaWduLWl0ZW1zOiBsZWZ0O1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogIzcyNzI3MjtcbiAgZm9udC1zaXplOiAwLjc1cmVtO1xuICBwYWRkaW5nLXRvcDogMnJlbTtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLU1lZGl1bVwiO1xufVxuXG4uYWNjb3VudC1pbnB1dCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogIzQyNDI0MjtcbiAgcGFkZGluZzogMXJlbSAwIDAgMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgYm9yZGVyLWJvdHRvbTogMC4wMDFyZW0gc29saWQgI2EwYTBhMDtcbn1cblxuaW5wdXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgYm9yZGVyOiAwO1xuICBmb250LXNpemU6IDFyZW07XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgcGFkZGluZy1ib3R0b206IDAuNXJlbTtcbn1cblxuaW5wdXRbdHlwZT1wYXNzd29yZF0ge1xuICAtd2Via2l0LXRleHQtc3Ryb2tlLXdpZHRoOiAwLjE4cmVtO1xuICBsZXR0ZXItc3BhY2luZzogMC4xOHJlbTtcbn1cblxuLnNldHRpbmdzLWJ1dHRvbiB7XG4gIHBhZGRpbmctdG9wOiAxLjVyZW07XG59XG5cbi5leWUtYnRuIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LXNpemU6IDFyZW07XG59XG5cbi5zZXR0aW5ncy1idG4ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gIGhlaWdodDogM3JlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLnNhdmUtYnRuIHtcbiAgYmFja2dyb3VuZDogI0Y0NzczQiAxMDAlICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpICFpbXBvcnRhbnQ7XG59XG5cbnNwYW4ge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufSIsIlxuLy8gY29sb3JzXG4vLyAkcHJpbWFyeS1jb2xvci0xOiAjMDBBM0M4O1xuJHByaW1hcnktY29sb3ItMTogIzAwM0E1ODtcbiRwcmltYXJ5LWNvbG9yLTI6ICNGQUFGNDA7XG4kcHJpbWFyeS1jb2xvci0zOiAjRjQ3NzNCO1xuXG4kd2hpdGU6ICNmZmZmZmY7XG5cbi8vICRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgcmdiYSgyNTUsMjU1LDI1NSwwLjUwKSAwJSwgcmdiYSgwLDAsMCwwLjUwKSAxMDAlKTtcbiRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KC0xODBkZWcsICNGRjg4NDAgMCUsICNFRTQwMzYgMTAwJSk7IiwiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2NvbW1vbi5zY3NzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XG5cblxuaW9uLXRpdGxlIHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgcGFkZGluZy10b3A6IDFyZW07XG4gICAgbWFyZ2luLXJpZ2h0OiA1cmVtO1xufVxuXG4vLyBpb24tYnV0dG9ucyB7XG4vLyAgICAgLy8gbWFyZ2luLWJvdHRvbTogMXJlbTtcbi8vICAgICAvLyBwYWRkaW5nLWJvdHRvbTogM3JlbTtcbi8vIH1cblxuLmZsb2F0LWxlZnR7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWFyZ2luLWJvdHRvbTogM3JlbTtcbn1cblxuaW9uLWdyaWQge1xuICAgIG1heC13aWR0aDogMzUwcHg7XG59XG5cbi5idXR0b24tdGV4dCB7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTEgIWltcG9ydGFudDtcbn1cblxuXG4uYWNjb3VudC1sYWJlbHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogbGVmdDtcbiAgICBhbGlnbi1pdGVtczogbGVmdDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6ICM3MjcyNzI7XG4gICAgZm9udC1zaXplOiAwLjc1cmVtO1xuICAgIHBhZGRpbmctdG9wOiAycmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLU1lZGl1bSc7XG59XG5cbi5hY2NvdW50LWlucHV0e1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6ICM0MjQyNDI7XG4gICAgcGFkZGluZzogMXJlbSAwIDAgMDtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1Cb2xkJztcbiAgICBib3JkZXItYm90dG9tOiAwLjAwMXJlbSBzb2xpZCAjYTBhMGEwO1xufVxuXG5pbnB1dHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXI6IDA7XG4gICAgLy8gYm9yZGVyLWJvdHRvbTogMC4wMDFyZW0gc29saWQgd2hpdGU7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHBhZGRpbmctYm90dG9tOiAwLjVyZW07XG59XG5cbmlucHV0W3R5cGU9XCJwYXNzd29yZFwiXXtcbiAgICAtd2Via2l0LXRleHQtc3Ryb2tlLXdpZHRoOiAwLjE4cmVtO1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjE4cmVtO1xufVxuXG4uc2V0dGluZ3MtYnV0dG9ue1xuICAgIHBhZGRpbmctdG9wOiAxLjVyZW07XG59XG5cbi5leWUtYnRuIHtcbiAgICAvLyBoZWlnaHQ6IDUwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIFxufVxuXG4uc2V0dGluZ3MtYnRue1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLy8gYmFja2dyb3VuZC1pbWFnZTogJGJ1dHRvbi1ncmFkaWVudC0xO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLnNhdmUtYnRuIHtcbiAgICBiYWNrZ3JvdW5kOiAjRjQ3NzNCIDEwMCUgIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiAkYnV0dG9uLWdyYWRpZW50LTEgIWltcG9ydGFudDtcbn1cblxuc3BhbntcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiAwLjc1cmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/settings/account-settings/account-settings.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/settings/account-settings/account-settings.page.ts ***!
  \********************************************************************/
/*! exports provided: AccountSettingsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountSettingsPage", function() { return AccountSettingsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_shared_models_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/shared/models/user.model */ "./src/shared/models/user.model.ts");
/* harmony import */ var src_shared_services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/shared/services/user.service */ "./src/shared/services/user.service.ts");
/* harmony import */ var src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/shared/services/alert.service */ "./src/shared/services/alert.service.ts");
/* harmony import */ var src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/shared/services/toast.service */ "./src/shared/services/toast.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/shared/services/loader.service */ "./src/shared/services/loader.service.ts");










// import { NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
let AccountSettingsPage = class AccountSettingsPage {
    constructor(formBuilder, userService, alertService, toastr, domCtrl, router, loader, nav, toastService) {
        this.formBuilder = formBuilder;
        this.userService = userService;
        this.alertService = alertService;
        this.toastr = toastr;
        this.domCtrl = domCtrl;
        this.router = router;
        this.loader = loader;
        this.nav = nav;
        this.toastService = toastService;
        this.titleSize = "large";
    }
    ngOnInit() {
        if (localStorage.getItem('current_user')) {
            this.currentUser = JSON.parse(localStorage.getItem('current_user'));
        }
        this.createAccountForm();
        console.log(this.currentUser);
    }
    ionViewDidEnter() {
    }
    onContentScroll(ev) {
        this.domCtrl.write(() => {
            // this.updateHeader(ev);
            // console.log(ev.detail.scrollTop);
            if (ev.detail.scrollTop > 50) {
                this.titleSize = undefined;
            }
            else {
                this.titleSize = "large";
            }
        });
    }
    createAccountForm() {
        this.accountForm = this.formBuilder.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            firstName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^([a-zA-Z]+[\-\' ]?)+[a-zA-Z]$/)]],
            middleName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^([a-zA-Z]+[\-\' ]?)+[a-zA-Z]$/)]],
            lastName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^([a-zA-Z]+[\-\' ]?)+[a-zA-Z]$/)]],
            homePhone: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].min(1000000000), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].max(9999999999)]],
            mobilePhone: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].min(1000000000), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].max(9999999999)]],
        });
        this.setValues();
    }
    setValues() {
        this.fc.email.patchValue(this.currentUser.email);
        this.fc.firstName.patchValue(this.currentUser.firstname);
        this.fc.middleName.patchValue(this.currentUser.middlename);
        this.fc.lastName.patchValue(this.currentUser.lastname);
        this.fc.homePhone.patchValue(this.currentUser.homePhone);
        this.fc.mobilePhone.patchValue(this.currentUser.mobilePhone);
        // this.fc.vehicleType.patchValue(this.currentUser.vehicles[0].type);
        // this.fc.plateNumber.patchValue(this.currentUser.vehicles[0].licensePlate);
    }
    save() {
        this.markFormGroupTouched(this.accountForm);
        if (this.accountForm.invalid) {
            this.toastr.presentToast("Please complete the required fields");
            return;
        }
        this.newUserInfo = JSON.parse(localStorage.getItem("current_user"));
        let newUser = new src_shared_models_user_model__WEBPACK_IMPORTED_MODULE_3__["User"];
        newUser.firstname = this.fc.firstName.value;
        newUser.lastname = this.fc.lastName.value;
        if (this.fc.middleName.value != null) {
            newUser.middlename = this.fc.middleName.value;
        }
        else {
            newUser.middlename = '';
        }
        if (this.fc.homePhone.value != null) {
            newUser.homePhone = this.fc.homePhone.value.toString();
        }
        else {
            newUser.homePhone = '';
        }
        console.log("homephone: ", newUser.homePhone);
        newUser.mobilePhone = this.fc.mobilePhone.value.toString();
        this.loader.createLoader("Saving Changes...");
        this.userService.updateUser(newUser).subscribe(result => {
            console.log("Success", result);
            this.newUserInfo.firstname = result.user.firstname;
            this.newUserInfo.lastname = result.user.lastname;
            this.newUserInfo.middlename = result.user.middlename;
            this.newUserInfo.homePhone = result.user.homePhone;
            this.newUserInfo.mobilePhone = result.user.mobilePhone;
            localStorage.setItem("current_user", JSON.stringify(this.newUserInfo));
            this.currentUser = JSON.parse(localStorage.getItem('current_user'));
            this.setValues();
            this.loader.dismissLoader();
            this.toastr.presentToast("User succesfully updated");
        }, error => {
            if (error.status == 0) {
                this.toastService.presentToast("Check your internet connection and try again.");
            }
            this.loader.dismissLoader();
            console.log("Error", error);
        });
    }
    changePassword() {
        console.log("change Password");
        this.router.navigate([`/tabs/settings/change-password`]);
    }
    get fc() { return this.accountForm.controls; }
    markFormGroupTouched(formGroup) {
        Object.values(formGroup.controls).forEach(control => {
            control.markAsTouched();
        });
    }
    doRefresh(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log('Begin async operation');
            this.ngOnInit();
            event.target.complete();
        });
    }
};
AccountSettingsPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: src_shared_services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
    { type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_6__["ToastService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["DomController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] },
    { type: src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_9__["LoaderService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] },
    { type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_6__["ToastService"] }
];
AccountSettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-account-settings',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./account-settings.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/account-settings/account-settings.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./account-settings.page.scss */ "./src/app/settings/account-settings/account-settings.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        src_shared_services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
        src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"],
        src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_6__["ToastService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["DomController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
        src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_9__["LoaderService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"],
        src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_6__["ToastService"]])
], AccountSettingsPage);



/***/ }),

/***/ "./src/shared/models/user.model.ts":
/*!*****************************************!*\
  !*** ./src/shared/models/user.model.ts ***!
  \*****************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class User {
}


/***/ })

}]);
//# sourceMappingURL=settings-account-settings-account-settings-module-es2015.js.map