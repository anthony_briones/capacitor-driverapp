function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~home-home-module~routes-route-details-route-details-module~routes-routes-module~shyfts-shyft~3de54bc4"], {
  /***/
  "./src/shared/pipes/convert24hrto12hr.pipe.ts":
  /*!****************************************************!*\
    !*** ./src/shared/pipes/convert24hrto12hr.pipe.ts ***!
    \****************************************************/

  /*! exports provided: Convert24hrto12hrPipe */

  /***/
  function srcSharedPipesConvert24hrto12hrPipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Convert24hrto12hrPipe", function () {
      return Convert24hrto12hrPipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var Convert24hrto12hrPipe =
    /*#__PURE__*/
    function () {
      function Convert24hrto12hrPipe() {
        _classCallCheck(this, Convert24hrto12hrPipe);
      }

      _createClass(Convert24hrto12hrPipe, [{
        key: "transform",
        value: function transform(time) {
          var hour = time.split(':')[0];
          var min = time.split(':')[1];
          var part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
          min = (min + '').length == 1 ? "0".concat(min) : min;
          hour = hour > 12 ? hour - 12 : hour; // hour = parseInt(hour, 10);

          hour = (hour + '').length == 1 ? "".concat(hour) : hour;
          return "".concat(hour, ":").concat(min, " ").concat(part);
        }
      }]);

      return Convert24hrto12hrPipe;
    }();

    Convert24hrto12hrPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'convert24hrto12hr'
    })], Convert24hrto12hrPipe);
    /***/
  },

  /***/
  "./src/shared/pipes/custom-date.pipe.ts":
  /*!**********************************************!*\
    !*** ./src/shared/pipes/custom-date.pipe.ts ***!
    \**********************************************/

  /*! exports provided: CustomDatePipe */

  /***/
  function srcSharedPipesCustomDatePipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CustomDatePipe", function () {
      return CustomDatePipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var ordinals = ['th', 'st', 'nd', 'rd'];

    var CustomDatePipe =
    /*#__PURE__*/
    function () {
      function CustomDatePipe() {
        _classCallCheck(this, CustomDatePipe);
      }

      _createClass(CustomDatePipe, [{
        key: "transform",
        value: function transform(value, args) {
          var newValue;
          var valueArray = value.split('-');
          var year = valueArray[0];
          var month = valueArray[1];
          var day = valueArray[2];
          var today = new Date(Date.now()); // today.setUTCFullYear(parseInt(valueArray[0]));
          // today.setUTCMonth(parseInt(valueArray[1]) - 1);
          // today.setUTCDate(parseInt(valueArray[2]));
          // if(valueArray[3]) today.setUTCHours(parseInt(valueArray[3]));
          // if(valueArray[4]) today.setUTCMinutes(parseInt(valueArray[4]));
          // if(valueArray[5]) today.setUTCSeconds(parseInt(valueArray[5]));
          // console.log("valueArray", valueArray);
          // console.log("today", today);
          // let year = today.getFullYear();
          // let month = today.getMonth() + 1;
          // let day = today.getDate();

          var deliveryDate = new Date("".concat(month, "/").concat(day, "/").concat(year));

          if (args === "shortTime") {
            // let hour = valueArray[3];
            // let minute = valueArray[4];
            // newValue = hour + ":" + minute;
            var hour = parseInt(valueArray[3]);
            var min = valueArray[4];
            var part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
            min = (min + '').length == 1 ? "0".concat(min) : min;
            hour = hour > 12 ? hour - 12 : hour;
            var hours = (hour + '').length == 1 ? "".concat(hour) : hour;
            return "".concat(hours, ":").concat(min, " ").concat(part);
          }

          if (args === "shortWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'short'
            });
          }

          if (args === "day") {
            newValue = day;
          }

          if (args === "longWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'long'
            });
          }

          if (args === "deliveryDate") {
            var v = parseInt(day) % 100; // let v = (day % 100);

            var ordinalDay = (parseInt(day) > 9 ? day : parseInt(day)) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]); // let ordinalDay = (day > 9 ? day : day) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);

            var monthString = deliveryDate.toLocaleString('default', {
              month: 'long'
            });
            newValue = "".concat(monthString, " ").concat(ordinalDay, " ").concat(year);
          }

          if (args === "longDate") {
            var _monthString = deliveryDate.toLocaleString('default', {
              month: 'long'
            });

            newValue = "".concat(_monthString, " ").concat(day, ", ").concat(year);
          }

          return newValue;
        }
      }]);

      return CustomDatePipe;
    }();

    CustomDatePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'customDate'
    })], CustomDatePipe);
    /***/
  },

  /***/
  "./src/shared/pipes/custom-utcdate.pipe.ts":
  /*!*************************************************!*\
    !*** ./src/shared/pipes/custom-utcdate.pipe.ts ***!
    \*************************************************/

  /*! exports provided: CustomUTCDatePipe */

  /***/
  function srcSharedPipesCustomUtcdatePipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CustomUTCDatePipe", function () {
      return CustomUTCDatePipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var ordinals = ['th', 'st', 'nd', 'rd'];

    var CustomUTCDatePipe =
    /*#__PURE__*/
    function () {
      function CustomUTCDatePipe() {
        _classCallCheck(this, CustomUTCDatePipe);
      }

      _createClass(CustomUTCDatePipe, [{
        key: "transform",
        value: function transform(value, args) {
          var newValue;
          var valueArray = value.split('-'); // let year = valueArray[0];
          // let month = valueArray[1];
          // let day = valueArray[2];

          var today = new Date(Date.now());
          today.setUTCFullYear(parseInt(valueArray[0]));
          today.setUTCMonth(parseInt(valueArray[1]) - 1);
          today.setUTCDate(parseInt(valueArray[2]));
          if (valueArray[3]) today.setUTCHours(parseInt(valueArray[3]));
          if (valueArray[4]) today.setUTCMinutes(parseInt(valueArray[4]));
          if (valueArray[5]) today.setUTCSeconds(parseInt(valueArray[5])); // console.log("valueArray", valueArray);
          // console.log("today", today);

          var year = today.getFullYear();
          var month = today.getMonth() + 1;
          var day = today.getDate();
          var deliveryDate = new Date("".concat(month, "/").concat(day, "/").concat(year));

          if (args === "shortTime") {
            // let hour = valueArray[3];
            // let minute = valueArray[4];
            // newValue = hour + ":" + minute;
            var hour = today.getHours();
            var min = today.getMinutes();
            var part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
            var minute = (min + '').length == 1 ? "0".concat(min) : min;
            hour = hour > 12 ? hour - 12 : hour;
            var hours = (hour + '').length == 1 ? "".concat(hour) : hour;
            return "".concat(hours, ":").concat(minute, " ").concat(part);
          }

          if (args === "shortWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'short'
            });
          }

          if (args === "day") {
            newValue = day;
          }

          if (args === "longWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'long'
            });
          }

          if (args === "deliveryDate") {
            // let v = (parseInt(day)) % 100;
            var v = day % 100; // let ordinalDay = (parseInt(day) > 9 ? day : parseInt(day)) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);

            var ordinalDay = (day > 9 ? day : day) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);
            var monthString = deliveryDate.toLocaleString('default', {
              month: 'long'
            });
            newValue = "".concat(monthString, " ").concat(ordinalDay, " ").concat(year);
          }

          if (args === "longDate") {
            var _monthString2 = deliveryDate.toLocaleString('default', {
              month: 'long'
            });

            newValue = "".concat(_monthString2, " ").concat(day, ", ").concat(year);
          }

          return newValue;
        }
      }]);

      return CustomUTCDatePipe;
    }();

    CustomUTCDatePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'customUtcdate'
    })], CustomUTCDatePipe);
    /***/
  },

  /***/
  "./src/shared/services/routing.service.ts":
  /*!************************************************!*\
    !*** ./src/shared/services/routing.service.ts ***!
    \************************************************/

  /*! exports provided: RoutingService */

  /***/
  function srcSharedServicesRoutingServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RoutingService", function () {
      return RoutingService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var RoutingService =
    /*#__PURE__*/
    function () {
      function RoutingService(http) {
        _classCallCheck(this, RoutingService);

        this.http = http;
      }

      _createClass(RoutingService, [{
        key: "activeRoute",
        value: function activeRoute() {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.activeRouteURL, this.getHeaders());
        } // getUserRoute() {
        //     return this.http.get<Route[]>(environment.baseUrl + environment.services.bookingURL + `?mode=user`, this.getHeaders());
        // }

      }, {
        key: "routeCheckIn",
        value: function routeCheckIn(routeId, checkInTime) {
          var checkIn = {
            routeId: routeId,
            checkInTime: checkInTime
          };
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.checkInRouteURL, checkIn, this.getHeaders());
        }
      }, {
        key: "getModeUserRoute",
        value: function getModeUserRoute() {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeURL + "?mode=user", this.getHeaders());
        }
      }, {
        key: "getUserRoute",
        value: function getUserRoute() {
          var apiMode = {
            mode: "routeShell"
          };
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.sapApiURL, apiMode, this.getHeaders());
        }
      }, {
        key: "getUserRouteDetails",
        value: function getUserRouteDetails(routeId) {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeDetailURL + "?routeId=".concat(routeId), this.getHeaders());
        }
      }, {
        key: "cancelRoute",
        value: function cancelRoute(routeId) {
          var body = {
            routeId: routeId
          };
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeCancelURL, body, this.getHeaders());
        }
      }, {
        key: "getHeaders",
        value: function getHeaders() {
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
              'Authorization': 'Bearer ' + localStorage.getItem('token'),
              'Content-type': 'application/json'
            })
          };
          return httpOptions;
        }
      }]);

      return RoutingService;
    }();

    RoutingService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }];
    };

    RoutingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])], RoutingService);
    /***/
  },

  /***/
  "./src/shared/services/schedule.service.ts":
  /*!*************************************************!*\
    !*** ./src/shared/services/schedule.service.ts ***!
    \*************************************************/

  /*! exports provided: ScheduleService */

  /***/
  function srcSharedServicesScheduleServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ScheduleService", function () {
      return ScheduleService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var ScheduleService =
    /*#__PURE__*/
    function () {
      function ScheduleService(http) {
        _classCallCheck(this, ScheduleService);

        this.http = http;
      }

      _createClass(ScheduleService, [{
        key: "getSchedules",
        value: function getSchedules(mode) {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.scheduleURL + "?mode=".concat(mode), this.getHeaders());
        } // getUserSchedule() {
        //     return this.http.get<Schedule[]>(environment.baseUrl + environment.services.userScheduleURL, this.getHeaders());    
        // }
        // sendDesiredSchedule(schedule) {
        //     return this.http.post<any>(environment.baseUrl + environment.services.scheduleURL, schedule, this.getHeaders());
        // }

      }, {
        key: "sendDesiredSchedule",
        value: function sendDesiredSchedule(schedule) {
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.scheduleURL, schedule, this.getHeaders());
        } // cancelSchedule(route) {
        //     let scheduleDetail = {
        //         mode: "desired",
        //         scheduleId: route.scheduleId,
        //         udsId: route.udsId
        //     }
        //     return this.http.post<any>(environment.baseUrl + environment.services.scheduleURL, schedule, this.getHeaders());
        // }
        // cancelSchedule(booking) {
        //     let scheduleDetail = {
        //         mode: "desired",
        //         scheduleId: booking.scheduleId,
        //         udsId: booking.udsId
        //     }
        //     return this.http.delete<any>(environment.baseUrl + environment.services.scheduleDetailURl +
        //         `?mode=${scheduleDetail.mode}&scheduleId=${scheduleDetail.scheduleId}&udsId=${scheduleDetail.udsId}`, this.getHeaders())
        // }

      }, {
        key: "cancelSchedule",
        value: function cancelSchedule(schedule) {
          var scheduleToCancel = {
            scheduleId: schedule.scheduleId
          };
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.scheduleCancelURL, scheduleToCancel, this.getHeaders());
        }
      }, {
        key: "getHeaders",
        value: function getHeaders() {
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
              'Authorization': 'Bearer ' + localStorage.getItem('token'),
              'Content-type': 'application/json'
            })
          };
          return httpOptions;
        }
      }]);

      return ScheduleService;
    }();

    ScheduleService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }];
    };

    ScheduleService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])], ScheduleService);
    /***/
  },

  /***/
  "./src/shared/shared.module.ts":
  /*!*************************************!*\
    !*** ./src/shared/shared.module.ts ***!
    \*************************************/

  /*! exports provided: SharedModule */

  /***/
  function srcSharedSharedModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
      return SharedModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./pipes/custom-date.pipe */
    "./src/shared/pipes/custom-date.pipe.ts");
    /* harmony import */


    var _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./pipes/convert24hrto12hr.pipe */
    "./src/shared/pipes/convert24hrto12hr.pipe.ts");
    /* harmony import */


    var _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./pipes/custom-utcdate.pipe */
    "./src/shared/pipes/custom-utcdate.pipe.ts");

    var SharedModule = function SharedModule() {
      _classCallCheck(this, SharedModule);
    };

    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__["CustomDatePipe"], _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__["Convert24hrto12hrPipe"], _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__["CustomUTCDatePipe"]],
      entryComponents: [],
      imports: [],
      exports: [_pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__["CustomDatePipe"], _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__["Convert24hrto12hrPipe"], _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__["CustomUTCDatePipe"]],
      providers: []
    })], SharedModule);
    /***/
  }
}]);
//# sourceMappingURL=default~home-home-module~routes-route-details-route-details-module~routes-routes-module~shyfts-shyft~3de54bc4-es5.js.map