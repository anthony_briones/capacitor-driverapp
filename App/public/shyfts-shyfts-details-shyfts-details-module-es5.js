function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["shyfts-shyfts-details-shyfts-details-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shyfts/shyfts-details/shyfts-details.component.html":
  /*!***********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shyfts/shyfts-details/shyfts-details.component.html ***!
    \***********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppShyftsShyftsDetailsShyftsDetailsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Shyft Details</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content ion-padding>\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-item>\n    <ion-row class=\"gig-row\">\n      <ion-col size=\"5\" class=\"left-assigned\">\n        <div class=\"left-date-assigned\">\n          {{schedule?.deliveryDate | customDate:'shortWeekDay'}}\n        </div>\n        <div class=\"left-day-assigned\">\n          {{schedule?.deliveryDate | customDate:'day'}}\n        </div>\n\n      </ion-col>\n      <ion-col size=\"7\" class=\"ion-padding-start\">\n        <p class=\"delivery-day-routed\">{{schedule.deliveryDate | customDate:'longWeekDay'}}</p>\n        <p class=\"delivery-date-routed\">{{schedule.deliveryDate | customDate:'deliveryDate'}}</p>\n        <p class=\"booking-time-routed\">\n          <span>{{schedule.startTime | convert24hrto12hr}}</span> -\n          <span>{{schedule.endTime | convert24hrto12hr}}</span>\n        </p>\n      </ion-col>\n\n      <!-- <ion-col size=\"11\" class=\"calendar-save\" (click)=\"cancelShyftAlert(schedule)\">\n        <div class=\"right-panel-assigned\">\n          <ion-label class=\"right-label\">\n            CANCEL\n          </ion-label>\n        </div>\n      </ion-col>\n\n      <ion-col size=\"11\" class=\"\">\n        <div class=\"right-panel-assigned\">\n          <ion-label class=\"right-label\">\n\n          </ion-label>\n        </div>\n      </ion-col>\n\n      <ion-col size=\"11\" class=\"right-assigned \" (click)=\"saveScheduletoCalendar()\">\n        <div class=\"right-panel-assigned\">\n          <ion-label class=\"right-label\">\n            Save to calendar\n          </ion-label>\n        </div>\n      </ion-col> -->\n\n    </ion-row>\n  </ion-item>\n\n\n  <div class=\"ion-padding\">\n    <button class=\"cancelShyft\" (click)=\"cancelShyftAlert(schedule)\">Cancel</button>\n  </div>\n\n  <div class=\"ion-padding\">\n    <button class=\"saveToCalendar\" (click)=\"saveScheduletoCalendar()\">Save to Calendar</button>\n  </div>\n\n  <!-- Skeleton -->\n  <ng-container *ngIf=\"showDeliveries\">\n    <ion-item lines=\"none\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"delivery-type\">\n          <!-- <ion-icon name=\"md-home\"></ion-icon> -->\n          <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"2\" class=\"\">\n          <ion-skeleton-text class=\"\" animated class=\"h-100\"></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"7\" class=\"delivery-details\">\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n        </ion-col>\n\n        <!-- <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n          <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n          <ion-text><ion-skeleton-text animated></ion-skeleton-text></ion-text>\n        </ion-col> -->\n\n      </ion-row>\n\n    </ion-item>\n\n    <ion-item lines=\"none\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"delivery-type\">\n          <!-- <ion-icon name=\"md-home\"></ion-icon> -->\n          <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"2\" class=\"\">\n          <ion-skeleton-text class=\"\" animated class=\"h-100\"></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"7\" class=\"delivery-details\">\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n        </ion-col>\n\n        <!-- <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n            <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n            <ion-text><ion-skeleton-text animated></ion-skeleton-text></ion-text>\n          </ion-col> -->\n\n      </ion-row>\n\n    </ion-item>\n\n    <ion-item lines=\"none\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"delivery-type\">\n          <!-- <ion-icon name=\"md-home\"></ion-icon> -->\n          <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"2\" class=\"\">\n          <ion-skeleton-text class=\"\" animated class=\"h-100\"></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"7\" class=\"delivery-details\">\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n        </ion-col>\n\n        <!-- <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n              <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n              <ion-text><ion-skeleton-text animated></ion-skeleton-text></ion-text>\n            </ion-col> -->\n\n      </ion-row>\n\n    </ion-item>\n\n  </ng-container>\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/shyfts/shyfts-details/shyfts-details.component.scss":
  /*!*********************************************************************!*\
    !*** ./src/app/shyfts/shyfts-details/shyfts-details.component.scss ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppShyftsShyftsDetailsShyftsDetailsComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nbutton {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n}\n\n.cancelShyft {\n  background-color: #F4773B;\n}\n\n.saveToCalendar {\n  background-color: #003A58;\n}\n\n.custom-skeleton ion-skeleton-text {\n  line-height: 13px;\n}\n\n.custom-skeleton ion-skeleton-text:last-child {\n  margin-bottom: 5px;\n}\n\nion-title {\n  color: #003A58;\n}\n\nion-segment-button {\n  --background: white;\n}\n\nion-content {\n  --background: white;\n}\n\nion-toolbar {\n  margin-top: 1rem !important;\n  font-family: \"Montserrat-Bold\";\n}\n\nion-segment-button {\n  border: none;\n}\n\nion-checkbox {\n  --background-checked: #F4773B;\n  --border-color-checked: #F4773B;\n}\n\nion-list {\n  margin-bottom: 0;\n}\n\nion-header {\n  margin-top: 1rem;\n}\n\n.segment-class {\n  font-size: small;\n  color: #8C8C8C;\n  padding: 0.875rem;\n  font-family: \"Montserrat-SemiBold\";\n  border-bottom: solid 3px #D9D9D9;\n}\n\n.segment-class-selected {\n  font-family: \"Montserrat-Bold\";\n  background-color: white;\n  border-bottom: solid 3px #003A58;\n  color: #003A58;\n}\n\n.gig-request {\n  padding-right: 20px;\n}\n\n.gig-panel {\n  background-color: white;\n  padding: 1rem;\n}\n\n.myGig-label {\n  font-size: 1.25rem;\n  color: #003A58;\n  padding: 1rem;\n  font-family: \"Montserrat-Bold\";\n  letter-spacing: 0;\n}\n\n.gig-row {\n  width: 100%;\n  padding: 1rem 0;\n}\n\n.gig-request {\n  padding-right: 20px;\n}\n\n.available-gig-label {\n  font-size: 1.25rem;\n  color: #003A58;\n  font-family: \"Montserrat-Bold\";\n  letter-spacing: 0;\n  padding: 1rem;\n}\n\n.available-gig {\n  padding: 0 1rem 1rem 1rem;\n  background-color: white;\n}\n\n.no-available-gig {\n  padding: 1rem;\n  background-color: white;\n}\n\n.left-date-requested {\n  color: #003A58;\n  background-color: #E5F3FF;\n  text-align: center;\n  font-size: 0.875rem;\n  font-weight: bolder;\n}\n\n.left-day-requested {\n  color: #003A58;\n  background-color: #E5F3FF;\n  text-align: center;\n  font-size: 1.3125rem;\n  font-weight: bold;\n}\n\n.left-panel {\n  margin: auto;\n}\n\n.left-assigned {\n  margin: auto;\n  background-color: #003A58;\n  border-radius: 5px;\n  padding: 5px 0;\n}\n\n.left-assigned-skeleton {\n  border-radius: 5px;\n  padding: 5px 0;\n}\n\n.left-date-assigned {\n  font-family: \"Montserrat-Bold\";\n  font-size: 0.75rem;\n  color: #FFFFFF;\n  letter-spacing: 0;\n  text-align: center;\n}\n\n.left-day-assigned {\n  font-family: \"Montserrat-Bold\";\n  font-size: 1.25rem;\n  color: #FFFFFF;\n  letter-spacing: 0;\n  text-align: center;\n  background-color: #003A58;\n}\n\n.delivery-day-routed {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Bold\";\n  font-size: 0.875rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.3125rem;\n}\n\n.delivery-day-scheduled {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Bold\";\n  font-size: 0.875rem;\n  color: #676767;\n  letter-spacing: 0;\n  line-height: 1.3125rem;\n}\n\n.delivery-date-routed {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Medium\";\n  font-size: 1rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.5rem;\n}\n\n.delivery-date-scheduled {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Medium\";\n  font-size: 1rem;\n  color: #676767;\n  letter-spacing: 0;\n  line-height: 1.5rem;\n}\n\n.delivery-date {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-weight: bold;\n  font-family: \"Roboto-Bold\";\n  font-size: 0.865rem;\n}\n\n.booking-time-routed {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Regular\";\n  font-size: 0.875rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.3125rem;\n}\n\n.booking-time-scheduled {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Regular\";\n  font-size: 0.875rem;\n  color: #676767;\n  letter-spacing: 0;\n  line-height: 1.3125rem;\n}\n\n.right-assigned {\n  margin: auto;\n  background-color: #003A58;\n  border-radius: 1rem;\n}\n\n.calendar-save {\n  margin: auto;\n  background-color: #F4773B;\n  border-radius: 1rem;\n}\n\n.right-assigned-skeleton {\n  margin: auto;\n}\n\n.right-panel-assigned {\n  color: white;\n  font-size: 0.625rem;\n  text-align: center;\n}\n\n.right-label {\n  font-family: \"Roboto-Bold\";\n  font-size: 0.6875rem;\n  letter-spacing: 0;\n  line-height: 0.9375rem;\n}\n\n.left-requested {\n  margin: auto;\n  background-color: #E5F3FF;\n  border-radius: 5px;\n  padding: 5px 0;\n}\n\n.left-requested-skeleton {\n  border-radius: 5px;\n  padding: 5px 0;\n}\n\n.right-requested {\n  margin: auto;\n  background-color: #F4773B;\n  border-radius: 1rem;\n}\n\n.right-requested-skeleton {\n  margin: auto;\n}\n\n.right-panel-requested {\n  color: white;\n  font-size: 0.625rem;\n  text-align: center;\n  font-weight: bold;\n}\n\n.delivery-sched-requested {\n  padding: 1rem 0;\n}\n\n.delivery-date-requested {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-weight: bold;\n  color: #424242;\n  font-size: 1rem;\n}\n\n.booking-time-requested {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  color: #424242;\n  font-size: 0.875rem;\n}\n\nbutton {\n  border: none !important;\n}\n\n.shyft-select {\n  position: fixed;\n  bottom: 0;\n  background: #003A58;\n  color: white;\n  width: 100%;\n}\n\n.shyft-select .select-clear {\n  size: 1rem;\n  padding-left: 1rem;\n}\n\n.shyft-select .select-submit {\n  size: 1rem;\n  font-weight: bold;\n  text-align: right;\n  padding-right: 1rem;\n}\n\n.availability-label {\n  padding: 1rem 0 1rem 0;\n}\n\n.l-align {\n  text-align: left;\n  margin: auto;\n}\n\n.r-align {\n  text-align: right;\n  margin: auto;\n  font-family: \"Montserrat-Regular\";\n}\n\n.available-date {\n  font-family: \"Roboto-Bold\";\n  font-size: 1rem;\n}\n\n.calendar-weekday {\n  font-size: 0.875rem !important;\n  color: #003A58;\n  font-family: \"Montserrat-SemiBold\";\n  text-align: center;\n}\n\n.calendar-date {\n  font-size: 2rem;\n}\n\n.event-bullet {\n  margin: 2px auto;\n  height: 7px;\n  width: 7px;\n  background-color: #F4773B;\n  border-radius: 30px;\n}\n\n.event-date {\n  text-align: center;\n  color: #003A58;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.today-date {\n  text-align: center;\n  font-family: \"Montserrat-Bold\";\n  color: #003A58;\n}\n\n.other-date {\n  text-align: center;\n  color: #8C8C8C;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.left-done {\n  margin: auto;\n  background-color: white;\n  border-radius: 5px;\n  padding: 5px 0;\n  border: 1px solid #003A58;\n}\n\n.left-done-skeleton {\n  border-radius: 5px;\n  padding: 5px 0;\n}\n\n.left-date-done {\n  font-family: \"Montserrat-Bold\";\n  font-size: 0.75rem;\n  color: #003A58;\n  letter-spacing: 0;\n  text-align: center;\n}\n\n.left-day-done {\n  font-family: \"Montserrat-Bold\";\n  font-size: 1.25rem;\n  color: #003A58;\n  letter-spacing: 0;\n  text-align: center;\n  background-color: white;\n}\n\n.right-done {\n  margin: auto;\n  background-color: white;\n  border-radius: 1rem;\n  border: 1px solid #003A58;\n}\n\n.right-done-skeleton {\n  margin: auto;\n}\n\n.right-panel-done {\n  color: #003A58;\n  font-size: 0.625rem;\n  text-align: center;\n}\n\n.footer-toolbar {\n  background-color: #003A58;\n}\n\n.footer-button {\n  color: #ffffff;\n  font-family: \"Montserrat-SemiBold\";\n  background-color: #003A58;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL3NoeWZ0cy9zaHlmdHMtZGV0YWlscy9zaHlmdHMtZGV0YWlscy5jb21wb25lbnQuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MiLCIvVXNlcnMvYW50aG9ueS5icmlvbmVzL0RvY3VtZW50cy9DYXBEcml2ZXIvc3JjL2FwcC9zaHlmdHMvc2h5ZnRzLWRldGFpbHMvc2h5ZnRzLWRldGFpbHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBT0E7RUFDSSx5QkFBQTtFQUNBLGdEQUFBO0FDTko7O0FEU0E7RUFDSSx5QkFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCRWxCYztFRm1CZCxXQUFBO0VBQ0EsY0VoQkk7RUZpQkosbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLCtCQUFBO0VBQUEsd0JBQUE7RUFDQSxnQ0FBQTtFQUNBLG9DQUFBO1VBQUEsOEJBQUE7QUNOSjs7QURTQTtFQUNJLG1DQUFBO1VBQUEsa0NBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO1VBQUEseUJBQUE7QUNOSjs7QURTQTtFQUNJLHdCQUFBO1VBQUEsdUJBQUE7QUNOSjs7QURTQTtFQUNJLFlBQUE7QUNOSjs7QUVoREE7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUZtREo7O0FFaERBO0VBQ0kseUJBQUE7QUZtREo7O0FFaERBO0VBQ0kseUJBQUE7QUZtREo7O0FFaERBO0VBQ0ksaUJBQUE7QUZtREo7O0FFaERFO0VBQ0Usa0JBQUE7QUZtREo7O0FFaERBO0VBQ0ksY0QxQmM7QUQ2RWxCOztBRWhEQTtFQUNJLG1CQUFBO0FGbURKOztBRWhEQTtFQUNJLG1CQUFBO0FGbURKOztBRWhEQTtFQUNJLDJCQUFBO0VBQ0EsOEJBQUE7QUZtREo7O0FFL0NBO0VBQ0ksWUFBQTtBRmtESjs7QUU5Q0E7RUFDSSw2QkFBQTtFQUNBLCtCQUFBO0FGaURKOztBRTlDQTtFQUNJLGdCQUFBO0FGaURKOztBRTlDQTtFQUNJLGdCQUFBO0FGaURKOztBRTlDQTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0NBQUE7RUFDQSxnQ0FBQTtBRmlESjs7QUU5Q0E7RUFDSSw4QkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxjRHpFYztBRDBIbEI7O0FFOUNBO0VBQ0ksbUJBQUE7QUZpREo7O0FFOUNBO0VBQ0ksdUJBQUE7RUFFQSxhQUFBO0FGZ0RKOztBRTdDQTtFQUNJLGtCQUFBO0VBQ0EsY0R4RmM7RUMwRmQsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsaUJBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksbUJBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksa0JBQUE7RUFDQSxjRDFHYztFQzJHZCw4QkFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtBRitDSjs7QUU1Q0E7RUFDSSx5QkFBQTtFQUVBLHVCQUFBO0FGOENKOztBRTVDQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBRitDSjs7QUU1Q0E7RUFDSSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksY0FBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0FGK0NKOztBRTVDQTtFQUNJLFlBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksWUFBQTtFQUNBLHlCRGhKYztFQ2lKZCxrQkFBQTtFQUNBLGNBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0FGK0NKOztBRTNDQTtFQUVJLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBRjZDSjs7QUV6Q0E7RUFDSSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkQzS2M7QUR1TmxCOztBRXhDQTtFQUNJLGFBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDBCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBRjJDSjs7QUV4Q0E7RUFDSSxhQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUYyQ0o7O0FFeENBO0VBQ0ksYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUYyQ0o7O0FFeENBO0VBQ0ksYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUYyQ0o7O0FFdkNBO0VBQ0ksYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQkFBQTtFQUNBLG1CQUFBO0FGMENKOztBRXZDQTtFQUNJLGFBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBRjBDSjs7QUV0Q0E7RUFDSSxhQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUZ5Q0o7O0FFdENBO0VBQ0ksWUFBQTtFQUNBLHlCRHJRYztFQ3NRZCxtQkFBQTtBRnlDSjs7QUV0Q0E7RUFDSSxZQUFBO0VBQ0EseUJEelFjO0VDMFFkLG1CQUFBO0FGeUNKOztBRXRDQTtFQUNJLFlBQUE7QUZ5Q0o7O0FFcENBO0VBQ0ksWUFBQTtFQUVBLG1CQUFBO0VBQ0Esa0JBQUE7QUZzQ0o7O0FFbkNBO0VBQ0ksMEJBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUZzQ0o7O0FFbkNBO0VBQ0ksWUFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FGc0NKOztBRW5DQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtBRnNDSjs7QUVuQ0E7RUFDSSxZQUFBO0VBQ0EseUJEL1NjO0VDZ1RkLG1CQUFBO0FGc0NKOztBRW5DQTtFQUNJLFlBQUE7QUZzQ0o7O0FFakNBO0VBQ0ksWUFBQTtFQUVBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBRm1DSjs7QUVoQ0E7RUFDSSxlQUFBO0FGbUNKOztBRWhDQTtFQUNJLGFBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUZtQ0o7O0FFaENBO0VBQ0ksYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0FGbUNKOztBRWhDQTtFQUNJLHVCQUFBO0FGbUNKOztBRWhDQTtFQUNJLGVBQUE7RUFDQSxTQUFBO0VBQ0EsbUJEaldjO0VDa1dkLFlBQUE7RUFDQSxXQUFBO0FGbUNKOztBRWpDSTtFQUNJLFVBQUE7RUFDQSxrQkFBQTtBRm1DUjs7QUVoQ0k7RUFDSSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FGa0NSOztBRTlCQTtFQUNJLHNCQUFBO0FGaUNKOztBRTlCQTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtBRmlDSjs7QUU5QkE7RUFDSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxpQ0FBQTtBRmlDSjs7QUU5QkE7RUFDSSwwQkFBQTtFQUNBLGVBQUE7QUZpQ0o7O0FFMUJBO0VBQ0ksOEJBQUE7RUFDQSxjRDVZYztFQzZZZCxrQ0FBQTtFQUNBLGtCQUFBO0FGNkJKOztBRTFCQTtFQUNJLGVBQUE7QUY2Qko7O0FFMUJBO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLHlCRHZaYztFQ3daZCxtQkFBQTtBRjZCSjs7QUUxQkE7RUFDSSxrQkFBQTtFQUVBLGNEaGFjO0VDaWFkLGtDQUFBO0FGNEJKOztBRXpCQTtFQUNJLGtCQUFBO0VBRUEsOEJBQUE7RUFDQSxjRHhhYztBRG1jbEI7O0FFeEJBO0VBQ0ksa0JBQUE7RUFFQSxjQUFBO0VBQ0Esa0NBQUE7QUYwQko7O0FFckJBO0VBQ0ksWUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7QUZ3Qko7O0FFcEJBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0FGdUJKOztBRWxCQTtFQUVJLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjRHhjYztFQ3ljZCxpQkFBQTtFQUNBLGtCQUFBO0FGb0JKOztBRWpCQTtFQUNJLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjRGhkYztFQ2lkZCxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7QUZvQko7O0FFZEE7RUFDSSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FGaUJKOztBRWRBO0VBQ0ksWUFBQTtBRmlCSjs7QUVYQTtFQUNJLGNEeGVjO0VDMGVkLG1CQUFBO0VBQ0Esa0JBQUE7QUZhSjs7QUUrREE7RUFDSSx5QkR4akJjO0FENGZsQjs7QUUrREE7RUFDSSxjRHhqQkk7RUN5akJKLGtDQUFBO0VBQ0EseUJEOWpCYztBRGtnQmxCIiwiZmlsZSI6InNyYy9hcHAvc2h5ZnRzL3NoeWZ0cy1kZXRhaWxzL3NoeWZ0cy1kZXRhaWxzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2ZvbnRzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMnO1xuXG4vLyAuaXMtdmFsaWQge1xuLy8gICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWVcbi8vIH1cblxuLmlzLWludmFsaWQge1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNFRTQwMzY7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDNcbn1cblxuLmlzLWludmFsaWQtc2VsZWN0IHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6ICR3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgaGVpZ2h0OiAycmVtO1xufVxuXG4uYmctY29sb3ItMXtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgICBkaXNwbGF5OiBmbGV4IWltcG9ydGFudDtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXIhaW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5mbGV4LWFsaWduLWNlbnRlciB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgICBvcGFjaXR5OiAwLjU7XG59XG5cblxuXG5cbiIsIi5pcy1pbnZhbGlkIHtcbiAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICBib3JkZXI6IDFweCBzb2xpZCAjNUVCMDAzO1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWFsaWduLXJpZ2h0IHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgb3BhY2l0eTogMC41O1xufVxuXG5idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gIGhlaWdodDogM3JlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuLmNhbmNlbFNoeWZ0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0Y0NzczQjtcbn1cblxuLnNhdmVUb0NhbGVuZGFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbn1cblxuLmN1c3RvbS1za2VsZXRvbiBpb24tc2tlbGV0b24tdGV4dCB7XG4gIGxpbmUtaGVpZ2h0OiAxM3B4O1xufVxuXG4uY3VzdG9tLXNrZWxldG9uIGlvbi1za2VsZXRvbi10ZXh0Omxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbmlvbi10aXRsZSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xufVxuXG5pb24tc2VnbWVudC1idXR0b24ge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgbWFyZ2luLXRvcDogMXJlbSAhaW1wb3J0YW50O1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG5pb24tY2hlY2tib3gge1xuICAtLWJhY2tncm91bmQtY2hlY2tlZDogI0Y0NzczQjtcbiAgLS1ib3JkZXItY29sb3ItY2hlY2tlZDogI0Y0NzczQjtcbn1cblxuaW9uLWxpc3Qge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG5pb24taGVhZGVyIHtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbn1cblxuLnNlZ21lbnQtY2xhc3Mge1xuICBmb250LXNpemU6IHNtYWxsO1xuICBjb2xvcjogIzhDOEM4QztcbiAgcGFkZGluZzogMC44NzVyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgM3B4ICNEOUQ5RDk7XG59XG5cbi5zZWdtZW50LWNsYXNzLXNlbGVjdGVkIHtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAzcHggIzAwM0E1ODtcbiAgY29sb3I6ICMwMDNBNTg7XG59XG5cbi5naWctcmVxdWVzdCB7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG5cbi5naWctcGFuZWwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMXJlbTtcbn1cblxuLm15R2lnLWxhYmVsIHtcbiAgZm9udC1zaXplOiAxLjI1cmVtO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgcGFkZGluZzogMXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xufVxuXG4uZ2lnLXJvdyB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAxcmVtIDA7XG59XG5cbi5naWctcmVxdWVzdCB7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG5cbi5hdmFpbGFibGUtZ2lnLWxhYmVsIHtcbiAgZm9udC1zaXplOiAxLjI1cmVtO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICBwYWRkaW5nOiAxcmVtO1xufVxuXG4uYXZhaWxhYmxlLWdpZyB7XG4gIHBhZGRpbmc6IDAgMXJlbSAxcmVtIDFyZW07XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4ubm8tYXZhaWxhYmxlLWdpZyB7XG4gIHBhZGRpbmc6IDFyZW07XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4ubGVmdC1kYXRlLXJlcXVlc3RlZCB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRTVGM0ZGO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi5sZWZ0LWRheS1yZXF1ZXN0ZWQge1xuICBjb2xvcjogIzAwM0E1ODtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0U1RjNGRjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDEuMzEyNXJlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5sZWZ0LXBhbmVsIHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4ubGVmdC1hc3NpZ25lZCB7XG4gIG1hcmdpbjogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiA1cHggMDtcbn1cblxuLmxlZnQtYXNzaWduZWQtc2tlbGV0b24ge1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDVweCAwO1xufVxuXG4ubGVmdC1kYXRlLWFzc2lnbmVkIHtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgY29sb3I6ICNGRkZGRkY7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5sZWZ0LWRheS1hc3NpZ25lZCB7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBmb250LXNpemU6IDEuMjVyZW07XG4gIGNvbG9yOiAjRkZGRkZGO1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xufVxuXG4uZGVsaXZlcnktZGF5LXJvdXRlZCB7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgY29sb3I6ICM0MjQyNDI7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4uZGVsaXZlcnktZGF5LXNjaGVkdWxlZCB7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgY29sb3I6ICM2NzY3Njc7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4uZGVsaXZlcnktZGF0ZS1yb3V0ZWQge1xuICBtYXJnaW4tdG9wOiAwO1xuICBwYWRkaW5nLXRvcDogMDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgcGFkZGluZy1ib3R0b206IDA7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1NZWRpdW1cIjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBjb2xvcjogIzQyNDI0MjtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjVyZW07XG59XG5cbi5kZWxpdmVyeS1kYXRlLXNjaGVkdWxlZCB7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLU1lZGl1bVwiO1xuICBmb250LXNpemU6IDFyZW07XG4gIGNvbG9yOiAjNjc2NzY3O1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgbGluZS1oZWlnaHQ6IDEuNXJlbTtcbn1cblxuLmRlbGl2ZXJ5LWRhdGUge1xuICBtYXJnaW4tdG9wOiAwO1xuICBwYWRkaW5nLXRvcDogMDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgcGFkZGluZy1ib3R0b206IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tQm9sZFwiO1xuICBmb250LXNpemU6IDAuODY1cmVtO1xufVxuXG4uYm9va2luZy10aW1lLXJvdXRlZCB7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLVJlZ3VsYXJcIjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgY29sb3I6ICM0MjQyNDI7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4uYm9va2luZy10aW1lLXNjaGVkdWxlZCB7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLVJlZ3VsYXJcIjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgY29sb3I6ICM2NzY3Njc7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4ucmlnaHQtYXNzaWduZWQge1xuICBtYXJnaW46IGF1dG87XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XG59XG5cbi5jYWxlbmRhci1zYXZlIHtcbiAgbWFyZ2luOiBhdXRvO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjQ3NzNCO1xuICBib3JkZXItcmFkaXVzOiAxcmVtO1xufVxuXG4ucmlnaHQtYXNzaWduZWQtc2tlbGV0b24ge1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5yaWdodC1wYW5lbC1hc3NpZ25lZCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAwLjYyNXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ucmlnaHQtbGFiZWwge1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tQm9sZFwiO1xuICBmb250LXNpemU6IDAuNjg3NXJlbTtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAwLjkzNzVyZW07XG59XG5cbi5sZWZ0LXJlcXVlc3RlZCB7XG4gIG1hcmdpbjogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogI0U1RjNGRjtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiA1cHggMDtcbn1cblxuLmxlZnQtcmVxdWVzdGVkLXNrZWxldG9uIHtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiA1cHggMDtcbn1cblxuLnJpZ2h0LXJlcXVlc3RlZCB7XG4gIG1hcmdpbjogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogI0Y0NzczQjtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbn1cblxuLnJpZ2h0LXJlcXVlc3RlZC1za2VsZXRvbiB7XG4gIG1hcmdpbjogYXV0bztcbn1cblxuLnJpZ2h0LXBhbmVsLXJlcXVlc3RlZCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAwLjYyNXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmRlbGl2ZXJ5LXNjaGVkLXJlcXVlc3RlZCB7XG4gIHBhZGRpbmc6IDFyZW0gMDtcbn1cblxuLmRlbGl2ZXJ5LWRhdGUtcmVxdWVzdGVkIHtcbiAgbWFyZ2luLXRvcDogMDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM0MjQyNDI7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbn1cblxuLmJvb2tpbmctdGltZS1yZXF1ZXN0ZWQge1xuICBtYXJnaW4tdG9wOiAwO1xuICBwYWRkaW5nLXRvcDogMDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgcGFkZGluZy1ib3R0b206IDA7XG4gIGNvbG9yOiAjNDI0MjQyO1xuICBmb250LXNpemU6IDAuODc1cmVtO1xufVxuXG5idXR0b24ge1xuICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLnNoeWZ0LXNlbGVjdCB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm90dG9tOiAwO1xuICBiYWNrZ3JvdW5kOiAjMDAzQTU4O1xuICBjb2xvcjogd2hpdGU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnNoeWZ0LXNlbGVjdCAuc2VsZWN0LWNsZWFyIHtcbiAgc2l6ZTogMXJlbTtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xufVxuLnNoeWZ0LXNlbGVjdCAuc2VsZWN0LXN1Ym1pdCB7XG4gIHNpemU6IDFyZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcbn1cblxuLmF2YWlsYWJpbGl0eS1sYWJlbCB7XG4gIHBhZGRpbmc6IDFyZW0gMCAxcmVtIDA7XG59XG5cbi5sLWFsaWduIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4uci1hbGlnbiB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBtYXJnaW46IGF1dG87XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtUmVndWxhclwiO1xufVxuXG4uYXZhaWxhYmxlLWRhdGUge1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tQm9sZFwiO1xuICBmb250LXNpemU6IDFyZW07XG59XG5cbi5jYWxlbmRhci13ZWVrZGF5IHtcbiAgZm9udC1zaXplOiAwLjg3NXJlbSAhaW1wb3J0YW50O1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jYWxlbmRhci1kYXRlIHtcbiAgZm9udC1zaXplOiAycmVtO1xufVxuXG4uZXZlbnQtYnVsbGV0IHtcbiAgbWFyZ2luOiAycHggYXV0bztcbiAgaGVpZ2h0OiA3cHg7XG4gIHdpZHRoOiA3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGNDc3M0I7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG59XG5cbi5ldmVudC1kYXRlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xufVxuXG4udG9kYXktZGF0ZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGNvbG9yOiAjMDAzQTU4O1xufVxuXG4ub3RoZXItZGF0ZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICM4QzhDOEM7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbn1cblxuLmxlZnQtZG9uZSB7XG4gIG1hcmdpbjogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZzogNXB4IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDNBNTg7XG59XG5cbi5sZWZ0LWRvbmUtc2tlbGV0b24ge1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDVweCAwO1xufVxuXG4ubGVmdC1kYXRlLWRvbmUge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgZm9udC1zaXplOiAwLjc1cmVtO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxlZnQtZGF5LWRvbmUge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgZm9udC1zaXplOiAxLjI1cmVtO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5yaWdodC1kb25lIHtcbiAgbWFyZ2luOiBhdXRvO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwM0E1ODtcbn1cblxuLnJpZ2h0LWRvbmUtc2tlbGV0b24ge1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5yaWdodC1wYW5lbC1kb25lIHtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtc2l6ZTogMC42MjVyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZvb3Rlci10b29sYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbn1cblxuLmZvb3Rlci1idXR0b24ge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xufSIsIlxuLy8gY29sb3JzXG4vLyAkcHJpbWFyeS1jb2xvci0xOiAjMDBBM0M4O1xuJHByaW1hcnktY29sb3ItMTogIzAwM0E1ODtcbiRwcmltYXJ5LWNvbG9yLTI6ICNGQUFGNDA7XG4kcHJpbWFyeS1jb2xvci0zOiAjRjQ3NzNCO1xuXG4kd2hpdGU6ICNmZmZmZmY7XG5cbi8vICRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgcmdiYSgyNTUsMjU1LDI1NSwwLjUwKSAwJSwgcmdiYSgwLDAsMCwwLjUwKSAxMDAlKTtcbiRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KC0xODBkZWcsICNGRjg4NDAgMCUsICNFRTQwMzYgMTAwJSk7IiwiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2NvbW1vbi5zY3NzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XG5cbmJ1dHRvbntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDEuMTI1cmVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICAgIGhlaWdodDogM3JlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnXG59XG5cbi5jYW5jZWxTaHlmdHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjQ3NzNCO1xufVxuXG4uc2F2ZVRvQ2FsZW5kYXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbn1cblxuLmN1c3RvbS1za2VsZXRvbiBpb24tc2tlbGV0b24tdGV4dCB7XG4gICAgbGluZS1oZWlnaHQ6IDEzcHg7XG4gIH1cbiAgXG4gIC5jdXN0b20tc2tlbGV0b24gaW9uLXNrZWxldG9uLXRleHQ6bGFzdC1jaGlsZCB7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICB9XG5cbmlvbi10aXRsZXtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbn1cblxuaW9uLXNlZ21lbnQtYnV0dG9ue1xuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbmlvbi1jb250ZW50e1xuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgICBtYXJnaW4tdG9wOiAxcmVtICFpbXBvcnRhbnQ7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuXG59XG5cbmlvbi1zZWdtZW50LWJ1dHRvbntcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgLy8gd2lkdGg6IGNhbGMoMTAwJSAvIDIpO1xufVxuXG5pb24tY2hlY2tib3h7XG4gICAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6ICNGNDc3M0I7XG4gICAgLS1ib3JkZXItY29sb3ItY2hlY2tlZDogI0Y0NzczQjtcbn1cblxuaW9uLWxpc3Qge1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbmlvbi1oZWFkZXIge1xuICAgIG1hcmdpbi10b3A6IDFyZW07XG59XG5cbi5zZWdtZW50LWNsYXNzIHtcbiAgICBmb250LXNpemU6IHNtYWxsO1xuICAgIGNvbG9yOiAjOEM4QzhDO1xuICAgIHBhZGRpbmc6IDAuODc1cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG4gICAgYm9yZGVyLWJvdHRvbTogc29saWQgM3B4ICNEOUQ5RDk7XG59XG5cbi5zZWdtZW50LWNsYXNzLXNlbGVjdGVkIHtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLWJvdHRvbTogc29saWQgM3B4ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG59XG5cbi5naWctcmVxdWVzdHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuXG4uZ2lnLXBhbmVse1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIC8vIHBhZGRpbmc6IDAgMXJlbSAwIDFyZW07XG4gICAgcGFkZGluZzogMXJlbTtcbn1cblxuLm15R2lnLWxhYmVse1xuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTsgXG4gICAgLy8gZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgcGFkZGluZzogMXJlbTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG59XG5cbi5naWctcm93e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDFyZW0gMDtcbn1cblxuLmdpZy1yZXF1ZXN0e1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG5cbi5hdmFpbGFibGUtZ2lnLWxhYmVse1xuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgcGFkZGluZzogMXJlbTtcbn1cblxuLmF2YWlsYWJsZS1naWd7XG4gICAgcGFkZGluZzogMCAxcmVtIDFyZW0gMXJlbTtcbiAgICAvLyBwYWRkaW5nOiAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuLm5vLWF2YWlsYWJsZS1naWd7XG4gICAgcGFkZGluZzogMXJlbTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmxlZnQtZGF0ZS1yZXF1ZXN0ZWR7XG4gICAgY29sb3I6ICMwMDNBNTg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0U1RjNGRjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuXG4ubGVmdC1kYXktcmVxdWVzdGVke1xuICAgIGNvbG9yOiAjMDAzQTU4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNFNUYzRkY7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMS4zMTI1cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubGVmdC1wYW5lbHtcbiAgICBtYXJnaW46IGF1dG87XG59XG5cbi5sZWZ0LWFzc2lnbmVke1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBwYWRkaW5nOiA1cHggMDtcbn1cblxuLmxlZnQtYXNzaWduZWQtc2tlbGV0b257XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBhZGRpbmc6IDVweCAwO1xufVxuXG5cbi5sZWZ0LWRhdGUtYXNzaWduZWR7XG5cbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgZm9udC1zaXplOiAwLjc1cmVtO1xuICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuXG4ubGVmdC1kYXktYXNzaWduZWR7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbn1cblxuXG4uZGVsaXZlcnktZGF5LXJvdXRlZCB7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJvbGQnO1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgY29sb3I6ICM0MjQyNDI7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLmRlbGl2ZXJ5LWRheS1zY2hlZHVsZWQge1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1Cb2xkJztcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGNvbG9yOiAjNjc2NzY3O1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5kZWxpdmVyeS1kYXRlLXJvdXRlZCB7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLU1lZGl1bSc7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGNvbG9yOiAjNDI0MjQyO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjVyZW1cbn1cblxuLmRlbGl2ZXJ5LWRhdGUtc2NoZWR1bGVkIHtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIHBhZGRpbmctdG9wOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tTWVkaXVtJztcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgY29sb3I6ICM2NzY3Njc7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuNXJlbVxufVxuXG5cbi5kZWxpdmVyeS1kYXRlIHtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIHBhZGRpbmctdG9wOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tQm9sZCc7XG4gICAgZm9udC1zaXplOiAwLjg2NXJlbTtcbn1cblxuLmJvb2tpbmctdGltZS1yb3V0ZWQge1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1SZWd1bGFyJztcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGNvbG9yOiAjNDI0MjQyO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cblxuLmJvb2tpbmctdGltZS1zY2hlZHVsZWQge1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1SZWd1bGFyJztcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGNvbG9yOiAjNjc2NzY3O1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5yaWdodC1hc3NpZ25lZHtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xufVxuXG4uY2FsZW5kYXItc2F2ZXtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMztcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xufVxuXG4ucmlnaHQtYXNzaWduZWQtc2tlbGV0b257XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLy8gYm9yZGVyLXJhZGl1czogMXJlbTtcbn1cblxuLnJpZ2h0LXBhbmVsLWFzc2lnbmVke1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtc2l6ZTogMC42MjVyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ucmlnaHQtbGFiZWwge1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJvbGQnO1xuICAgIGZvbnQtc2l6ZTogMC42ODc1cmVtO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAwLjkzNzVyZW07XG59XG5cbi5sZWZ0LXJlcXVlc3RlZHtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0U1RjNGRjs7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBhZGRpbmc6IDVweCAwO1xufVxuXG4ubGVmdC1yZXF1ZXN0ZWQtc2tlbGV0b257XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBhZGRpbmc6IDVweCAwO1xufVxuXG4ucmlnaHQtcmVxdWVzdGVke1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0zO1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG59XG5cbi5yaWdodC1yZXF1ZXN0ZWQtc2tlbGV0b257XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTM7XG4gICAgLy8gYm9yZGVyLXJhZGl1czogMXJlbTtcbn1cblxuLnJpZ2h0LXBhbmVsLXJlcXVlc3RlZHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMztcbiAgICBmb250LXNpemU6IDAuNjI1cmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmRlbGl2ZXJ5LXNjaGVkLXJlcXVlc3RlZHtcbiAgICBwYWRkaW5nOiAxcmVtIDA7XG59XG5cbi5kZWxpdmVyeS1kYXRlLXJlcXVlc3RlZCB7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGNvbG9yOiAjNDI0MjQyO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbn1cblxuLmJvb2tpbmctdGltZS1yZXF1ZXN0ZWQge1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgICBjb2xvcjogIzQyNDI0MjtcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xufVxuXG5idXR0b257XG4gICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5zaHlmdC1zZWxlY3Qge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBib3R0b206IDA7XG4gICAgYmFja2dyb3VuZDogJHByaW1hcnktY29sb3ItMTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICAuc2VsZWN0LWNsZWFye1xuICAgICAgICBzaXplOiAxcmVtO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gICAgfVxuXG4gICAgLnNlbGVjdC1zdWJtaXR7XG4gICAgICAgIHNpemU6IDFyZW07XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMXJlbTtcbiAgICB9XG59XG5cbi5hdmFpbGFiaWxpdHktbGFiZWwge1xuICAgIHBhZGRpbmc6IDFyZW0gMCAxcmVtIDA7XG59XG5cbi5sLWFsaWduIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIG1hcmdpbjogYXV0bztcbn1cblxuLnItYWxpZ24ge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtUmVndWxhcic7XG59XG5cbi5hdmFpbGFibGUtZGF0ZSB7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tQm9sZCc7XG4gICAgZm9udC1zaXplOiAxcmVtO1xufVxuLy8gLnN1Ym1pdC1naWd7XG4vLyAgICAgd2lkdGg6IDUwJTtcbi8vIH1cblxuLy9jYWxlbmRhciBjc3Ncbi5jYWxlbmRhci13ZWVrZGF5IHtcbiAgICBmb250LXNpemU6IDAuODc1cmVtICFpbXBvcnRhbnQ7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jYWxlbmRhci1kYXRlIHtcbiAgICBmb250LXNpemU6IDJyZW07XG59XG5cbi5ldmVudC1idWxsZXQge1xuICAgIG1hcmdpbjogMnB4IGF1dG87XG4gICAgaGVpZ2h0OiA3cHg7XG4gICAgd2lkdGg6IDdweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0zO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG59XG5cbi5ldmVudC1kYXRlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgLy8gZm9udC1zaXplOiAycmVtO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG59XG5cbi50b2RheS1kYXRlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgLy8gZm9udC1zaXplOiAycmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbn1cblxuLm90aGVyLWRhdGUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAvLyBmb250LXNpemU6IDJyZW07XG4gICAgY29sb3I6ICM4QzhDOEM7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbn1cblxuLy9kb25lIHBhbmVsXG5cbi5sZWZ0LWRvbmV7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBwYWRkaW5nOiA1cHggMDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAkcHJpbWFyeS1jb2xvci0xO1xuICAgIC8vIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xICFpbXBvcnRhbnQ7XG59XG5cbi5sZWZ0LWRvbmUtc2tlbGV0b257XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBhZGRpbmc6IDVweCAwO1xuICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLy8gY29sb3I6ICRwcmltYXJ5LWNvbG9yLTEgIWltcG9ydGFudDtcbn1cblxuLmxlZnQtZGF0ZS1kb25le1xuXG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5sZWZ0LWRheS1kb25le1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBmb250LXNpemU6IDEuMjVyZW07XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDM7XG4gICAgLy8gYm9yZGVyLWxlZnQ6IDAuNXB4IHNvbGlkICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLy8gYm9yZGVyLXJpZ2h0OiAwLjVweCBzb2xpZCAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG4ucmlnaHQtZG9uZXtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG4ucmlnaHQtZG9uZS1za2VsZXRvbntcbiAgICBtYXJnaW46IGF1dG87XG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgLy8gYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgICAvLyBib3JkZXI6IDFweCBzb2xpZCAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG4ucmlnaHQtcGFuZWwtZG9uZXtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtc2l6ZTogMC42MjVyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5cblxuXG4vLyAuZ2lnLWNhbGVuZGVye1xuLy8gICAgIFtjb2wtMV0ge1xuLy8gICAgICAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xuLy8gICAgICAgICAtd2Via2l0LWZsZXg6IDAgMCA4LjMzMzMzJTtcbi8vICAgICAgICAgLW1zLWZsZXg6IDAgMCA4LjMzMzMlO1xuLy8gICAgICAgICBmbGV4OiAwIDAgMTQuMjg1NzE0Mjg1NzE0Mjg2JTtcbi8vICAgICAgICAgd2lkdGg6IDE0LjI4NTcxNDI4NTcxNDI4NiU7XG4vLyAgICAgICAgIG1heC13aWR0aDogMTQuMjg1NzE0Mjg1NzE0Mjg2JTtcbi8vICAgICB9XG4vLyAgICAgLmNvbCB7XG4vLyAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbi8vICAgICAgICAgcGFkZGluZzogNXB4O1xuLy8gICAgIH1cbi8vICAgICAubGFzdC1tb250aCwgLm5leHQtbW9udGgge1xuLy8gICAgICAgICBjb2xvcjogIzk5OTk5OTtcbi8vICAgICAgICAgZm9udC1zaXplOiA5MCU7XG4vLyAgICAgfVxuLy8gICAgIC5jdXJyZW50RGF0ZSwgLm90aGVyRGF0ZSB7XG4vLyAgICAgICAgIHBhZGRpbmc6IDVweDtcbi8vICAgICB9XG4vLyAgICAgLmN1cnJlbnREYXRlIHtcbi8vICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4vLyAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbi8vICAgICAgICAgY29sb3I6IHdoaXRlO1xuLy8gICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuLy8gICAgIH1cbi8vICAgICAuY2FsZW5kYXItaGVhZGVyIHtcbi8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzFhNzMyZDtcbi8vICAgICAgICAgY29sb3I6ICNGRkZGRkY7XG4vLyAgICAgfVxuLy8gICAgIC5ldmVudC1idWxsZXQge1xuLy8gICAgICAgICBtYXJnaW46IDJweCBhdXRvO1xuLy8gICAgICAgICBoZWlnaHQ6IDVweDtcbi8vICAgICAgICAgd2lkdGg6IDVweDtcbi8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMztcbi8vICAgICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcbi8vICAgICB9XG4vLyAgICAgLnNlbGVjdGVkLWRhdGUge1xuLy8gICAgICAgICB3aWR0aDogMjBweDtcbi8vICAgICAgICAgaGVpZ2h0OiAycHg7XG4vLyAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XG4vLyAgICAgfVxuLy8gICAgIC51bnNlbGVjdGVkLWRhdGUge1xuLy8gICAgICAgICBib3JkZXI6IG5vbmU7XG4vLyAgICAgfVxuLy8gICAgIC5jYWxlbmRhci1ib2R5e1xuLy8gICAgICAgICAuZ3JpZCB7XG4vLyAgICAgICAgICAgICBwYWRkaW5nOiAwO1xuLy8gICAgICAgICB9XG4vLyAgICAgICAgIC5jb2w6bGFzdC1jaGlsZCB7XG4vLyAgICAgICAgICAgICBib3JkZXItcmlnaHQ6IG5vbmU7XG4vLyAgICAgICAgIH1cbi8vICAgICAgICAgLmNhbGVuZGFyLXdlZWtkYXksIC5jYWxlbmRhci1kYXRlIHtcbi8vICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbi8vICAgICAgICAgICAgIG1hcmdpbjogMDtcbi8vICAgICAgICAgfVxuLy8gICAgICAgICAuY2FsZW5kYXItd2Vla2RheSB7XG4vLyAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbi8vICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjMUE3MzJEO1xuLy8gICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzI2QUI1Njtcbi8vICAgICAgICAgfVxuLy8gICAgICAgICAuY2FsZW5kYXItZGF0ZSB7XG4vLyAgICAgICAgICAgICBib3JkZXItdG9wOiBzb2xpZCAxcHggIzFBNzMyRDtcbi8vICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjMWE3MzJEO1xuLy8gICAgICAgICB9XG4vLyAgICAgfVxuLy8gfVxuXG4vL2VuZCBvZiBjYWxlbmRhciBjc3NcblxuLmZvb3Rlci10b29sYmFyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG4uZm9vdGVyLWJ1dHRvbiB7XG4gICAgY29sb3I6ICR3aGl0ZTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG59XG5cbiJdfQ== */";
    /***/
  },

  /***/
  "./src/app/shyfts/shyfts-details/shyfts-details.component.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/shyfts/shyfts-details/shyfts-details.component.ts ***!
    \*******************************************************************/

  /*! exports provided: ShyftsDetailsPage */

  /***/
  function srcAppShyftsShyftsDetailsShyftsDetailsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShyftsDetailsPage", function () {
      return ShyftsDetailsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/shared/services/routing.service */
    "./src/shared/services/routing.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/shared/services/loader.service */
    "./src/shared/services/loader.service.ts");
    /* harmony import */


    var src_shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/shared/services/schedule.service */
    "./src/shared/services/schedule.service.ts");
    /* harmony import */


    var src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/shared/services/toast.service */
    "./src/shared/services/toast.service.ts");
    /* harmony import */


    var _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/calendar/ngx */
    "./node_modules/@ionic-native/calendar/ngx/index.js");

    var ShyftsDetailsPage =
    /*#__PURE__*/
    function () {
      function ShyftsDetailsPage(route, router, routingService, alertController, loaderService, scheduleService, toastService, calendar, platform) {
        _classCallCheck(this, ShyftsDetailsPage);

        this.route = route;
        this.router = router;
        this.routingService = routingService;
        this.alertController = alertController;
        this.loaderService = loaderService;
        this.scheduleService = scheduleService;
        this.toastService = toastService;
        this.calendar = calendar;
        this.platform = platform;
        this.userDesiredSchedules = [];
        this.allSchedules = [];
        this.events = [];
      }

      _createClass(ShyftsDetailsPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.schedule = JSON.parse(localStorage.getItem("selected-schedule"));
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.schedule = JSON.parse(localStorage.getItem("selected-schedule"));
          console.log(this.schedule); // let today = new Date();
          // let sched = new Date(this.schedule.deliveryDate);
          // let endTime = this.schedule.endTime.split(":")[0];
          // sched.setHours(endTime);
          // let dateDiff = sched.getTime() - today.getTime() / 3600000;
          // console.log("Diff", dateDiff);
          // // console.log("sched", sched);
          // if (dateDiff < 0) {
          //   // console.log(dateDiff); 
          // }
        }
      }, {
        key: "cancelShyftAlert",
        value: function cancelShyftAlert(schedule) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            var _this = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.alertController.create({
                      header: 'Confirm Shyft Cancel',
                      message: 'Are you sure you want to cancel this shyft?',
                      animated: true,
                      mode: "ios",
                      cssClass: "cancel-alert",
                      buttons: [{
                        text: 'No',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: function handler() {
                          console.log('Shyft Not Cancelled');
                        }
                      }, {
                        text: 'Yes',
                        handler: function handler() {
                          var today = new Date();
                          var scheduleDate = new Date(_this.schedule.deliveryDate);
                          console.log("Delivery Date", scheduleDate);
                          var diff = Math.abs(scheduleDate.getTime() - today.getTime()) / 3600000;
                          console.log("Straight Date Difference", diff);

                          if (diff <= 24 && scheduleDate > today) {
                            _this.toastService.presentToast("Schedule is already in the next 24 hours. Unable to cancel Shyft");
                          } else {
                            _this.loaderService.createLoader("Cancelling Shyft...");

                            _this.scheduleService.cancelSchedule(schedule).subscribe(function (data) {
                              console.log('Confirm Shyft Cancel'); // this.getUserDesiredSchedule().then(() => {
                              //   if (this.userDesiredSchedules.length == 0) {
                              //     this.getAllSchedule();
                              //     this.currentSegment = "availableShyfts"
                              //   }
                              //   this.loaderService.dismissLoader();
                              //   this.toastService.presentToast("Shyft Successfully cancelled");
                              // });

                              _this.loaderService.dismissLoader();

                              localStorage.removeItem("selected-schedule");
                              console.log("To deliver", JSON.stringify(schedule));

                              _this.toastService.presentToast("Shyft Successfully cancelled");

                              _this.router.navigate(["/tabs/shyfts"]);
                            }, function (error) {
                              console.log(error);

                              _this.toastService.presentToast("Unable to cancel Shyft");
                            });
                          }

                          _this.loaderService.dismissLoader();
                        }
                      }]
                    });

                  case 2:
                    alert = _context.sent;
                    _context.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "getUserDesiredSchedule",
        value: function getUserDesiredSchedule() {
          return new Promise(function (resolve, reject) {
            var _this2 = this;

            this.scheduleService.getSchedules("user").subscribe(function (result) {
              console.log("Get User Desired Schedule", result);
              _this2.userDesiredSchedules = result;
              resolve(true);
            }, function (error) {
              console.log("Error", error);
              resolve(false);
            });
          }.bind(this));
        }
      }, {
        key: "getAllSchedule",
        value: function getAllSchedule() {
          var _this3 = this;

          // this.loaderService.createLoader("Loading...");
          this.scheduleService.getSchedules("available").subscribe(function (result) {
            console.log("Get All Schedule", result);
            _this3.skeletonBoolean = false;
            _this3.allSchedules = result;
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
              for (var _iterator = _this3.allSchedules[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var schedule = _step.value;
                schedule.checked = false;
              }
            } catch (err) {
              _didIteratorError = true;
              _iteratorError = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }
              } finally {
                if (_didIteratorError) {
                  throw _iteratorError;
                }
              }
            }

            _this3.checkboxChecker = false;

            _this3.checkDates();

            _this3.skeletonBoolean = false; // this.loaderService.dismissLoader();
          }, function (error) {
            console.log("Error", error);
            _this3.skeletonBoolean = false; // this.loaderService.dismissLoader();
          });
        }
      }, {
        key: "checkDates",
        value: function checkDates() {
          var curr = new Date();
          var first = curr.getDate() - curr.getDay() + 1;
          console.log(curr.getDate());
          console.log(curr.getDay());
          var last = first + 6;
          var thisDay = new Date(Date.now());
          this.weekdates = new Array();
          this.events = new Array();
          var event = false;
          this.today = thisDay.getDate();
          console.log('this is today: ' + thisDay); // let firstDay = new Date(curr.setDate(first)).toLocaleDateString();
          // let lastDay = new Date(curr.setDate(last)).toLocaleDateString();
          // console.log(this.today.toString());
          // console.log(curr.getDay().toLocaleString());
          // this.firstDay = new Date(curr.setDate(first)).toLocaleDateString();
          // console.log(firstDay);
          // console.log(lastDay);

          for (var firstD = first; firstD <= last; firstD++) {
            var day = new Date(curr.setDate(firstD)).toLocaleDateString(); // this.weekdates.push(day);

            var events = {
              date: "",
              event: false
            };
            events.date = day;
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
              for (var _iterator2 = this.allSchedules[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var schedule = _step2.value;
                var sched = schedule.deliveryDate.split("-");
                var month = day.split("/");

                if (sched[2] == firstD && sched[1] == month[0]) {
                  event = true;
                  break;
                } else {
                  event = false;
                }
              } // this.events.push(event);

            } catch (err) {
              _didIteratorError2 = true;
              _iteratorError2 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }
              } finally {
                if (_didIteratorError2) {
                  throw _iteratorError2;
                }
              }
            }

            events.event = event;
            console.log(events);
            this.weekdates.push(events);
          }

          console.log(this.weekdates);
        }
      }, {
        key: "saveScheduletoCalendar",
        value: function saveScheduletoCalendar() {
          var _this4 = this;

          var startTime = this.schedule.startTime + ":00";
          var endTime = this.schedule.endTime + ":00";
          console.log("start", startTime);
          var startDate = new Date(this.schedule.deliveryDate.replace(/-/g, "/") + ' ' + startTime);
          var endDate = new Date(this.schedule.deliveryDate.replace(/-/g, "/") + ' ' + endTime);
          console.log(this.schedule.deliveryDate.replace(/-/g, "/"));
          console.log("startDate", startDate);
          this.calendar.hasReadWritePermission().then(function (result) {
            console.log("has permission: " + result);

            if (result) {
              // this.loaderService.createLoader("Saving Event in your calendar..");
              var calOptions = _this4.calendar.getCalendarOptions();

              calOptions.calendarId = 1; //Androind Only

              calOptions.calendarName = "Driver-App Schedule"; //iOS only

              if (_this4.platform.is("android")) {
                _this4.calendar.createEventInteractively("My Shyft for " + _this4.schedule.deliveryDate, "", "Saved from Cynch Driver App", startDate, endDate).then(function (response) {
                  console.log("Added successfully" + response);

                  _this4.loaderService.dismissLoader();

                  _this4.toastService.presentToast("Event has been saved");
                }, function (error) {
                  console.log(error);

                  _this4.loaderService.dismissLoader();
                });
              } else {
                _this4.calendar.createCalendar(calOptions).then(function () {
                  return _this4.calendar.createEventInteractively("My Shyft for " + _this4.schedule.deliveryDate, "", "Saved from Cynch Driver App", startDate, endDate).then(function (response) {
                    console.log("Added successfully" + response);

                    _this4.loaderService.dismissLoader();

                    _this4.toastService.presentToast("Event has been saved");
                  }, function (error) {
                    console.log(error);

                    _this4.loaderService.dismissLoader();
                  });
                });
              }
            } else {
              _this4.calendar.requestReadWritePermission().then(function (result) {
                // this.loaderService.createLoader("Saving Event in your calendar..");
                _this4.calendar.createEventInteractively("My Shyft" + _this4.schedule.deliveryDate, "", "Saved from Cynch Driver App", startDate, endDate).then(function (response) {
                  console.log("Added successfully" + response);

                  _this4.loaderService.dismissLoader();

                  _this4.toastService.presentToast("Event has been saved");
                }, function (error) {
                  console.log(error);

                  _this4.loaderService.dismissLoader();
                });
              }, function (error) {
                console.log(error);

                _this4.loaderService.dismissLoader();
              });
            }
          }, function (error) {
            _this4.loaderService.dismissLoader();

            console.log(error);
          });
        }
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ionViewDidEnter();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }]);

      return ShyftsDetailsPage;
    }();

    ShyftsDetailsPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_3__["RoutingService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }, {
        type: src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"]
      }, {
        type: src_shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_6__["ScheduleService"]
      }, {
        type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"]
      }, {
        type: _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_8__["Calendar"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]
      }];
    };

    ShyftsDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-shyfts-details',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./shyfts-details.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shyfts/shyfts-details/shyfts-details.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./shyfts-details.component.scss */
      "./src/app/shyfts/shyfts-details/shyfts-details.component.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_3__["RoutingService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"], src_shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_6__["ScheduleService"], src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"], _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_8__["Calendar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]])], ShyftsDetailsPage);
    /***/
  },

  /***/
  "./src/app/shyfts/shyfts-details/shyfts-details.module.ts":
  /*!****************************************************************!*\
    !*** ./src/app/shyfts/shyfts-details/shyfts-details.module.ts ***!
    \****************************************************************/

  /*! exports provided: ShyftsDetailsModule */

  /***/
  function srcAppShyftsShyftsDetailsShyftsDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShyftsDetailsModule", function () {
      return ShyftsDetailsModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/shared/shared.module */
    "./src/shared/shared.module.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _shyfts_details_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./shyfts-details.component */
    "./src/app/shyfts/shyfts-details/shyfts-details.component.ts");

    var routes = [{
      path: '',
      component: _shyfts_details_component__WEBPACK_IMPORTED_MODULE_7__["ShyftsDetailsPage"]
    }];

    var ShyftsDetailsModule = function ShyftsDetailsModule() {
      _classCallCheck(this, ShyftsDetailsModule);
    };

    ShyftsDetailsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_shyfts_details_component__WEBPACK_IMPORTED_MODULE_7__["ShyftsDetailsPage"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)]
    })], ShyftsDetailsModule);
    /***/
  }
}]);
//# sourceMappingURL=shyfts-shyfts-details-shyfts-details-module-es5.js.map