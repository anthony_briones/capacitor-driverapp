(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../drive/delivery-cancel/delivery-cancel.module": [
		"./src/app/drive/delivery-cancel/delivery-cancel.module.ts",
		"common",
		"drive-delivery-cancel-delivery-cancel-module"
	],
	"../drive/delivery-confirmation/delivery-confirmation.module": [
		"./src/app/drive/delivery-confirmation/delivery-confirmation.module.ts",
		"common",
		"drive-delivery-confirmation-delivery-confirmation-module"
	],
	"../drive/delivery-detail/delivery-detail.module": [
		"./src/app/drive/delivery-detail/delivery-detail.module.ts",
		"common",
		"drive-delivery-detail-delivery-detail-module"
	],
	"../drive/delivery-edit/delivery-edit.module": [
		"./src/app/drive/delivery-edit/delivery-edit.module.ts",
		"drive-delivery-edit-delivery-edit-module"
	],
	"../drive/delivery-summary/delivery-summary.module": [
		"./src/app/drive/delivery-summary/delivery-summary.module.ts",
		"common",
		"drive-delivery-summary-delivery-summary-module"
	],
	"../drive/drive.module": [
		"./src/app/drive/drive.module.ts",
		"common",
		"drive-drive-module"
	],
	"../drive/signature/signature.module": [
		"./src/app/drive/signature/signature.module.ts",
		"drive-signature-signature-module"
	],
	"../home/home.module": [
		"./src/app/home/home.module.ts",
		"default~home-home-module~routes-route-details-route-details-module~routes-routes-module~shyfts-shyft~3de54bc4",
		"common",
		"home-home-module"
	],
	"../routes/route-details/route-details.module": [
		"./src/app/routes/route-details/route-details.module.ts",
		"default~home-home-module~routes-route-details-route-details-module~routes-routes-module~shyfts-shyft~3de54bc4",
		"common",
		"routes-route-details-route-details-module"
	],
	"../routes/route-done/route-done.module": [
		"./src/app/routes/route-done/route-done.module.ts",
		"routes-route-done-route-done-module"
	],
	"../routes/routes.module": [
		"./src/app/routes/routes.module.ts",
		"default~home-home-module~routes-route-details-route-details-module~routes-routes-module~shyfts-shyft~3de54bc4",
		"common",
		"routes-routes-module"
	],
	"../settings/account-settings/account-settings.module": [
		"./src/app/settings/account-settings/account-settings.module.ts",
		"common",
		"settings-account-settings-account-settings-module"
	],
	"../settings/change-password/change-password.module": [
		"./src/app/settings/change-password/change-password.module.ts",
		"settings-change-password-change-password-module"
	],
	"../settings/help/help.module": [
		"./src/app/settings/help/help.module.ts",
		"settings-help-help-module"
	],
	"../settings/navigation-settings/navigation-settings.module": [
		"./src/app/settings/navigation-settings/navigation-settings.module.ts",
		"common",
		"settings-navigation-settings-navigation-settings-module"
	],
	"../settings/settings.module": [
		"./src/app/settings/settings.module.ts",
		"common",
		"settings-settings-module"
	],
	"../shyfts/shyfts-details/shyfts-details.module": [
		"./src/app/shyfts/shyfts-details/shyfts-details.module.ts",
		"default~home-home-module~routes-route-details-route-details-module~routes-routes-module~shyfts-shyft~3de54bc4",
		"shyfts-shyfts-details-shyfts-details-module"
	],
	"../shyfts/shyfts.module": [
		"./src/app/shyfts/shyfts.module.ts",
		"default~home-home-module~routes-route-details-route-details-module~routes-routes-module~shyfts-shyft~3de54bc4",
		"common",
		"shyfts-shyfts-module"
	],
	"./login/forgot-password/forgot-password.module": [
		"./src/app/login/forgot-password/forgot-password.module.ts",
		"login-forgot-password-forgot-password-module"
	],
	"./login/login.module": [
		"./src/app/login/login.module.ts",
		"common",
		"login-login-module"
	],
	"./tabs/tabs.module": [
		"./src/app/tabs/tabs.module.ts",
		"tabs-tabs-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet-controller_8.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-controller_8.entry.js",
		"common",
		0
	],
	"./ion-action-sheet-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-ios.entry.js",
		"common",
		1
	],
	"./ion-action-sheet-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-md.entry.js",
		"common",
		2
	],
	"./ion-alert-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-ios.entry.js",
		"common",
		3
	],
	"./ion-alert-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-md.entry.js",
		"common",
		4
	],
	"./ion-app_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-ios.entry.js",
		"common",
		5
	],
	"./ion-app_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-md.entry.js",
		"common",
		6
	],
	"./ion-avatar_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-ios.entry.js",
		"common",
		7
	],
	"./ion-avatar_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-md.entry.js",
		"common",
		8
	],
	"./ion-back-button-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-ios.entry.js",
		"common",
		9
	],
	"./ion-back-button-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-md.entry.js",
		"common",
		10
	],
	"./ion-backdrop-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-ios.entry.js",
		11
	],
	"./ion-backdrop-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-md.entry.js",
		12
	],
	"./ion-button_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-ios.entry.js",
		"common",
		13
	],
	"./ion-button_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-md.entry.js",
		"common",
		14
	],
	"./ion-card_5-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-ios.entry.js",
		"common",
		15
	],
	"./ion-card_5-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-md.entry.js",
		"common",
		16
	],
	"./ion-checkbox-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-ios.entry.js",
		"common",
		17
	],
	"./ion-checkbox-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-md.entry.js",
		"common",
		18
	],
	"./ion-chip-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-ios.entry.js",
		"common",
		19
	],
	"./ion-chip-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-md.entry.js",
		"common",
		20
	],
	"./ion-col_3.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js",
		21
	],
	"./ion-datetime_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-ios.entry.js",
		"common",
		22
	],
	"./ion-datetime_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-md.entry.js",
		"common",
		23
	],
	"./ion-fab_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-ios.entry.js",
		"common",
		24
	],
	"./ion-fab_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-md.entry.js",
		"common",
		25
	],
	"./ion-img.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-img.entry.js",
		26
	],
	"./ion-infinite-scroll_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-ios.entry.js",
		"common",
		27
	],
	"./ion-infinite-scroll_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-md.entry.js",
		"common",
		28
	],
	"./ion-input-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-ios.entry.js",
		"common",
		29
	],
	"./ion-input-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-md.entry.js",
		"common",
		30
	],
	"./ion-item-option_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-ios.entry.js",
		"common",
		31
	],
	"./ion-item-option_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-md.entry.js",
		"common",
		32
	],
	"./ion-item_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-ios.entry.js",
		"common",
		33
	],
	"./ion-item_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-md.entry.js",
		"common",
		34
	],
	"./ion-loading-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-ios.entry.js",
		"common",
		35
	],
	"./ion-loading-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-md.entry.js",
		"common",
		36
	],
	"./ion-menu_4-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-ios.entry.js",
		"common",
		37
	],
	"./ion-menu_4-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-md.entry.js",
		"common",
		38
	],
	"./ion-modal-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-ios.entry.js",
		"common",
		39
	],
	"./ion-modal-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-md.entry.js",
		"common",
		40
	],
	"./ion-nav_5.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-nav_5.entry.js",
		"common",
		41
	],
	"./ion-popover-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-ios.entry.js",
		"common",
		42
	],
	"./ion-popover-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-md.entry.js",
		"common",
		43
	],
	"./ion-progress-bar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-ios.entry.js",
		"common",
		44
	],
	"./ion-progress-bar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-md.entry.js",
		"common",
		45
	],
	"./ion-radio_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-ios.entry.js",
		"common",
		46
	],
	"./ion-radio_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-md.entry.js",
		"common",
		47
	],
	"./ion-range-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-ios.entry.js",
		"common",
		48
	],
	"./ion-range-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-md.entry.js",
		"common",
		49
	],
	"./ion-refresher_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-ios.entry.js",
		"common",
		50
	],
	"./ion-refresher_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-md.entry.js",
		"common",
		51
	],
	"./ion-reorder_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-ios.entry.js",
		"common",
		52
	],
	"./ion-reorder_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-md.entry.js",
		"common",
		53
	],
	"./ion-ripple-effect.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js",
		54
	],
	"./ion-route_4.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js",
		"common",
		55
	],
	"./ion-searchbar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-ios.entry.js",
		"common",
		56
	],
	"./ion-searchbar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-md.entry.js",
		"common",
		57
	],
	"./ion-segment_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-ios.entry.js",
		"common",
		58
	],
	"./ion-segment_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-md.entry.js",
		"common",
		59
	],
	"./ion-select_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-ios.entry.js",
		"common",
		60
	],
	"./ion-select_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-md.entry.js",
		"common",
		61
	],
	"./ion-slide_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-ios.entry.js",
		62
	],
	"./ion-slide_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-md.entry.js",
		63
	],
	"./ion-spinner.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js",
		"common",
		64
	],
	"./ion-split-pane-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-ios.entry.js",
		65
	],
	"./ion-split-pane-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-md.entry.js",
		66
	],
	"./ion-tab-bar_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-ios.entry.js",
		"common",
		67
	],
	"./ion-tab-bar_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-md.entry.js",
		"common",
		68
	],
	"./ion-tab_2.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js",
		"common",
		69
	],
	"./ion-text.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-text.entry.js",
		"common",
		70
	],
	"./ion-textarea-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-ios.entry.js",
		"common",
		71
	],
	"./ion-textarea-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-md.entry.js",
		"common",
		72
	],
	"./ion-toast-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-ios.entry.js",
		"common",
		73
	],
	"./ion-toast-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-md.entry.js",
		"common",
		74
	],
	"./ion-toggle-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-ios.entry.js",
		"common",
		75
	],
	"./ion-toggle-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-md.entry.js",
		"common",
		76
	],
	"./ion-virtual-scroll.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js",
		77
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\n  <ion-router-outlet></ion-router-outlet>\n</ion-app>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-edit/delivery-edit.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-edit/delivery-edit.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <!-- <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Delivery Option</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button text=\"Skip\"icon=\"null\" (click)=\"goToConfirmation()\">Skip</ion-button>\n    </ion-buttons>\n  </ion-toolbar> -->\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-row>\n    <ion-col size=\"12\" class=\"vertical-align horizontal-align\">\n      <ion-label class=\"edit-label\">\n        Edit Quantity\n      </ion-label>\n    </ion-col>\n    <ion-col size=\"5\" class=\"vertical-align right\">\n      <ion-button class=\"quantity-btn\" (click)=\"subtract()\">\n        <ion-icon name=\"ios-remove-circle-outline\" class=\"big\"> </ion-icon>\n      </ion-button>\n    </ion-col>\n    <ion-col size=\"2\" class=\"vertical-align horizontal-align\">\n      <ion-label class=\"quantity\">\n        {{qty}}\n      </ion-label>\n    </ion-col>\n    <ion-col size=\"5\" class=\"ion-align-self-end\">\n      <ion-button class=\"quantity-btn\" (click)=\"add()\">\n        <ion-icon name=\"ios-add-circle-outline\" class=\"big\"> </ion-icon>\n      </ion-button>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"vertical-align horizontal-align\">\n      <ion-button (click)=\"updateQty()\" class=\"update-btn\">Update</ion-button>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"vertical-align horizontal-align\">\n        <ion-button (click)=\"dismiss()\" class=\"cancel-btn\">Cancel</ion-button>\n      </ion-col>\n  </ion-row>\n\n\n\n\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/signature/signature.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/drive/signature/signature.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>signature</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding no-bounce>\n  <!-- <signature-pad [options]=\"signaturePadOptions\" id=\"signatureCanvas\"></signature-pad>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"4\">\n        <button ion-button full color=\"danger\" (click)=\"drawCancel()\">Cancel</button>\n      </ion-col>\n      <ion-col size=\"4\">\n        <button ion-button full color=\"light\" (click)=\"drawClear()\">Clear</button>\n      </ion-col>\n      <ion-col size=\"4\">\n        <button ion-button full color=\"secondary\" (click)=\"drawComplete()\">Done</button>\n      </ion-col>\n    </ion-row>\n  </ion-grid> -->\n\n  <signature-pad class=\"vertical-align horizontal-align\" id=\"signatureCanvas\" (onBeginEvent)=\"drawStart()\" (onEndEvent)=\"drawComplete()\"></signature-pad>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"4\">\n        <ion-button color=\"danger\" class=\"vertical-align horizontal-align\" (click)=\"drawCancel()\">Cancel</ion-button>\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-button color=\"light\" class=\"vertical-align horizontal-align\" (click)=\"drawClear()\">Clear</ion-button>\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-button color=\"secondary\" class=\"vertical-align horizontal-align\" (click)=\"signDone()\">Done</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <!-- <p>{{signatureImage}}</p> -->\n  <img *ngIf=\"signatureImage\" [src]=\"signatureImage\"/>\n\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/forgot-password/forgot-password.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/forgot-password/forgot-password.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Forgot password</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content scroll-y=\"false\">\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <form [formGroup]=\"emailForm\">\n    <ion-grid>\n      <ion-row>\n\n        <ion-col size=\"12\">\n          <div class=\"login-label ion-text-uppercase\">\n            <label for=\"email\">\n              Email Address\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"login-input\">\n          <input type=\"text\" [(ngModel)]=\"email\" formControlName=\"email\" id=\"email\" [ngClass]=\"{ 'is-invalid': (fc.email.dirty || fc.email.touched || submitted) && fc.email.errors, 'is-valid': (fc.email.dirty || fc.email.touched) && !fc.email.errors }\">\n        </ion-col>\n\n        <ion-col size=\"6\" class=\"ion-padding-top\">\n          <button class=\"btn shadow-lg primary-1-btn\" icon-only round ion-button (click)=\"dismiss()\">\n            Cancel\n          </button>\n        </ion-col>\n\n        <ion-col size=\"6\" class=\"ion-padding-top\">\n          <button class=\"btn shadow-lg primary-2-btn\" [disabled]=\"emailForm.invalid\" [ngClass]=\"{'opacity' : emailForm.invalid}\" icon-only round ion-button (click)=\"confirm()\">\n            Confirm\n          </button>\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n  </form>\n</ion-content>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/guards/auth.guard */ "./src/shared/guards/auth.guard.ts");




const routes = [
    { path: '', redirectTo: 'tabs', pathMatch: 'full' },
    // { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
    { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule', canActivate: [_shared_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]] },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    { path: 'forgot-password', loadChildren: './login/forgot-password/forgot-password.module#ForgotPasswordPageModule' },
    { path: '**', redirectTo: 'login' },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_shared_services_network_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/shared/services/network.service */ "./src/shared/services/network.service.ts");







let AppComponent = class AppComponent {
    constructor(platform, splashScreen, statusBar, navCtrl, router, networkService) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.navCtrl = navCtrl;
        this.router = router;
        this.networkService = networkService;
        this.isLoggedIn = false;
        this.initializeApp();
    }
    initializeApp() {
        // this.platform.ready().then(() => {
        //   this.statusBar.styleDefault();
        //   this.splashScreen.hide();
        //   if (localStorage.getItem("current_user")) {
        //     this.isLoggedIn = true;
        //   };
        //   if (this.isLoggedIn) {
        //     this.router.navigate(['tabs'])
        //   } else {
        //     this.router.navigate(['login'])
        //   }  
        // });
        this.networkService.checkConnection();
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"] },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_shared_services_network_service__WEBPACK_IMPORTED_MODULE_6__["NetworkService"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"],
        _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
        src_shared_services_network_service__WEBPACK_IMPORTED_MODULE_6__["NetworkService"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ "./node_modules/@ionic-native/launch-navigator/ngx/index.js");
/* harmony import */ var _shared_interceptors_error_interceptor__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../shared/interceptors/error.interceptor */ "./src/shared/interceptors/error.interceptor.ts");
/* harmony import */ var _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/calendar/ngx */ "./node_modules/@ionic-native/calendar/ngx/index.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _drive_delivery_edit_delivery_edit_page__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./drive/delivery-edit/delivery-edit.page */ "./src/app/drive/delivery-edit/delivery-edit.page.ts");
/* harmony import */ var _drive_signature_signature_page__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./drive/signature/signature.page */ "./src/app/drive/signature/signature.page.ts");
/* harmony import */ var angular2_signaturepad__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! angular2-signaturepad */ "./node_modules/angular2-signaturepad/index.js");
/* harmony import */ var angular2_signaturepad__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(angular2_signaturepad__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var ionicsignaturepad__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ionicsignaturepad */ "./node_modules/ionicsignaturepad/dist/index.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var ng_lottie__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ng-lottie */ "./node_modules/ng-lottie/dist/esm/src/index.js");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");
/* harmony import */ var _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @ionic-native/location-accuracy/ngx */ "./node_modules/@ionic-native/location-accuracy/ngx/index.js");
/* harmony import */ var _login_forgot_password_forgot_password_page__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./login/forgot-password/forgot-password.page */ "./src/app/login/forgot-password/forgot-password.page.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _node_modules_ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../../node_modules/@ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");




















// import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';


// import { CallNumber } from '@ionic-native/call-number/ngx';





// import { Network } from '@ionic-native/network/ngx';
// import { Geofence } from '@ionic-native/geofence/ngx';
// import { NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
            _drive_delivery_edit_delivery_edit_page__WEBPACK_IMPORTED_MODULE_16__["DeliveryEditPage"],
            _drive_signature_signature_page__WEBPACK_IMPORTED_MODULE_17__["SignaturePage"],
            _login_forgot_password_forgot_password_page__WEBPACK_IMPORTED_MODULE_24__["ForgotPasswordPage"],
        ],
        entryComponents: [
            _drive_delivery_edit_delivery_edit_page__WEBPACK_IMPORTED_MODULE_16__["DeliveryEditPage"],
            _drive_signature_signature_page__WEBPACK_IMPORTED_MODULE_17__["SignaturePage"],
            _login_forgot_password_forgot_password_page__WEBPACK_IMPORTED_MODULE_24__["ForgotPasswordPage"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
            angular2_signaturepad__WEBPACK_IMPORTED_MODULE_18__["SignaturePadModule"],
            ionicsignaturepad__WEBPACK_IMPORTED_MODULE_19__["IonicSignaturePadModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_20__["BrowserAnimationsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_25__["ReactiveFormsModule"],
            ng_lottie__WEBPACK_IMPORTED_MODULE_21__["LottieAnimationViewModule"].forRoot(),
        ],
        providers: [
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"],
            _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_14__["Spherical"],
            _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_11__["LaunchNavigator"],
            _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_13__["Calendar"],
            // ScreenOrientation,
            _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_14__["GoogleMaps"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_15__["Camera"],
            // CallNumber,
            _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_22__["AndroidPermissions"],
            _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_23__["LocationAccuracy"],
            ionicsignaturepad__WEBPACK_IMPORTED_MODULE_19__["IonicsignaturepadProvider"],
            _node_modules_ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_26__["AppVersion"],
            // Network,
            // NativePageTransitions,
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HTTP_INTERCEPTORS"], useClass: _shared_interceptors_error_interceptor__WEBPACK_IMPORTED_MODULE_12__["ErrorInterceptor"], multi: true }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/drive/delivery-edit/delivery-edit.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/drive/delivery-edit/delivery-edit.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-title {\n  color: #003A58;\n}\n\n.edit-label {\n  color: #003A58;\n  font-family: \"Montserrat-SemiBold\";\n  font-size: 1.5rem;\n  padding-top: 1rem;\n}\n\nion-icon.big {\n  width: 50px;\n  height: 50px;\n}\n\n.quantity-btn {\n  color: #1982FC;\n  background-color: white;\n  --background: white;\n}\n\n.right {\n  -webkit-box-pack: end !important;\n          justify-content: flex-end !important;\n}\n\n.quantity {\n  color: #003A58;\n  font-size: 3rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.update-btn {\n  width: 60%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n  background: #003A58;\n  --background: $primary-color-1;\n}\n\n.cancel-btn {\n  width: 60%;\n  font-size: 0.875rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: #4A4A4A;\n  font-family: \"Montserrat-Bold\";\n  --background: #E8E8E8;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL2RyaXZlL2RlbGl2ZXJ5LWVkaXQvZGVsaXZlcnktZWRpdC5wYWdlLnNjc3MiLCIvVXNlcnMvYW50aG9ueS5icmlvbmVzL0RvY3VtZW50cy9DYXBEcml2ZXIvc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hcHAvZHJpdmUvZGVsaXZlcnktZWRpdC9kZWxpdmVyeS1lZGl0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFPQTtFQUNJLHlCQUFBO0VBQ0EsZ0RBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkFBQTtBQ05KOztBRFNBO0VBQ0kseUJFbEJjO0VGbUJkLFdBQUE7RUFDQSxjRWhCSTtFRmlCSixtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtBQ05KOztBRFNBO0VBQ0ksK0JBQUE7RUFBQSx3QkFBQTtFQUNBLGdDQUFBO0VBQ0Esb0NBQUE7VUFBQSw4QkFBQTtBQ05KOztBRFNBO0VBQ0ksbUNBQUE7VUFBQSxrQ0FBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7VUFBQSx5QkFBQTtBQ05KOztBRFNBO0VBQ0ksd0JBQUE7VUFBQSx1QkFBQTtBQ05KOztBRFNBO0VBQ0ksWUFBQTtBQ05KOztBRWhEQTtFQUNJLGNERGM7QURvRGxCOztBRWhEQTtFQUNJLGNETGM7RUNNZCxrQ0FBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUZtREo7O0FFL0NJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUZrRFI7O0FFOUNBO0VBQ0ksY0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUZpREo7O0FFOUNBO0VBQ0csZ0NBQUE7VUFBQSxvQ0FBQTtBRmlESDs7QUU5Q0E7RUFDSSxjRDdCYztFQzhCZCxlQUFBO0VBQ0Esa0NBQUE7QUZpREo7O0FFOUNBO0VBQ0ksVUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJEekNjO0VDMENkLDhCQUFBO0FGaURKOztBRTdDQTtFQUNJLFVBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtFQUVBLHFCQUFBO0FGK0NKIiwiZmlsZSI6InNyYy9hcHAvZHJpdmUvZGVsaXZlcnktZWRpdC9kZWxpdmVyeS1lZGl0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy9mb250cyc7XG5AaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzJztcblxuLy8gLmlzLXZhbGlkIHtcbi8vICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlXG4vLyB9XG5cbi5pcy1pbnZhbGlkIHtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcInNyYy9hc3NldHMvaWNvbi9pY29uLWhlbHAuc3ZnXCIpIG5vLXJlcGVhdDtcbn1cblxuLmlzLXZhbGlkIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjNUVCMDAzXG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0VFNDAzNjtcbn1cblxuLnByaW1hcnktYnV0dG9uIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAkd2hpdGU7XG4gICAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTF7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4O1xufVxuXG5pb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi52ZXJ0aWNhbC1hbGlnbiB7XG4gICAgZGlzcGxheTogZmxleCFpbXBvcnRhbnQ7XG4gICAgYWxpZ24tY29udGVudDogY2VudGVyIWltcG9ydGFudDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyIWltcG9ydGFudDtcbn1cblxuLmhvcml6b250YWwtYWxpZ24ge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWFsaWduLXJpZ2h0IHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZGlzYWJsZSB7XG4gICAgb3BhY2l0eTogMC41O1xufVxuXG5cblxuXG4iLCIuaXMtaW52YWxpZCB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNFRTQwMzY7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcInNyYy9hc3NldHMvaWNvbi9pY29uLWhlbHAuc3ZnXCIpIG5vLXJlcGVhdDtcbn1cblxuLmlzLXZhbGlkIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwMztcbn1cblxuLmlzLWludmFsaWQtc2VsZWN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgI0VFNDAzNjtcbn1cblxuLnByaW1hcnktYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgaGVpZ2h0OiAycmVtO1xufVxuXG4uYmctY29sb3ItMSB7XG4gIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi52ZXJ0aWNhbC1hbGlnbiB7XG4gIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmhvcml6b250YWwtYWxpZ24ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5mbGV4LWFsaWduLWNlbnRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZGlzYWJsZSB7XG4gIG9wYWNpdHk6IDAuNTtcbn1cblxuaW9uLXRpdGxlIHtcbiAgY29sb3I6ICMwMDNBNTg7XG59XG5cbi5lZGl0LWxhYmVsIHtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIHBhZGRpbmctdG9wOiAxcmVtO1xufVxuXG5pb24taWNvbi5iaWcge1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuXG4ucXVhbnRpdHktYnRuIHtcbiAgY29sb3I6ICMxOTgyRkM7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG4ucmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kICFpbXBvcnRhbnQ7XG59XG5cbi5xdWFudGl0eSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LXNpemU6IDNyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbn1cblxuLnVwZGF0ZS1idG4ge1xuICB3aWR0aDogNjAlO1xuICBmb250LXNpemU6IDEuMTI1cmVtO1xuICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgaGVpZ2h0OiAzcmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBiYWNrZ3JvdW5kOiAjMDAzQTU4O1xuICAtLWJhY2tncm91bmQ6ICRwcmltYXJ5LWNvbG9yLTE7XG59XG5cbi5jYW5jZWwtYnRuIHtcbiAgd2lkdGg6IDYwJTtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gIGhlaWdodDogM3JlbTtcbiAgY29sb3I6ICM0QTRBNEE7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICAtLWJhY2tncm91bmQ6ICNFOEU4RTg7XG59IiwiXG4vLyBjb2xvcnNcbi8vICRwcmltYXJ5LWNvbG9yLTE6ICMwMEEzQzg7XG4kcHJpbWFyeS1jb2xvci0xOiAjMDAzQTU4O1xuJHByaW1hcnktY29sb3ItMjogI0ZBQUY0MDtcbiRwcmltYXJ5LWNvbG9yLTM6ICNGNDc3M0I7XG5cbiR3aGl0ZTogI2ZmZmZmZjtcblxuLy8gJGJ1dHRvbi1ncmFkaWVudC0xOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCByZ2JhKDI1NSwyNTUsMjU1LDAuNTApIDAlLCByZ2JhKDAsMCwwLDAuNTApIDEwMCUpO1xuJGJ1dHRvbi1ncmFkaWVudC0xOiBsaW5lYXItZ3JhZGllbnQoLTE4MGRlZywgI0ZGODg0MCAwJSwgI0VFNDAzNiAxMDAlKTsiLCJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzJztcblxuaW9uLXRpdGxlIHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbn1cblxuLmVkaXQtbGFiZWwge1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG4gICAgZm9udC1zaXplOiAxLjVyZW07XG4gICAgcGFkZGluZy10b3A6IDFyZW07XG59XG5cbmlvbi1pY29uIHtcbiAgICAmLmJpZ3tcbiAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgIGhlaWdodDogNTBweDtcbiAgICB9XG59XG5cbi5xdWFudGl0eS1idG4ge1xuICAgIGNvbG9yOiAjMTk4MkZDO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbi5yaWdodCB7XG4gICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kICFpbXBvcnRhbnQ7XG59XG5cbi5xdWFudGl0eSB7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1zaXplOiAzcmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG59XG5cbi51cGRhdGUtYnRuIHtcbiAgICB3aWR0aDogNjAlO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgYmFja2dyb3VuZDogJHByaW1hcnktY29sb3ItMTtcbiAgICAtLWJhY2tncm91bmQ6ICRwcmltYXJ5LWNvbG9yLTE7XG59XG5cblxuLmNhbmNlbC1idG4ge1xuICAgIHdpZHRoOiA2MCU7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgICBoZWlnaHQ6IDNyZW07XG4gICAgY29sb3I6ICM0QTRBNEE7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIC8vIGJhY2tncm91bmQ6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRThFOEU4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/drive/delivery-edit/delivery-edit.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/drive/delivery-edit/delivery-edit.page.ts ***!
  \***********************************************************/
/*! exports provided: DeliveryEditPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryEditPage", function() { return DeliveryEditPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




// import { Router } from '../../../../node_modules/@angular/router';
let DeliveryEditPage = class DeliveryEditPage {
    constructor(router, navParams, modalController) {
        this.router = router;
        this.navParams = navParams;
        this.modalController = modalController;
    }
    ngOnInit() {
        if (localStorage.getItem("current_delivery")) {
            this.currentDelivery = JSON.parse(localStorage.getItem("current_delivery"));
        }
        this.qty = this.navParams.get('quantity');
        this.isReturn = this.navParams.get('isReturn');
        console.log("isReturn", this.isReturn);
        this.getMaterialCode();
        // console.log(navParams.get('params1'));
    }
    goToConfirmation() {
        console.log("testing");
        this.router.navigate([`/tabs/drive/${this.currentDelivery.stopId}/confirmation`]);
    }
    dismiss() {
        this.modalController.dismiss({
            'dismissed': true
        });
    }
    add() {
        this.qty = this.qty + 1;
    }
    subtract() {
        if (this.qty > 0) {
            this.qty = this.qty - 1;
        }
    }
    updateQty() {
        let i = this.navParams.get('index');
        console.log('index', i);
        if (this.currentDelivery.items.length < i) {
            this.fillItem();
            this.currentDelivery.items.push(this.item);
            console.log("items", this.currentDelivery.items);
            localStorage.setItem("current_delivery", JSON.stringify(this.currentDelivery));
            // this.dismiss();
        }
        else {
            if (this.isReturn) {
                this.currentDelivery.items[i].quantityReturned = this.qty;
                localStorage.setItem("current_delivery", JSON.stringify(this.currentDelivery));
                // this.dismiss();
            }
            else {
                this.currentDelivery.items[i].quantityDelivered = this.qty;
                this.currentDelivery.items[i].quantity = this.qty;
                localStorage.setItem("current_delivery", JSON.stringify(this.currentDelivery));
                // this.dismiss();
            }
        }
        this.dismiss();
    }
    fillItem() {
        this.item = {
            deliveryItemId: null,
            itemNumber: this.getItemNumber(),
            materialCode: this.materialCode[1].materialCode,
            quantity: this.qty,
            quantityDelivered: this.qty,
            quantityReturned: 0,
            uom: "EA",
            unitPrice: 0,
            transferType: "1",
            type: "PRODUCT"
        };
        // this.item.deliveryItemId = null;
        // this.item.itemNumber = this.getItemNumber();
        // this.item.materialCode = "CYL-205";
        // this.item.quantity = this.qty;
        // this.item.quantityDelivered = this.qty;
        // this.item.quantityReturned = 0;
        // this.item.uom = "EA";
        // this.item.unitPrice = 49.99;
        // this.item.transferType = "1";
        // this.item.type = "PRODUCT";
    }
    getItemNumber() {
        let itemNumber = 0;
        if (this.currentDelivery.services.length > 0) {
            for (let services of this.currentDelivery.services) {
                if (parseInt(services.itemNumber) > itemNumber) {
                    itemNumber = parseInt(services.itemNumber);
                }
            }
        }
        for (let tanks of this.currentDelivery.items) {
            if (parseInt(tanks.itemNumber) > itemNumber) {
                itemNumber = parseInt(tanks.itemNumber);
            }
        }
        itemNumber = itemNumber + 10;
        let itemNumberTemp = `0000${itemNumber}`;
        itemNumberTemp = itemNumberTemp.substring(itemNumberTemp.length - 6, itemNumberTemp.length);
        return itemNumberTemp;
    }
    getMaterialCode() {
        this.materialCode = JSON.parse(localStorage.getItem("material_code"));
    }
};
DeliveryEditPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], DeliveryEditPage.prototype, "quantity", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], DeliveryEditPage.prototype, "index", void 0);
DeliveryEditPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-delivery-edit',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./delivery-edit.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-edit/delivery-edit.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./delivery-edit.page.scss */ "./src/app/drive/delivery-edit/delivery-edit.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]])
], DeliveryEditPage);



/***/ }),

/***/ "./src/app/drive/signature/signature.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/drive/signature/signature.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nsignature-pad canvas {\n  border: 1px solid red;\n  width: 100%;\n  height: 200px;\n}\n\nsignature-pad .color-block {\n  height: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL2RyaXZlL3NpZ25hdHVyZS9zaWduYXR1cmUucGFnZS5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXBwL2RyaXZlL3NpZ25hdHVyZS9zaWduYXR1cmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU9BO0VBQ0kseUJBQUE7RUFDQSxnREFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkVsQmM7RUZtQmQsV0FBQTtFQUNBLGNFaEJJO0VGaUJKLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSwrQkFBQTtFQUFBLHdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxvQ0FBQTtVQUFBLDhCQUFBO0FDTko7O0FEU0E7RUFDSSxtQ0FBQTtVQUFBLGtDQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtVQUFBLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx3QkFBQTtVQUFBLHVCQUFBO0FDTko7O0FEU0E7RUFDSSxZQUFBO0FDTko7O0FFL0NJO0VBRUkscUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtBRmlEUjs7QUUvQ0k7RUFDSSxZQUFBO0FGaURSIiwiZmlsZSI6InNyYy9hcHAvZHJpdmUvc2lnbmF0dXJlL3NpZ25hdHVyZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvZm9udHMnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcyc7XG5cbi8vIC5pcy12YWxpZCB7XG4vLyAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZVxuLy8gfVxuXG4uaXMtaW52YWxpZCB7XG4gICAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwM1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogJHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xe1xuICAgIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICAgIGRpc3BsYXk6IGZsZXghaW1wb3J0YW50O1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlciFpbXBvcnRhbnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuXG5cblxuIiwiLmlzLWludmFsaWQge1xuICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDM7XG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDFyZW07XG4gIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTEge1xuICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbnNpZ25hdHVyZS1wYWQgY2FudmFzIHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmVkO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyMDBweDtcbn1cbnNpZ25hdHVyZS1wYWQgLmNvbG9yLWJsb2NrIHtcbiAgaGVpZ2h0OiA0MHB4O1xufSIsIlxuLy8gY29sb3JzXG4vLyAkcHJpbWFyeS1jb2xvci0xOiAjMDBBM0M4O1xuJHByaW1hcnktY29sb3ItMTogIzAwM0E1ODtcbiRwcmltYXJ5LWNvbG9yLTI6ICNGQUFGNDA7XG4kcHJpbWFyeS1jb2xvci0zOiAjRjQ3NzNCO1xuXG4kd2hpdGU6ICNmZmZmZmY7XG5cbi8vICRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgcmdiYSgyNTUsMjU1LDI1NSwwLjUwKSAwJSwgcmdiYSgwLDAsMCwwLjUwKSAxMDAlKTtcbiRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KC0xODBkZWcsICNGRjg4NDAgMCUsICNFRTQwMzYgMTAwJSk7IiwiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2NvbW1vbi5zY3NzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XG5cbnNpZ25hdHVyZS1wYWQge1xuICAgIGNhbnZhcyB7XG4gICAgICAgIC8vIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCByZWQ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDIwMHB4O1xuICAgIH1cbiAgICAuY29sb3ItYmxvY2sge1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgIH1cbn1cbi8vIGNhbnZhcyB7XG4vLyAgICAgZGlzcGxheTogYmxvY2s7XG4vLyAgICAgYm9yZGVyOiAxcHggc29saWQgcmVkO1xuLy8gfVxuLy8gLmNvbG9yLWJsb2NrIHtcbi8vICAgICBoZWlnaHQ6IDQwcHg7XG4vLyAgICAgfVxuICAgIFxuXG5cbnNpZ25hdHVyZS1wYWQge1xuICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/drive/signature/signature.page.ts":
/*!***************************************************!*\
  !*** ./src/app/drive/signature/signature.page.ts ***!
  \***************************************************/
/*! exports provided: SignaturePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignaturePage", function() { return SignaturePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-signaturepad/signature-pad */ "./node_modules/angular2-signaturepad/signature-pad.js");
/* harmony import */ var angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_3__);




let SignaturePage = class SignaturePage {
    // public signatureOptions : Object = {
    //   'minWidth': 2,
    //   'canvasWidth': 340,
    //   'canvasHeight': 200
    // };
    // public signatureImage : string;
    // @ViewChild('signatureCanvas', {static: false}) canvasEL : any;
    // public canvasElement: any;
    // public _CONTEXT : any;
    // saveX: number;
    // saveY: number;
    // signatureImg: any;
    // @ViewChild(IonContent, {static: false}) content: IonContent; 
    // @ViewChild('fixedContainer', {static: false}) fixedContainer: any;
    constructor(navCtrl, modalController, renderer, plt) {
        this.navCtrl = navCtrl;
        this.modalController = modalController;
        this.renderer = renderer;
        this.plt = plt;
    }
    ngOnInit() {
    }
    dismiss() {
        this.modalController.dismiss({
            'dismissed': true
        });
    }
    drawStart() {
        console.log("start drawing");
    }
    drawComplete() {
        console.log("end drawing");
        this.signatureImage = this.signaturePad.toDataURL();
        console.log(this.signatureImage);
        // localStorage.setItem("signage", this.signatureImage);
    }
    signDone() {
        this.modifyDataImage();
        console.log(this.signatureImage);
        localStorage.setItem("signage", this.signatureImage);
        console.log("set item");
        // this.dismiss(this.signatureImage);
        this.modalController.dismiss(this.signatureImage);
    }
    drawCancel() {
        this.signatureImage = null;
        this.modalController.dismiss(this.signatureImage);
        // this.navCtrl.back();
        // this.navCtrl.navigateForward('../delivery-confirmation/delivery-confirmation.page');
    }
    modifyDataImage() {
        let dataImage = this.signatureImage.split(";")[1];
        let newDataImage = "data:image/jpeg;" + dataImage;
        this.signatureImage = newDataImage;
    }
    drawClear() {
        this.signaturePad.clear();
        this.signatureImage = null;
    }
};
SignaturePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_3__["SignaturePad"], { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", angular2_signaturepad_signature_pad__WEBPACK_IMPORTED_MODULE_3__["SignaturePad"])
], SignaturePage.prototype, "signaturePad", void 0);
SignaturePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-signature',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./signature.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/signature/signature.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./signature.page.scss */ "./src/app/drive/signature/signature.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]])
], SignaturePage);



/***/ }),

/***/ "./src/app/login/forgot-password/forgot-password.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/login/forgot-password/forgot-password.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-grid {\n  max-width: 350px;\n}\n\nion-title {\n  color: #003A58;\n}\n\n.login-label {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: left;\n          justify-content: left;\n  -webkit-box-align: left;\n          align-items: left;\n  height: 100%;\n  width: 100%;\n  color: #003A58;\n  font-size: 0.75rem;\n  padding-top: 2rem;\n  font-family: \"Roboto-Medium\";\n}\n\n.login-input {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  height: 100%;\n  width: 100%;\n  color: #003A58;\n  padding: 1rem 0 0 0;\n  font-family: \"Roboto-Bold\";\n  border-bottom: 0.001rem solid gray;\n}\n\ninput {\n  background-color: transparent;\n  border: 0;\n  font-size: 1rem;\n  width: 100%;\n  font-weight: bold;\n  padding-bottom: 0.5rem;\n}\n\n.primary-1-btn {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n  background: #003A58;\n}\n\n.primary-2-btn {\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036));\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%);\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-SemiBold\";\n  line-height: 1.3125rem;\n}\n\n.opacity {\n  opacity: 0.5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL2xvZ2luL2ZvcmdvdC1wYXNzd29yZC9mb3Jnb3QtcGFzc3dvcmQucGFnZS5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXBwL2xvZ2luL2ZvcmdvdC1wYXNzd29yZC9mb3Jnb3QtcGFzc3dvcmQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU9BO0VBQ0kseUJBQUE7RUFDQSxnREFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkVsQmM7RUZtQmQsV0FBQTtFQUNBLGNFaEJJO0VGaUJKLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSwrQkFBQTtFQUFBLHdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxvQ0FBQTtVQUFBLDhCQUFBO0FDTko7O0FEU0E7RUFDSSxtQ0FBQTtVQUFBLGtDQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtVQUFBLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx3QkFBQTtVQUFBLHVCQUFBO0FDTko7O0FEU0E7RUFDSSxZQUFBO0FDTko7O0FFaERBO0VBQ0ksZ0JBQUE7QUZtREo7O0FFaERBO0VBQ0ksY0RMYztBRHdEbEI7O0FFaERBO0VBQ0ksb0JBQUE7RUFBQSxhQUFBO0VBQ0Esc0JBQUE7VUFBQSxxQkFBQTtFQUNBLHVCQUFBO1VBQUEsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGNEZGM7RUNlZCxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7QUZtREo7O0FFaERBO0VBQ0ksb0JBQUE7RUFBQSxhQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGNEMUJjO0VDMkJkLG1CQUFBO0VBQ0EsMEJBQUE7RUFDQSxrQ0FBQTtBRm1ESjs7QUVoREE7RUFDSSw2QkFBQTtFQUNBLFNBQUE7RUFFQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUZrREo7O0FFL0NBO0VBRUksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUVBLDhCQUFBO0VBQ0EsbUJEbkRjO0FEbUdsQjs7QUUzQ0E7RUFDSSw2RkRsRGdCO0VDa0RoQixvRURsRGdCO0VDbURoQixXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0NBQUE7RUFDQSxzQkFBQTtBRjhDSjs7QUUzQ0E7RUFDSSxZQUFBO0FGOENKIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvZm9udHMnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcyc7XG5cbi8vIC5pcy12YWxpZCB7XG4vLyAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZVxuLy8gfVxuXG4uaXMtaW52YWxpZCB7XG4gICAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwM1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogJHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xe1xuICAgIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICAgIGRpc3BsYXk6IGZsZXghaW1wb3J0YW50O1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlciFpbXBvcnRhbnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuXG5cblxuIiwiLmlzLWludmFsaWQge1xuICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDM7XG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDFyZW07XG4gIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTEge1xuICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbmlvbi1ncmlkIHtcbiAgbWF4LXdpZHRoOiAzNTBweDtcbn1cblxuaW9uLXRpdGxlIHtcbiAgY29sb3I6ICMwMDNBNTg7XG59XG5cbi5sb2dpbi1sYWJlbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogbGVmdDtcbiAgYWxpZ24taXRlbXM6IGxlZnQ7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LXNpemU6IDAuNzVyZW07XG4gIHBhZGRpbmctdG9wOiAycmVtO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tTWVkaXVtXCI7XG59XG5cbi5sb2dpbi1pbnB1dCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgcGFkZGluZzogMXJlbSAwIDAgMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgYm9yZGVyLWJvdHRvbTogMC4wMDFyZW0gc29saWQgZ3JheTtcbn1cblxuaW5wdXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgYm9yZGVyOiAwO1xuICBmb250LXNpemU6IDFyZW07XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgcGFkZGluZy1ib3R0b206IDAuNXJlbTtcbn1cblxuLnByaW1hcnktMS1idG4ge1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gIGhlaWdodDogM3JlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuLnByaW1hcnktMi1idG4ge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoLTE4MGRlZywgI0ZGODg0MCAwJSwgI0VFNDAzNiAxMDAlKTtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICBoZWlnaHQ6IDNyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xuICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4ub3BhY2l0eSB7XG4gIG9wYWNpdHk6IDAuNTtcbn0iLCJcbi8vIGNvbG9yc1xuLy8gJHByaW1hcnktY29sb3ItMTogIzAwQTNDODtcbiRwcmltYXJ5LWNvbG9yLTE6ICMwMDNBNTg7XG4kcHJpbWFyeS1jb2xvci0yOiAjRkFBRjQwO1xuJHByaW1hcnktY29sb3ItMzogI0Y0NzczQjtcblxuJHdoaXRlOiAjZmZmZmZmO1xuXG4vLyAkYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsIHJnYmEoMjU1LDI1NSwyNTUsMC41MCkgMCUsIHJnYmEoMCwwLDAsMC41MCkgMTAwJSk7XG4kYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpOyIsIkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy9jb21tb24uc2Nzcyc7XG5AaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MnO1xuXG5pb24tZ3JpZHtcbiAgICBtYXgtd2lkdGg6IDM1MHB4O1xufVxuXG5pb24tdGl0bGUge1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG4ubG9naW4tbGFiZWx7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGxlZnQ7XG4gICAgYWxpZ24taXRlbXM6IGxlZnQ7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgICBwYWRkaW5nLXRvcDogMnJlbTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xufVxuXG4ubG9naW4taW5wdXR7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBwYWRkaW5nOiAxcmVtIDAgMCAwO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJvbGQnO1xuICAgIGJvcmRlci1ib3R0b206IDAuMDAxcmVtIHNvbGlkIGdyYXk7XG59XG5cbmlucHV0e1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlcjogMDtcbiAgICAvLyBib3JkZXItYm90dG9tOiAwLjAwMXJlbSBzb2xpZCB3aGl0ZTtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgcGFkZGluZy1ib3R0b206IDAuNXJlbTtcbn1cblxuLnByaW1hcnktMS1idG4ge1xuICBcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDEuMTI1cmVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICAgIGhlaWdodDogM3JlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgLy8gZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgYmFja2dyb3VuZDogJHByaW1hcnktY29sb3ItMTtcbiAgICAvLyBiYWNrZ3JvdW5kLWltYWdlOiAkYnV0dG9uLWdyYWRpZW50LTE7XG5cbn1cblxuLnByaW1hcnktMi1idG57XG4gICAgYmFja2dyb3VuZC1pbWFnZTogJGJ1dHRvbi1ncmFkaWVudC0xO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5vcGFjaXR5IHtcbiAgICBvcGFjaXR5OiAwLjU7XG59Il19 */");

/***/ }),

/***/ "./src/app/login/forgot-password/forgot-password.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/login/forgot-password/forgot-password.page.ts ***!
  \***************************************************************/
/*! exports provided: ForgotPasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPage", function() { return ForgotPasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_shared_services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/shared/services/user.service */ "./src/shared/services/user.service.ts");
/* harmony import */ var src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/shared/services/toast.service */ "./src/shared/services/toast.service.ts");
/* harmony import */ var src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/shared/services/loader.service */ "./src/shared/services/loader.service.ts");







// import { Keyboard } from '@ionic-native/keyboard'
let ForgotPasswordPage = class ForgotPasswordPage {
    constructor(formBuilder, navParams, modalController, userService, toastr, loader) {
        this.formBuilder = formBuilder;
        this.navParams = navParams;
        this.modalController = modalController;
        this.userService = userService;
        this.toastr = toastr;
        this.loader = loader;
    }
    ngOnInit() {
        this.initializeForm();
        // this.keyboard.disableScroll(true)
        if (this.navParams.get('email')) {
            this.email = this.navParams.get('email');
            console.log("email", this.email);
            this.fc.email.patchValue(this.email);
        }
    }
    initializeForm() {
        this.emailForm = this.formBuilder.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^(([a-zA-Z0-9]+(?!.*[\-\.\_]{2}).*_+)|([a-zA-Z0-9]+(?!.*[\-\.\_]{2}).*\-+)|([a-zA-Z0-9]+(?!.*[\-\.\_]{2}).*\.+)|([a-zA-Z0-9]+(?!.*[\-\.\_]{2}).*\++))*[a-zA-Z0-9]+@[a-zA-Z0-9-]+(\.[a-zA-Z]{2,})+$/)]],
        });
    }
    get fc() { return this.emailForm.controls; }
    dismiss() {
        this.modalController.dismiss({
            'dismissed': true
        });
    }
    confirm() {
        if (this.emailForm.invalid) {
            this.toastr.errorToast("Email is invalid");
            return;
        }
        this.loader.createLoader("Loading...");
        this.email = this.fc.email.value;
        this.userService.forgotPassword(this.email).subscribe(result => {
            console.log("Success", result.message);
            this.toastr.errorToast(result.message);
            this.loader.dismissLoader();
            this.dismiss();
        }, error => {
            console.log("Error", error.error.error);
            this.toastr.errorToast(error.error.error);
            this.loader.dismissLoader();
            this.dismiss();
        });
    }
    doRefresh(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log('Begin async operation');
            this.ngOnInit();
            event.target.complete();
        });
    }
};
ForgotPasswordPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: src_shared_services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"] },
    { type: src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"] }
];
ForgotPasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-forgot-password',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./forgot-password.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/forgot-password/forgot-password.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./forgot-password.page.scss */ "./src/app/login/forgot-password/forgot-password.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        src_shared_services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
        src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"],
        src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]])
], ForgotPasswordPage);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false,
    baseUrl: "https://eu2qaaspmobilecynch.azurewebsites.net",
    // baseUrl: "https://eu2devasp03cynch02.azurewebsites.net",
    services: {
        loginURL: "/api/auth/login",
        changePasswordURL: "/api/auth/change-password",
        forgotPasswordURL: "/api/auth/forgot-password",
        scheduleURL: "/api/user/schedule",
        scheduleDetailURl: "/api/user/schedule-detail",
        scheduleCancelURL: "/api/user/schedule-cancel",
        routeURL: "/api/user/route",
        routeDetailURL: "/api/user/route-detail",
        routeCancelURL: "/api/user/route-cancel",
        deliveryURL: "/api/delivery",
        sapApiURL: "/api/sap-api",
        constantsApiURL: "/api/constants",
        activeRouteURL: "/api/user/active-route",
        userDetailURL: "/api/user-details",
        checkInRouteURL: "/api/user/route/check-in",
        uploadPhotoURL: "/api/upload",
        allSchedulesURL: "/api/driver/schedule",
        userScheduleURL: "/api/driver/user/schedule",
        userBookingURL: "/api/driver/user/booking",
        userBookingDetailsURL: "/api/driver/user/booking-detail"
    },
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ "./src/shared/guards/auth.guard.ts":
/*!*****************************************!*\
  !*** ./src/shared/guards/auth.guard.ts ***!
  \*****************************************/
/*! exports provided: AuthGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuardService", function() { return AuthGuardService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let AuthGuardService = class AuthGuardService {
    constructor(router) {
        this.router = router;
    }
    canActivate(route) {
        console.log(route);
        let authInfo = {
            authenticated: false
        };
        if (localStorage.getItem("current_user")) {
            authInfo.authenticated = true;
        }
        if (!authInfo.authenticated) {
            this.router.navigate(["login"]);
            return false;
        }
        return true;
    }
};
AuthGuardService.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AuthGuardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], AuthGuardService);



/***/ }),

/***/ "./src/shared/interceptors/error.interceptor.ts":
/*!******************************************************!*\
  !*** ./src/shared/interceptors/error.interceptor.ts ***!
  \******************************************************/
/*! exports provided: ErrorInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return ErrorInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user.service */ "./src/shared/services/user.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");






let ErrorInterceptor = class ErrorInterceptor {
    constructor(router, userService) {
        this.router = router;
        this.userService = userService;
    }
    intercept(request, next) {
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(err => {
            // let oldError = JSON.parse(JSON.stringify(err));
            //console.log("HTTP OLD ERROR", oldError);
            if (err.status === 401) {
                this.userService.signOut();
                this.router.navigate(['']);
                return;
            }
            else if (err.status === 500) {
            }
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(err);
        }));
    }
};
ErrorInterceptor.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] }
];
ErrorInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]])
], ErrorInterceptor);



/***/ }),

/***/ "./src/shared/services/loader.service.ts":
/*!***********************************************!*\
  !*** ./src/shared/services/loader.service.ts ***!
  \***********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.isLoggedIn = false;
        this.animationSpeed = 1;
        this.lottieConfig = {
            path: 'assets/images/loading.json',
            renderer: 'canvas',
            autoplay: true,
            loop: true
        };
    }
    createLoader(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loading = yield this.loadingCtrl.create({
                message: message,
                // message: `<img src="/assets/images/loader.gif" width="28" height="28">` + "   " + message ,
                translucent: true,
                cssClass: 'custom-loader',
            });
            this.loading.present();
        });
    }
    dismissLoader() {
        if (this.loading) {
            this.loading.dismiss();
        }
    }
    handleAnimation(anim) {
        this.anim = anim;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
], LoaderService);



/***/ }),

/***/ "./src/shared/services/network.service.ts":
/*!************************************************!*\
  !*** ./src/shared/services/network.service.ts ***!
  \************************************************/
/*! exports provided: NetworkService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NetworkService", function() { return NetworkService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");


// import { Network } from '@ionic-native/network/ngx';

const { Network } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"];
let NetworkService = class NetworkService {
    // public alert: any;
    constructor(
    // private alertController: AlertController,
    // private network: Network
    ) {
    }
    checkConnection() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            // let disconnectSubs = this.network.onDisconnect().subscribe(
            //     () => {
            //         console.log('Network was disconnected.');
            //         window.alert("Network was disconnected");
            //     }
            // )
            let handler = Network.addListener('networkStatusChange', (status) => {
                console.log("Network status changed", status);
                if (!status.connected) {
                    window.alert("Network was disconnected");
                }
            });
            let stat = yield Network.getStatus();
            console.log("status** ", stat);
        });
    }
};
NetworkService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], NetworkService);



/***/ }),

/***/ "./src/shared/services/toast.service.ts":
/*!**********************************************!*\
  !*** ./src/shared/services/toast.service.ts ***!
  \**********************************************/
/*! exports provided: ToastService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastService", function() { return ToastService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let ToastService = class ToastService {
    constructor(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: message,
                cssClass: "toast-scheme",
                duration: 2000,
                position: 'top'
            });
            toast.present();
        });
    }
    errorToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: message,
                cssClass: "toast-error",
                duration: 2000,
                position: 'top'
            });
            toast.present();
        });
    }
};
ToastService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
], ToastService);



/***/ }),

/***/ "./src/shared/services/user.service.ts":
/*!*********************************************!*\
  !*** ./src/shared/services/user.service.ts ***!
  \*********************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let UserService = class UserService {
    constructor(http, router) {
        this.http = http;
        this.router = router;
    }
    signIn(email, password) {
        let user = {
            email: email,
            password: password
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.loginURL, user);
    }
    saveUser(user) {
        localStorage.setItem("current_user", JSON.stringify(user));
    }
    saveToken(token) {
        localStorage.setItem("token", token);
    }
    updateUser(user) {
        return this.http.put(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.userDetailURL, user, this.getHeaders());
    }
    changePassword(oldPassword, newPassword, confirmPassword) {
        let passwords = {
            password: oldPassword,
            newPassword: newPassword,
            retypePassword: confirmPassword
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.changePasswordURL, passwords, this.getHeaders());
    }
    forgotPassword(userEmail) {
        let email = {
            email: userEmail
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.forgotPasswordURL, email, this.getHeaders());
    }
    signOut() {
        localStorage.removeItem("current_delivery");
        localStorage.removeItem("stop_sequence");
        localStorage.removeItem("route_id");
        localStorage.removeItem("stops_length");
        localStorage.removeItem("mileage");
        localStorage.removeItem("current_route_started");
        localStorage.removeItem("material_code");
        localStorage.removeItem("token");
        localStorage.removeItem("current_user");
        localStorage.removeItem("signage");
        localStorage.removeItem("shyft_toggle");
        localStorage.removeItem("start_time");
        localStorage.removeItem("previous_loc");
        localStorage.removeItem("selected-schedule");
        this.router.navigate(["login"]);
    }
    getHeaders() {
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-type': 'application/json'
            })
        };
        return httpOptions;
    }
};
UserService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], UserService);



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/anthony.briones/Documents/CapDriver/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map