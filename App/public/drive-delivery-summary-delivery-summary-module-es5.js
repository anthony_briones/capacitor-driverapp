function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["drive-delivery-summary-delivery-summary-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-summary/delivery-summary.page.html":
  /*!*********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-summary/delivery-summary.page.html ***!
    \*********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDriveDeliverySummaryDeliverySummaryPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <!-- <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons> -->\n    <ion-title>Delivery Summary</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content ion-padding>\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n\n  <!-- Dynamic -->\n  <ion-item lines=\"none\" *ngFor=\"let delivery of deliveries; let i = index\">\n    <ion-row>\n      <ion-col size=\"12\" class=\"delivery-type\">\n        <ion-icon name=\"md-home\"></ion-icon>\n        {{getOrderSource(delivery.orderSource)}}\n      </ion-col>\n      <ion-col size=\"2\" class=\"delivery-card\">\n        <div class=\"delivery-count\">\n          {{i+1}}\n        </div>\n        <div class=\"delivery-total\">\n          of {{deliveries.length}}\n        </div>\n      </ion-col>\n      <ion-col size=\"7\" class=\"delivery-details\">\n        <div class=\"delivery-address\">\n          {{delivery.street}}\n        </div>\n        <div class=\"delivery-address\">\n          {{delivery.city}}, {{delivery.state}} {{delivery.zipcode}}\n        </div>\n      </ion-col>\n\n      <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n        <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n        <ion-text>{{getTotalQuantity(delivery.items)}}</ion-text>\n      </ion-col>\n\n    </ion-row>\n  </ion-item>\n\n  <!-- Skeleton -->\n  <ng-container *ngIf=\"showDeliveries\">\n    <ion-item lines=\"none\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"delivery-type\">\n          <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"2\" class=\"\">\n          <ion-skeleton-text class=\"\" animated class=\"h-100\"></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"7\" class=\"delivery-details\">\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n        </ion-col>\n      </ion-row>\n\n    </ion-item>\n\n    <ion-item lines=\"none\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"delivery-type\">\n          <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"2\" class=\"\">\n          <ion-skeleton-text class=\"\" animated class=\"h-100\"></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"7\" class=\"delivery-details\">\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n        </ion-col>\n      </ion-row>\n\n    </ion-item>\n\n    <ion-item lines=\"none\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"delivery-type\">\n          <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"2\" class=\"\">\n          <ion-skeleton-text class=\"\" animated class=\"h-100\"></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"7\" class=\"delivery-details\">\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n        </ion-col>\n      </ion-row>\n\n    </ion-item>\n\n  </ng-container>\n\n  <ion-row>\n    <ion-col size=\"6\" class=\"ion-padding-start vertical-align\">\n      <button class=\"navigate-btn vertical-align horizontal-align\" (click)=\"navigate()\">\n        <ion-icon src=\"assets/icon/icon-navigation-blue.svg\"></ion-icon> Return to Depot\n      </button>\n    </ion-col>\n    <ion-col size=\"6\" class=\"ion-padding-end vertical-align\">\n      <button class=\"end-route-btn\" (click)=\"checkGPSPermission()\">End Route</button>\n    </ion-col>\n  </ion-row>\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/drive/delivery-summary/delivery-summary.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/drive/delivery-summary/delivery-summary.module.ts ***!
    \*******************************************************************/

  /*! exports provided: DeliverySummaryPageModule */

  /***/
  function srcAppDriveDeliverySummaryDeliverySummaryModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeliverySummaryPageModule", function () {
      return DeliverySummaryPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _delivery_summary_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./delivery-summary.page */
    "./src/app/drive/delivery-summary/delivery-summary.page.ts");

    var routes = [{
      path: '',
      component: _delivery_summary_page__WEBPACK_IMPORTED_MODULE_6__["DeliverySummaryPage"]
    }];

    var DeliverySummaryPageModule = function DeliverySummaryPageModule() {
      _classCallCheck(this, DeliverySummaryPageModule);
    };

    DeliverySummaryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_delivery_summary_page__WEBPACK_IMPORTED_MODULE_6__["DeliverySummaryPage"]]
    })], DeliverySummaryPageModule);
    /***/
  },

  /***/
  "./src/app/drive/delivery-summary/delivery-summary.page.scss":
  /*!*******************************************************************!*\
    !*** ./src/app/drive/delivery-summary/delivery-summary.page.scss ***!
    \*******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppDriveDeliverySummaryDeliverySummaryPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-title {\n  color: #003A58;\n}\n\nion-item {\n  margin: 1rem 0;\n}\n\n.delivery-type {\n  padding-top: 1rem;\n  padding-bottom: 1rem;\n  font-family: \"Montserrat-Bold\";\n  font-size: 0.75rem;\n  color: #003A58;\n  letter-spacing: 0;\n}\n\n.delivery-skeleton {\n  width: 10rem;\n}\n\n.delivery-card {\n  border: 1px solid #003A58;\n  border-radius: 4px;\n  padding: 0 10px;\n}\n\n.delivery-card .delivery-count {\n  font-family: \"Montserrat-Bold\";\n  font-size: 2.5rem;\n  color: #003A58;\n  letter-spacing: 0;\n  text-align: center;\n}\n\n.delivery-card .delivery-total {\n  font-family: \"Montserrat-Regular\";\n  font-size: 1rem;\n  color: #003A58;\n  letter-spacing: 0;\n  text-align: center;\n}\n\n.delivery-details {\n  padding-left: 1rem;\n  padding-right: 1rem;\n}\n\n.delivery-details .delivery-name {\n  font-family: \"Roboto-Bold\";\n  font-size: 1rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.5rem;\n}\n\n.delivery-details .delivery-address {\n  font-family: \"Roboto-Regular\";\n  font-size: 0.875rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.3125rem;\n}\n\n.delivery-tanks ion-img {\n  position: relative;\n  display: inline-block;\n}\n\n.delivery-tanks ion-text {\n  position: absolute;\n  text-align: center;\n  font-family: \"Montserrat-Bold\";\n  font-size: 1.5rem;\n  color: #003A58;\n  letter-spacing: 0;\n}\n\n.button-padding {\n  padding: 0 1rem;\n}\n\n.end-route-btn {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-weight: bolder;\n  background: #003A58;\n  font-family: \"Montserrat-Bold\";\n}\n\n.navigate-btn {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: #003A58;\n  border: solid 1px #003A58;\n  font-family: \"Montserrat-Bold\";\n  background: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL2RyaXZlL2RlbGl2ZXJ5LXN1bW1hcnkvZGVsaXZlcnktc3VtbWFyeS5wYWdlLnNjc3MiLCIvVXNlcnMvYW50aG9ueS5icmlvbmVzL0RvY3VtZW50cy9DYXBEcml2ZXIvc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hcHAvZHJpdmUvZGVsaXZlcnktc3VtbWFyeS9kZWxpdmVyeS1zdW1tYXJ5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFPQTtFQUNJLHlCQUFBO0VBQ0EsZ0RBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkFBQTtBQ05KOztBRFNBO0VBQ0kseUJFbEJjO0VGbUJkLFdBQUE7RUFDQSxjRWhCSTtFRmlCSixtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtBQ05KOztBRFNBO0VBQ0ksK0JBQUE7RUFBQSx3QkFBQTtFQUNBLGdDQUFBO0VBQ0Esb0NBQUE7VUFBQSw4QkFBQTtBQ05KOztBRFNBO0VBQ0ksbUNBQUE7VUFBQSxrQ0FBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7VUFBQSx5QkFBQTtBQ05KOztBRFNBO0VBQ0ksd0JBQUE7VUFBQSx1QkFBQTtBQ05KOztBRFNBO0VBQ0ksWUFBQTtBQ05KOztBRWhEQTtFQUNJLGNERGM7QURvRGxCOztBRWhEQTtFQUNJLGNBQUE7QUZtREo7O0FFaERBO0VBQ0ksaUJBQUE7RUFDQSxvQkFBQTtFQUNBLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjRGJjO0VDY2QsaUJBQUE7QUZtREo7O0FFaERBO0VBQ0ksWUFBQTtBRm1ESjs7QUVoREE7RUFFSSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBRmtESjs7QUVoREk7RUFDSSw4QkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0Q5QlU7RUMrQlYsaUJBQUE7RUFDQSxrQkFBQTtBRmtEUjs7QUUvQ0k7RUFDSSxpQ0FBQTtFQUNBLGVBQUE7RUFDQSxjRHRDVTtFQ3VDVixpQkFBQTtFQUNBLGtCQUFBO0FGaURSOztBRTdDQTtFQUVJLGtCQUFBO0VBQ0EsbUJBQUE7QUYrQ0o7O0FFN0NJO0VBQ0ksMEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUYrQ1I7O0FFNUNJO0VBQ0ksNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0FGOENSOztBRXpDSTtFQUVJLGtCQUFBO0VBQ0EscUJBQUE7QUYyQ1I7O0FFeENJO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLDhCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjRDlFVTtFQytFVixpQkFBQTtBRjBDUjs7QUVyQ0E7RUFDSSxlQUFBO0FGd0NKOztBRXBDQTtFQUVJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLG1CRGpHYztFQ2tHZCw4QkFBQTtBRnNDSjs7QUVsQ0E7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxjRDNHYztFQzRHZCx5QkFBQTtFQUNBLDhCQUFBO0VBQ0EsaUJBQUE7QUZxQ0oiLCJmaWxlIjoic3JjL2FwcC9kcml2ZS9kZWxpdmVyeS1zdW1tYXJ5L2RlbGl2ZXJ5LXN1bW1hcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2ZvbnRzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMnO1xuXG4vLyAuaXMtdmFsaWQge1xuLy8gICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWVcbi8vIH1cblxuLmlzLWludmFsaWQge1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNFRTQwMzY7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDNcbn1cblxuLmlzLWludmFsaWQtc2VsZWN0IHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6ICR3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgaGVpZ2h0OiAycmVtO1xufVxuXG4uYmctY29sb3ItMXtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgICBkaXNwbGF5OiBmbGV4IWltcG9ydGFudDtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXIhaW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5mbGV4LWFsaWduLWNlbnRlciB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgICBvcGFjaXR5OiAwLjU7XG59XG5cblxuXG5cbiIsIi5pcy1pbnZhbGlkIHtcbiAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICBib3JkZXI6IDFweCBzb2xpZCAjNUVCMDAzO1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWFsaWduLXJpZ2h0IHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgb3BhY2l0eTogMC41O1xufVxuXG5pb24tdGl0bGUge1xuICBjb2xvcjogIzAwM0E1ODtcbn1cblxuaW9uLWl0ZW0ge1xuICBtYXJnaW46IDFyZW0gMDtcbn1cblxuLmRlbGl2ZXJ5LXR5cGUge1xuICBwYWRkaW5nLXRvcDogMXJlbTtcbiAgcGFkZGluZy1ib3R0b206IDFyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBmb250LXNpemU6IDAuNzVyZW07XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBsZXR0ZXItc3BhY2luZzogMDtcbn1cblxuLmRlbGl2ZXJ5LXNrZWxldG9uIHtcbiAgd2lkdGg6IDEwcmVtO1xufVxuXG4uZGVsaXZlcnktY2FyZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDNBNTg7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgcGFkZGluZzogMCAxMHB4O1xufVxuLmRlbGl2ZXJ5LWNhcmQgLmRlbGl2ZXJ5LWNvdW50IHtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGZvbnQtc2l6ZTogMi41cmVtO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5kZWxpdmVyeS1jYXJkIC5kZWxpdmVyeS10b3RhbCB7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtUmVndWxhclwiO1xuICBmb250LXNpemU6IDFyZW07XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uZGVsaXZlcnktZGV0YWlscyB7XG4gIHBhZGRpbmctbGVmdDogMXJlbTtcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcbn1cbi5kZWxpdmVyeS1kZXRhaWxzIC5kZWxpdmVyeS1uYW1lIHtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBjb2xvcjogIzQyNDI0MjtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjVyZW07XG59XG4uZGVsaXZlcnktZGV0YWlscyAuZGVsaXZlcnktYWRkcmVzcyB7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1SZWd1bGFyXCI7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGNvbG9yOiAjNDI0MjQyO1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLmRlbGl2ZXJ5LXRhbmtzIGlvbi1pbWcge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cbi5kZWxpdmVyeS10YW5rcyBpb24tdGV4dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBsZXR0ZXItc3BhY2luZzogMDtcbn1cblxuLmJ1dHRvbi1wYWRkaW5nIHtcbiAgcGFkZGluZzogMCAxcmVtO1xufVxuXG4uZW5kLXJvdXRlLWJ0biB7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXNpemU6IDEuMTI1cmVtO1xuICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgaGVpZ2h0OiAzcmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIGJhY2tncm91bmQ6ICMwMDNBNTg7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xufVxuXG4ubmF2aWdhdGUtYnRuIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICBoZWlnaHQ6IDNyZW07XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBib3JkZXI6IHNvbGlkIDFweCAjMDAzQTU4O1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59IiwiXG4vLyBjb2xvcnNcbi8vICRwcmltYXJ5LWNvbG9yLTE6ICMwMEEzQzg7XG4kcHJpbWFyeS1jb2xvci0xOiAjMDAzQTU4O1xuJHByaW1hcnktY29sb3ItMjogI0ZBQUY0MDtcbiRwcmltYXJ5LWNvbG9yLTM6ICNGNDc3M0I7XG5cbiR3aGl0ZTogI2ZmZmZmZjtcblxuLy8gJGJ1dHRvbi1ncmFkaWVudC0xOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCByZ2JhKDI1NSwyNTUsMjU1LDAuNTApIDAlLCByZ2JhKDAsMCwwLDAuNTApIDEwMCUpO1xuJGJ1dHRvbi1ncmFkaWVudC0xOiBsaW5lYXItZ3JhZGllbnQoLTE4MGRlZywgI0ZGODg0MCAwJSwgI0VFNDAzNiAxMDAlKTsiLCJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzJztcblxuaW9uLXRpdGxle1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG5pb24taXRlbSB7XG4gICAgbWFyZ2luOiAxcmVtIDA7XG59XG5cbi5kZWxpdmVyeS10eXBlIHtcbiAgICBwYWRkaW5nLXRvcDogMXJlbTtcbiAgICBwYWRkaW5nLWJvdHRvbTogMXJlbTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgZm9udC1zaXplOiAwLjc1cmVtO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xufVxuXG4uZGVsaXZlcnktc2tlbGV0b257XG4gICAgd2lkdGg6IDEwcmVtO1xufVxuXG4uZGVsaXZlcnktY2FyZCB7XG5cbiAgICBib3JkZXI6IDFweCBzb2xpZCAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBwYWRkaW5nOiAwIDEwcHg7XG5cbiAgICAuZGVsaXZlcnktY291bnR7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICAgICAgZm9udC1zaXplOiAyLjVyZW07XG4gICAgICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuICAgIC5kZWxpdmVyeS10b3RhbCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1SZWd1bGFyJztcbiAgICAgICAgZm9udC1zaXplOiAxcmVtO1xuICAgICAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG59XG5cbi5kZWxpdmVyeS1kZXRhaWxzIHtcblxuICAgIHBhZGRpbmctbGVmdDogMXJlbTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xuICAgIFxuICAgIC5kZWxpdmVyeS1uYW1lIHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgY29sb3I6ICM0MjQyNDI7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgICAgICBsaW5lLWhlaWdodDogMS41cmVtO1xuICAgIH1cblxuICAgIC5kZWxpdmVyeS1hZGRyZXNzIHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tUmVndWxhcic7XG4gICAgICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgICAgIGNvbG9yOiAjNDI0MjQyO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbiAgICB9XG59XG5cbi5kZWxpdmVyeS10YW5rcyB7XG4gICAgaW9uLWltZyB7XG4gICAgICAgIC8vIHdpZHRoOiA1MCU7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cblxuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XG4gICAgICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICB9XG59XG5cblxuLmJ1dHRvbi1wYWRkaW5ne1xuICAgIHBhZGRpbmc6IDAgMXJlbTs7XG59XG5cblxuLmVuZC1yb3V0ZS1idG57XG5cbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDEuMTI1cmVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICAgIGhlaWdodDogM3JlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBiYWNrZ3JvdW5kOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICAvLyBiYWNrZ3JvdW5kLWltYWdlOiAkYnV0dG9uLWdyYWRpZW50LTE7XG59XG5cbi5uYXZpZ2F0ZS1idG4ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGJvcmRlcjogc29saWQgMXB4ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/drive/delivery-summary/delivery-summary.page.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/drive/delivery-summary/delivery-summary.page.ts ***!
    \*****************************************************************/

  /*! exports provided: DeliverySummaryPage */

  /***/
  function srcAppDriveDeliverySummaryDeliverySummaryPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeliverySummaryPage", function () {
      return DeliverySummaryPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/shared/services/routing.service */
    "./src/shared/services/routing.service.ts");
    /* harmony import */


    var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/launch-navigator/ngx */
    "./node_modules/@ionic-native/launch-navigator/ngx/index.js");
    /* harmony import */


    var src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/shared/services/sap-api.service */
    "./src/shared/services/sap-api.service.ts");
    /* harmony import */


    var src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/shared/services/loader.service */
    "./src/shared/services/loader.service.ts");
    /* harmony import */


    var _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../shared/services/lauch-navigator.service */
    "./src/shared/services/lauch-navigator.service.ts");
    /* harmony import */


    var src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/shared/services/toast.service */
    "./src/shared/services/toast.service.ts");
    /* harmony import */


    var _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/location-accuracy/ngx */
    "./node_modules/@ionic-native/location-accuracy/ngx/index.js");
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/ngx/index.js");
    /* harmony import */


    var _capacitor_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @capacitor/core */
    "./node_modules/@capacitor/core/dist/esm/index.js"); // import { Geolocation } from '@ionic-native/geolocation/ngx';


    var Geolocation = _capacitor_core__WEBPACK_IMPORTED_MODULE_11__["Plugins"].Geolocation;

    var DeliverySummaryPage =
    /*#__PURE__*/
    function () {
      function DeliverySummaryPage(route, router, routingService, // private geolocation: Geolocation,
      launchNavigator, sapApiService, loaderService, launchNavService, toastService, locationAccuracy, androidPermissions // private spherical: Spherical
      ) {
        _classCallCheck(this, DeliverySummaryPage);

        this.route = route;
        this.router = router;
        this.routingService = routingService;
        this.launchNavigator = launchNavigator;
        this.sapApiService = sapApiService;
        this.loaderService = loaderService;
        this.launchNavService = launchNavService;
        this.toastService = toastService;
        this.locationAccuracy = locationAccuracy;
        this.androidPermissions = androidPermissions;
        this.deliveries = [];
        this.showDeliveries = true;
        this.routeId = this.route.snapshot.paramMap.get('id');
      }

      _createClass(DeliverySummaryPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          console.log(this.routeId);
          this.getRoutes();
          this.currentUser = JSON.parse(localStorage.getItem("current_user"));
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    this.loaderService.createLoader("Computing Mileage...");
                    _context.next = 3;
                    return this.computeMileage();

                  case 3:
                    if (!localStorage.getItem("route_id")) {
                      this.router.navigate(["/tabs/drive"]);
                    }

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "getRoutes",
        value: function getRoutes() {
          var _this = this;

          this.routingService.getUserRouteDetails(this.routeId).subscribe(function (result) {
            console.log("Done Results: ", result);
            _this.showDeliveries = false;
            _this.deliveries = result.deliveries;
          }, function (error) {
            console.log("Error", error.message);
          });
        }
      }, {
        key: "getTotalQuantity",
        value: function getTotalQuantity(items) {
          var total = 0;
          items.forEach(function (item) {
            total += item.quantityDelivered;
          });
          return total;
        }
      }, {
        key: "getOrderSource",
        value: function getOrderSource(orderSource) {
          orderSource = orderSource.toUpperCase();

          switch (orderSource) {
            case "CYNCH":
              return "RESIDENTIAL DELIVERY";

            case "ACE":
              return "COMMERCIAL DELIVERY";

            default:
              return "RESIDENTIAL DELIVERY";
          }
        }
      }, {
        key: "computeMileage",
        value: function computeMileage() {
          var _this2 = this;

          this.getLocation().then(function (result) {
            var previousLoc = JSON.parse(localStorage.getItem("previous_loc"));
            console.log("previous Lococation", previousLoc);
            var depotLatitude;
            var depotLongitude;

            if (result) {
              var directionsService = new google.maps.DirectionsService();
              var directionsRenderer = new google.maps.DirectionsRenderer();
              var _iteratorNormalCompletion = true;
              var _didIteratorError = false;
              var _iteratorError = undefined;

              try {
                for (var _iterator = _this2.currentUser.msa.plants[0].storages[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                  var storage = _step.value;

                  if (storage.isCylinderLoc) {
                    depotLatitude = storage.latitude;
                    depotLongitude = storage.longitude;
                  }
                }
              } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
              } finally {
                try {
                  if (!_iteratorNormalCompletion && _iterator.return != null) {
                    _iterator.return();
                  }
                } finally {
                  if (_didIteratorError) {
                    throw _iteratorError;
                  }
                }
              }

              var route = {
                origin: {
                  lat: previousLoc.latitude,
                  lng: previousLoc.longitude
                },
                destination: {
                  lat: depotLatitude,
                  lng: depotLongitude
                },
                travelMode: 'DRIVING'
              };
              directionsService.route(route, function (response, status) {
                if (status !== 'OK') {
                  window.alert('Directions request failed due to ' + status);
                  return;
                } else {
                  // directionsRenderer.setDirections(response);
                  var directionsData = response.routes[0].legs[0];
                  var mileage;

                  if (!directionsData) {
                    window.alert('Directions request failed');
                    return;
                  } else {
                    var data = directionsData.distance.text.split(" ");

                    if (data[1] === "ft") {
                      mileage = parseFloat((parseFloat(data[0]) / 5280).toFixed(4));
                    } else {
                      mileage = data[0];
                    }

                    this.mileage = parseFloat(mileage);
                    console.log("Data", directionsData);
                  }
                }

                this.loaderService.dismissLoader();
              }.bind(_this2));
            }
          });
        }
      }, {
        key: "deliveryDone",
        value: function deliveryDone() {
          var _this3 = this;

          var depotLatitude;
          var depotLongitude;
          var distance = 0;
          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = this.currentUser.msa.plants[0].storages[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var storage = _step2.value;

              if (storage.isCylinderLoc) {
                depotLatitude = storage.latitude;
                depotLongitude = storage.longitude;
              }
            } // this.askToTurnOnGPS();
            // depotLatitude = 14.656804; 
            // depotLongitude = 121.056531;

          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                _iterator2.return();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }

          this.getLocation().then(function (result) {
            if (result) {
              distance = _this3.checkDistance(_this3.latitude, _this3.longitude, depotLatitude, depotLongitude);
              console.log("Distance*****: ", distance);

              if (distance <= 1000) {
                // this.loaderService.createLoader("Ending Route...");
                var endTime = _this3.sapApiService.getConfirmationDateTime();

                console.log(_this3.routeId);

                _this3.sapApiService.submitRoute(parseInt(_this3.routeId), endTime, _this3.mileage).subscribe(function (result) {
                  console.log(result);

                  _this3.clearFinishedDelivery();

                  _this3.loaderService.dismissLoader();

                  _this3.router.navigate(["/tabs/routes"]);
                }, function (error) {
                  console.log("Error", error);

                  _this3.loaderService.dismissLoader();
                });
              } else {
                _this3.loaderService.dismissLoader();

                _this3.toastService.presentToast("You have to be in the depot to end route!");
              }
            }
          }); // ---------------------------------
          // let endTime = this.sapApiService.getConfirmationDateTime();
          // console.log(this.routeId);
          // this.sapApiService.submitRoute(parseInt(this.routeId), endTime, this.mileage ).subscribe(
          //   result => {
          //     console.log(result);
          //     this.clearFinishedDelivery();
          //     this.loaderService.dismissLoader();
          //     this.router.navigate([`/tabs/routes`]);
          //   },
          //   error => {
          //     console.log("Error", error);
          //     this.loaderService.dismissLoader();
          //   }
          // )
          // this.router.navigate([`/tabs/routes`]);
        }
      }, {
        key: "getLocation",
        value: function getLocation() {
          return new Promise(function (resolve, reject) {
            var _this4 = this;

            Geolocation.getCurrentPosition().then(function (resp) {
              _this4.latitude = resp.coords.latitude;
              _this4.longitude = resp.coords.longitude;
              console.log("Current Location", _this4.latitude);
              console.log("Current Location", _this4.longitude);
              resolve(true);
            }).catch(function (error) {
              if (error.code == 1) {
                _this4.toastService.presentToast("Please enable location services to continue");
              } else if (error.code == 2) {
                _this4.toastService.presentToast("Location Unavailable");
              } else if (error.code == 3) {
                _this4.toastService.presentToast("Request Timeout");
              }

              console.log('Error getting location', error.code);
              resolve(false);
            });
          }.bind(this));
        }
      }, {
        key: "navigate",
        value: function navigate() {
          var _this5 = this;

          this.getLocation().then(function (result) {
            if (result) {
              _this5.launchNavService.checkExistingSelectedMap().then(function (result) {
                if (result) {
                  _this5.launchNavService.getUserSelectedMap().then(function (preferredMap) {
                    console.log("Selected", preferredMap);

                    if (preferredMap === "this.launchNavigator.APP.APPLE_MAPS") {
                      var options = {
                        start: "".concat(_this5.latitude, ", ").concat(_this5.longitude),
                        app: _this5.launchNavigator.APP.APPLE_MAPS
                      };

                      _this5.launchNavigatorMapApp(options);
                    } else if (preferredMap === "this.launchNavigator.APP.WAZE") {
                      var _options = {
                        start: "".concat(_this5.latitude, ", ").concat(_this5.longitude),
                        app: _this5.launchNavigator.APP.WAZE
                      };

                      _this5.launchNavigatorMapApp(_options);
                    } else if (preferredMap === "this.launchNavigator.APP.GOOGLE_MAPS") {
                      var _options2 = {
                        start: "".concat(_this5.latitude, ", ").concat(_this5.longitude),
                        app: _this5.launchNavigator.APP.GOOGLE_MAPS
                      };

                      _this5.launchNavigatorMapApp(_options2);
                    } else {
                      var _options3 = {
                        start: "".concat(_this5.latitude, ", ").concat(_this5.longitude)
                      };

                      _this5.launchNavigatorMapApp(_options3);
                    }
                  });
                } else {
                  var options = {
                    start: "".concat(_this5.latitude, ", ").concat(_this5.longitude),
                    appSelection: {
                      rememberChoice: {
                        enabled: true
                      }
                    }
                  };

                  _this5.launchNavigatorMapApp(options);

                  console.log("None selected");
                }
              });
            }
          });
        }
      }, {
        key: "launchNavigatorMapApp",
        value: function launchNavigatorMapApp(options) {
          var depotLatitude;
          var depotLongitude;
          var _iteratorNormalCompletion3 = true;
          var _didIteratorError3 = false;
          var _iteratorError3 = undefined;

          try {
            for (var _iterator3 = this.currentUser.msa.plants[0].storages[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
              var storage = _step3.value;

              if (storage.isCylinderLoc) {
                depotLatitude = storage.latitude;
                depotLongitude = storage.longitude;
              }
            }
          } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                _iterator3.return();
              }
            } finally {
              if (_didIteratorError3) {
                throw _iteratorError3;
              }
            }
          }

          this.launchNavigator.navigate("".concat(depotLatitude, ", ").concat(depotLongitude), options).then(function (success) {
            console.log("Success Return");
          }, function (error) {
            console.log("Error", error);
          });
        }
      }, {
        key: "clearFinishedDelivery",
        value: function clearFinishedDelivery() {
          localStorage.removeItem("current_delivery");
          localStorage.removeItem("stop_sequence");
          localStorage.removeItem("route_id");
          localStorage.removeItem("stops_length");
          localStorage.removeItem("mileage");
          localStorage.removeItem("current_route_started");
          localStorage.removeItem("material_code");
          localStorage.removeItem("start_time");
          localStorage.removeItem("signage");
        }
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ionViewDidEnter();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "checkDistance",
        value: function checkDistance(fromLat, fromLng, toLat, toLng) {
          var fromCord = new google.maps.LatLng(fromLat, fromLng);
          var toCord = new google.maps.LatLng(toLat, toLng);
          console.log("from", fromCord);
          console.log("to", toCord);
          var computedDistance = (google.maps.geometry.spherical.computeDistanceBetween(fromCord, toCord) / 1000).toFixed(2);
          console.log("****Computed Distance", computedDistance); // return Spherical.computeDistanceBetween(fromCord, toCord);
          // return google.maps.geometry.spherical.computeDistanceBetween(fromCord, toCord);

          return parseInt(computedDistance);
        }
      }, {
        key: "checkGPSPermission",
        value: function checkGPSPermission() {
          var _this6 = this;

          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function (result) {
            if (result.hasPermission) {
              //If hvaing permission show 'Turn on GPS' dialogue
              // this.toastService.presentToast("Has Permission");
              _this6.askToTurnOnGPS();
            } else {
              //If not having permission ask for permission
              // this.toastService.presentToast("not having permission");
              _this6.requestGPSPermissions();
            }
          }, function (err) {
            alert(err);
          });
        }
      }, {
        key: "requestGPSPermissions",
        value: function requestGPSPermissions() {
          var _this7 = this;

          this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
              console.log("4"); // this.toastService.presentToast("4");
            } else {
              _this7.androidPermissions.requestPermission(_this7.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function () {
                // call method to turn on GPS
                _this7.askToTurnOnGPS();
              }, function (error) {
                // this.toastService.presentToast("Please enable location services to continue");
                console.log("Request permission error" + error); // alert('request permission Error requesting location permission' + error)
              });
            }
          });
        }
      }, {
        key: "askToTurnOnGPS",
        value: function askToTurnOnGPS() {
          var _this8 = this;

          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
            _this8.loaderService.createLoader("Ending Route...");

            _this8.deliveryDone();
          }, function (error) {
            _this8.toastService.presentToast("Please enable location services to continue");

            console.log('Error requesting location permissions ' + JSON.stringify(error));
          });
        }
      }]);

      return DeliverySummaryPage;
    }();

    DeliverySummaryPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_3__["RoutingService"]
      }, {
        type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_4__["LaunchNavigator"]
      }, {
        type: src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_5__["SapApiService"]
      }, {
        type: src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]
      }, {
        type: _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_7__["LaunchNavigatorService"]
      }, {
        type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]
      }, {
        type: _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_9__["LocationAccuracy"]
      }, {
        type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_10__["AndroidPermissions"]
      }];
    };

    DeliverySummaryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-delivery-summary',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./delivery-summary.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-summary/delivery-summary.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./delivery-summary.page.scss */
      "./src/app/drive/delivery-summary/delivery-summary.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_3__["RoutingService"], _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_4__["LaunchNavigator"], src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_5__["SapApiService"], src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"], _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_7__["LaunchNavigatorService"], src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"], _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_9__["LocationAccuracy"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_10__["AndroidPermissions"] // private spherical: Spherical
    ])], DeliverySummaryPage);
    /***/
  },

  /***/
  "./src/shared/services/routing.service.ts":
  /*!************************************************!*\
    !*** ./src/shared/services/routing.service.ts ***!
    \************************************************/

  /*! exports provided: RoutingService */

  /***/
  function srcSharedServicesRoutingServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RoutingService", function () {
      return RoutingService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var RoutingService =
    /*#__PURE__*/
    function () {
      function RoutingService(http) {
        _classCallCheck(this, RoutingService);

        this.http = http;
      }

      _createClass(RoutingService, [{
        key: "activeRoute",
        value: function activeRoute() {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.activeRouteURL, this.getHeaders());
        } // getUserRoute() {
        //     return this.http.get<Route[]>(environment.baseUrl + environment.services.bookingURL + `?mode=user`, this.getHeaders());
        // }

      }, {
        key: "routeCheckIn",
        value: function routeCheckIn(routeId, checkInTime) {
          var checkIn = {
            routeId: routeId,
            checkInTime: checkInTime
          };
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.checkInRouteURL, checkIn, this.getHeaders());
        }
      }, {
        key: "getModeUserRoute",
        value: function getModeUserRoute() {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeURL + "?mode=user", this.getHeaders());
        }
      }, {
        key: "getUserRoute",
        value: function getUserRoute() {
          var apiMode = {
            mode: "routeShell"
          };
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.sapApiURL, apiMode, this.getHeaders());
        }
      }, {
        key: "getUserRouteDetails",
        value: function getUserRouteDetails(routeId) {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeDetailURL + "?routeId=".concat(routeId), this.getHeaders());
        }
      }, {
        key: "cancelRoute",
        value: function cancelRoute(routeId) {
          var body = {
            routeId: routeId
          };
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeCancelURL, body, this.getHeaders());
        }
      }, {
        key: "getHeaders",
        value: function getHeaders() {
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
              'Authorization': 'Bearer ' + localStorage.getItem('token'),
              'Content-type': 'application/json'
            })
          };
          return httpOptions;
        }
      }]);

      return RoutingService;
    }();

    RoutingService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }];
    };

    RoutingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])], RoutingService);
    /***/
  }
}]);
//# sourceMappingURL=drive-delivery-summary-delivery-summary-module-es5.js.map