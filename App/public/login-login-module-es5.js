function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
  /*!*****************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
    \*****************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>Login</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content class=\"bg-color-1\">\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <form [formGroup]=\"signInForm\" (ngSubmit)=\"signIn()\">\n    <ion-grid>\n      <ion-row>\n        <div class=\"login-img\">\n          <img src=\"./assets/images/Cynch-logo-white.svg\">\n        </div>\n      </ion-row>\n\n\n      <ion-row>\n\n        <ion-col size=\"12\">\n          <div class=\"login-label ion-text-uppercase\">\n            <label for=\"email\">\n              Email Address\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"login-input\">\n          <input type=\"text\" [(ngModel)]=\"email\" formControlName=\"email\" id=\"email\" [ngClass]=\"{ 'is-invalid': (fc.email.dirty || fc.email.touched || submitted) && fc.email.errors, 'is-valid': (fc.email.dirty || fc.email.touched) && !fc.email.errors }\">\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col size=\"12\" class=\"login-label ion-text-uppercase\">\n          <label for=\"password\">\n            Password\n          </label>\n        </ion-col>\n        <ion-col size=\"12\" class=\"login-input\">\n          <input *ngIf=\"!showPw\" type=\"password\" [(ngModel)]=\"password\" formControlName=\"password\" id=\"password\"\n            [ngClass]=\"{ 'is-invalid': (fc.password.dirty || fc.password.touched || submitted) && fc.password.errors, 'is-valid': (fc.password.dirty || fc.password.touched) && !fc.password.errors }\">\n          <input *ngIf=\"showPw\" type=\"text\" [(ngModel)]=\"password\" formControlName=\"password\" id=\"password\" [ngClass]=\"{ 'is-invalid': (fc.password.dirty || fc.password.touched || submitted) && fc.password.errors, 'is-valid': (fc.password.dirty || fc.password.touched) && !fc.password.errors }\">\n          <label class=\"show-input\">\n            <button ion-button clear color=\"dark\" type=\"button\" class=\"eye-btn\" (click)=\"showPw = !showPw\" item-right>\n              <ion-icon name=\"md-eye\"></ion-icon>\n            </button>\n          </label>\n        </ion-col>\n        <!-- <ion-col size=\"1\" class=\"show-input\"> \n          <button ion-button clear color=\"dark\" type=\"button\" class=\"eye-btn\"\n            (click)=\"showPw = !showPw\" item-right>\n            <ion-icon name=\"md-eye\"></ion-icon>\n          </button>\n        </ion-col> -->\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col size=\"12\" class=\"login-button shadow\">\n          <button class=\"btn shadow-lg login-btn\" [disabled]=\"!email || !password\" icon-only round ion-button type=\"submit\">\n            Log In\n          </button>\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <div class=\"login-forgotpw\">\n            <span (click)=\"openModal(email)\">Forgot Password?</span>\n          </div>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n  </form>\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/login/login.module.ts":
  /*!***************************************!*\
    !*** ./src/app/login/login.module.ts ***!
    \***************************************/

  /*! exports provided: LoginPageModule */

  /***/
  function srcAppLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
      return LoginPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./login.page */
    "./src/app/login/login.page.ts");

    var routes = [{
      path: '',
      component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }];

    var LoginPageModule = function LoginPageModule() {
      _classCallCheck(this, LoginPageModule);
    };

    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })], LoginPageModule);
    /***/
  },

  /***/
  "./src/app/login/login.page.scss":
  /*!***************************************!*\
    !*** ./src/app/login/login.page.scss ***!
    \***************************************/

  /*! exports provided: default */

  /***/
  function srcAppLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\n.login-content {\n  --background: #003A58 !important;\n}\n\nion-grid {\n  max-width: 90vw;\n}\n\n.login-label {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: left;\n          justify-content: left;\n  -webkit-box-align: left;\n          align-items: left;\n  height: 100%;\n  width: 100%;\n  color: white;\n  font-size: 0.75rem;\n  padding-top: 2rem;\n  font-family: \"Roboto-Medium\";\n}\n\n.login-input {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  height: 100%;\n  width: 100%;\n  color: white;\n  padding: 1rem 0 0 0;\n  font-family: \"Roboto-Bold\";\n  border-bottom: 0.001rem solid white;\n}\n\n.show-input {\n  float: right;\n  position: absolute;\n  z-index: 2;\n  padding: 0;\n  right: 1vw;\n}\n\ninput {\n  background-color: transparent;\n  border: 0;\n  font-size: 1rem;\n  width: 100%;\n  font-weight: bold;\n  padding-bottom: 0.5rem;\n}\n\ninput[type=password] {\n  -webkit-text-stroke-width: 0.18rem;\n  letter-spacing: 0.18rem;\n}\n\n.login-button {\n  padding-top: 1rem;\n}\n\n.login-img {\n  -webkit-box-align: center;\n          align-items: center;\n  margin: 50px auto 20px auto;\n}\n\nimg {\n  width: 100%;\n}\n\n.eye-btn {\n  background-color: transparent;\n  color: white;\n  font-size: 1rem;\n}\n\n.login-btn {\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036));\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%);\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-SemiBold\";\n  line-height: 1.3125rem;\n}\n\nbutton:disabled {\n  opacity: 0.5;\n}\n\nspan {\n  color: white;\n  font-size: 0.75rem;\n  text-align: center;\n  width: 100%;\n  cursor: pointer;\n}\n\n.login-forgotpw {\n  width: 100%;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MiLCIvVXNlcnMvYW50aG9ueS5icmlvbmVzL0RvY3VtZW50cy9DYXBEcml2ZXIvc3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBT0E7RUFDSSx5QkFBQTtFQUNBLGdEQUFBO0FDTko7O0FEU0E7RUFDSSx5QkFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCRWxCYztFRm1CZCxXQUFBO0VBQ0EsY0VoQkk7RUZpQkosbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLCtCQUFBO0VBQUEsd0JBQUE7RUFDQSxnQ0FBQTtFQUNBLG9DQUFBO1VBQUEsOEJBQUE7QUNOSjs7QURTQTtFQUNJLG1DQUFBO1VBQUEsa0NBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO1VBQUEseUJBQUE7QUNOSjs7QURTQTtFQUNJLHdCQUFBO1VBQUEsdUJBQUE7QUNOSjs7QURTQTtFQUNJLFlBQUE7QUNOSjs7QUUvQ0E7RUFDSSxnQ0FBQTtBRmtESjs7QUUvQ0E7RUFDSSxlQUFBO0FGa0RKOztBRS9DQTtFQUNJLG9CQUFBO0VBQUEsYUFBQTtFQUNBLHNCQUFBO1VBQUEscUJBQUE7RUFDQSx1QkFBQTtVQUFBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLDRCQUFBO0FGa0RKOztBRS9DQTtFQUNJLG9CQUFBO0VBQUEsYUFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSwwQkFBQTtFQUNBLG1DQUFBO0FGa0RKOztBRW5DQTtFQUNJLFlBQUE7RUFHQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtBRm9DSjs7QUVqQ0E7RUFDSSw2QkFBQTtFQUNBLFNBQUE7RUFFQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUZtQ0o7O0FFaENBO0VBQ0ksa0NBQUE7RUFDQSx1QkFBQTtBRm1DSjs7QUVoQ0E7RUFDSSxpQkFBQTtBRm1DSjs7QUVoQ0E7RUFDSSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0EsMkJBQUE7QUZtQ0o7O0FFaENBO0VBQ0ksV0FBQTtBRm1DSjs7QUVoQ0E7RUFFSSw2QkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FGa0NKOztBRTlCQTtFQUNJLDZGRHJGZ0I7RUNxRmhCLG9FRHJGZ0I7RUNzRmhCLFdBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxrQ0FBQTtFQUNBLHNCQUFBO0FGaUNKOztBRTlCQTtFQUVJLFlBQUE7QUZnQ0o7O0FFNUJBO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBRitCSjs7QUU1QkE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7QUYrQkoiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvZm9udHMnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcyc7XG5cbi8vIC5pcy12YWxpZCB7XG4vLyAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZVxuLy8gfVxuXG4uaXMtaW52YWxpZCB7XG4gICAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwM1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogJHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xe1xuICAgIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICAgIGRpc3BsYXk6IGZsZXghaW1wb3J0YW50O1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlciFpbXBvcnRhbnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuXG5cblxuIiwiLmlzLWludmFsaWQge1xuICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDM7XG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDFyZW07XG4gIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTEge1xuICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbi5sb2dpbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1ncmlkIHtcbiAgbWF4LXdpZHRoOiA5MHZ3O1xufVxuXG4ubG9naW4tbGFiZWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGxlZnQ7XG4gIGFsaWduLWl0ZW1zOiBsZWZ0O1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgcGFkZGluZy10b3A6IDJyZW07XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1NZWRpdW1cIjtcbn1cblxuLmxvZ2luLWlucHV0IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMXJlbSAwIDAgMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgYm9yZGVyLWJvdHRvbTogMC4wMDFyZW0gc29saWQgd2hpdGU7XG59XG5cbi5zaG93LWlucHV0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDI7XG4gIHBhZGRpbmc6IDA7XG4gIHJpZ2h0OiAxdnc7XG59XG5cbmlucHV0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGJvcmRlcjogMDtcbiAgZm9udC1zaXplOiAxcmVtO1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHBhZGRpbmctYm90dG9tOiAwLjVyZW07XG59XG5cbmlucHV0W3R5cGU9cGFzc3dvcmRdIHtcbiAgLXdlYmtpdC10ZXh0LXN0cm9rZS13aWR0aDogMC4xOHJlbTtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMThyZW07XG59XG5cbi5sb2dpbi1idXR0b24ge1xuICBwYWRkaW5nLXRvcDogMXJlbTtcbn1cblxuLmxvZ2luLWltZyB7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbjogNTBweCBhdXRvIDIwcHggYXV0bztcbn1cblxuaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5leWUtYnRuIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxcmVtO1xufVxuXG4ubG9naW4tYnRuIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KC0xODBkZWcsICNGRjg4NDAgMCUsICNFRTQwMzYgMTAwJSk7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXNpemU6IDEuMTI1cmVtO1xuICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgaGVpZ2h0OiAzcmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuYnV0dG9uOmRpc2FibGVkIHtcbiAgb3BhY2l0eTogMC41O1xufVxuXG5zcGFuIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDAuNzVyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmxvZ2luLWZvcmdvdHB3IHtcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iLCJcbi8vIGNvbG9yc1xuLy8gJHByaW1hcnktY29sb3ItMTogIzAwQTNDODtcbiRwcmltYXJ5LWNvbG9yLTE6ICMwMDNBNTg7XG4kcHJpbWFyeS1jb2xvci0yOiAjRkFBRjQwO1xuJHByaW1hcnktY29sb3ItMzogI0Y0NzczQjtcblxuJHdoaXRlOiAjZmZmZmZmO1xuXG4vLyAkYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsIHJnYmEoMjU1LDI1NSwyNTUsMC41MCkgMCUsIHJnYmEoMCwwLDAsMC41MCkgMTAwJSk7XG4kYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpOyIsIkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy9jb21tb24uc2Nzcyc7XG5AaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MnO1xuXG5cbi5sb2dpbi1jb250ZW50e1xuICAgIC0tYmFja2dyb3VuZDogIzAwM0E1OCAhaW1wb3J0YW50O1xufVxuXG5pb24tZ3JpZHtcbiAgICBtYXgtd2lkdGg6IDkwdnc7XG59XG5cbi5sb2dpbi1sYWJlbHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogbGVmdDtcbiAgICBhbGlnbi1pdGVtczogbGVmdDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgICBwYWRkaW5nLXRvcDogMnJlbTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xufVxuXG4ubG9naW4taW5wdXR7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogMXJlbSAwIDAgMDtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1Cb2xkJztcbiAgICBib3JkZXItYm90dG9tOiAwLjAwMXJlbSBzb2xpZCB3aGl0ZTtcbn1cblxuLy8gLnNob3ctaW5wdXR7XG4vLyAgICAgZGlzcGxheTogZmxleDtcbi8vICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbi8vICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuLy8gICAgIGhlaWdodDogMTAwJTtcbi8vICAgICB3aWR0aDogMTAwJTtcbi8vICAgICBjb2xvcjogd2hpdGU7XG4vLyAgICAgcGFkZGluZzogMS41cmVtIDAgMCAwO1xuLy8gICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJvbGQnO1xuLy8gICAgIGJvcmRlci1ib3R0b206IDAuMDAxcmVtIHNvbGlkIHdoaXRlO1xuLy8gfVxuXG4uc2hvdy1pbnB1dCB7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIC8vIG1hcmdpbi1sZWZ0OiAtMzVweDtcbiAgICAvLyBtYXJnaW4tdG9wOiAtMjVweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgei1pbmRleDogMjtcbiAgICBwYWRkaW5nOiAwO1xuICAgIHJpZ2h0OiAxdnc7XG59XG5cbmlucHV0e1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlcjogMDtcbiAgICAvLyBib3JkZXItYm90dG9tOiAwLjAwMXJlbSBzb2xpZCB3aGl0ZTtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgcGFkZGluZy1ib3R0b206IDAuNXJlbTtcbn1cblxuaW5wdXRbdHlwZT1cInBhc3N3b3JkXCJde1xuICAgIC13ZWJraXQtdGV4dC1zdHJva2Utd2lkdGg6IDAuMThyZW07XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMThyZW07XG59XG5cbi5sb2dpbi1idXR0b257XG4gICAgcGFkZGluZy10b3A6IDFyZW07XG59XG5cbi5sb2dpbi1pbWd7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBtYXJnaW46IDUwcHggYXV0byAyMHB4IGF1dG87XG59XG5cbmltZ3tcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmV5ZS1idG4ge1xuICAgIC8vIGhlaWdodDogNTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIFxufVxuXG4ubG9naW4tYnRue1xuICAgIGJhY2tncm91bmQtaW1hZ2U6ICRidXR0b24tZ3JhZGllbnQtMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDEuMTI1cmVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICAgIGhlaWdodDogM3JlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbiAgICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG5idXR0b246ZGlzYWJsZWQge1xuICAgIC8vIGNvbG9yOiBncmF5O1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuXG5zcGFue1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDAuNzVyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmxvZ2luLWZvcmdvdHB3e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAvLyBtYXJnaW4tdG9wOiAwLjc1cmVtO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/login/login.page.ts":
  /*!*************************************!*\
    !*** ./src/app/login/login.page.ts ***!
    \*************************************/

  /*! exports provided: LoginPage */

  /***/
  function srcAppLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
      return LoginPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _shared_services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../shared/services/user.service */
    "./src/shared/services/user.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../shared/services/loader.service */
    "./src/shared/services/loader.service.ts");
    /* harmony import */


    var _shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../shared/services/alert.service */
    "./src/shared/services/alert.service.ts");
    /* harmony import */


    var _shared_services_toast_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../shared/services/toast.service */
    "./src/shared/services/toast.service.ts");
    /* harmony import */


    var _forgot_password_forgot_password_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./forgot-password/forgot-password.page */
    "./src/app/login/forgot-password/forgot-password.page.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _shared_services_network_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../shared/services/network.service */
    "./src/shared/services/network.service.ts"); // import { Network } from '@ionic-native/network/ngx';
    // import { Plugins } from '@capacitor/core';
    // const { Network } = Plugins;


    var LoginPage =
    /*#__PURE__*/
    function () {
      function LoginPage(formBuilder, userService, router, loaderService, alertService, toastService, modalController, // private network: Network,
      networkService) {
        _classCallCheck(this, LoginPage);

        this.formBuilder = formBuilder;
        this.userService = userService;
        this.router = router;
        this.loaderService = loaderService;
        this.alertService = alertService;
        this.toastService = toastService;
        this.modalController = modalController;
        this.networkService = networkService;
        this.submitted = false;
        this.showPw = false;
      }

      _createClass(LoginPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.initializeForm(); // this.networkService.checkConnection();
          // let disconnectSubs = this.network.onDisconnect().subscribe(
          //   () => {
          //     console.log('Network was disconnected.');
          //     window.alert("Network was disconnected");
          //   }
          // )
        }
      }, {
        key: "initializeForm",
        value: function initializeForm() {
          this.signInForm = this.formBuilder.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^(([a-zA-Z0-9]+(?!.*[\-\.\_]{2}).*_+)|([a-zA-Z0-9]+(?!.*[\-\.\_]{2}).*\-+)|([a-zA-Z0-9]+(?!.*[\-\.\_]{2}).*\.+)|([a-zA-Z0-9]+(?!.*[\-\.\_]{2}).*\++))*[a-zA-Z0-9]+@[a-zA-Z0-9-]+(\.[a-zA-Z]{2,})+$/)]],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
          });
        }
      }, {
        key: "signIn",
        value: function signIn() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            var _this = this;

            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!this.signInForm.invalid) {
                      _context.next = 3;
                      break;
                    }

                    this.toastService.errorToast("Email is invalid");
                    return _context.abrupt("return");

                  case 3:
                    this.loaderService.createLoader("Logging in ...");
                    this.submitted = true;
                    _context.next = 7;
                    return this.userService.signIn(this.signInForm.value.email, this.signInForm.value.password).subscribe(function (result) {
                      console.log(result);

                      _this.userService.saveUser(result.user);

                      _this.userService.saveToken(result.token);

                      _this.loaderService.dismissLoader();

                      _this.signInForm.reset();

                      _this.router.navigate(['tabs']);
                    }, function (error) {
                      _this.loaderService.dismissLoader();

                      if (error.status == 0) {
                        _this.toastService.errorToast("Check your internet connection and try again.");
                      } else {
                        _this.toastService.errorToast(error.error.error);
                      }

                      console.log("Error", error.status); // this.loaderService.dismissLoader();
                    });

                  case 7:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "forgotPassword",
        value: function forgotPassword() {
          // this.alertService.createAlert("Sorry Not Available Yet");
          this.router.navigate(['/forgot-password']);
        }
      }, {
        key: "openModal",
        value: function openModal(email) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var modal;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.modalController.create({
                      component: _forgot_password_forgot_password_page__WEBPACK_IMPORTED_MODULE_8__["ForgotPasswordPage"],
                      componentProps: {
                        'email': email
                      },
                      cssClass: 'forgot-modal'
                    });

                  case 2:
                    modal = _context2.sent;
                    _context2.next = 5;
                    return modal.present();

                  case 5:
                    return _context2.abrupt("return", _context2.sent);

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {}
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee3() {
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ngOnInit();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "fc",
        get: function get() {
          return this.signInForm.controls;
        }
      }]);

      return LoginPage;
    }();

    LoginPage.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _shared_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"]
      }, {
        type: _shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"]
      }, {
        type: _shared_services_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["ModalController"]
      }, {
        type: _shared_services_network_service__WEBPACK_IMPORTED_MODULE_10__["NetworkService"]
      }];
    };

    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-login',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./login.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./login.page.scss */
      "./src/app/login/login.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _shared_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"], _shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"], _shared_services_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["ModalController"], _shared_services_network_service__WEBPACK_IMPORTED_MODULE_10__["NetworkService"]])], LoginPage);
    /***/
  }
}]);
//# sourceMappingURL=login-login-module-es5.js.map