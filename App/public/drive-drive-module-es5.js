function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["drive-drive-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/drive.page.html":
  /*!*****************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/drive/drive.page.html ***!
    \*****************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDriveDrivePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title [size]=\"titleSize\">\n      Delivery\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [scrollEvents]=\"true\" (ionScroll)=\"onContentScroll($event)\">\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <div style=\"width: 100%; height: 350px; \" id=\"map\">\n\n  </div>\n\n  <!-- <div style=\"width: 100%; height: 350px;\" #map id=\"map\">\n\n  </div> -->\n\n  <!-- skeleton for Maps -->\n  <!-- <div style=\"width: 100%; height: 350px; \" *ngIf=\"showskeleton\">\n    <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n  </div> -->\n\n  <div *ngIf=\"!isFromChecklist && !showskeleton\">\n    <div *ngIf=\"routes.length != 0\">\n      <div *ngFor=\"let route of routes\">\n        <ion-card class=\"next-gig-panel\">\n          <ion-card-header>\n            <ion-item-divider class=\"next-gig-header\">\n              <ion-label class=\"header-font\">\n                Your Next Route\n              </ion-label>\n            </ion-item-divider>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-row class=\"ion-padding\">\n              <ion-col size=\"8\" class=\"next-gig-date ion-no-padding\">\n                {{route.deliveryDate | customDate:'longWeekDay'}},\n                {{route.deliveryDate | customDate:'deliveryDate'}}\n              </ion-col>\n\n              <ion-col size=\"4\" class=\"next-gig-time ion-no-padding\">\n                <i class=\"fa fa-clock-o\"></i>\n                {{route.startTime | convert24hrto12hr}}\n              </ion-col>\n\n              <ion-col size=\"12\" class=\"next-gig-delivery ion-no-padding\">\n                {{route.deliveryCount}} Deliveries\n              </ion-col>\n\n              <!-- <ion-col size=\"6\" class=\"next-gig-details vertical-align\">\n                        <ion-icon class=\"icon-delivery\" src=\"/assets/icon/icon-home-delivery.svg\"> </ion-icon> 4 Residential\n                      </ion-col>\n                      <ion-col size=\"6\" class=\"next-gig-details vertical-align\">\n                          <ion-icon class=\"icon-delivery\" src=\"/assets/icon/icon-commercial-delivery.svg\">testing </ion-icon>  1 Commercial\n                      </ion-col> -->\n\n              <ion-col size=\"1\" class=\"ion-no-padding vertical-align\">\n                <ion-img class=\"tank-img\" src=\"/assets/icon/icon-tank-cynch-white.svg\"></ion-img>\n              </ion-col>\n              <ion-col size=\"6\" class=\"ion-no-padding vertical-align\">\n                <ion-label class=\"other-number\">{{route.cynchCount}}</ion-label>\n                <ion-label class=\"other-tanks\">Cynch tanks</ion-label>\n              </ion-col>\n              <ion-col size=\"1\" class=\"ion-no-padding vertical-align\">\n                <ion-img class=\"tank-img\" src=\"/assets/icon/icon-tank-amerigas-white.svg\"></ion-img>\n              </ion-col>\n              <ion-col size=\"4\" class=\"ion-no-padding vertical-align\">\n\n                <ion-label class=\"other-number\">{{route.aceCount}}</ion-label>\n                <ion-label class=\"other-tanks\">ACE tanks</ion-label>\n              </ion-col>\n\n\n              <ion-col size=\"12\" class=\"login-button shadow ion-no-padding\">\n                <button class=\"btn shadow-lg vertical-align go-depot-btn\" round ion-button (click)=\"openMapApp(route)\">\n                  <ion-icon class=\"navigate-icon\" slot=\"start\" src=\"/assets/icon/icon-navigation.svg\"></ion-icon>\n                  Go To Depot\n                </button>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n\n        </ion-card>\n      </div>\n    </div>\n\n    <ion-card class=\"next-gig-panel-noDelivery\" *ngIf=\"routes.length == 0\">\n      <div>\n        <ion-card-header>\n          <ion-item-divider class=\"next-gig-header-noDelivery\">\n            <div class=\"header-font-noDelivery\">\n              There are no available routes right now\n              <!-- <button class=\"btn\" (click)=\"noTankLeftOutConfirmation()\">Test Only</button> -->\n            </div>\n          </ion-item-divider>\n        </ion-card-header>\n        <!-- <ion-card-content>\n          <ion-row>\n            <ion-col size=\"8\" class=\"next-gig-date-noDelivery\">\n              {{active[0].deliveryDate | customDate:'longWeekDay'}},\n              <br> {{active[0].deliveryDate | customDate:'deliveryDate'}}\n            </ion-col>\n\n            <ion-col size=\"4\" class=\"next-gig-time-noDelivery\">\n              <i class=\"fa fa-clock-o\"></i>\n              {{active[0].startTime | convert24hrto12hr}}\n            </ion-col>\n\n          </ion-row>\n        </ion-card-content> -->\n      </div>\n    </ion-card>\n  </div>\n\n  <!-- skeleton for nochecklist checklist -->\n  <div *ngIf=\"!isFromChecklist && showskeleton\">\n    <div>\n      <div>\n        <ion-card class=\"next-gig-panel\">\n          <ion-card-header>\n            <ion-item-divider mode=\"ios\" class=\"next-gig-header\">\n              <ion-label class=\"header-font-skeleton\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </ion-label>\n            </ion-item-divider>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-row class=\"ion-padding\">\n\n              <ion-col size=\"2\" class=\"\">\n                <!-- <ion-img class=\"tank-img\" src=\"/assets/icon/icon-tank-cynch-white.svg\"></ion-img> -->\n                <ion-skeleton-text class=\"tank-img-skeleton\" animated></ion-skeleton-text>\n              </ion-col>\n\n              <ion-col size=\"8\" class=\"\">\n                <div>\n                  <ion-skeleton-text class=\"w-75\" animated></ion-skeleton-text>\n                </div>\n                <div>\n                  <ion-skeleton-text class=\"w-50\" animated></ion-skeleton-text>\n                </div>\n              </ion-col>\n\n              <ion-col size=\"5\" class=\"\">\n              </ion-col>\n\n              <ion-col size=\"12\" class=\"ion-no-padding\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </ion-col>\n\n              <ion-col size=\"12\" class=\"ion-no-padding\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </ion-col>\n\n              <ion-col size=\"12\" class=\"ion-no-padding\">\n                <ion-skeleton-text animated class=\"w-75\"></ion-skeleton-text>\n              </ion-col>\n\n            </ion-row>\n          </ion-card-content>\n\n        </ion-card>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"ion-padding delivery-card\" *ngIf=\"isFromChecklist && !showskeleton\">\n    <ion-row *ngIf=\"currentDelivery\">\n      <ion-col size=\"5\" class=\"card-delivery ion-no-padding\">\n        <div class=\"card-label\">\n          Delivery\n        </div>\n        <div class=\"vertical-align horizontal-align\">\n          <ion-label class=\"card-number-delivery\">\n            {{currentDelivery.stopSequenceNum}}\n          </ion-label>\n          <ion-label class=\"card-total-delivery\">\n            of {{currentDelivery.stopsLength}}\n          </ion-label>\n        </div>\n\n      </ion-col>\n      <ion-col size=\"7\" class=\"ion-no-padding\">\n\n        <div *ngIf=\"isCynchOrder\" class=\"delivery-type vertical-align\">\n          <ion-icon name=\"md-home\"></ion-icon> RESIDENTIAL\n        </div>\n        <div *ngIf=\"!isCynchOrder\" class=\"delivery-type vertical-align\">\n          <ion-icon name=\"md-home\"></ion-icon> COMMERCIAL\n        </div>\n        <div class=\"delivery-name\">\n          {{currentDelivery.customerName}}\n        </div>\n        <div class=\"delivery-address\">\n          {{currentDelivery.street}}, {{currentDelivery.city}}, {{currentDelivery.state}} {{currentDelivery.zipcode}}\n        </div>\n        <!-- <div class=\"delivery-address\">\n          Delivery Id {{deliveries[currentDelivery].deliveryId}}\n        </div>\n        <div class=\"delivery-button ion-padding\">\n          <button class=\"btn\" (click)=\"openDeliveryDetails(1)\">Details & Delivery</button>\n        </div> -->\n      </ion-col>\n      <ion-col size=\"5\">\n\n      </ion-col>\n\n      <ng-container *ngIf=\"isCynchOrder\">\n        <ion-col *ngIf=\"currentDelivery\" size=\"7\" class=\"ion-padding-start tank-details vertical-align\">\n          <ion-icon src=\"assets/icon/propane-tank-graphic.svg\"></ion-icon>\n          {{tankQuantity}}\n        </ion-col>\n      </ng-container>\n      <ng-container *ngIf=\"!isCynchOrder\">\n        <ion-col size=\"7\" class=\"ion-padding-start tank-details vertical-align\">\n          <ion-icon src=\"assets/icon/propane-tank-graphic.svg\"></ion-icon>\n          {{tankQuantity}}\n        </ion-col>\n      </ng-container>\n\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\" class=\"ion-padding-start ion-padding-top\">\n        <button class=\"btn navigate-btn vertical-align horizontal-align\" (click)=\"navigateToStop()\">\n          <ion-icon src=\"assets/icon/icon-navigation-blue.svg\"></ion-icon> NAVIGATE\n        </button>\n      </ion-col>\n      <ion-col size=\"6\" class=\"ion-padding-end ion-padding-top\">\n\n        <button class=\"btn details-btn vertical-align horizontal-align\" (click)=\"openDeliveryDetails()\">\n          <ion-icon name=\"md-information-circle-outline\"></ion-icon>DETAILS\n        </button>\n      </ion-col>\n\n      <ion-col size=\"6\" class=\"ion-padding-top\" *ngIf=\"!currentDelivery?.customerPhone.length == 0\">\n        <a href=\"tel: {{currentDelivery.customerPhone}}\">\n          <button class=\"customer-col vertical-align horizontal-align\">\n            <ion-img src=\"assets/icon/icon-phone.svg\"></ion-img>\n          </button>\n        </a>\n      </ion-col>\n      <ion-col size=\"6\" class=\"ion-padding-top disable\" *ngIf=\"currentDelivery?.customerPhone.length == 0\">\n        <button class=\"customer-col vertical-align horizontal-align\">\n          <ion-img src=\"assets/icon/icon-phone.svg\"></ion-img>\n        </button>\n      </ion-col>\n      <ion-col size=\"6\" class=\"ion-padding-top\">\n        <a href=\"tel: 8885252899\">\n          <button class=\"office-col vertical-align horizontal-align\">\n            <ion-img src=\"assets/icon/icon-phone.svg\"></ion-img>\n          </button>\n        </a>\n      </ion-col>\n\n      <ion-col size=\"6\" *ngIf=\"!currentDelivery?.customerPhone.length == 0\">\n        <a href=\"tel: {{currentDelivery.customerPhone}}\">\n          <ion-label class=\"customer-label vertical-align horizontal-align\">\n            CALL CUSTOMER\n          </ion-label>\n        </a>\n      </ion-col>\n      <ion-col size=\"6\" *ngIf=\"currentDelivery?.customerPhone.length == 0\">\n\n        <ion-label class=\"customer-label vertical-align horizontal-align disable\">\n          CALL CUSTOMER\n        </ion-label>\n\n      </ion-col>\n      <ion-col size=\"6\">\n        <a href=\"tel: 8885252899\">\n          <ion-label class=\"office-label vertical-align horizontal-align\">\n            CALL OFFICE\n          </ion-label>\n        </a>\n      </ion-col>\n    </ion-row>\n\n\n  </div>\n\n  <!-- skeleton for checklist -->\n  <div class=\"ion-padding delivery-card\" *ngIf=\"isFromChecklist && showskeleton\">\n    <ion-row>\n      <ion-col size=\"5\" class=\"card-delivery-skeleton ion-no-padding\">\n        <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n      </ion-col>\n      <ion-col size=\"7\" class=\"ion-no-padding\">\n\n        <!-- <div class=\"delivery-type vertical-align\">\n          <ion-icon name=\"md-home\"></ion-icon> <ion-skeleton-text animated></ion-skeleton-text>\n        </div> -->\n        <div class=\"delivery-type vertical-align\">\n          <ion-icon name=\"md-home\"></ion-icon>\n          <ion-skeleton-text animated></ion-skeleton-text>\n        </div>\n        <div class=\"delivery-name\">\n          <ion-skeleton-text animated></ion-skeleton-text>\n        </div>\n        <div class=\"delivery-address\">\n          <ion-skeleton-text animated></ion-skeleton-text>\n        </div>\n        <!-- <div class=\"delivery-address\">\n            Delivery Id {{deliveries[currentDelivery].deliveryId}}\n          </div>\n          <div class=\"delivery-button ion-padding\">\n            <button class=\"btn\" (click)=\"openDeliveryDetails(1)\">Details & Delivery</button>\n          </div> -->\n      </ion-col>\n      <ion-col size=\"5\">\n\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\" class=\"ion-padding-start ion-padding-top\">\n        <button class=\"btn navigate-btn vertical-align horizontal-align\" (click)=\"navigateToStop()\">\n          <ion-icon src=\"assets/icon/icon-navigation-blue.svg\"></ion-icon> NAVIGATE\n        </button>\n      </ion-col>\n      <ion-col size=\"6\" class=\"ion-padding-end ion-padding-top\">\n\n        <button class=\"btn details-btn vertical-align horizontal-align\" (click)=\"openDeliveryDetails()\">\n          <ion-icon name=\"md-information-circle-outline\"></ion-icon>DETAILS\n        </button>\n      </ion-col>\n\n      <ion-col size=\"6\" class=\"ion-padding-top\">\n        <button class=\"customer-col vertical-align horizontal-align\">\n          <ion-img src=\"assets/icon/icon-phone.svg\"></ion-img>\n        </button>\n\n      </ion-col>\n      <ion-col size=\"6\" class=\"ion-padding-top\">\n        <button class=\"office-col vertical-align horizontal-align\">\n          <ion-img src=\"assets/icon/icon-phone.svg\"></ion-img>\n        </button>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-label class=\"customer-label vertical-align horizontal-align\">\n          CALL CUSTOMER\n        </ion-label>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-label class=\"office-label vertical-align horizontal-align\">\n          CALL OFFICE\n        </ion-label>\n      </ion-col>\n    </ion-row>\n\n\n  </div>\n\n\n\n\n\n\n  <!-- <button class=\"primary-button\" (click)=\"openMapApp()\"> Open Map app </button> -->\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/drive/drive.module.ts":
  /*!***************************************!*\
    !*** ./src/app/drive/drive.module.ts ***!
    \***************************************/

  /*! exports provided: DrivePageModule */

  /***/
  function srcAppDriveDriveModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DrivePageModule", function () {
      return DrivePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _drive_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./drive.page */
    "./src/app/drive/drive.page.ts");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../shared/shared.module */
    "./src/shared/shared.module.ts");

    var routes = [{
      path: '',
      component: _drive_page__WEBPACK_IMPORTED_MODULE_6__["DrivePage"]
    }];

    var DrivePageModule = function DrivePageModule() {
      _classCallCheck(this, DrivePageModule);
    };

    DrivePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_drive_page__WEBPACK_IMPORTED_MODULE_6__["DrivePage"]]
    })], DrivePageModule);
    /***/
  },

  /***/
  "./src/app/drive/drive.page.scss":
  /*!***************************************!*\
    !*** ./src/app/drive/drive.page.scss ***!
    \***************************************/

  /*! exports provided: default */

  /***/
  function srcAppDriveDrivePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\n.custom-skeleton ion-skeleton-text {\n  line-height: 13px;\n}\n\n.custom-skeleton ion-skeleton-text:last-child {\n  margin-bottom: 5px;\n}\n\nion-title {\n  color: #003A58;\n  font-family: \"Montserrat-Bold\";\n}\n\nion-toolbar {\n  margin-top: 1rem !important;\n  font-family: \"Montserrat-Bold\";\n}\n\n.next-gig-panel {\n  background-color: #003A58;\n  margin: 1rem;\n}\n\n.next-gig-header {\n  background-color: #003A58;\n}\n\n.next-gig-header-noDelivery {\n  background-color: #FFFFFF;\n}\n\n.header-font {\n  color: #FAAF40;\n  font-size: 1rem;\n  font-family: \"Montserrat-Bold\";\n}\n\n.header-font-skeleton {\n  width: 10rem;\n}\n\n.header-font-noDelivery {\n  color: #003A58;\n  font-size: 0.875rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.next-gig-date {\n  font-size: 0.875rem;\n  color: white;\n  font-family: \"Roboto-Bold\";\n  line-height: 1.3125rem;\n}\n\n.next-gig-time {\n  font-size: 0.875rem;\n  color: white;\n  font-family: \"Roboto-Bold\";\n  line-height: 1.3125rem;\n}\n\n.next-gig-date-noDelivery {\n  font-size: 0.875rem;\n  padding-left: 1rem;\n  padding-top: 0.625rem;\n  color: #003A58;\n  font-family: \"Roboto-Bold\";\n  line-height: 1.3125rem;\n}\n\n.next-gig-time-noDelivery {\n  font-size: 0.875rem;\n  padding-right: 1rem;\n  padding-top: 0.625rem;\n  color: #003A58;\n  font-family: \"Roboto-Bold\";\n  line-height: 1.3125rem;\n}\n\n.next-gig-delivery {\n  font-size: 1.75rem;\n  color: white;\n  font-weight: bolder;\n  font-family: \"Montserrat-Bold\";\n}\n\n.next-gig-details {\n  padding: 0.625rem 1rem;\n  color: white;\n  font-family: \"Roboto-Medium\";\n  font-size: 0.875rem;\n  line-height: 1.3125rem;\n}\n\n.login-button {\n  padding-top: 1rem;\n}\n\nion-card-header,\nion-card-content {\n  padding: 0;\n}\n\nion-title {\n  color: #003A58;\n  font-family: \"Montserrat-Bold\";\n  letter-spacing: 0;\n}\n\n.card-delivery {\n  border: 1px solid gray;\n  text-align: center;\n  padding: 0.75rem;\n}\n\n.card-delivery-skeleton {\n  text-align: center;\n}\n\n.card-label {\n  text-transform: uppercase;\n  color: #003A58;\n  font-size: 0.875rem;\n  font-family: \"Montserrat-Bold\";\n}\n\n.card-number-delivery {\n  color: #003A58;\n  font-size: 3.5rem;\n  font-family: \"Montserrat-Bold\";\n}\n\n.card-total-delivery {\n  color: #003A58;\n  font-size: 1.75rem;\n  font-family: \"Montserrat-Bold\";\n  margin-left: 0.625rem;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.delivery-type {\n  font-family: \"Montserrat-SemiBold\";\n  font-size: 0.875rem;\n  color: #003A58;\n  letter-spacing: 0;\n  padding: 0 1rem;\n}\n\nion-icon {\n  padding-right: 5px;\n}\n\n.delivery-name {\n  font-family: \"Montserrat-Bold\";\n  font-size: 1.125rem;\n  color: #003A58;\n  letter-spacing: 0;\n  line-height: 1.6875rem;\n  padding: 0.875rem 0 0 1rem;\n}\n\n.delivery-address {\n  font-family: \"Roboto-Regular\";\n  font-size: 1.125rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.5rem;\n  padding-left: 1rem;\n}\n\n.delivery-card {\n  background-color: #ffffff;\n}\n\n#map_canvas {\n  background-color: #D9D9D9;\n}\n\nbutton {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n  background: #003A58;\n}\n\n.tank-details {\n  color: #003A58;\n  font-size: 0.875rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.navigate-btn {\n  color: #003A58;\n  border: solid 1px #003A58;\n  font-family: \"Montserrat-Bold\";\n  background: white;\n}\n\n.go-depot-btn {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n  background: #F4773B 100%;\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036));\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%);\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.details-btn {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: #003A58;\n  border: solid 1px #003A58;\n  font-family: \"Montserrat-Bold\";\n  background: white;\n}\n\n.customer-col {\n  margin: 2px auto;\n  height: 50px;\n  width: 50px;\n  background-color: #F4773B;\n  border-radius: 30px;\n}\n\n.office-col {\n  margin: 2px auto;\n  height: 50px;\n  width: 50px;\n  background-color: #003A58;\n  border-radius: 30px;\n}\n\n.customer-label {\n  color: #F4773B;\n  font-size: 1rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.office-label {\n  color: #003A58;\n  font-size: 1rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.tank-img {\n  height: 100%;\n}\n\n.tank-img-skeleton {\n  background-size: 1.5rem 1.5rem;\n  height: 2rem;\n  background-image: -webkit-gradient(linear, left top, right top, from(#004C73), color-stop(20%, #005580), color-stop(40%, #004C73), to(#004C73));\n  background-image: linear-gradient(to right, #004C73 0%, #005580 20%, #004C73 40%, #004C73 100%);\n}\n\n.other-tanks {\n  font-size: 0.875rem;\n  color: white;\n  letter-spacing: 0;\n  font-family: \"Roboto-Medium\";\n}\n\n.other-number {\n  font-size: 1.25rem;\n  color: #ffffff;\n  font-family: \"Montserrat-Bold\";\n  padding-right: 5px;\n  padding-left: 8px;\n}\n\n.disable {\n  opacity: 0.5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL2RyaXZlL2RyaXZlLnBhZ2Uuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MiLCIvVXNlcnMvYW50aG9ueS5icmlvbmVzL0RvY3VtZW50cy9DYXBEcml2ZXIvc3JjL2FwcC9kcml2ZS9kcml2ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBT0E7RUFDSSx5QkFBQTtFQUNBLGdEQUFBO0FDTko7O0FEU0E7RUFDSSx5QkFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCRWxCYztFRm1CZCxXQUFBO0VBQ0EsY0VoQkk7RUZpQkosbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLCtCQUFBO0VBQUEsd0JBQUE7RUFDQSxnQ0FBQTtFQUNBLG9DQUFBO1VBQUEsOEJBQUE7QUNOSjs7QURTQTtFQUNJLG1DQUFBO1VBQUEsa0NBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO1VBQUEseUJBQUE7QUNOSjs7QURTQTtFQUNJLHdCQUFBO1VBQUEsdUJBQUE7QUNOSjs7QURTQTtFQUNJLFlBQUE7QUNOSjs7QUVoREE7RUFDSSxpQkFBQTtBRm1ESjs7QUVoREU7RUFDRSxrQkFBQTtBRm1ESjs7QUUvQ0E7RUFDSSxjRFZjO0VDWWQsOEJBQUE7QUZpREo7O0FFOUNBO0VBQ0ksMkJBQUE7RUFDQSw4QkFBQTtBRmlESjs7QUU3Q0E7RUFDSSx5QkR0QmM7RUN1QmQsWUFBQTtBRmdESjs7QUU3Q0E7RUFDSSx5QkQzQmM7QUQyRWxCOztBRTdDQTtFQUNJLHlCQUFBO0FGZ0RKOztBRTdDQTtFQUNJLGNEbENjO0VDbUNkLGVBQUE7RUFFQSw4QkFBQTtBRitDSjs7QUU1Q0E7RUFDSSxZQUFBO0FGK0NKOztBRXRDQTtFQUNJLGNEcERjO0VDcURkLG1CQUFBO0VBQ0Esa0NBQUE7QUZ5Q0o7O0FFdENBO0VBQ0ksbUJBQUE7RUFHQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxzQkFBQTtBRnVDSjs7QUVwQ0E7RUFDSSxtQkFBQTtFQUdBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLHNCQUFBO0FGcUNKOztBRWxDQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLGNEL0VjO0VDZ0ZkLDBCQUFBO0VBQ0Esc0JBQUE7QUZxQ0o7O0FFbENBO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0R4RmM7RUN5RmQsMEJBQUE7RUFDQSxzQkFBQTtBRnFDSjs7QUVsQ0E7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUVBLDhCQUFBO0FGb0NKOztBRWpDQTtFQUNJLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLDRCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBRm9DSjs7QUVqQ0E7RUFFSSxpQkFBQTtBRm1DSjs7QUVoQ0E7O0VBRUksVUFBQTtBRm1DSjs7QUVoQ0E7RUFDSSxjRHhIYztFQzBIZCw4QkFBQTtFQUNBLGlCQUFBO0FGa0NKOztBRS9CQTtFQUNJLHNCQUFBO0VBRUEsa0JBQUE7RUFDQSxnQkFBQTtBRmlDSjs7QUU5QkE7RUFDSSxrQkFBQTtBRmlDSjs7QUU5QkE7RUFDSSx5QkFBQTtFQUNBLGNEM0ljO0VDNElkLG1CQUFBO0VBQ0EsOEJBQUE7QUZpQ0o7O0FFOUJBO0VBQ0ksY0RqSmM7RUNrSmQsaUJBQUE7RUFDQSw4QkFBQTtBRmlDSjs7QUU3QkE7RUFDSSxjRHhKYztFQ3lKZCxrQkFBQTtFQUNBLDhCQUFBO0VBQ0EscUJBQUE7QUZnQ0o7O0FFN0JBO0VBQ0ksd0JBQUE7VUFBQSx1QkFBQTtBRmdDSjs7QUU3QkE7RUFDSSxrQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsY0RyS2M7RUNzS2QsaUJBQUE7RUFDQSxlQUFBO0FGZ0NKOztBRTdCQTtFQUNJLGtCQUFBO0FGZ0NKOztBRTdCQTtFQUNJLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjRGpMYztFQ2tMZCxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsMEJBQUE7QUZnQ0o7O0FFN0JBO0VBQ0ksNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUZnQ0o7O0FFN0JBO0VBQ0kseUJEN0xJO0FENk5SOztBRTdCQTtFQUNJLHlCQUFBO0FGZ0NKOztBRTdCQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFFQSw4QkFBQTtFQUNBLG1CRGhOYztBRCtPbEI7O0FFM0JBO0VBQ0ksY0RyTmM7RUNzTmQsbUJBQUE7RUFDQSxrQ0FBQTtBRjhCSjs7QUUzQkE7RUFDSSxjRDNOYztFQzROZCx5QkFBQTtFQUVBLDhCQUFBO0VBQ0EsaUJBQUE7QUY2Qko7O0FFM0JBO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUVBLDhCQUFBO0VBQ0Esd0JBQUE7RUFDQSw2RkRuT2dCO0VDbU9oQixvRURuT2dCO0VDb09oQix3QkFBQTtVQUFBLHVCQUFBO0FGNkJKOztBRTFCQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLGNEblBjO0VDb1BkLHlCQUFBO0VBRUEsOEJBQUE7RUFDQSxpQkFBQTtBRjRCSjs7QUV6QkE7RUFDSSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EseUJENVBjO0VDNlBkLG1CQUFBO0FGNEJKOztBRXpCQTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSx5QkR0UWM7RUN1UWQsbUJBQUE7QUY0Qko7O0FFekJBO0VBQ0ksY0R6UWM7RUMwUWQsZUFBQTtFQUNBLGtDQUFBO0FGNEJKOztBRXpCQTtFQUNJLGNEalJjO0VDa1JkLGVBQUE7RUFDQSxrQ0FBQTtBRjRCSjs7QUV6QkE7RUFDSSxZQUFBO0FGNEJKOztBRXZCQTtFQUNJLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLCtJQUFBO0VBQUEsK0ZBQUE7QUYwQko7O0FFdEJBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSw0QkFBQTtBRnlCSjs7QUVwQkE7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUZ1Qko7O0FFcEJBO0VBQ0ksWUFBQTtBRnVCSiIsImZpbGUiOiJzcmMvYXBwL2RyaXZlL2RyaXZlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy9mb250cyc7XG5AaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzJztcblxuLy8gLmlzLXZhbGlkIHtcbi8vICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlXG4vLyB9XG5cbi5pcy1pbnZhbGlkIHtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcInNyYy9hc3NldHMvaWNvbi9pY29uLWhlbHAuc3ZnXCIpIG5vLXJlcGVhdDtcbn1cblxuLmlzLXZhbGlkIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjNUVCMDAzXG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0VFNDAzNjtcbn1cblxuLnByaW1hcnktYnV0dG9uIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAkd2hpdGU7XG4gICAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTF7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4O1xufVxuXG5pb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi52ZXJ0aWNhbC1hbGlnbiB7XG4gICAgZGlzcGxheTogZmxleCFpbXBvcnRhbnQ7XG4gICAgYWxpZ24tY29udGVudDogY2VudGVyIWltcG9ydGFudDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyIWltcG9ydGFudDtcbn1cblxuLmhvcml6b250YWwtYWxpZ24ge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWFsaWduLXJpZ2h0IHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZGlzYWJsZSB7XG4gICAgb3BhY2l0eTogMC41O1xufVxuXG5cblxuXG4iLCIuaXMtaW52YWxpZCB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNFRTQwMzY7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcInNyYy9hc3NldHMvaWNvbi9pY29uLWhlbHAuc3ZnXCIpIG5vLXJlcGVhdDtcbn1cblxuLmlzLXZhbGlkIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwMztcbn1cblxuLmlzLWludmFsaWQtc2VsZWN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgI0VFNDAzNjtcbn1cblxuLnByaW1hcnktYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgaGVpZ2h0OiAycmVtO1xufVxuXG4uYmctY29sb3ItMSB7XG4gIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi52ZXJ0aWNhbC1hbGlnbiB7XG4gIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmhvcml6b250YWwtYWxpZ24ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5mbGV4LWFsaWduLWNlbnRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZGlzYWJsZSB7XG4gIG9wYWNpdHk6IDAuNTtcbn1cblxuLmN1c3RvbS1za2VsZXRvbiBpb24tc2tlbGV0b24tdGV4dCB7XG4gIGxpbmUtaGVpZ2h0OiAxM3B4O1xufVxuXG4uY3VzdG9tLXNrZWxldG9uIGlvbi1za2VsZXRvbi10ZXh0Omxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbmlvbi10aXRsZSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuaW9uLXRvb2xiYXIge1xuICBtYXJnaW4tdG9wOiAxcmVtICFpbXBvcnRhbnQ7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xufVxuXG4ubmV4dC1naWctcGFuZWwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xuICBtYXJnaW46IDFyZW07XG59XG5cbi5uZXh0LWdpZy1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xufVxuXG4ubmV4dC1naWctaGVhZGVyLW5vRGVsaXZlcnkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGRkZGO1xufVxuXG4uaGVhZGVyLWZvbnQge1xuICBjb2xvcjogI0ZBQUY0MDtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuLmhlYWRlci1mb250LXNrZWxldG9uIHtcbiAgd2lkdGg6IDEwcmVtO1xufVxuXG4uaGVhZGVyLWZvbnQtbm9EZWxpdmVyeSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LVNlbWlCb2xkXCI7XG59XG5cbi5uZXh0LWdpZy1kYXRlIHtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tQm9sZFwiO1xuICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4ubmV4dC1naWctdGltZSB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLm5leHQtZ2lnLWRhdGUtbm9EZWxpdmVyeSB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIHBhZGRpbmctbGVmdDogMXJlbTtcbiAgcGFkZGluZy10b3A6IDAuNjI1cmVtO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLm5leHQtZ2lnLXRpbWUtbm9EZWxpdmVyeSB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XG4gIHBhZGRpbmctdG9wOiAwLjYyNXJlbTtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1Cb2xkXCI7XG4gIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5uZXh0LWdpZy1kZWxpdmVyeSB7XG4gIGZvbnQtc2l6ZTogMS43NXJlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuLm5leHQtZ2lnLWRldGFpbHMge1xuICBwYWRkaW5nOiAwLjYyNXJlbSAxcmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1NZWRpdW1cIjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLmxvZ2luLWJ1dHRvbiB7XG4gIHBhZGRpbmctdG9wOiAxcmVtO1xufVxuXG5pb24tY2FyZC1oZWFkZXIsXG5pb24tY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMDtcbn1cblxuaW9uLXRpdGxlIHtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBsZXR0ZXItc3BhY2luZzogMDtcbn1cblxuLmNhcmQtZGVsaXZlcnkge1xuICBib3JkZXI6IDFweCBzb2xpZCBncmF5O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDAuNzVyZW07XG59XG5cbi5jYXJkLWRlbGl2ZXJ5LXNrZWxldG9uIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uY2FyZC1sYWJlbCB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuLmNhcmQtbnVtYmVyLWRlbGl2ZXJ5IHtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtc2l6ZTogMy41cmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuLmNhcmQtdG90YWwtZGVsaXZlcnkge1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1zaXplOiAxLjc1cmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgbWFyZ2luLWxlZnQ6IDAuNjI1cmVtO1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZGVsaXZlcnktdHlwZSB7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICBwYWRkaW5nOiAwIDFyZW07XG59XG5cbmlvbi1pY29uIHtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xufVxuXG4uZGVsaXZlcnktbmFtZSB7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBmb250LXNpemU6IDEuMTI1cmVtO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjY4NzVyZW07XG4gIHBhZGRpbmc6IDAuODc1cmVtIDAgMCAxcmVtO1xufVxuXG4uZGVsaXZlcnktYWRkcmVzcyB7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1SZWd1bGFyXCI7XG4gIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gIGNvbG9yOiAjNDI0MjQyO1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgbGluZS1oZWlnaHQ6IDEuNXJlbTtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xufVxuXG4uZGVsaXZlcnktY2FyZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59XG5cbiNtYXBfY2FudmFzIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0Q5RDlEOTtcbn1cblxuYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICBoZWlnaHQ6IDNyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbi50YW5rLWRldGFpbHMge1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xufVxuXG4ubmF2aWdhdGUtYnRuIHtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGJvcmRlcjogc29saWQgMXB4ICMwMDNBNTg7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cblxuLmdvLWRlcG90LWJ0biB7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXNpemU6IDEuMTI1cmVtO1xuICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgaGVpZ2h0OiAzcmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBiYWNrZ3JvdW5kOiAjRjQ3NzNCIDEwMCU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRldGFpbHMtYnRuIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICBoZWlnaHQ6IDNyZW07XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBib3JkZXI6IHNvbGlkIDFweCAjMDAzQTU4O1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbi5jdXN0b21lci1jb2wge1xuICBtYXJnaW46IDJweCBhdXRvO1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjQ3NzNCO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuXG4ub2ZmaWNlLWNvbCB7XG4gIG1hcmdpbjogMnB4IGF1dG87XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG59XG5cbi5jdXN0b21lci1sYWJlbCB7XG4gIGNvbG9yOiAjRjQ3NzNCO1xuICBmb250LXNpemU6IDFyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbn1cblxuLm9mZmljZS1sYWJlbCB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LXNpemU6IDFyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbn1cblxuLnRhbmstaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4udGFuay1pbWctc2tlbGV0b24ge1xuICBiYWNrZ3JvdW5kLXNpemU6IDEuNXJlbSAxLjVyZW07XG4gIGhlaWdodDogMnJlbTtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMDA0QzczIDAlLCAjMDA1NTgwIDIwJSwgIzAwNEM3MyA0MCUsICMwMDRDNzMgMTAwJSk7XG59XG5cbi5vdGhlci10YW5rcyB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1NZWRpdW1cIjtcbn1cblxuLm90aGVyLW51bWJlciB7XG4gIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIHBhZGRpbmctbGVmdDogOHB4O1xufVxuXG4uZGlzYWJsZSB7XG4gIG9wYWNpdHk6IDAuNTtcbn0iLCJcbi8vIGNvbG9yc1xuLy8gJHByaW1hcnktY29sb3ItMTogIzAwQTNDODtcbiRwcmltYXJ5LWNvbG9yLTE6ICMwMDNBNTg7XG4kcHJpbWFyeS1jb2xvci0yOiAjRkFBRjQwO1xuJHByaW1hcnktY29sb3ItMzogI0Y0NzczQjtcblxuJHdoaXRlOiAjZmZmZmZmO1xuXG4vLyAkYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsIHJnYmEoMjU1LDI1NSwyNTUsMC41MCkgMCUsIHJnYmEoMCwwLDAsMC41MCkgMTAwJSk7XG4kYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpOyIsIkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy9jb21tb24uc2Nzcyc7XG5AaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MnO1xuXG4uY3VzdG9tLXNrZWxldG9uIGlvbi1za2VsZXRvbi10ZXh0IHtcbiAgICBsaW5lLWhlaWdodDogMTNweDtcbiAgfVxuICBcbiAgLmN1c3RvbS1za2VsZXRvbiBpb24tc2tlbGV0b24tdGV4dDpsYXN0LWNoaWxkIHtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIH1cbiAgXG4gIFxuaW9uLXRpdGxle1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIC8vIHNpemU6IDNyZW0gIWltcG9ydGFudDtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgICBtYXJnaW4tdG9wOiAxcmVtICFpbXBvcnRhbnQ7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xufVxuXG5cbi5uZXh0LWdpZy1wYW5lbHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIG1hcmdpbjogMXJlbTtcbn1cblxuLm5leHQtZ2lnLWhlYWRlcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG4ubmV4dC1naWctaGVhZGVyLW5vRGVsaXZlcnkge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XG59XG5cbi5oZWFkZXItZm9udHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMjtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgLy8gZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG59XG5cbi5oZWFkZXItZm9udC1za2VsZXRvbntcbiAgICB3aWR0aDogMTByZW07XG59XG5cbi8vIC5oZWFkZXItZm9udC1ub0RlbGl2ZXJ5IHtcbi8vICAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbi8vICAgICBmb250LXNpemU6IDFyZW07XG4vLyAgICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuLy8gfVxuXG4uaGVhZGVyLWZvbnQtbm9EZWxpdmVyeSB7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xufVxuXG4ubmV4dC1naWctZGF0ZXtcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIC8vIHBhZGRpbmctbGVmdDogMXJlbTtcbiAgICAvLyBwYWRkaW5nLXRvcDogMC42MjVyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJvbGQnO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5uZXh0LWdpZy10aW1le1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgLy8gcGFkZGluZy1yaWdodDogMXJlbTtcbiAgICAvLyBwYWRkaW5nLXRvcDogMC42MjVyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJvbGQnO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5uZXh0LWdpZy1kYXRlLW5vRGVsaXZlcnl7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gICAgcGFkZGluZy10b3A6IDAuNjI1cmVtO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJvbGQnO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5uZXh0LWdpZy10aW1lLW5vRGVsaXZlcnl7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xuICAgIHBhZGRpbmctdG9wOiAwLjYyNXJlbTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1Cb2xkJztcbiAgICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4ubmV4dC1naWctZGVsaXZlcnl7XG4gICAgZm9udC1zaXplOiAxLjc1cmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIC8vIHBhZGRpbmc6IDAgMXJlbTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG59XG5cbi5uZXh0LWdpZy1kZXRhaWxze1xuICAgIHBhZGRpbmc6IDAuNjI1cmVtIDFyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLU1lZGl1bSc7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4ubG9naW4tYnV0dG9ue1xuICAgIC8vIHBhZGRpbmc6IDEuMjVyZW0gMXJlbTtcbiAgICBwYWRkaW5nLXRvcDogMXJlbTtcbn1cblxuaW9uLWNhcmQtaGVhZGVyLFxuaW9uLWNhcmQtY29udGVudHtcbiAgICBwYWRkaW5nOiAwO1xufVxuXG5pb24tdGl0bGV7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLy8gZm9udC1zaXplOiAycmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbn1cblxuLmNhcmQtZGVsaXZlcnkge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyYXk7XG4gICAgLy8gbWFyZ2luOiAxcmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAwLjc1cmVtO1xufVxuXG4uY2FyZC1kZWxpdmVyeS1za2VsZXRvbntcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jYXJkLWxhYmVsIHtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xufVxuXG4uY2FyZC1udW1iZXItZGVsaXZlcnkge1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtc2l6ZTogMy41cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcblxufVxuXG4uY2FyZC10b3RhbC1kZWxpdmVyeSB7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1zaXplOiAxLjc1cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBtYXJnaW4tbGVmdDogMC42MjVyZW07XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRlbGl2ZXJ5LXR5cGV7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIHBhZGRpbmc6IDAgMXJlbTtcbn1cblxuaW9uLWljb24ge1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbn1cblxuLmRlbGl2ZXJ5LW5hbWUge1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBmb250LXNpemU6IDEuMTI1cmVtO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjY4NzVyZW07XG4gICAgcGFkZGluZzogMC44NzVyZW0gMCAwIDFyZW07XG59XG5cbi5kZWxpdmVyeS1hZGRyZXNzIHtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1SZWd1bGFyJztcbiAgICBmb250LXNpemU6IDEuMTI1cmVtO1xuICAgIGNvbG9yOiAjNDI0MjQyO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjVyZW07XG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xufVxuXG4uZGVsaXZlcnktY2FyZCB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHdoaXRlO1xufVxuXG4jbWFwX2NhbnZhcyB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0Q5RDlEOVxufVxuXG5idXR0b257XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgICBoZWlnaHQ6IDNyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIC8vIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIGJhY2tncm91bmQ6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLy8gYmFja2dyb3VuZC1pbWFnZTogJGJ1dHRvbi1ncmFkaWVudC0xO1xufVxuXG4udGFuay1kZXRhaWxzIHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG59XG5cbi5uYXZpZ2F0ZS1idG4ge1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGJvcmRlcjogc29saWQgMXB4ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLy8gZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG59XG4uZ28tZGVwb3QtYnRue1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAvLyBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBiYWNrZ3JvdW5kOiAjRjQ3NzNCIDEwMCU7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogJGJ1dHRvbi1ncmFkaWVudC0xO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufSAgXG5cbi5kZXRhaWxzLWJ0biB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgICBoZWlnaHQ6IDNyZW07XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgYm9yZGVyOiBzb2xpZCAxcHggJHByaW1hcnktY29sb3ItMTtcbiAgICAvLyBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cblxuLmN1c3RvbWVyLWNvbCB7XG4gICAgbWFyZ2luOiAycHggYXV0bztcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgd2lkdGg6IDUwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMztcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuXG4ub2ZmaWNlLWNvbCB7XG4gICAgbWFyZ2luOiAycHggYXV0bztcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgd2lkdGg6IDUwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuXG4uY3VzdG9tZXItbGFiZWwge1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0zO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xufVxuXG4ub2ZmaWNlLWxhYmVsIHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbn1cblxuLnRhbmstaW1nIHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgLy8gd2lkdGg6IDEwMCU7XG4gICAgLy8gbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG4udGFuay1pbWctc2tlbGV0b257XG4gICAgYmFja2dyb3VuZC1zaXplOiAxLjVyZW0gMS41cmVtO1xuICAgIGhlaWdodDogMnJlbTtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMwMDRDNzMgMCUsICMwMDU1ODAgMjAlLCAjMDA0QzczIDQwJSwgIzAwNEM3MyAxMDAlKTtcbn1cblxuXG4ub3RoZXItdGFua3N7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tTWVkaXVtJztcbiAgICAvLyBtYXJnaW46IGF1dG87XG4gICAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbn1cblxuLm90aGVyLW51bWJlcntcbiAgICBmb250LXNpemU6IDEuMjVyZW07XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDhweDtcbn1cblxuLmRpc2FibGUgeyBcbiAgICBvcGFjaXR5OiAwLjU7XG59XG5cblxuXG5cbiJdfQ== */";
    /***/
  },

  /***/
  "./src/app/drive/drive.page.ts":
  /*!*************************************!*\
    !*** ./src/app/drive/drive.page.ts ***!
    \*************************************/

  /*! exports provided: DrivePage */

  /***/
  function srcAppDriveDrivePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DrivePage", function () {
      return DrivePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic-native/launch-navigator/ngx */
    "./node_modules/@ionic-native/launch-navigator/ngx/index.js");
    /* harmony import */


    var _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../shared/services/loader.service */
    "./src/shared/services/loader.service.ts");
    /* harmony import */


    var _shared_services_routing_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../shared/services/routing.service */
    "./src/shared/services/routing.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../node_modules/@ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../shared/services/sap-api.service */
    "./src/shared/services/sap-api.service.ts");
    /* harmony import */


    var _shared_services_constant_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../shared/services/constant.service */
    "./src/shared/services/constant.service.ts");
    /* harmony import */


    var src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! src/shared/services/alert.service */
    "./src/shared/services/alert.service.ts");
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/ngx/index.js");
    /* harmony import */


    var _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @ionic-native/location-accuracy/ngx */
    "./node_modules/@ionic-native/location-accuracy/ngx/index.js");
    /* harmony import */


    var _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../shared/services/lauch-navigator.service */
    "./src/shared/services/lauch-navigator.service.ts");
    /* harmony import */


    var src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! src/shared/services/toast.service */
    "./src/shared/services/toast.service.ts"); // import { GoogleMap, ILatLng, GoogleMaps, BaseArrayClass, GoogleMapsAnimation, MarkerOptions, Marker } from '../../../node_modules/@ionic-native/google-maps';
    // import { CallNumber } from '@ionic-native/call-number/ngx';


    var DrivePage =
    /*#__PURE__*/
    function () {
      function DrivePage(geolocation, launchNavigator, loaderService, routingService, router, platform, route, sapApiService, constantService, domCtrl, // private callNumber: CallNumber,
      alertService, alertController, androidPermissions, locationAccuracy, launchNavService, toastService) {
        _classCallCheck(this, DrivePage);

        this.geolocation = geolocation;
        this.launchNavigator = launchNavigator;
        this.loaderService = loaderService;
        this.routingService = routingService;
        this.router = router;
        this.platform = platform;
        this.route = route;
        this.sapApiService = sapApiService;
        this.constantService = constantService;
        this.domCtrl = domCtrl;
        this.alertService = alertService;
        this.alertController = alertController;
        this.androidPermissions = androidPermissions;
        this.locationAccuracy = locationAccuracy;
        this.launchNavService = launchNavService;
        this.toastService = toastService;
        this.sampleLat = 18.649664500000002;
        this.sampleLong = 129.06789579999997;
        this.routes = [];
        this.isFromChecklist = false;
        this.isMapLoaded = false;
        this.showskeleton = true;
        this.titleSize = "large";
        this.routed = [];
        this.active = [];
      }

      _createClass(DrivePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {// let disconnectSubs = this.network.onDisconnect().subscribe(
          //   () => {
          //     console.log('Network was disconnected.');
          //     window.alert("Network was disconnected");
          //   }
          // )
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.platform.ready();

                  case 2:
                    console.log("map*** ", this.map); // for google map plugin
                    // for javascript map
                    // this.loadMap();
                    // if (this.platform.is("android")) {
                    //   this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
                    //     result => {
                    //       console.log("request Permission", result);
                    //       this.showskeleton = true;
                    //       this.checkIfFromChecklist();
                    //       // this.confirmDelivery();
                    //       if (!localStorage.getItem("material_code")) {
                    //         this.getMaterialCode();
                    //       }
                    //       if (!localStorage.getItem("start_time") && localStorage.getItem("route_id")) {
                    //         localStorage.setItem("start_time", this.sapApiService.getConfirmationDateTime())
                    //       }
                    //     }
                    //   )
                    // }
                    // else {

                    this.showskeleton = true;
                    this.checkIfFromChecklist(); // this.confirmDelivery();

                    if (!localStorage.getItem("material_code")) {
                      this.getMaterialCode();
                    }

                    if (!localStorage.getItem("start_time") && localStorage.getItem("route_id")) {
                      localStorage.setItem("start_time", this.sapApiService.getConfirmationDateTime());
                    } // }


                  case 7:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          console.log("Leaving Page...");
          this.map.clear();
        }
      }, {
        key: "confirmDelivery",
        value: function confirmDelivery() {
          this.router.navigate(["/tabs/drive/1/confirmation"]);
        }
      }, {
        key: "getMaterialCode",
        value: function getMaterialCode() {
          this.constantService.getMaterialCodes().subscribe(function (result) {
            localStorage.setItem("material_code", JSON.stringify(result));
          }, function (error) {
            console.log("Error", error);
          });
        } // generateInitialMap() {
        //   return new Promise(function (resolve, reject) {
        //     console.log("Is map loaded", this.isMapLoaded);
        //     if (!this.isMapLoaded) {
        //       console.log("Is map loaded 2", this.isMapLoaded);
        //       this.getLocation().then(result => {
        //         if (result) {
        //           console.log("Waiting for location...");
        //           let mapOptions: GoogleMapOptions = {
        //             camera: {
        //               target: {
        //                 lat: this.latitude,
        //                 lng: this.longitude
        //               },
        //               zoom: 18,
        //               tilt: 30
        //             }
        //           };
        //           this.showskeleton = false;
        //           this.map = GoogleMaps.create('map_canvas', mapOptions);
        //           this.map.clear();
        //           this.isMapLoaded = true;
        //           resolve(true);
        //         }
        //       })
        //     } else {
        //       console.log("Unable to Get Location.");
        //       resolve(false);
        //     }
        //   }.bind(this));
        // }

      }, {
        key: "generateInitialMap",
        value: function generateInitialMap() {
          return new Promise(function (resolve, reject) {
            var _this = this;

            console.log("Is map loaded", this.isMapLoaded);

            if (!this.isMapLoaded) {
              this.map = null;
              console.log("Is map loaded 2", this.isMapLoaded);
              this.getLocation().then(function (result) {
                if (result) {
                  console.log("Waiting for location...");
                  var mapOptions = {
                    camera: {
                      target: {
                        lat: _this.latitude,
                        lng: _this.longitude
                      },
                      zoom: 18,
                      tilt: 30
                    }
                  };
                  _this.showskeleton = false;
                  var markerIcon = new google.maps.MarkerImage('https://hxreu2devcynch.blob.core.windows.net/images/driverapp/Map-Pin-Cynch@3x.png', null, null, null, new google.maps.Size(42, 68));
                  var myLoc = {
                    lat: _this.latitude,
                    lng: _this.longitude
                  };
                  _this.map = new google.maps.Map(document.getElementById('map'), {
                    center: {
                      lat: _this.latitude,
                      lng: _this.longitude
                    },
                    zoom: 18
                  });
                  var marker = new google.maps.Marker({
                    position: myLoc,
                    icon: markerIcon,
                    map: _this.map
                  }); // this.map = GoogleMaps.create('map_canvas', mapOptions);
                  // this.map.clear();

                  _this.isMapLoaded = true;
                  resolve(true);
                }
              });
            } else {
              console.log("Unable to Get Location.");
              resolve(false);
            }
          }.bind(this));
        }
      }, {
        key: "checkIfFromChecklist",
        value: function checkIfFromChecklist() {
          var _this2 = this;

          this.generateInitialMap().then(function (result) {
            if (localStorage.getItem("route_id")) {
              _this2.routeId = localStorage.getItem("route_id");
              _this2.stopSequenceNum = parseInt(localStorage.getItem("stop_sequence"));
              _this2.currentRoute = JSON.parse(localStorage.getItem("current_route_started"));
              _this2.isFromChecklist = true;
              _this2.stopsLength = parseInt(localStorage.getItem("stops_length"));

              if (_this2.stopSequenceNum > _this2.stopsLength) {
                _this2.router.navigate(["/tabs/drive/".concat(_this2.currentRoute.routeId, "/delivery-summary")]);
              } else {
                _this2.getDeliveryDetails();
              }
            } else {
              _this2.currentUser = JSON.parse(localStorage.getItem("current_user"));
              _this2.isFromChecklist = false;

              _this2.generateMap();

              _this2.getUserRoute();
            }
          });
        }
      }, {
        key: "getTankQuantity",
        value: function getTankQuantity() {
          if (this.currentDelivery.orderSource.toUpperCase() === "CYNCH") {
            this.isCynchOrder = true;
          } else {
            this.isCynchOrder = false;
          }

          var quantity = 0;
          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = this.currentDelivery.items[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var item = _step.value;
              quantity += item.quantity;
            }
          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }

          if (quantity > 1) {
            this.tankQuantity = quantity + " tanks";
          } else {
            this.tankQuantity = quantity + " tank";
          }

          console.log("Quantity", this.tankQuantity);
        }
      }, {
        key: "loadMap",
        value: function loadMap() {
          var latLng = new google.maps.LatLng(40.774102, -73.971734);
          var mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions); // const dakota = { lat: 40.7767644, lng: -73.9761399 };
          // const frick = { lat: 40.771209, lng: -73.9673991 };
        }
      }, {
        key: "computeMileage",
        value: function computeMileage() {
          var _this3 = this;

          this.getLocation().then(function (result) {
            var previousLoc = JSON.parse(localStorage.getItem("previous_loc"));

            if (result) {
              var directionsService = new google.maps.DirectionsService();
              var directionsRenderer = new google.maps.DirectionsRenderer();
              var route = {
                origin: {
                  lat: previousLoc.latitude,
                  lng: previousLoc.longitude
                },
                destination: {
                  lat: _this3.currentDelivery.latitude,
                  lng: _this3.currentDelivery.longitude
                },
                travelMode: 'DRIVING'
              };
              directionsService.route(route, function (response, status) {
                if (status !== 'OK') {
                  window.alert('Directions request failed due to ' + status);
                  return;
                } else {
                  // directionsRenderer.setDirections(response);
                  var directionsData = response.routes[0].legs[0];
                  var mileage;

                  if (!directionsData) {
                    window.alert('Directions request failed');
                    return;
                  } else {
                    var data = directionsData.distance.text.split(" ");

                    if (data[1] === "ft") {
                      mileage = (parseFloat(data[0]) / 5280).toFixed(4);
                    } else {
                      mileage = data[0];
                    }

                    localStorage.setItem("mileage", mileage);
                    console.log("Data", directionsData);
                  }
                }
              });
            }
          });
        }
      }, {
        key: "getUserRoute",
        value: function getUserRoute() {
          var _this4 = this;

          // this.loaderService.createLoader("Loading...");
          this.sapApiService.getUserRouteShell().subscribe(function (result) {
            if (result.length === 0) {
              _this4.routingService.getUserRoute().subscribe(function (result) {
                _this4.showskeleton = false;
                _this4.routes = []; // let book = result;

                _this4.routes = result;
                console.log("res", _this4.routes);
                _this4.active = [];
                _this4.routed = []; // for(let booked of book){
                //   if(booked.routeStatus.routeStatusId.toString() == "001"){
                //     this.active.push(booked);
                //   }else if(booked.routeStatus.routeStatusId.toString() == "006"){
                //     this.routed.push(booked);
                //   }
                // }

                console.log("Success", result);

                _this4.loaderService.dismissLoader();
              }, function (error) {
                console.log("Error", error);
                _this4.showskeleton = false;

                _this4.loaderService.dismissLoader();
              });
            } else {
              console.log("Result r", result);
              _this4.showskeleton = false;
              var book = result;
              _this4.routes = [];

              _this4.routes.push(book[0]);

              _this4.loaderService.dismissLoader();

              console.log("routes", _this4.routes);
            }
          }, function (error) {
            console.log("Error", error);
            _this4.showskeleton = false;

            _this4.loaderService.dismissLoader();
          });
        } // getLocation() {
        //   let that = this;
        //   return new Promise(function (resolve, reject) {
        //     if (that.platform.is("android")) {
        //       console.log("is android");
        //       that.locationAccuracy.canRequest().then((canRequest: boolean) => {
        //         console.log('Can Request: ' + canRequest);
        //         if (canRequest) {
        //           // the accuracy option will be ignored by iOS
        //           that.locationAccuracy.request(that.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        //             () => {
        //               console.log('Location is successfully turned on');
        //               that.geolocation.getCurrentPosition().then((resp) => {
        //                 that.latitude = resp.coords.latitude;
        //                 that.longitude = resp.coords.longitude;
        //                 console.log("Current Location", that.latitude);
        //                 console.log("Current Location", that.longitude);
        //                 resolve(true);
        //               }).catch((error) => {
        //                 console.log('Error getting location', error);
        //                 resolve(false);
        //               });
        //             },
        //             error => {
        //               console.log("error", error);
        //             }
        //           );
        //         }
        //         else {
        //           that.geolocation.getCurrentPosition().then((resp) => {
        //             that.locationAccuracy.canRequest().then((canRequest: boolean) => {
        //               console.log('Can Request 2: ', canRequest);
        //               if (canRequest) {
        //                 // the accuracy option will be ignored by iOS
        //                 that.locationAccuracy.request(that.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        //                   () => {
        //                     console.log('Location is successfully turned on');
        //                     that.latitude = resp.coords.latitude;
        //                     that.longitude = resp.coords.longitude;
        //                     console.log("Current Location", that.latitude);
        //                     console.log("Current Location", that.longitude);
        //                     resolve(true);
        //                   },
        //                   error => {
        //                     console.log("error", error);
        //                   });
        //               }
        //             }).catch((error) => {
        //               console.log('Error getting location', error);
        //               resolve(false);
        //             });
        //           });
        //         }
        //       });
        //     }
        //     else {
        //       that.geolocation.getCurrentPosition().then((resp) => {
        //         that.latitude = resp.coords.latitude;
        //         that.longitude = resp.coords.longitude;
        //         console.log("Current Location", that.latitude);
        //         console.log("Current Location", that.longitude);
        //         resolve(true);
        //       }).catch((error) => {
        //         console.log('Error getting location', error);
        //         resolve(false);
        //       });
        //     }
        //   });
        // }
        // addPermission(androidPermission: any) {
        //   return new Promise((resolve, reject) => {
        //     let permissions = this.androidPermissions;
        //     permissions.requestPermission(androidPermission).then(status => {
        //       console.log("Request for Permission:", status.hasPermission);
        //       if (status.hasPermission) {
        //         resolve(true);
        //       } else {
        //         reject(false);
        //       }
        //     })
        //   });
        // }
        // getLocationFunction() {
        //   return new Promise(function (resolve, reject) {
        //     this.geolocation.getCurrentPosition().then((resp) => {
        //       this.latitude = resp.coords.latitude;
        //       this.longitude = resp.coords.longitude;
        //       console.log("Current Location", this.latitude);
        //       console.log("Current Location", this.longitude);
        //       resolve(true);
        //     }).catch((error) => {
        //       console.log('Error getting location', error);
        //       resolve(false);
        //     });
        //   }.bind(this));
        // }

      }, {
        key: "getLocation",
        value: function getLocation() {
          return new Promise(function (resolve, reject) {
            var _this5 = this;

            this.geolocation.getCurrentPosition().then(function (resp) {
              _this5.latitude = resp.coords.latitude;
              _this5.longitude = resp.coords.longitude;
              console.log("Current Location", _this5.latitude);
              console.log("Current Location", _this5.longitude);
              resolve(true);
            }).catch(function (error) {
              if (error.code == 1) {
                _this5.toastService.presentToast("Please enable location services to continue");
              } else if (error.code == 2) {
                _this5.toastService.presentToast("Location Unavailable");
              } else if (error.code == 3) {
                _this5.toastService.presentToast("Request Timeout");
              }

              console.log('Error getting location', error);
              resolve(false);
            });
          }.bind(this));
        }
      }, {
        key: "generateMap",
        value: function generateMap() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var depotLatitude, depotLongitude, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, storage, search_address, map, that, geocoder, longitude, latitude;

            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    if (this.isFromChecklist) {
                      _context2.next = 25;
                      break;
                    }

                    console.log("Current User", this.currentUser);
                    _iteratorNormalCompletion2 = true;
                    _didIteratorError2 = false;
                    _iteratorError2 = undefined;
                    _context2.prev = 5;

                    for (_iterator2 = this.currentUser.msa.plants[0].storages[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                      storage = _step2.value;

                      if (storage.isCylinderLoc) {
                        depotLatitude = storage.latitude;
                        depotLongitude = storage.longitude;
                      }
                    }

                    _context2.next = 13;
                    break;

                  case 9:
                    _context2.prev = 9;
                    _context2.t0 = _context2["catch"](5);
                    _didIteratorError2 = true;
                    _iteratorError2 = _context2.t0;

                  case 13:
                    _context2.prev = 13;
                    _context2.prev = 14;

                    if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                      _iterator2.return();
                    }

                  case 16:
                    _context2.prev = 16;

                    if (!_didIteratorError2) {
                      _context2.next = 19;
                      break;
                    }

                    throw _iteratorError2;

                  case 19:
                    return _context2.finish(16);

                  case 20:
                    return _context2.finish(13);

                  case 21:
                    console.log("delivery", this.currentDelivery); // await this.map.moveCamera({
                    //   target: {
                    //     lat: depotLatitude,
                    //     lng: depotLongitude
                    //   }
                    // })

                    this.setMapMarker(depotLongitude, depotLatitude);
                    _context2.next = 26;
                    break;

                  case 25:
                    if (!this.isCynchOrder) {
                      search_address = "".concat(this.currentDelivery.street, " ").concat(this.currentDelivery.city, ", ").concat(this.currentDelivery.state);
                      map = this.map;
                      that = this;
                      geocoder = new google.maps.Geocoder();
                      geocoder.geocode({
                        address: search_address
                      }, function (results, status) {
                        console.log("Result", results); // map.moveCamera({
                        //   target: {
                        //     lat: results[0].geometry.location.lat(),
                        //     lng: results[0].geometry.location.lng()
                        //   }
                        // })

                        latitude = results[0].geometry.location.lat();
                        longitude = results[0].geometry.location.lng();
                        var locCoor = new google.maps.LatLng(latitude, longitude);
                        console.log("LOC COOR **** ", locCoor);
                        var markerIcon = new google.maps.MarkerImage('https://hxreu2devcynch.blob.core.windows.net/images/driverapp/Map-Pin-Cynch@3x.png', null, null, null, new google.maps.Size(42, 68));
                        this.map = new google.maps.Map(document.getElementById('map'), {
                          center: {
                            lat: this.latitude,
                            lng: this.longitude
                          },
                          zoom: 18
                        });
                        var marker = new google.maps.Marker({
                          position: locCoor,
                          icon: markerIcon,
                          map: this.map
                        });
                        that.setMapMarker(results[0].geometry.location.lng(), results[0].geometry.location.lat());
                        that.currentDelivery.longitude = results[0].geometry.location.lng();
                        that.currentDelivery.latitude = results[0].geometry.location.lat();
                        localStorage.setItem("current_delivery", JSON.stringify(that.currentDelivery));
                      });
                    } else {
                      console.log("Test", this.currentDelivery.latitude); // await this.map.moveCamera({
                      //   target: {
                      //     lat: this.currentDelivery.latitude,
                      //     lng: this.currentDelivery.longitude
                      //   }
                      // })
                      // await this.map.map.setCenter(new google.maps.LatLng(this.currentDelivery.latitude, this.currentDelivery.longitude))

                      this.setMapMarker(this.currentDelivery.longitude, this.currentDelivery.latitude);
                    }

                  case 26:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this, [[5, 9, 13, 21], [14,, 16, 20]]);
          }));
        }
      }, {
        key: "setMapMarker",
        value: function setMapMarker(long, lat) {
          console.log("***LONG* ", long);
          console.log("***LAT* ", lat);
          var markerIcon;
          var markerTitle;
          this.map = null;

          if (this.isCynchOrder) {
            markerIcon = new google.maps.MarkerImage('https://hxreu2devcynch.blob.core.windows.net/images/driverapp/Map-Pin-Cynch@3x.png', null, null, null, new google.maps.Size(42, 68));
            markerTitle = "Delivery Marker"; // let latlng = new google.maps.LatLng(lat,long);
            // this.map = new google.maps.Map(document.getElementById('map'), {
            //   center: { lat: lat, lng: long },
            //   zoom: 18
            // });
            // let marker = new google.maps.Marker({
            //   position: latlng,
            //   icon: markerIcon,
            //   map: this.map});
            //End Marker Code
          } else if (!this.isFromChecklist) {
            markerIcon = new google.maps.MarkerImage('https://hxreu2devcynch.blob.core.windows.net/images/driverapp/Map-Pin-Cynch@3x.png', null, null, null, new google.maps.Size(42, 68));
            markerTitle = "Depot Marker";
          } else {
            markerIcon = new google.maps.MarkerImage('https://hxreu2devcynch.blob.core.windows.net/images/driverapp/Map-Pin-Amerigas@3x.png', null, null, null, new google.maps.Size(42, 68));
            markerTitle = "Delivery Marker";
          }

          var latlng = new google.maps.LatLng(lat, long);
          this.map = new google.maps.Map(document.getElementById('map'), {
            center: {
              lat: lat,
              lng: long
            },
            zoom: 18
          });
          var marker = new google.maps.Marker({
            position: latlng,
            icon: markerIcon,
            map: this.map,
            title: markerTitle
          });
        }
      }, {
        key: "createCanvas",
        value: function createCanvas() {
          var canvas, context;
          canvas = document.createElement("canvas");
          var x = 0,
              y = -1,
              width = 45,
              height = 15,
              radius = 0,
              stroke = true;
          canvas.width = 50;
          canvas.height = 30;
          context = canvas.getContext("2d");

          if (typeof stroke == "undefined") {
            stroke = true;
          }

          if (typeof radius == "undefined") {
            radius = 5;
          } //Begin changing location


          context.beginPath();
          context.moveTo(x, y - 10);
          context.textAlign = "center";
          context.fillStyle = "white";
          context.font = "1rem Montserrat-Bold";
          context.fillText(this.stopSequenceNum, 25, 12);
          var pngUrl = canvas.toDataURL("image/jpg");
          return pngUrl;
        }
      }, {
        key: "navigateToStop",
        value: function navigateToStop() {
          var _this6 = this;

          this.getLocation().then(function (result) {
            if (result) {
              _this6.launchNavService.checkExistingSelectedMap().then(function (result) {
                if (result) {
                  _this6.launchNavService.getUserSelectedMap().then(function (preferredMap) {
                    console.log("Selected", preferredMap);

                    if (preferredMap === "this.launchNavigator.APP.APPLE_MAPS") {
                      var options = {
                        start: "".concat(_this6.latitude, ", ").concat(_this6.longitude),
                        app: _this6.launchNavigator.APP.APPLE_MAPS
                      };

                      _this6.launchNavigatorMapApp(options);
                    } else if (preferredMap === "this.launchNavigator.APP.WAZE") {
                      var _options = {
                        start: "".concat(_this6.latitude, ", ").concat(_this6.longitude),
                        app: _this6.launchNavigator.APP.WAZE
                      };

                      _this6.launchNavigatorMapApp(_options);
                    } else if (preferredMap === "this.launchNavigator.APP.GOOGLE_MAPS") {
                      var _options2 = {
                        start: "".concat(_this6.latitude, ", ").concat(_this6.longitude),
                        app: _this6.launchNavigator.APP.GOOGLE_MAPS
                      };

                      _this6.launchNavigatorMapApp(_options2);
                    } else {
                      var _options3 = {
                        start: "".concat(_this6.latitude, ", ").concat(_this6.longitude)
                      };

                      _this6.launchNavigatorMapApp(_options3);
                    }
                  });
                } else {
                  var options = {
                    start: "".concat(_this6.latitude, ", ").concat(_this6.longitude),
                    appSelection: {
                      rememberChoice: {
                        enabled: true
                      }
                    }
                  };

                  _this6.launchNavigatorMapApp(options);

                  console.log("None selected");
                }
              });
            }
          });
        }
      }, {
        key: "launchNavigatorMapApp",
        value: function launchNavigatorMapApp(options) {
          this.launchNavigator.navigate("".concat(this.currentDelivery.street, " ").concat(this.currentDelivery.city, ", ").concat(this.currentDelivery.state), options).then(function (success) {
            console.log("Success Return");
          }, function (error) {
            console.log("Error", error);
          });
        }
      }, {
        key: "openMapApp",
        value: function openMapApp(route) {
          var _this7 = this;

          this.openRouteDetails(route);
          this.getLocation().then(function (result) {
            if (result) {
              var options = {
                start: "".concat(_this7.latitude, ", ").concat(_this7.longitude)
              };
              var depotStreet;
              var depotCity;
              var depotState;
              var _iteratorNormalCompletion3 = true;
              var _didIteratorError3 = false;
              var _iteratorError3 = undefined;

              try {
                for (var _iterator3 = _this7.currentUser.msa.plants[0].storages[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                  var storage = _step3.value;

                  if (storage.isCylinderLoc) {
                    depotStreet = storage.street;
                    depotCity = storage.city;
                    depotState = storage.state; // depotLatitude = storage.latitude;
                    // depotLongitude = storage.longitude;
                  }
                }
              } catch (err) {
                _didIteratorError3 = true;
                _iteratorError3 = err;
              } finally {
                try {
                  if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                    _iterator3.return();
                  }
                } finally {
                  if (_didIteratorError3) {
                    throw _iteratorError3;
                  }
                }
              }

              _this7.launchNavigator.navigate("".concat(depotStreet, " ").concat(depotCity, ", ").concat(depotState), options).then(function (success) {
                console.log("Success Return");
              }, function (error) {
                console.log("Error", error);
              });
            }
          });
        }
      }, {
        key: "openDeliveryDetails",
        value: function openDeliveryDetails() {
          this.router.navigate(["/tabs/drive/".concat(this.currentDelivery.stopId)]);
        }
      }, {
        key: "openRouteDetails",
        value: function openRouteDetails(route) {
          localStorage.setItem("current_route", JSON.stringify(route));
          this.router.navigate(["/tabs/routes"]);
        }
      }, {
        key: "getDeliveryDetails",
        value: function getDeliveryDetails() {
          var _this8 = this;

          this.sapApiService.getRouteDetails(this.currentRoute.sapRouteId, this.stopSequenceNum).subscribe(function (result) {
            console.log("Result", result);
            _this8.showskeleton = false; // this.currentDelivery = result.delivery;

            _this8.currentDelivery = result;
            _this8.currentDelivery.routeId = _this8.currentRoute.routeId; // this.currentDelivery.totalRemainingCyls = this.currentRoute.totalRemainingCyls;

            console.log("Current Delivery Drive", _this8.currentDelivery);
            console.log("Current Routes Drive", _this8.currentRoute);

            _this8.getTankQuantity();

            localStorage.setItem("current_delivery", JSON.stringify(result));
            localStorage.setItem("stops_length", JSON.stringify(_this8.currentDelivery.stopsLength));

            _this8.computeMileage();

            _this8.generateMap();
          });
        }
      }, {
        key: "onContentScroll",
        value: function onContentScroll(ev) {
          var _this9 = this;

          this.domCtrl.write(function () {
            if (ev.detail.scrollTop > 50) {
              _this9.titleSize = undefined;
            } else {
              _this9.titleSize = "large";
            }
          });
        } // useCall(source: string) {
        //   switch (source) {
        //     case "customer":
        //       this.callNumber.callNumber(this.currentDelivery.customerPhone, true)
        //         .then(res => console.log('Launched dialer!', res))
        //         .catch(err => console.log('Error launching dialer', err));
        //       break;
        //     case "office":
        //       this.callNumber.callNumber("18885252899", true)
        //         .then(res => console.log('Launched dialer!', res))
        //         .catch(err => console.log('Error launching dialer', err));
        //       break;
        //     default:
        //       this.callNumber.callNumber("18885252899", true)
        //         .then(res => console.log('Launched dialer!', res))
        //         .catch(err => console.log('Error launching dialer', err));
        //       break;
        //   }
        // }
        //for test only

      }, {
        key: "noTankLeftOutConfirmation",
        value: function noTankLeftOutConfirmation() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee3() {
            var _this10 = this;

            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    this.alertService.createNTLOAlerts("Once you start the NTLO process you cannot go back", "Proceed", "Cancel").then(function (resp) {
                      if (resp) {
                        // let message = "Check to make sure you are at the correct location. \n" +
                        //   "Are you at \n" + this.currentDelivery.street
                        // "," + this.currentDelivery.state + this.currentDelivery.zipcode;
                        var message = "Test";

                        _this10.alertService.createNTLOAlerts(message, "Yes, continue", "Wrong address", "NTLO").then(function (result) {
                          if (result) {
                            message = "Knock on the front door. If someone answers, ask if they\n                have an empty tank to bring out for you";

                            _this10.alertService.createNTLOAlerts(message, "No tank", "Customer is home", "NTLO").then(function (response) {
                              if (response) {
                                _this10.createNTLOCallAlert("Try calling the customer", "No Tank", "Tank found", "NTLO").then(function (result) {
                                  if (result) {
                                    message = "Look on the back porch and the sides of the house.";

                                    _this10.alertService.createNTLOAlerts(message, "No tank", "Tank Found", "NTLO").then(function (response) {
                                      if (response) {
                                        message = "Check that tank on the grill. If the tank is completely empty, exchange that tank\n                                      and tap Exchanged Tank From Grill. If it is not completely empty, tap NTLO and continue to the next delivery.";

                                        _this10.alertService.createNTLOAlerts(message, "NTLO", "Exchanged Tank From Grill", "NTLO").then(function (result) {
                                          if (result) {//Update Delivery Here
                                          }
                                        });
                                      }
                                    });
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    });

                  case 1:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "createNTLOCallAlert",
        value: function createNTLOCallAlert(message, confirmText, cancelText, title) {
          return new Promise(function (resolve, reject) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
            /*#__PURE__*/
            regeneratorRuntime.mark(function _callee4() {
              var _this11 = this;

              var choiceAlert;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.alertController.create({
                        // message: "Once you start the NTLO process you cannot go back",
                        header: title,
                        message: message,
                        mode: "ios",
                        cssClass: "custom-alert",
                        buttons: [{
                          text: "Call Number",
                          handler: function handler() {
                            _this11.useCall("customer");
                          }
                        }, {
                          text: confirmText,
                          handler: function handler() {
                            resolve(true);
                          }
                        }, {
                          text: cancelText,
                          cssClass: "alert-cancel",
                          handler: function handler() {
                            resolve(false);
                          }
                        }]
                      });

                    case 2:
                      choiceAlert = _context4.sent;
                      _context4.next = 5;
                      return choiceAlert.present();

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }.bind(this));
        }
      }, {
        key: "updateDeliveryNTLO",
        value: function updateDeliveryNTLO() {
          var _this12 = this;

          this.currentRoute = JSON.parse(localStorage.getItem("current_route_started"));
          this.loaderService.createLoader("Submitting..."); // this.items = JSON.parse(localStorage.getItem("material_code"));

          var editedDelivery = JSON.parse(JSON.stringify(this.currentDelivery)); // let transferedItem: any
          // for (let item of editedDelivery.items) {
          //   if (item.materialCode === "CYL-204") {
          //     transferedItem = JSON.parse(JSON.stringify(item));
          //     transferedItem.transferType = "2";
          //   }
          // }
          // editedDelivery.items.push(transferedItem);

          console.log("Current Delivery", editedDelivery);
          editedDelivery.startTime = localStorage.getItem("start_time");
          editedDelivery.endTime = this.sapApiService.getConfirmationDateTime();
          editedDelivery.deliveryStatus.deliveryStatusId = "006";
          var tankDelivered = 0;
          var tankRemaining; // for (let item of editedDelivery.items) {
          //   tankDelivered += item.quantityDelivered;
          // }
          // tankRemaining = this.currentRoute.totalRemainingCyls;
          // editedDelivery.totalRemainingCyls = tankRemaining;

          if (localStorage.getItem("mileage")) {
            editedDelivery.mileage = parseFloat(localStorage.getItem("mileage"));
          } else {
            editedDelivery.mileage = 0;
          }

          editedDelivery.nonDeliveryReason = "130";
          this.sapApiService.updateDelivery(editedDelivery, this.currentRoute).subscribe(function (result) {
            console.log("Update Delivery", result); // this.currentRoute.totalRemainingCyls = tankRemaining;

            localStorage.setItem("current_route_started", JSON.stringify(_this12.currentRoute));
            localStorage.setItem("stop_sequence", _this12.stopSequenceNum.toString());
            _this12.stopsLength = result.stopsLength;
            localStorage.setItem("stops_length", _this12.stopsLength.toString()); // if (this.stopsLength < this.stopSequenceNum) {
            //   this.sapApiService.submitRoute(this.currentRoute.routeId, editedDelivery.endTime).subscribe(
            //     result => {
            //       console.log(result);
            //       this.clearFinishedDelivery();
            //     },
            //     error => {
            //       console.log("Error", error);
            //       this.loaderService.dismissLoader();
            //     }
            //   )
            // }

            if (_this12.stopsLength < _this12.stopSequenceNum) {
              // let routeId = localStorage.getItem("route_id");
              _this12.router.navigate(["/tabs/drive/".concat(_this12.currentRoute.routeId, "/delivery-summary")]);
            } else {
              localStorage.removeItem("start_time");

              _this12.router.navigate(["/tabs/drive"]);
            } // this.router.navigate([`/tabs/drive`]);


            _this12.loaderService.dismissLoader();
          }, function (error) {
            console.log("Error", error);

            _this12.loaderService.dismissLoader();
          });
        }
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee5() {
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ionViewDidEnter();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }]);

      return DrivePage;
    }();

    DrivePage.ctorParameters = function () {
      return [{
        type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_2__["Geolocation"]
      }, {
        type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_3__["LaunchNavigator"]
      }, {
        type: _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"]
      }, {
        type: _shared_services_routing_service__WEBPACK_IMPORTED_MODULE_5__["RoutingService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
      }, {
        type: _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]
      }, {
        type: _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_8__["SapApiService"]
      }, {
        type: _shared_services_constant_service__WEBPACK_IMPORTED_MODULE_9__["ConstantService"]
      }, {
        type: _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["DomController"]
      }, {
        type: src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_10__["AlertService"]
      }, {
        type: _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_11__["AndroidPermissions"]
      }, {
        type: _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_12__["LocationAccuracy"]
      }, {
        type: _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_13__["LaunchNavigatorService"]
      }, {
        type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_14__["ToastService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map', {
      static: false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], DrivePage.prototype, "mapElement", void 0);
    DrivePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-drive',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./drive.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/drive.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./drive.page.scss */
      "./src/app/drive/drive.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_2__["Geolocation"], _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_3__["LaunchNavigator"], _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"], _shared_services_routing_service__WEBPACK_IMPORTED_MODULE_5__["RoutingService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"], _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_8__["SapApiService"], _shared_services_constant_service__WEBPACK_IMPORTED_MODULE_9__["ConstantService"], _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["DomController"], src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_10__["AlertService"], _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_11__["AndroidPermissions"], _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_12__["LocationAccuracy"], _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_13__["LaunchNavigatorService"], src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_14__["ToastService"]])], DrivePage);
    /***/
  },

  /***/
  "./src/shared/pipes/convert24hrto12hr.pipe.ts":
  /*!****************************************************!*\
    !*** ./src/shared/pipes/convert24hrto12hr.pipe.ts ***!
    \****************************************************/

  /*! exports provided: Convert24hrto12hrPipe */

  /***/
  function srcSharedPipesConvert24hrto12hrPipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Convert24hrto12hrPipe", function () {
      return Convert24hrto12hrPipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var Convert24hrto12hrPipe =
    /*#__PURE__*/
    function () {
      function Convert24hrto12hrPipe() {
        _classCallCheck(this, Convert24hrto12hrPipe);
      }

      _createClass(Convert24hrto12hrPipe, [{
        key: "transform",
        value: function transform(time) {
          var hour = time.split(':')[0];
          var min = time.split(':')[1];
          var part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
          min = (min + '').length == 1 ? "0".concat(min) : min;
          hour = hour > 12 ? hour - 12 : hour; // hour = parseInt(hour, 10);

          hour = (hour + '').length == 1 ? "".concat(hour) : hour;
          return "".concat(hour, ":").concat(min, " ").concat(part);
        }
      }]);

      return Convert24hrto12hrPipe;
    }();

    Convert24hrto12hrPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'convert24hrto12hr'
    })], Convert24hrto12hrPipe);
    /***/
  },

  /***/
  "./src/shared/pipes/custom-date.pipe.ts":
  /*!**********************************************!*\
    !*** ./src/shared/pipes/custom-date.pipe.ts ***!
    \**********************************************/

  /*! exports provided: CustomDatePipe */

  /***/
  function srcSharedPipesCustomDatePipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CustomDatePipe", function () {
      return CustomDatePipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var ordinals = ['th', 'st', 'nd', 'rd'];

    var CustomDatePipe =
    /*#__PURE__*/
    function () {
      function CustomDatePipe() {
        _classCallCheck(this, CustomDatePipe);
      }

      _createClass(CustomDatePipe, [{
        key: "transform",
        value: function transform(value, args) {
          var newValue;
          var valueArray = value.split('-');
          var year = valueArray[0];
          var month = valueArray[1];
          var day = valueArray[2];
          var today = new Date(Date.now()); // today.setUTCFullYear(parseInt(valueArray[0]));
          // today.setUTCMonth(parseInt(valueArray[1]) - 1);
          // today.setUTCDate(parseInt(valueArray[2]));
          // if(valueArray[3]) today.setUTCHours(parseInt(valueArray[3]));
          // if(valueArray[4]) today.setUTCMinutes(parseInt(valueArray[4]));
          // if(valueArray[5]) today.setUTCSeconds(parseInt(valueArray[5]));
          // console.log("valueArray", valueArray);
          // console.log("today", today);
          // let year = today.getFullYear();
          // let month = today.getMonth() + 1;
          // let day = today.getDate();

          var deliveryDate = new Date("".concat(month, "/").concat(day, "/").concat(year));

          if (args === "shortTime") {
            // let hour = valueArray[3];
            // let minute = valueArray[4];
            // newValue = hour + ":" + minute;
            var hour = parseInt(valueArray[3]);
            var min = valueArray[4];
            var part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
            min = (min + '').length == 1 ? "0".concat(min) : min;
            hour = hour > 12 ? hour - 12 : hour;
            var hours = (hour + '').length == 1 ? "".concat(hour) : hour;
            return "".concat(hours, ":").concat(min, " ").concat(part);
          }

          if (args === "shortWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'short'
            });
          }

          if (args === "day") {
            newValue = day;
          }

          if (args === "longWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'long'
            });
          }

          if (args === "deliveryDate") {
            var v = parseInt(day) % 100; // let v = (day % 100);

            var ordinalDay = (parseInt(day) > 9 ? day : parseInt(day)) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]); // let ordinalDay = (day > 9 ? day : day) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);

            var monthString = deliveryDate.toLocaleString('default', {
              month: 'long'
            });
            newValue = "".concat(monthString, " ").concat(ordinalDay, " ").concat(year);
          }

          if (args === "longDate") {
            var _monthString = deliveryDate.toLocaleString('default', {
              month: 'long'
            });

            newValue = "".concat(_monthString, " ").concat(day, ", ").concat(year);
          }

          return newValue;
        }
      }]);

      return CustomDatePipe;
    }();

    CustomDatePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'customDate'
    })], CustomDatePipe);
    /***/
  },

  /***/
  "./src/shared/pipes/custom-utcdate.pipe.ts":
  /*!*************************************************!*\
    !*** ./src/shared/pipes/custom-utcdate.pipe.ts ***!
    \*************************************************/

  /*! exports provided: CustomUTCDatePipe */

  /***/
  function srcSharedPipesCustomUtcdatePipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CustomUTCDatePipe", function () {
      return CustomUTCDatePipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var ordinals = ['th', 'st', 'nd', 'rd'];

    var CustomUTCDatePipe =
    /*#__PURE__*/
    function () {
      function CustomUTCDatePipe() {
        _classCallCheck(this, CustomUTCDatePipe);
      }

      _createClass(CustomUTCDatePipe, [{
        key: "transform",
        value: function transform(value, args) {
          var newValue;
          var valueArray = value.split('-'); // let year = valueArray[0];
          // let month = valueArray[1];
          // let day = valueArray[2];

          var today = new Date(Date.now());
          today.setUTCFullYear(parseInt(valueArray[0]));
          today.setUTCMonth(parseInt(valueArray[1]) - 1);
          today.setUTCDate(parseInt(valueArray[2]));
          if (valueArray[3]) today.setUTCHours(parseInt(valueArray[3]));
          if (valueArray[4]) today.setUTCMinutes(parseInt(valueArray[4]));
          if (valueArray[5]) today.setUTCSeconds(parseInt(valueArray[5])); // console.log("valueArray", valueArray);
          // console.log("today", today);

          var year = today.getFullYear();
          var month = today.getMonth() + 1;
          var day = today.getDate();
          var deliveryDate = new Date("".concat(month, "/").concat(day, "/").concat(year));

          if (args === "shortTime") {
            // let hour = valueArray[3];
            // let minute = valueArray[4];
            // newValue = hour + ":" + minute;
            var hour = today.getHours();
            var min = today.getMinutes();
            var part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
            var minute = (min + '').length == 1 ? "0".concat(min) : min;
            hour = hour > 12 ? hour - 12 : hour;
            var hours = (hour + '').length == 1 ? "".concat(hour) : hour;
            return "".concat(hours, ":").concat(minute, " ").concat(part);
          }

          if (args === "shortWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'short'
            });
          }

          if (args === "day") {
            newValue = day;
          }

          if (args === "longWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'long'
            });
          }

          if (args === "deliveryDate") {
            // let v = (parseInt(day)) % 100;
            var v = day % 100; // let ordinalDay = (parseInt(day) > 9 ? day : parseInt(day)) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);

            var ordinalDay = (day > 9 ? day : day) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);
            var monthString = deliveryDate.toLocaleString('default', {
              month: 'long'
            });
            newValue = "".concat(monthString, " ").concat(ordinalDay, " ").concat(year);
          }

          if (args === "longDate") {
            var _monthString2 = deliveryDate.toLocaleString('default', {
              month: 'long'
            });

            newValue = "".concat(_monthString2, " ").concat(day, ", ").concat(year);
          }

          return newValue;
        }
      }]);

      return CustomUTCDatePipe;
    }();

    CustomUTCDatePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'customUtcdate'
    })], CustomUTCDatePipe);
    /***/
  },

  /***/
  "./src/shared/services/constant.service.ts":
  /*!*************************************************!*\
    !*** ./src/shared/services/constant.service.ts ***!
    \*************************************************/

  /*! exports provided: ConstantService */

  /***/
  function srcSharedServicesConstantServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ConstantService", function () {
      return ConstantService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var ConstantService =
    /*#__PURE__*/
    function () {
      function ConstantService(http) {
        _classCallCheck(this, ConstantService);

        this.http = http;
      }

      _createClass(ConstantService, [{
        key: "getMaterialCodes",
        value: function getMaterialCodes() {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.constantsApiURL + "?mode=products", this.getHeaders());
        }
      }, {
        key: "getDeliveryTransferType",
        value: function getDeliveryTransferType() {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.constantsApiURL + "?mode=deliveryTransferType", this.getHeaders());
        }
      }, {
        key: "getStorageTransferType",
        value: function getStorageTransferType() {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.constantsApiURL + "?mode=storageTransferType", this.getHeaders());
        }
      }, {
        key: "getHeaders",
        value: function getHeaders() {
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
              'Authorization': 'Bearer ' + localStorage.getItem('token'),
              'Content-type': 'application/json'
            })
          };
          return httpOptions;
        }
      }]);

      return ConstantService;
    }();

    ConstantService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }];
    };

    ConstantService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])], ConstantService);
    /***/
  },

  /***/
  "./src/shared/services/routing.service.ts":
  /*!************************************************!*\
    !*** ./src/shared/services/routing.service.ts ***!
    \************************************************/

  /*! exports provided: RoutingService */

  /***/
  function srcSharedServicesRoutingServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RoutingService", function () {
      return RoutingService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var RoutingService =
    /*#__PURE__*/
    function () {
      function RoutingService(http) {
        _classCallCheck(this, RoutingService);

        this.http = http;
      }

      _createClass(RoutingService, [{
        key: "activeRoute",
        value: function activeRoute() {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.activeRouteURL, this.getHeaders());
        } // getUserRoute() {
        //     return this.http.get<Route[]>(environment.baseUrl + environment.services.bookingURL + `?mode=user`, this.getHeaders());
        // }

      }, {
        key: "routeCheckIn",
        value: function routeCheckIn(routeId, checkInTime) {
          var checkIn = {
            routeId: routeId,
            checkInTime: checkInTime
          };
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.checkInRouteURL, checkIn, this.getHeaders());
        }
      }, {
        key: "getModeUserRoute",
        value: function getModeUserRoute() {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeURL + "?mode=user", this.getHeaders());
        }
      }, {
        key: "getUserRoute",
        value: function getUserRoute() {
          var apiMode = {
            mode: "routeShell"
          };
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.sapApiURL, apiMode, this.getHeaders());
        }
      }, {
        key: "getUserRouteDetails",
        value: function getUserRouteDetails(routeId) {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeDetailURL + "?routeId=".concat(routeId), this.getHeaders());
        }
      }, {
        key: "cancelRoute",
        value: function cancelRoute(routeId) {
          var body = {
            routeId: routeId
          };
          return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeCancelURL, body, this.getHeaders());
        }
      }, {
        key: "getHeaders",
        value: function getHeaders() {
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
              'Authorization': 'Bearer ' + localStorage.getItem('token'),
              'Content-type': 'application/json'
            })
          };
          return httpOptions;
        }
      }]);

      return RoutingService;
    }();

    RoutingService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }];
    };

    RoutingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])], RoutingService);
    /***/
  },

  /***/
  "./src/shared/shared.module.ts":
  /*!*************************************!*\
    !*** ./src/shared/shared.module.ts ***!
    \*************************************/

  /*! exports provided: SharedModule */

  /***/
  function srcSharedSharedModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
      return SharedModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./pipes/custom-date.pipe */
    "./src/shared/pipes/custom-date.pipe.ts");
    /* harmony import */


    var _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./pipes/convert24hrto12hr.pipe */
    "./src/shared/pipes/convert24hrto12hr.pipe.ts");
    /* harmony import */


    var _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./pipes/custom-utcdate.pipe */
    "./src/shared/pipes/custom-utcdate.pipe.ts");

    var SharedModule = function SharedModule() {
      _classCallCheck(this, SharedModule);
    };

    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__["CustomDatePipe"], _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__["Convert24hrto12hrPipe"], _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__["CustomUTCDatePipe"]],
      entryComponents: [],
      imports: [],
      exports: [_pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__["CustomDatePipe"], _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__["Convert24hrto12hrPipe"], _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__["CustomUTCDatePipe"]],
      providers: []
    })], SharedModule);
    /***/
  }
}]);
//# sourceMappingURL=drive-drive-module-es5.js.map