(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["drive-signature-signature-module"],{

/***/ "./src/app/drive/signature/signature.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/drive/signature/signature.module.ts ***!
  \*****************************************************/
/*! exports provided: SignaturePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignaturePageModule", function() { return SignaturePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _signature_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signature.page */ "./src/app/drive/signature/signature.page.ts");
/* harmony import */ var ionicsignaturepad__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionicsignaturepad */ "./node_modules/ionicsignaturepad/dist/index.js");








const routes = [
    {
        path: '',
        component: _signature_page__WEBPACK_IMPORTED_MODULE_6__["SignaturePage"]
    }
];
let SignaturePageModule = class SignaturePageModule {
};
SignaturePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            ionicsignaturepad__WEBPACK_IMPORTED_MODULE_7__["IonicSignaturePadModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        exports: [
            ionicsignaturepad__WEBPACK_IMPORTED_MODULE_7__["IonicsignaturepadComponent"]
        ],
        declarations: [_signature_page__WEBPACK_IMPORTED_MODULE_6__["SignaturePage"]]
    })
], SignaturePageModule);



/***/ })

}]);
//# sourceMappingURL=drive-signature-signature-module-es2015.js.map