function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["routes-routes-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/routes/routes.component.html":
  /*!************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/routes/routes.component.html ***!
    \************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppRoutesRoutesComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <!-- <ion-title size=\"large\">Shyfts</ion-title> -->\n    <ion-title [size]=\"titleSize\">Routes</ion-title>\n    \n  </ion-toolbar>\n</ion-header>\n\n<ion-content\n  [scrollEvents]=\"true\" (ionScroll)=\"onContentScroll($event)\">\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-segment mode=\"md\" (ionChange)=\"segmentChanged($event)\" value=\"myRoutes\">\n    <ion-segment-button mode=\"ios\" [ngClass]=\"{'segment-class-selected' : currentSegment == 'myRoutes'}\"\n      class=\"segment-class\" value=\"myRoutes\">\n      <!-- <ion-label>My Shyfts</ion-label> -->\n      My Routes\n    </ion-segment-button>\n    <!-- <ion-segment-button mode=\"ios\" [ngClass]=\"{'segment-class-selected' : currentSegment == 'availablegigs'}\"\n      class=\"segment-class\" value=\"availablegigs\">\n      Available\n    </ion-segment-button> -->\n    <ion-segment-button mode=\"ios\" [ngClass]=\"{'segment-class-selected' : currentSegment == 'completedShyfts'}\"\n      class=\"segment-class\" value=\"completedShyfts\">\n      <!-- <ion-label>Available Shyfts</ion-label> -->\n      Completed\n    </ion-segment-button>\n  </ion-segment>\n\n  <!-- My shyfts section -->\n  <div *ngIf=\"currentSegment == 'myRoutes'\">\n    <ion-row>\n      <ion-col class=\"myGig-label\">\n        My Routes\n      </ion-col>\n    </ion-row>\n    <ion-list *ngIf=\"userRoutes\">\n\n      <ng-container *ngFor=\"let userRoute of userRoutes\">\n\n\n        <!-- <ion-item detail lines=\"none\" *ngIf=\"userRoute.routeStatus.routeStatusId == 006\" button=true -->\n        <ion-item detail lines=\"none\" *ngIf=\"userRoute\" button=true\n        [disabled]=\"hasRoute\"\n          (click)=\"openRouteDetails(userRoute)\">\n          <ion-row class=\"gig-row\">\n            <ion-col size=\"2\" class=\"left-assigned\">\n              <div class=\"left-date-assigned\">\n                {{userRoute.deliveryDate | customDate:'shortWeekDay'}}\n              </div>\n              <div class=\"left-day-assigned\">\n                <!-- <ion-label> -->\n                {{userRoute.deliveryDate | customDate:'day'}}\n                <!-- </ion-label> -->\n              </div>\n\n            </ion-col>\n            <ion-col size=\"7\" class=\"ion-padding-start\">\n              <p class=\"delivery-day-routed\">{{userRoute.deliveryDate | customDate:'longWeekDay'}}</p>\n              <p class=\"delivery-date-routed\">{{userRoute.deliveryDate | customDate:'deliveryDate'}}</p>\n              <p class=\"booking-time-routed\">\n                <span>{{userRoute.startTime | convert24hrto12hr}}</span> \n                <!-- <span>{{userRoute.endTime | convert24hrto12hr}}</span> -->\n              </p>\n            </ion-col>\n\n            <ion-col size=\"3\" class=\"right-assigned\">\n              <div class=\"right-panel-assigned\">\n                <ion-label class=\"right-label\">\n                  ROUTED\n                </ion-label>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n\n        <ion-item detail lines=\"none\" *ngIf=\"!userRoute\" button=true\n          (click)=\"openRouteDetails(userRoute)\">\n          <ion-row class=\"gig-row\">\n            <ion-col size=\"2\" class=\"left-requested\">\n              <div class=\"left-date-requested\">\n                {{userRoute?.deliveryDate | customDate:'shortWeekDay'}}\n              </div>\n              <div class=\"left-day-requested\">\n                {{userRoute?.deliveryDate | customDate:'day'}}\n              </div>\n\n            </ion-col>\n            <ion-col size=\"7\" class=\"ion-padding-start\">\n\n              <p class=\"delivery-day-scheduled\">{{userRoute.deliveryDate | customDate:'longWeekDay'}}</p>\n              <p class=\"delivery-date-scheduled\">{{userRoute.deliveryDate | customDate:'deliveryDate'}}</p>\n              <p class=\"booking-time-scheduled\">\n                <span>{{userRoute.startTime | convert24hrto12hr}}</span>\n                <!-- <span>{{userRoute.endTime | convert24hrto12hr}}</span> -->\n              </p>\n            </ion-col>\n            <ion-col size=\"3\" class=\"right-requested\">\n              <div class=\"right-panel-requested\">\n                <ion-label class=\"right-label\">\n                  SCHEDULED\n                </ion-label>\n              </div>\n            </ion-col>\n          </ion-row>\n\n        </ion-item>\n\n      </ng-container>\n\n    </ion-list>\n\n    <ion-list *ngIf=\"userRoutes.length == 0 && !skeletonBoolean\">\n        <div class=\"no-route-class\">There are no Available Routes right now.</div>\n    </ion-list>\n\n    <!-- skeleton here -->\n    <ion-list *ngIf=\"skeletonBoolean\">\n      <ng-container>\n        <ion-item detail lines=\"none\" button=true>\n          <ion-row class=\"gig-row\">\n            <ion-col size=\"2\" class=\"left-assigned-skeleton\">\n              <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n            </ion-col>\n            <ion-col size=\"7\" class=\"ion-padding-start\">\n              <p class=\"delivery-day-routed\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"delivery-date-routed\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"booking-time-routed\">\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n                <ion-skeleton-text animated></ion-skeleton-text>\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n              </p>\n            </ion-col>\n\n            <ion-col size=\"3\" class=\"right-assigned-skeleton\">\n              <div class=\"right-panel-assigned\">\n                <ion-label class=\"right-label\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </ion-label>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n\n        <ion-item detail lines=\"none\" button=true>\n          <ion-row class=\"gig-row\">\n            <ion-col size=\"2\" class=\"left-requested-skeleton\">\n              <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n\n            </ion-col>\n            <ion-col size=\"7\" class=\"ion-padding-start\">\n\n              <p class=\"delivery-day-scheduled\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"delivery-date-scheduled\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"booking-time-scheduled\">\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n                <ion-skeleton-text animated></ion-skeleton-text>\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n              </p>\n            </ion-col>\n            <ion-col size=\"3\" class=\"right-requested-skeleton\">\n              <div class=\"right-panel-requested\">\n                <ion-label class=\"right-label\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </ion-label>\n              </div>\n            </ion-col>\n          </ion-row>\n\n        </ion-item>\n\n        <ion-item detail lines=\"none\" button=true>\n          <ion-row class=\"gig-row\">\n            <ion-col size=\"2\" class=\"left-done-skeleton\">\n              <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n\n            </ion-col>\n            <ion-col size=\"7\" class=\"ion-padding-start\">\n              <p class=\"delivery-day-routed\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"delivery-date-routed\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"booking-time-routed\">\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span> \n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n              </p>\n            </ion-col>\n\n            <ion-col size=\"3\" class=\"right-done-skeleton\">\n              <div class=\"right-panel-done\">\n                <ion-label class=\"right-label\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </ion-label>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n\n        <ion-item detail lines=\"none\" button=true>\n          <ion-row class=\"gig-row\">\n            <ion-col size=\"2\" class=\"left-assigned-skeleton\">\n              <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n            </ion-col>\n            <ion-col size=\"7\" class=\"ion-padding-start\">\n              <p class=\"delivery-day-routed\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"delivery-date-routed\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"booking-time-routed\">\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n                <ion-skeleton-text animated></ion-skeleton-text>\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n              </p>\n            </ion-col>\n\n            <ion-col size=\"3\" class=\"right-assigned-skeleton\">\n              <div class=\"right-panel-assigned\">\n                <ion-label class=\"right-label\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </ion-label>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n\n        <ion-item detail lines=\"none\" button=true>\n          <ion-row class=\"gig-row\">\n            <ion-col size=\"2\" class=\"left-assigned-skeleton\">\n              <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n            </ion-col>\n            <ion-col size=\"7\" class=\"ion-padding-start\">\n              <p class=\"delivery-day-routed\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"delivery-date-routed\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"booking-time-routed\">\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n                <ion-skeleton-text animated></ion-skeleton-text>\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n              </p>\n            </ion-col>\n\n            <ion-col size=\"3\" class=\"right-assigned-skeleton\">\n              <div class=\"right-panel-assigned\">\n                <ion-label class=\"right-label\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </ion-label>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n\n        <ion-item detail lines=\"none\" button=true>\n          <ion-row class=\"gig-row\">\n            <ion-col size=\"2\" class=\"left-assigned-skeleton\">\n              <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n            </ion-col>\n            <ion-col size=\"7\" class=\"ion-padding-start\">\n              <p class=\"delivery-day-routed\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"delivery-date-routed\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </p>\n              <p class=\"booking-time-routed\">\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n                <ion-skeleton-text animated></ion-skeleton-text>\n                <span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </span>\n              </p>\n            </ion-col>\n\n            <ion-col size=\"3\" class=\"right-assigned-skeleton\">\n              <div class=\"right-panel-assigned\">\n                <ion-label class=\"right-label\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </ion-label>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n\n      </ng-container>\n    </ion-list>\n\n\n  </div>\n\n  <!-- Completed Shyfts -->\n  <div *ngIf=\"currentSegment == 'completedShyfts'\">\n      <ion-row>\n        <ion-col class=\"myGig-label\">\n          Completed Routes\n        </ion-col>\n      </ion-row>\n      <ion-list *ngIf=\"completedRoutes.length != 0 && !skeletonBoolean\">\n  \n        <!-- for comment completed -->\n        <ng-container *ngFor=\"let userRoute of completedRoutes\">\n\n          <ion-item detail lines=\"none\" *ngIf=\"userRoute.routeStatus.routeStatusId == 004\" button=true\n          (click)=\"goToGigsDone(userRoute)\">\n            <ion-row class=\"gig-row\">\n              <ion-col size=\"2\" class=\"left-requested\">\n                <div class=\"left-date-requested\">\n                  {{userRoute.deliveryDate | customUtcdate:'shortWeekDay'}}\n                </div>\n                <div class=\"left-day-requested\">\n                 \n                  {{userRoute.deliveryDate | customUtcdate:'day'}}\n              \n                </div>\n              </ion-col>\n              <ion-col size=\"7\" class=\"ion-padding-start\">\n                <p class=\"delivery-day-routed\">{{userRoute.deliveryDate | customUtcdate:'longWeekDay'}}</p>\n                <p class=\"delivery-date-routed\">{{userRoute.deliveryDate | customUtcdate:'deliveryDate'}}</p>\n                <p class=\"booking-time-routed\">\n                  <span>{{userRoute.startRoute | customUtcdate:'shortTime'}}</span> -\n                  <span>{{userRoute.endRoute | customUtcdate:'shortTime'}}</span>\n                </p>\n              </ion-col>\n  \n              <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n                <!-- <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n                <ion-text>{{getTotalQuantity(userRoute.items)}}</ion-text> -->\n              </ion-col>\n            </ion-row>\n          </ion-item>\n        </ng-container>\n  \n  \n        <!-- <ion-item *ngFor=\"let desiredSchedule of userDesiredSchedules\" class=\"gig-request\">\n          <ion-row class=\"gig-row\">\n            <ion-col size=\"2\" class=\"left-requested\">\n              <div class=\"left-date-requested\">\n                {{desiredSchedule?.deliveryDate | customDate:'shortWeekDay'}}\n              </div>\n              <div class=\"left-day-requested\">\n                {{desiredSchedule?.deliveryDate | customDate:'day'}}\n              </div>\n  \n            </ion-col>\n            <ion-col size=\"7\">\n  \n              <p class=\"delivery-date\">{{desiredSchedule.deliveryDate | customDate:'longWeekDay'}}</p>\n              <p class=\"delivery-date\">{{desiredSchedule.deliveryDate | customDate:'deliveryDate'}}</p>\n              <p class=\"booking-time\">\n                <span>{{desiredSchedule.startTime}}</span> -\n                <span>{{desiredSchedule.endTime}}</span>\n              </p>\n            </ion-col>\n            <ion-col size=\"3\" class=\"right-requested\">\n              <div class=\"right-panel-requested\">\n                <ion-label>\n                  REQUESTED\n                </ion-label>\n              </div>\n            </ion-col>\n          </ion-row>\n  \n        </ion-item> -->\n      </ion-list>\n\n      <ion-list *ngIf=\"completedRoutes.length == 0 && !skeletonBoolean\">\n        <div class=\"no-completed-routes\">You have not completed any routes.</div>\n      </ion-list>\n  \n      <!-- skeleton here -->\n      <ion-list *ngIf=\"skeletonBoolean\">\n        <ng-container>\n  \n  \n          <ion-item detail lines=\"none\" button=true>\n            <ion-row class=\"gig-row\">\n              <ion-col size=\"2\" class=\"left-assigned-skeleton\">\n                <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n                <!-- <div class=\"left-date-assigned\">\n                </div>\n                <div class=\"left-day-assigned\">\n                </div> -->\n              </ion-col>\n              <ion-col size=\"7\" class=\"ion-padding-start\">\n                <p class=\"delivery-day-routed\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"delivery-date-routed\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"booking-time-routed\">\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                </p>\n              </ion-col>\n  \n              <ion-col size=\"3\" class=\"right-assigned-skeleton\">\n                <div class=\"right-panel-assigned\">\n                  <ion-label class=\"right-label\">\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </ion-label>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n  \n          <ion-item detail lines=\"none\" button=true>\n            <ion-row class=\"gig-row\">\n              <ion-col size=\"2\" class=\"left-requested-skeleton\">\n                <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n  \n              </ion-col>\n              <ion-col size=\"7\" class=\"ion-padding-start\">\n  \n                <p class=\"delivery-day-scheduled\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"delivery-date-scheduled\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"booking-time-scheduled\">\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                </p>\n              </ion-col>\n              <ion-col size=\"3\" class=\"right-requested-skeleton\">\n                <div class=\"right-panel-requested\">\n                  <ion-label class=\"right-label\">\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </ion-label>\n                </div>\n              </ion-col>\n            </ion-row>\n  \n          </ion-item>\n\n          <ion-item detail lines=\"none\" button=true>\n            <ion-row class=\"gig-row\">\n              <ion-col size=\"2\" class=\"left-requested-skeleton\">\n                <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n  \n              </ion-col>\n              <ion-col size=\"7\" class=\"ion-padding-start\">\n  \n                <p class=\"delivery-day-scheduled\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"delivery-date-scheduled\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"booking-time-scheduled\">\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                </p>\n              </ion-col>\n              <ion-col size=\"3\" class=\"right-requested-skeleton\">\n                <div class=\"right-panel-requested\">\n                  <ion-label class=\"right-label\">\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </ion-label>\n                </div>\n              </ion-col>\n            </ion-row>\n  \n          </ion-item>\n\n          <ion-item detail lines=\"none\" button=true>\n            <ion-row class=\"gig-row\">\n              <ion-col size=\"2\" class=\"left-requested-skeleton\">\n                <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n  \n              </ion-col>\n              <ion-col size=\"7\" class=\"ion-padding-start\">\n  \n                <p class=\"delivery-day-scheduled\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"delivery-date-scheduled\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"booking-time-scheduled\">\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                </p>\n              </ion-col>\n              <ion-col size=\"3\" class=\"right-requested-skeleton\">\n                <div class=\"right-panel-requested\">\n                  <ion-label class=\"right-label\">\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </ion-label>\n                </div>\n              </ion-col>\n            </ion-row>\n  \n          </ion-item>\n\n          <ion-item detail lines=\"none\" button=true>\n            <ion-row class=\"gig-row\">\n              <ion-col size=\"2\" class=\"left-requested-skeleton\">\n                <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n  \n              </ion-col>\n              <ion-col size=\"7\" class=\"ion-padding-start\">\n  \n                <p class=\"delivery-day-scheduled\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"delivery-date-scheduled\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"booking-time-scheduled\">\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                </p>\n              </ion-col>\n              <ion-col size=\"3\" class=\"right-requested-skeleton\">\n                <div class=\"right-panel-requested\">\n                  <ion-label class=\"right-label\">\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </ion-label>\n                </div>\n              </ion-col>\n            </ion-row>\n  \n          </ion-item>\n  \n  \n  \n          <!-- </ng-container> -->\n  \n          <!-- <ion-item detail lines=\"none\" button=true>\n            <ion-row class=\"gig-row\">\n              <ion-col size=\"2\" class=\"left-done-skeleton\">\n                <ion-skeleton-text animated class=\"h-100\"></ion-skeleton-text>\n  \n              </ion-col>\n              <ion-col size=\"7\" class=\"ion-padding-start\">\n                <p class=\"delivery-day-routed\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"delivery-date-routed\">\n                  <ion-skeleton-text animated></ion-skeleton-text>\n                </p>\n                <p class=\"booking-time-routed\">\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span> -\n                  <span>\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </span>\n                </p>\n              </ion-col>\n  \n              <ion-col size=\"3\" class=\"right-done-skeleton\">\n                <div class=\"right-panel-done\">\n                  <ion-label class=\"right-label\">\n                    <ion-skeleton-text animated></ion-skeleton-text>\n                  </ion-label>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-item> -->\n        </ng-container>\n      </ion-list>\n  \n  \n    </div>\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/routes/routes.component.scss":
  /*!**********************************************!*\
    !*** ./src/app/routes/routes.component.scss ***!
    \**********************************************/

  /*! exports provided: default */

  /***/
  function srcAppRoutesRoutesComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\n.custom-skeleton ion-skeleton-text {\n  line-height: 13px;\n}\n\n.custom-skeleton ion-skeleton-text:last-child {\n  margin-bottom: 5px;\n}\n\nion-title {\n  color: #003A58;\n}\n\nion-segment-button {\n  --background: white;\n}\n\nion-content {\n  --background: white;\n}\n\nion-toolbar {\n  margin-top: 1rem !important;\n  font-family: \"Montserrat-Bold\";\n}\n\nion-segment-button {\n  border: none;\n}\n\nion-checkbox {\n  --background-checked: #F4773B;\n  --border-color-checked: #F4773B;\n}\n\nion-list {\n  margin-bottom: 0;\n}\n\n.segment-class {\n  font-size: small;\n  color: #8C8C8C;\n  padding: 0.875rem;\n  font-family: \"Montserrat-SemiBold\";\n  border-bottom: solid 3px #D9D9D9;\n}\n\n.segment-class-selected {\n  font-family: \"Montserrat-Bold\";\n  background-color: white;\n  border-bottom: solid 3px #003A58;\n  color: #003A58;\n}\n\n.gig-request {\n  padding-right: 20px;\n}\n\n.gig-panel {\n  background-color: white;\n  padding: 1rem;\n}\n\n.myGig-label {\n  font-size: 1.25rem;\n  color: #003A58;\n  padding: 1rem;\n  font-family: \"Montserrat-Bold\";\n  letter-spacing: 0;\n}\n\n.gig-row {\n  width: 100%;\n  padding: 1rem 0;\n}\n\n.gig-request {\n  padding-right: 20px;\n}\n\n.available-gig-label {\n  font-size: 1.25rem;\n  color: #003A58;\n  font-family: \"Montserrat-Bold\";\n  letter-spacing: 0;\n  padding: 1rem;\n}\n\n.available-gig {\n  padding: 0 1rem 1rem 1rem;\n  background-color: white;\n}\n\n.no-available-gig {\n  padding: 1rem;\n  background-color: white;\n}\n\n.left-date-requested {\n  color: #003A58;\n  background-color: #E5F3FF;\n  text-align: center;\n  font-size: 0.875rem;\n  font-weight: bolder;\n}\n\n.left-day-requested {\n  color: #003A58;\n  background-color: #E5F3FF;\n  text-align: center;\n  font-size: 1.3125rem;\n  font-weight: bold;\n}\n\n.left-panel {\n  margin: auto;\n}\n\n.left-assigned {\n  margin: auto;\n  background-color: #003A58;\n  border-radius: 5px;\n  padding: 5px 0;\n}\n\n.left-assigned-skeleton {\n  border-radius: 5px;\n  padding: 5px 0;\n}\n\n.left-date-assigned {\n  font-family: \"Montserrat-Bold\";\n  font-size: 0.75rem;\n  color: #FFFFFF;\n  letter-spacing: 0;\n  text-align: center;\n}\n\n.left-day-assigned {\n  font-family: \"Montserrat-Bold\";\n  font-size: 1.25rem;\n  color: #FFFFFF;\n  letter-spacing: 0;\n  text-align: center;\n  background-color: #003A58;\n}\n\n.delivery-day-routed {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Bold\";\n  font-size: 0.875rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.3125rem;\n}\n\n.delivery-day-scheduled {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Bold\";\n  font-size: 0.875rem;\n  color: #676767;\n  letter-spacing: 0;\n  line-height: 1.3125rem;\n}\n\n.delivery-date-routed {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Medium\";\n  font-size: 1rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.5rem;\n}\n\n.delivery-date-scheduled {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Medium\";\n  font-size: 1rem;\n  color: #676767;\n  letter-spacing: 0;\n  line-height: 1.5rem;\n}\n\n.delivery-date {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-weight: bold;\n  font-family: \"Roboto-Bold\";\n  font-size: 0.865rem;\n}\n\n.booking-time-routed {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Regular\";\n  font-size: 0.875rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.3125rem;\n}\n\n.booking-time-scheduled {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-family: \"Roboto-Regular\";\n  font-size: 0.875rem;\n  color: #676767;\n  letter-spacing: 0;\n  line-height: 1.3125rem;\n}\n\n.right-assigned {\n  margin: auto;\n  background-color: #003A58;\n  border-radius: 1rem;\n}\n\n.right-assigned-skeleton {\n  margin: auto;\n}\n\n.right-panel-assigned {\n  color: white;\n  font-size: 0.625rem;\n  text-align: center;\n}\n\n.right-label {\n  font-family: \"Roboto-Bold\";\n  font-size: 0.6875rem;\n  letter-spacing: 0;\n  line-height: 0.9375rem;\n}\n\n.left-requested {\n  margin: auto;\n  background-color: #E5F3FF;\n  border-radius: 5px;\n  padding: 5px 0;\n}\n\n.left-requested-skeleton {\n  border-radius: 5px;\n  padding: 5px 0;\n}\n\n.right-requested {\n  margin: auto;\n  background-color: #F4773B;\n  border-radius: 1rem;\n}\n\n.right-requested-skeleton {\n  margin: auto;\n}\n\n.right-panel-requested {\n  color: white;\n  font-size: 0.625rem;\n  text-align: center;\n  font-weight: bold;\n}\n\n.delivery-sched-requested {\n  padding: 1rem 0;\n}\n\n.delivery-date-requested {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  font-weight: bold;\n  color: #424242;\n  font-size: 1rem;\n}\n\n.booking-time-requested {\n  margin-top: 0;\n  padding-top: 0;\n  margin-bottom: 0;\n  padding-bottom: 0;\n  color: #424242;\n  font-size: 0.875rem;\n}\n\nbutton {\n  border: none !important;\n}\n\n.shyft-select {\n  position: fixed;\n  bottom: 0;\n  background: #003A58;\n  color: white;\n  width: 100%;\n}\n\n.shyft-select .select-clear {\n  size: 1rem;\n  padding-left: 1rem;\n}\n\n.shyft-select .select-submit {\n  size: 1rem;\n  font-weight: bold;\n  text-align: right;\n  padding-right: 1rem;\n}\n\n.availability-label {\n  padding: 1rem 0 1rem 0;\n}\n\n.l-align {\n  text-align: left;\n  margin: auto;\n}\n\n.r-align {\n  text-align: right;\n  margin: auto;\n  font-family: \"Montserrat-Regular\";\n}\n\n.available-date {\n  font-family: \"Roboto-Bold\";\n  font-size: 1rem;\n}\n\n.calendar-weekday {\n  font-size: 0.875rem !important;\n  color: #003A58;\n  font-family: \"Montserrat-SemiBold\";\n  text-align: center;\n}\n\n.calendar-date {\n  font-size: 2rem;\n}\n\n.event-bullet {\n  margin: 2px auto;\n  height: 7px;\n  width: 7px;\n  background-color: #F4773B;\n  border-radius: 30px;\n}\n\n.event-date {\n  text-align: center;\n  color: #003A58;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.today-date {\n  text-align: center;\n  font-family: \"Montserrat-Bold\";\n  color: #003A58;\n}\n\n.other-date {\n  text-align: center;\n  color: #8C8C8C;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.left-done {\n  margin: auto;\n  background-color: white;\n  border-radius: 5px;\n  padding: 5px 0;\n  border: 1px solid #003A58;\n}\n\n.left-done-skeleton {\n  border-radius: 5px;\n  padding: 5px 0;\n}\n\n.left-date-done {\n  font-family: \"Montserrat-Bold\";\n  font-size: 0.75rem;\n  color: #003A58;\n  letter-spacing: 0;\n  text-align: center;\n}\n\n.left-day-done {\n  font-family: \"Montserrat-Bold\";\n  font-size: 1.25rem;\n  color: #003A58;\n  letter-spacing: 0;\n  text-align: center;\n  background-color: white;\n}\n\n.right-done {\n  margin: auto;\n  background-color: white;\n  border-radius: 1rem;\n  border: 1px solid #003A58;\n}\n\n.right-done-skeleton {\n  margin: auto;\n}\n\n.right-panel-done {\n  color: #003A58;\n  font-size: 0.625rem;\n  text-align: center;\n}\n\n.footer-toolbar {\n  background-color: #003A58;\n}\n\n.footer-button {\n  color: #ffffff;\n  font-family: \"Montserrat-SemiBold\";\n  background-color: #003A58;\n}\n\n.no-route-class {\n  text-align: center;\n}\n\n.no-completed-routes {\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL3JvdXRlcy9yb3V0ZXMuY29tcG9uZW50LnNjc3MiLCIvVXNlcnMvYW50aG9ueS5icmlvbmVzL0RvY3VtZW50cy9DYXBEcml2ZXIvc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hcHAvcm91dGVzL3JvdXRlcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFPQTtFQUNJLHlCQUFBO0VBQ0EsZ0RBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkFBQTtBQ05KOztBRFNBO0VBQ0kseUJFbEJjO0VGbUJkLFdBQUE7RUFDQSxjRWhCSTtFRmlCSixtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtBQ05KOztBRFNBO0VBQ0ksK0JBQUE7RUFBQSx3QkFBQTtFQUNBLGdDQUFBO0VBQ0Esb0NBQUE7VUFBQSw4QkFBQTtBQ05KOztBRFNBO0VBQ0ksbUNBQUE7VUFBQSxrQ0FBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7VUFBQSx5QkFBQTtBQ05KOztBRFNBO0VBQ0ksd0JBQUE7VUFBQSx1QkFBQTtBQ05KOztBRFNBO0VBQ0ksWUFBQTtBQ05KOztBRWhEQTtFQUNJLGlCQUFBO0FGbURKOztBRWhERTtFQUNFLGtCQUFBO0FGbURKOztBRWhEQTtFQUNJLGNEVGM7QUQ0RGxCOztBRWhEQTtFQUNJLG1CQUFBO0FGbURKOztBRWhEQTtFQUNJLG1CQUFBO0FGbURKOztBRWhEQTtFQUNJLDJCQUFBO0VBQ0EsOEJBQUE7QUZtREo7O0FFL0NBO0VBQ0ksWUFBQTtBRmtESjs7QUU5Q0E7RUFDSSw2QkFBQTtFQUNBLCtCQUFBO0FGaURKOztBRTlDQTtFQUNJLGdCQUFBO0FGaURKOztBRTlDQTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0NBQUE7RUFDQSxnQ0FBQTtBRmlESjs7QUU5Q0E7RUFDSSw4QkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxjRHBEYztBRHFHbEI7O0FFOUNBO0VBQ0ksbUJBQUE7QUZpREo7O0FFOUNBO0VBQ0ksdUJBQUE7RUFFQSxhQUFBO0FGZ0RKOztBRTdDQTtFQUNJLGtCQUFBO0VBQ0EsY0RuRWM7RUNxRWQsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsaUJBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksbUJBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksa0JBQUE7RUFDQSxjRHJGYztFQ3NGZCw4QkFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtBRitDSjs7QUU1Q0E7RUFDSSx5QkFBQTtFQUVBLHVCQUFBO0FGOENKOztBRTVDQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBRitDSjs7QUU1Q0E7RUFDSSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksY0FBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0FGK0NKOztBRTVDQTtFQUNJLFlBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksWUFBQTtFQUNBLHlCRDNIYztFQzRIZCxrQkFBQTtFQUNBLGNBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0FGK0NKOztBRTNDQTtFQUVJLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBRjZDSjs7QUV6Q0E7RUFDSSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkR0SmM7QURrTWxCOztBRXhDQTtFQUNJLGFBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDBCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBRjJDSjs7QUV4Q0E7RUFDSSxhQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUYyQ0o7O0FFeENBO0VBQ0ksYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUYyQ0o7O0FFeENBO0VBQ0ksYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUYyQ0o7O0FFdkNBO0VBQ0ksYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQkFBQTtFQUNBLG1CQUFBO0FGMENKOztBRXZDQTtFQUNJLGFBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBRjBDSjs7QUV0Q0E7RUFDSSxhQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUZ5Q0o7O0FFdENBO0VBQ0ksWUFBQTtFQUNBLHlCRGhQYztFQ2lQZCxtQkFBQTtBRnlDSjs7QUV0Q0E7RUFDSSxZQUFBO0FGeUNKOztBRXBDQTtFQUNJLFlBQUE7RUFFQSxtQkFBQTtFQUNBLGtCQUFBO0FGc0NKOztBRW5DQTtFQUNJLDBCQUFBO0VBQ0Esb0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0FGc0NKOztBRW5DQTtFQUNJLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBRnNDSjs7QUVuQ0E7RUFDSSxrQkFBQTtFQUNBLGNBQUE7QUZzQ0o7O0FFbkNBO0VBQ0ksWUFBQTtFQUNBLHlCRHBSYztFQ3FSZCxtQkFBQTtBRnNDSjs7QUVuQ0E7RUFDSSxZQUFBO0FGc0NKOztBRWpDQTtFQUNJLFlBQUE7RUFFQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUZtQ0o7O0FFaENBO0VBQ0ksZUFBQTtBRm1DSjs7QUVoQ0E7RUFDSSxhQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FGbUNKOztBRWhDQTtFQUNJLGFBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBRm1DSjs7QUVoQ0E7RUFDSSx1QkFBQTtBRm1DSjs7QUVoQ0E7RUFDSSxlQUFBO0VBQ0EsU0FBQTtFQUNBLG1CRHRVYztFQ3VVZCxZQUFBO0VBQ0EsV0FBQTtBRm1DSjs7QUVqQ0k7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7QUZtQ1I7O0FFaENJO0VBQ0ksVUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBRmtDUjs7QUU5QkE7RUFDSSxzQkFBQTtBRmlDSjs7QUU5QkE7RUFDSSxnQkFBQTtFQUNBLFlBQUE7QUZpQ0o7O0FFOUJBO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUNBQUE7QUZpQ0o7O0FFOUJBO0VBQ0ksMEJBQUE7RUFDQSxlQUFBO0FGaUNKOztBRTFCQTtFQUNJLDhCQUFBO0VBQ0EsY0RqWGM7RUNrWGQsa0NBQUE7RUFDQSxrQkFBQTtBRjZCSjs7QUUxQkE7RUFDSSxlQUFBO0FGNkJKOztBRTFCQTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSx5QkQ1WGM7RUM2WGQsbUJBQUE7QUY2Qko7O0FFMUJBO0VBQ0ksa0JBQUE7RUFFQSxjRHJZYztFQ3NZZCxrQ0FBQTtBRjRCSjs7QUV6QkE7RUFDSSxrQkFBQTtFQUVBLDhCQUFBO0VBQ0EsY0Q3WWM7QUR3YWxCOztBRXhCQTtFQUNJLGtCQUFBO0VBRUEsY0FBQTtFQUNBLGtDQUFBO0FGMEJKOztBRXJCQTtFQUNJLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0FGd0JKOztBRXBCQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtBRnVCSjs7QUVsQkE7RUFFSSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0Q3YWM7RUM4YWQsaUJBQUE7RUFDQSxrQkFBQTtBRm9CSjs7QUVqQkE7RUFDSSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0RyYmM7RUNzYmQsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0FGb0JKOztBRWRBO0VBQ0ksWUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtBRmlCSjs7QUVkQTtFQUNJLFlBQUE7QUZpQko7O0FFWEE7RUFDSSxjRDdjYztFQytjZCxtQkFBQTtFQUNBLGtCQUFBO0FGYUo7O0FFK0RBO0VBQ0kseUJEN2hCYztBRGllbEI7O0FFK0RBO0VBQ0ksY0Q3aEJJO0VDOGhCSixrQ0FBQTtFQUNBLHlCRG5pQmM7QUR1ZWxCOztBRStEQTtFQUNJLGtCQUFBO0FGNURKOztBRStEQTtFQUNJLGtCQUFBO0FGNURKIiwiZmlsZSI6InNyYy9hcHAvcm91dGVzL3JvdXRlcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy9mb250cyc7XG5AaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzJztcblxuLy8gLmlzLXZhbGlkIHtcbi8vICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlXG4vLyB9XG5cbi5pcy1pbnZhbGlkIHtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcInNyYy9hc3NldHMvaWNvbi9pY29uLWhlbHAuc3ZnXCIpIG5vLXJlcGVhdDtcbn1cblxuLmlzLXZhbGlkIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjNUVCMDAzXG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0VFNDAzNjtcbn1cblxuLnByaW1hcnktYnV0dG9uIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAkd2hpdGU7XG4gICAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTF7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4O1xufVxuXG5pb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi52ZXJ0aWNhbC1hbGlnbiB7XG4gICAgZGlzcGxheTogZmxleCFpbXBvcnRhbnQ7XG4gICAgYWxpZ24tY29udGVudDogY2VudGVyIWltcG9ydGFudDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyIWltcG9ydGFudDtcbn1cblxuLmhvcml6b250YWwtYWxpZ24ge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWFsaWduLXJpZ2h0IHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZGlzYWJsZSB7XG4gICAgb3BhY2l0eTogMC41O1xufVxuXG5cblxuXG4iLCIuaXMtaW52YWxpZCB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNFRTQwMzY7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcInNyYy9hc3NldHMvaWNvbi9pY29uLWhlbHAuc3ZnXCIpIG5vLXJlcGVhdDtcbn1cblxuLmlzLXZhbGlkIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwMztcbn1cblxuLmlzLWludmFsaWQtc2VsZWN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgI0VFNDAzNjtcbn1cblxuLnByaW1hcnktYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgaGVpZ2h0OiAycmVtO1xufVxuXG4uYmctY29sb3ItMSB7XG4gIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi52ZXJ0aWNhbC1hbGlnbiB7XG4gIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmhvcml6b250YWwtYWxpZ24ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5mbGV4LWFsaWduLWNlbnRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZGlzYWJsZSB7XG4gIG9wYWNpdHk6IDAuNTtcbn1cblxuLmN1c3RvbS1za2VsZXRvbiBpb24tc2tlbGV0b24tdGV4dCB7XG4gIGxpbmUtaGVpZ2h0OiAxM3B4O1xufVxuXG4uY3VzdG9tLXNrZWxldG9uIGlvbi1za2VsZXRvbi10ZXh0Omxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbmlvbi10aXRsZSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xufVxuXG5pb24tc2VnbWVudC1idXR0b24ge1xuICAtLWJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgbWFyZ2luLXRvcDogMXJlbSAhaW1wb3J0YW50O1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG5pb24tY2hlY2tib3gge1xuICAtLWJhY2tncm91bmQtY2hlY2tlZDogI0Y0NzczQjtcbiAgLS1ib3JkZXItY29sb3ItY2hlY2tlZDogI0Y0NzczQjtcbn1cblxuaW9uLWxpc3Qge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG4uc2VnbWVudC1jbGFzcyB7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG4gIGNvbG9yOiAjOEM4QzhDO1xuICBwYWRkaW5nOiAwLjg3NXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAzcHggI0Q5RDlEOTtcbn1cblxuLnNlZ21lbnQtY2xhc3Mtc2VsZWN0ZWQge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDNweCAjMDAzQTU4O1xuICBjb2xvcjogIzAwM0E1ODtcbn1cblxuLmdpZy1yZXF1ZXN0IHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmdpZy1wYW5lbCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiAxcmVtO1xufVxuXG4ubXlHaWctbGFiZWwge1xuICBmb250LXNpemU6IDEuMjVyZW07XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBwYWRkaW5nOiAxcmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG59XG5cbi5naWctcm93IHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDFyZW0gMDtcbn1cblxuLmdpZy1yZXF1ZXN0IHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmF2YWlsYWJsZS1naWctbGFiZWwge1xuICBmb250LXNpemU6IDEuMjVyZW07XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIHBhZGRpbmc6IDFyZW07XG59XG5cbi5hdmFpbGFibGUtZ2lnIHtcbiAgcGFkZGluZzogMCAxcmVtIDFyZW0gMXJlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5uby1hdmFpbGFibGUtZ2lnIHtcbiAgcGFkZGluZzogMXJlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5sZWZ0LWRhdGUtcmVxdWVzdGVkIHtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNFNUYzRkY7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cblxuLmxlZnQtZGF5LXJlcXVlc3RlZCB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRTVGM0ZGO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMS4zMTI1cmVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmxlZnQtcGFuZWwge1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5sZWZ0LWFzc2lnbmVkIHtcbiAgbWFyZ2luOiBhdXRvO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDVweCAwO1xufVxuXG4ubGVmdC1hc3NpZ25lZC1za2VsZXRvbiB7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZzogNXB4IDA7XG59XG5cbi5sZWZ0LWRhdGUtYXNzaWduZWQge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgZm9udC1zaXplOiAwLjc1cmVtO1xuICBjb2xvcjogI0ZGRkZGRjtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxlZnQtZGF5LWFzc2lnbmVkIHtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgY29sb3I6ICNGRkZGRkY7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG59XG5cbi5kZWxpdmVyeS1kYXktcm91dGVkIHtcbiAgbWFyZ2luLXRvcDogMDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tQm9sZFwiO1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBjb2xvcjogIzQyNDI0MjtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5kZWxpdmVyeS1kYXktc2NoZWR1bGVkIHtcbiAgbWFyZ2luLXRvcDogMDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tQm9sZFwiO1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBjb2xvcjogIzY3Njc2NztcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5kZWxpdmVyeS1kYXRlLXJvdXRlZCB7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLU1lZGl1bVwiO1xuICBmb250LXNpemU6IDFyZW07XG4gIGNvbG9yOiAjNDI0MjQyO1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgbGluZS1oZWlnaHQ6IDEuNXJlbTtcbn1cblxuLmRlbGl2ZXJ5LWRhdGUtc2NoZWR1bGVkIHtcbiAgbWFyZ2luLXRvcDogMDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tTWVkaXVtXCI7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgY29sb3I6ICM2NzY3Njc7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICBsaW5lLWhlaWdodDogMS41cmVtO1xufVxuXG4uZGVsaXZlcnktZGF0ZSB7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1Cb2xkXCI7XG4gIGZvbnQtc2l6ZTogMC44NjVyZW07XG59XG5cbi5ib29raW5nLXRpbWUtcm91dGVkIHtcbiAgbWFyZ2luLXRvcDogMDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tUmVndWxhclwiO1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBjb2xvcjogIzQyNDI0MjtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5ib29raW5nLXRpbWUtc2NoZWR1bGVkIHtcbiAgbWFyZ2luLXRvcDogMDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tUmVndWxhclwiO1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBjb2xvcjogIzY3Njc2NztcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5yaWdodC1hc3NpZ25lZCB7XG4gIG1hcmdpbjogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbn1cblxuLnJpZ2h0LWFzc2lnbmVkLXNrZWxldG9uIHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4ucmlnaHQtcGFuZWwtYXNzaWduZWQge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMC42MjVyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnJpZ2h0LWxhYmVsIHtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgZm9udC1zaXplOiAwLjY4NzVyZW07XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICBsaW5lLWhlaWdodDogMC45Mzc1cmVtO1xufVxuXG4ubGVmdC1yZXF1ZXN0ZWQge1xuICBtYXJnaW46IGF1dG87XG4gIGJhY2tncm91bmQtY29sb3I6ICNFNUYzRkY7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZzogNXB4IDA7XG59XG5cbi5sZWZ0LXJlcXVlc3RlZC1za2VsZXRvbiB7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZzogNXB4IDA7XG59XG5cbi5yaWdodC1yZXF1ZXN0ZWQge1xuICBtYXJnaW46IGF1dG87XG4gIGJhY2tncm91bmQtY29sb3I6ICNGNDc3M0I7XG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XG59XG5cbi5yaWdodC1yZXF1ZXN0ZWQtc2tlbGV0b24ge1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5yaWdodC1wYW5lbC1yZXF1ZXN0ZWQge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMC42MjVyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5kZWxpdmVyeS1zY2hlZC1yZXF1ZXN0ZWQge1xuICBwYWRkaW5nOiAxcmVtIDA7XG59XG5cbi5kZWxpdmVyeS1kYXRlLXJlcXVlc3RlZCB7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNDI0MjQyO1xuICBmb250LXNpemU6IDFyZW07XG59XG5cbi5ib29raW5nLXRpbWUtcmVxdWVzdGVkIHtcbiAgbWFyZ2luLXRvcDogMDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBjb2xvcjogIzQyNDI0MjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbn1cblxuYnV0dG9uIHtcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5zaHlmdC1zZWxlY3Qge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJvdHRvbTogMDtcbiAgYmFja2dyb3VuZDogIzAwM0E1ODtcbiAgY29sb3I6IHdoaXRlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5zaHlmdC1zZWxlY3QgLnNlbGVjdC1jbGVhciB7XG4gIHNpemU6IDFyZW07XG4gIHBhZGRpbmctbGVmdDogMXJlbTtcbn1cbi5zaHlmdC1zZWxlY3QgLnNlbGVjdC1zdWJtaXQge1xuICBzaXplOiAxcmVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XG59XG5cbi5hdmFpbGFiaWxpdHktbGFiZWwge1xuICBwYWRkaW5nOiAxcmVtIDAgMXJlbSAwO1xufVxuXG4ubC1hbGlnbiB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIG1hcmdpbjogYXV0bztcbn1cblxuLnItYWxpZ24ge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgbWFyZ2luOiBhdXRvO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LVJlZ3VsYXJcIjtcbn1cblxuLmF2YWlsYWJsZS1kYXRlIHtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgZm9udC1zaXplOiAxcmVtO1xufVxuXG4uY2FsZW5kYXItd2Vla2RheSB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW0gIWltcG9ydGFudDtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uY2FsZW5kYXItZGF0ZSB7XG4gIGZvbnQtc2l6ZTogMnJlbTtcbn1cblxuLmV2ZW50LWJ1bGxldCB7XG4gIG1hcmdpbjogMnB4IGF1dG87XG4gIGhlaWdodDogN3B4O1xuICB3aWR0aDogN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjQ3NzNCO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuXG4uZXZlbnQtZGF0ZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbn1cblxuLnRvZGF5LWRhdGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBjb2xvcjogIzAwM0E1ODtcbn1cblxuLm90aGVyLWRhdGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjOEM4QzhDO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LVNlbWlCb2xkXCI7XG59XG5cbi5sZWZ0LWRvbmUge1xuICBtYXJnaW46IGF1dG87XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDVweCAwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDAzQTU4O1xufVxuXG4ubGVmdC1kb25lLXNrZWxldG9uIHtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiA1cHggMDtcbn1cblxuLmxlZnQtZGF0ZS1kb25lIHtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5sZWZ0LWRheS1kb25lIHtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4ucmlnaHQtZG9uZSB7XG4gIG1hcmdpbjogYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDNBNTg7XG59XG5cbi5yaWdodC1kb25lLXNrZWxldG9uIHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4ucmlnaHQtcGFuZWwtZG9uZSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LXNpemU6IDAuNjI1cmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5mb290ZXItdG9vbGJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG59XG5cbi5mb290ZXItYnV0dG9uIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbn1cblxuLm5vLXJvdXRlLWNsYXNzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubm8tY29tcGxldGVkLXJvdXRlcyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iLCJcbi8vIGNvbG9yc1xuLy8gJHByaW1hcnktY29sb3ItMTogIzAwQTNDODtcbiRwcmltYXJ5LWNvbG9yLTE6ICMwMDNBNTg7XG4kcHJpbWFyeS1jb2xvci0yOiAjRkFBRjQwO1xuJHByaW1hcnktY29sb3ItMzogI0Y0NzczQjtcblxuJHdoaXRlOiAjZmZmZmZmO1xuXG4vLyAkYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsIHJnYmEoMjU1LDI1NSwyNTUsMC41MCkgMCUsIHJnYmEoMCwwLDAsMC41MCkgMTAwJSk7XG4kYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpOyIsIkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy9jb21tb24uc2Nzcyc7XG5AaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MnO1xuXG4uY3VzdG9tLXNrZWxldG9uIGlvbi1za2VsZXRvbi10ZXh0IHtcbiAgICBsaW5lLWhlaWdodDogMTNweDtcbiAgfVxuICBcbiAgLmN1c3RvbS1za2VsZXRvbiBpb24tc2tlbGV0b24tdGV4dDpsYXN0LWNoaWxkIHtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIH1cblxuaW9uLXRpdGxle1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG5pb24tc2VnbWVudC1idXR0b257XG4gICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cblxuaW9uLWNvbnRlbnR7XG4gICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cblxuaW9uLXRvb2xiYXIge1xuICAgIG1hcmdpbi10b3A6IDFyZW0gIWltcG9ydGFudDtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG5cbn1cblxuaW9uLXNlZ21lbnQtYnV0dG9ue1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICAvLyB3aWR0aDogY2FsYygxMDAlIC8gMik7XG59XG5cbmlvbi1jaGVja2JveHtcbiAgICAtLWJhY2tncm91bmQtY2hlY2tlZDogI0Y0NzczQjtcbiAgICAtLWJvcmRlci1jb2xvci1jaGVja2VkOiAjRjQ3NzNCO1xufVxuXG5pb24tbGlzdCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbn1cblxuLnNlZ21lbnQtY2xhc3Mge1xuICAgIGZvbnQtc2l6ZTogc21hbGw7XG4gICAgY29sb3I6ICM4QzhDOEM7XG4gICAgcGFkZGluZzogMC44NzVyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbiAgICBib3JkZXItYm90dG9tOiBzb2xpZCAzcHggI0Q5RDlEOTtcbn1cblxuLnNlZ21lbnQtY2xhc3Mtc2VsZWN0ZWQge1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItYm90dG9tOiBzb2xpZCAzcHggJHByaW1hcnktY29sb3ItMTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbn1cblxuLmdpZy1yZXF1ZXN0e1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG5cbi5naWctcGFuZWx7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgLy8gcGFkZGluZzogMCAxcmVtIDAgMXJlbTtcbiAgICBwYWRkaW5nOiAxcmVtO1xufVxuXG4ubXlHaWctbGFiZWx7XG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xOyBcbiAgICAvLyBmb250LXdlaWdodDogYm9sZDtcbiAgICBwYWRkaW5nOiAxcmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbn1cblxuLmdpZy1yb3d7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMXJlbSAwO1xufVxuXG4uZ2lnLXJlcXVlc3R7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmF2YWlsYWJsZS1naWctbGFiZWx7XG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICBwYWRkaW5nOiAxcmVtO1xufVxuXG4uYXZhaWxhYmxlLWdpZ3tcbiAgICBwYWRkaW5nOiAwIDFyZW0gMXJlbSAxcmVtO1xuICAgIC8vIHBhZGRpbmc6IDA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG4ubm8tYXZhaWxhYmxlLWdpZ3tcbiAgICBwYWRkaW5nOiAxcmVtO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4ubGVmdC1kYXRlLXJlcXVlc3RlZHtcbiAgICBjb2xvcjogIzAwM0E1ODtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRTVGM0ZGO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi5sZWZ0LWRheS1yZXF1ZXN0ZWR7XG4gICAgY29sb3I6ICMwMDNBNTg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0U1RjNGRjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxLjMxMjVyZW07XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5sZWZ0LXBhbmVse1xuICAgIG1hcmdpbjogYXV0bztcbn1cblxuLmxlZnQtYXNzaWduZWR7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBhZGRpbmc6IDVweCAwO1xufVxuXG4ubGVmdC1hc3NpZ25lZC1za2VsZXRvbntcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgcGFkZGluZzogNXB4IDA7XG59XG5cblxuLmxlZnQtZGF0ZS1hc3NpZ25lZHtcblxuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBmb250LXNpemU6IDAuNzVyZW07XG4gICAgY29sb3I6ICNGRkZGRkY7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5cbi5sZWZ0LWRheS1hc3NpZ25lZHtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xuICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG5cbi5kZWxpdmVyeS1kYXktcm91dGVkIHtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIHBhZGRpbmctdG9wOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tQm9sZCc7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBjb2xvcjogIzQyNDI0MjtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4uZGVsaXZlcnktZGF5LXNjaGVkdWxlZCB7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJvbGQnO1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgY29sb3I6ICM2NzY3Njc7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLmRlbGl2ZXJ5LWRhdGUtcm91dGVkIHtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIHBhZGRpbmctdG9wOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tTWVkaXVtJztcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgY29sb3I6ICM0MjQyNDI7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuNXJlbVxufVxuXG4uZGVsaXZlcnktZGF0ZS1zY2hlZHVsZWQge1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBjb2xvcjogIzY3Njc2NztcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICBsaW5lLWhlaWdodDogMS41cmVtXG59XG5cblxuLmRlbGl2ZXJ5LWRhdGUge1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1Cb2xkJztcbiAgICBmb250LXNpemU6IDAuODY1cmVtO1xufVxuXG4uYm9va2luZy10aW1lLXJvdXRlZCB7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLVJlZ3VsYXInO1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgY29sb3I6ICM0MjQyNDI7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuXG4uYm9va2luZy10aW1lLXNjaGVkdWxlZCB7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLVJlZ3VsYXInO1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgY29sb3I6ICM2NzY3Njc7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLnJpZ2h0LWFzc2lnbmVke1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG59XG5cbi5yaWdodC1hc3NpZ25lZC1za2VsZXRvbntcbiAgICBtYXJnaW46IGF1dG87XG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICAvLyBib3JkZXItcmFkaXVzOiAxcmVtO1xufVxuXG4ucmlnaHQtcGFuZWwtYXNzaWduZWR7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1zaXplOiAwLjYyNXJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5yaWdodC1sYWJlbCB7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tQm9sZCc7XG4gICAgZm9udC1zaXplOiAwLjY4NzVyZW07XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDAuOTM3NXJlbTtcbn1cblxuLmxlZnQtcmVxdWVzdGVke1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRTVGM0ZGOztcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgcGFkZGluZzogNXB4IDA7XG59XG5cbi5sZWZ0LXJlcXVlc3RlZC1za2VsZXRvbntcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgcGFkZGluZzogNXB4IDA7XG59XG5cbi5yaWdodC1yZXF1ZXN0ZWR7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTM7XG4gICAgYm9yZGVyLXJhZGl1czogMXJlbTtcbn1cblxuLnJpZ2h0LXJlcXVlc3RlZC1za2VsZXRvbntcbiAgICBtYXJnaW46IGF1dG87XG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMztcbiAgICAvLyBib3JkZXItcmFkaXVzOiAxcmVtO1xufVxuXG4ucmlnaHQtcGFuZWwtcmVxdWVzdGVke1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0zO1xuICAgIGZvbnQtc2l6ZTogMC42MjVyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uZGVsaXZlcnktc2NoZWQtcmVxdWVzdGVke1xuICAgIHBhZGRpbmc6IDFyZW0gMDtcbn1cblxuLmRlbGl2ZXJ5LWRhdGUtcmVxdWVzdGVkIHtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIHBhZGRpbmctdG9wOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgY29sb3I6ICM0MjQyNDI7XG4gICAgZm9udC1zaXplOiAxcmVtO1xufVxuXG4uYm9va2luZy10aW1lLXJlcXVlc3RlZCB7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIGNvbG9yOiAjNDI0MjQyO1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG59XG5cbmJ1dHRvbntcbiAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLnNoeWZ0LXNlbGVjdCB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGJvdHRvbTogMDtcbiAgICBiYWNrZ3JvdW5kOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB3aWR0aDogMTAwJTtcblxuICAgIC5zZWxlY3QtY2xlYXJ7XG4gICAgICAgIHNpemU6IDFyZW07XG4gICAgICAgIHBhZGRpbmctbGVmdDogMXJlbTtcbiAgICB9XG5cbiAgICAuc2VsZWN0LXN1Ym1pdHtcbiAgICAgICAgc2l6ZTogMXJlbTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xuICAgIH1cbn1cblxuLmF2YWlsYWJpbGl0eS1sYWJlbCB7XG4gICAgcGFkZGluZzogMXJlbSAwIDFyZW0gMDtcbn1cblxuLmwtYWxpZ24ge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgbWFyZ2luOiBhdXRvO1xufVxuXG4uci1hbGlnbiB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1SZWd1bGFyJztcbn1cblxuLmF2YWlsYWJsZS1kYXRlIHtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1Cb2xkJztcbiAgICBmb250LXNpemU6IDFyZW07XG59XG4vLyAuc3VibWl0LWdpZ3tcbi8vICAgICB3aWR0aDogNTAlO1xuLy8gfVxuXG4vL2NhbGVuZGFyIGNzc1xuLmNhbGVuZGFyLXdlZWtkYXkge1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW0gIWltcG9ydGFudDtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNhbGVuZGFyLWRhdGUge1xuICAgIGZvbnQtc2l6ZTogMnJlbTtcbn1cblxuLmV2ZW50LWJ1bGxldCB7XG4gICAgbWFyZ2luOiAycHggYXV0bztcbiAgICBoZWlnaHQ6IDdweDtcbiAgICB3aWR0aDogN3B4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTM7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbn1cblxuLmV2ZW50LWRhdGUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAvLyBmb250LXNpemU6IDJyZW07XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbn1cblxuLnRvZGF5LWRhdGUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAvLyBmb250LXNpemU6IDJyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG4ub3RoZXItZGF0ZSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIC8vIGZvbnQtc2l6ZTogMnJlbTtcbiAgICBjb2xvcjogIzhDOEM4QztcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xufVxuXG4vL2RvbmUgcGFuZWxcblxuLmxlZnQtZG9uZXtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBhZGRpbmc6IDVweCAwO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLy8gY29sb3I6ICRwcmltYXJ5LWNvbG9yLTEgIWltcG9ydGFudDtcbn1cblxuLmxlZnQtZG9uZS1za2VsZXRvbntcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgcGFkZGluZzogNXB4IDA7XG4gICAgLy8gYm9yZGVyOiAxcHggc29saWQgJHByaW1hcnktY29sb3ItMTtcbiAgICAvLyBjb2xvcjogJHByaW1hcnktY29sb3ItMSAhaW1wb3J0YW50O1xufVxuXG4ubGVmdC1kYXRlLWRvbmV7XG5cbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgZm9udC1zaXplOiAwLjc1cmVtO1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxlZnQtZGF5LWRvbmV7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgLy8gYm9yZGVyOiAxcHggc29saWQgIzVFQjAwMztcbiAgICAvLyBib3JkZXItbGVmdDogMC41cHggc29saWQgJHByaW1hcnktY29sb3ItMTtcbiAgICAvLyBib3JkZXItcmlnaHQ6IDAuNXB4IHNvbGlkICRwcmltYXJ5LWNvbG9yLTE7XG59XG5cbi5yaWdodC1kb25le1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICRwcmltYXJ5LWNvbG9yLTE7XG59XG5cbi5yaWdodC1kb25lLXNrZWxldG9ue1xuICAgIG1hcmdpbjogYXV0bztcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAvLyBib3JkZXItcmFkaXVzOiAxcmVtO1xuICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICRwcmltYXJ5LWNvbG9yLTE7XG59XG5cbi5yaWdodC1wYW5lbC1kb25le1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1zaXplOiAwLjYyNXJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cblxuXG5cbi8vIC5naWctY2FsZW5kZXJ7XG4vLyAgICAgW2NvbC0xXSB7XG4vLyAgICAgICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XG4vLyAgICAgICAgIC13ZWJraXQtZmxleDogMCAwIDguMzMzMzMlO1xuLy8gICAgICAgICAtbXMtZmxleDogMCAwIDguMzMzMyU7XG4vLyAgICAgICAgIGZsZXg6IDAgMCAxNC4yODU3MTQyODU3MTQyODYlO1xuLy8gICAgICAgICB3aWR0aDogMTQuMjg1NzE0Mjg1NzE0Mjg2JTtcbi8vICAgICAgICAgbWF4LXdpZHRoOiAxNC4yODU3MTQyODU3MTQyODYlO1xuLy8gICAgIH1cbi8vICAgICAuY29sIHtcbi8vICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuLy8gICAgICAgICBwYWRkaW5nOiA1cHg7XG4vLyAgICAgfVxuLy8gICAgIC5sYXN0LW1vbnRoLCAubmV4dC1tb250aCB7XG4vLyAgICAgICAgIGNvbG9yOiAjOTk5OTk5O1xuLy8gICAgICAgICBmb250LXNpemU6IDkwJTtcbi8vICAgICB9XG4vLyAgICAgLmN1cnJlbnREYXRlLCAub3RoZXJEYXRlIHtcbi8vICAgICAgICAgcGFkZGluZzogNXB4O1xuLy8gICAgIH1cbi8vICAgICAuY3VycmVudERhdGUge1xuLy8gICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbi8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuLy8gICAgICAgICBjb2xvcjogd2hpdGU7XG4vLyAgICAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4vLyAgICAgfVxuLy8gICAgIC5jYWxlbmRhci1oZWFkZXIge1xuLy8gICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMWE3MzJkO1xuLy8gICAgICAgICBjb2xvcjogI0ZGRkZGRjtcbi8vICAgICB9XG4vLyAgICAgLmV2ZW50LWJ1bGxldCB7XG4vLyAgICAgICAgIG1hcmdpbjogMnB4IGF1dG87XG4vLyAgICAgICAgIGhlaWdodDogNXB4O1xuLy8gICAgICAgICB3aWR0aDogNXB4O1xuLy8gICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0zO1xuLy8gICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuLy8gICAgIH1cbi8vICAgICAuc2VsZWN0ZWQtZGF0ZSB7XG4vLyAgICAgICAgIHdpZHRoOiAyMHB4O1xuLy8gICAgICAgICBoZWlnaHQ6IDJweDtcbi8vICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcbi8vICAgICB9XG4vLyAgICAgLnVuc2VsZWN0ZWQtZGF0ZSB7XG4vLyAgICAgICAgIGJvcmRlcjogbm9uZTtcbi8vICAgICB9XG4vLyAgICAgLmNhbGVuZGFyLWJvZHl7XG4vLyAgICAgICAgIC5ncmlkIHtcbi8vICAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4vLyAgICAgICAgIH1cbi8vICAgICAgICAgLmNvbDpsYXN0LWNoaWxkIHtcbi8vICAgICAgICAgICAgIGJvcmRlci1yaWdodDogbm9uZTtcbi8vICAgICAgICAgfVxuLy8gICAgICAgICAuY2FsZW5kYXItd2Vla2RheSwgLmNhbGVuZGFyLWRhdGUge1xuLy8gICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuLy8gICAgICAgICAgICAgbWFyZ2luOiAwO1xuLy8gICAgICAgICB9XG4vLyAgICAgICAgIC5jYWxlbmRhci13ZWVrZGF5IHtcbi8vICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuLy8gICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICMxQTczMkQ7XG4vLyAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjZBQjU2O1xuLy8gICAgICAgICB9XG4vLyAgICAgICAgIC5jYWxlbmRhci1kYXRlIHtcbi8vICAgICAgICAgICAgIGJvcmRlci10b3A6IHNvbGlkIDFweCAjMUE3MzJEO1xuLy8gICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICMxYTczMkQ7XG4vLyAgICAgICAgIH1cbi8vICAgICB9XG4vLyB9XG5cbi8vZW5kIG9mIGNhbGVuZGFyIGNzc1xuXG4uZm9vdGVyLXRvb2xiYXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG59XG5cbi5mb290ZXItYnV0dG9uIHtcbiAgICBjb2xvcjogJHdoaXRlO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbn1cblxuLm5vLXJvdXRlLWNsYXNze1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm5vLWNvbXBsZXRlZC1yb3V0ZXN7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4iXX0= */";
    /***/
  },

  /***/
  "./src/app/routes/routes.component.ts":
  /*!********************************************!*\
    !*** ./src/app/routes/routes.component.ts ***!
    \********************************************/

  /*! exports provided: RoutesPage */

  /***/
  function srcAppRoutesRoutesComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RoutesPage", function () {
      return RoutesPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/shared/services/schedule.service */
    "./src/shared/services/schedule.service.ts");
    /* harmony import */


    var src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/shared/services/loader.service */
    "./src/shared/services/loader.service.ts");
    /* harmony import */


    var src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/shared/services/routing.service */
    "./src/shared/services/routing.service.ts");
    /* harmony import */


    var src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/shared/services/alert.service */
    "./src/shared/services/alert.service.ts");
    /* harmony import */


    var src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/shared/services/toast.service */
    "./src/shared/services/toast.service.ts");
    /* harmony import */


    var src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/shared/services/sap-api.service */
    "./src/shared/services/sap-api.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var RoutesPage =
    /*#__PURE__*/
    function () {
      // for calender
      // date: any;
      // daysInThisMonth: any;
      // daysInLastMonth: any;
      // daysInNextMonth: any;
      // monthNames: string[];
      // currentMonth: any;
      // currentYear: any;
      // currentDate: any;
      // event = {title: "", location: "", message: "", startDate: "", endDate: "" };
      // eventList: any;
      // selectedEvent: any;
      // isSelected: any;
      // end of calender
      function RoutesPage(scheduleService, loaderService, routingService, router, alertService, toastService, sapApiService, domCtrl) {
        _classCallCheck(this, RoutesPage);

        this.scheduleService = scheduleService;
        this.loaderService = loaderService;
        this.routingService = routingService;
        this.router = router;
        this.alertService = alertService;
        this.toastService = toastService;
        this.sapApiService = sapApiService;
        this.domCtrl = domCtrl;
        this.currentSegment = "myRoutes";
        this.skeletonBoolean = true;
        this.titleSize = "large";
        this.userRoutes = [];
        this.completedRoutes = [];
        this.routeShellRoutes = [];
        this.allSchedules = [];
        this.userDesiredSchedules = [];
        this.checkboxChecker = false;
      }

      _createClass(RoutesPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {// this.ionCalendar();
          // this.getDaysOfMonth();
          // this.checkDates();
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {} // onScroll(event) {
        //   // console.log("testing: ")
        //   // console.log(event.scrollTop);
        //   let footerHidden: boolean;
        //   if(event.detail.deltaY > 0 && footerHidden) return;
        //   if(event.detail.deltaY < 0 && !footerHidden) return;
        //   if(event.detail.deltaY > 50) {
        //     console.log("reveal footer");
        //     return;
        //   } else {
        //     console.log("hide footer");
        //     return;
        //   }
        // }

      }, {
        key: "onContentScroll",
        value: function onContentScroll(ev) {
          var _this = this;

          this.domCtrl.write(function () {
            // this.updateHeader(ev);
            // console.log(ev.detail.scrollTop);
            if (ev.detail.scrollTop > 50) {
              _this.titleSize = undefined;
            } else {
              _this.titleSize = "large";
            }
          });
        } // updateHeader(ev) {
        //   console.log(ev.scrollTop);
        // }

      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.skeletonBoolean = true;
          this.userRoutes = [];
          this.completedRoutes = [];
          this.checkIfFromOtherPage(); // this.delay(3000).then(any => {
          //   if(localStorage.getItem("current_route_started")){
          //     this.router.navigate([`/tabs/drive`]);
          //   }
          // });

          if (this.currentSegment == "completedShyfts") {
            this.getCompletedRoutes();
          }

          if (localStorage.getItem("shyft_toggle")) {
            localStorage.removeItem("shyft_toggle");
          }

          this.hasRoute = localStorage.getItem("route_id");
          console.log("has route", this.hasRoute);

          if (this.hasRoute && this.currentSegment != "completedShyfts") {
            this.toastService.presentToast("You cannot start another route");
          }
        }
      }, {
        key: "delay",
        value: function delay(ms) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return new Promise(function (resolve) {
                      return setTimeout(function () {
                        return resolve();
                      }, ms);
                    }).then(function () {
                      return console.log("fired");
                    });

                  case 2:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee);
          }));
        }
      }, {
        key: "checkIfFromOtherPage",
        value: function checkIfFromOtherPage() {
          // if (localStorage.getItem("current_route")) {
          //   let currentroute = JSON.parse(localStorage.getItem("current_route"));
          //   this.openrouteDetails(currentroute);
          //   localStorage.removeItem("current_route");
          //   localStorage.setItem("navigate", JSON.stringify(true));
          // } else {
          this.getUserRoutes(); // }
        }
      }, {
        key: "segmentChanged",
        value: function segmentChanged(event) {
          console.log("Event Changed", event.detail.value);
          this.currentSegment = event.detail.value;

          if ("myRoutes" === event.detail.value) {
            // this.getUserrouteAndDesiredSchedule();
            this.getUserRoutes();
            this.checkboxChecker = false;
          } else {
            this.completedRoutes = [];
            this.skeletonBoolean = true;
            this.getCompletedRoutes(); // this.getAllSchedule();
            // this.checkDates();
          }
        }
      }, {
        key: "getAllSchedule",
        value: function getAllSchedule() {
          var _this2 = this;

          // this.loaderService.createLoader("Loading...");
          this.scheduleService.getSchedules("available").subscribe(function (result) {
            console.log("Get All Schedule", result);
            _this2.allSchedules = result;
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
              for (var _iterator = _this2.allSchedules[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var schedule = _step.value;
                schedule.checked = false;
              }
            } catch (err) {
              _didIteratorError = true;
              _iteratorError = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }
              } finally {
                if (_didIteratorError) {
                  throw _iteratorError;
                }
              }
            }

            _this2.checkboxChecker = false;

            _this2.checkDates();

            _this2.skeletonBoolean = false; // this.loaderService.dismissLoader();
          }, function (error) {
            console.log("Error", error);
            _this2.skeletonBoolean = false; // this.loaderService.dismissLoader();
          });
        }
      }, {
        key: "getCompletedRoutes",
        value: function getCompletedRoutes() {
          var _this3 = this;

          this.routingService.getModeUserRoute().subscribe(function (result) {
            _this3.completedRoutes = result.reverse();
            _this3.skeletonBoolean = false;
            console.log("Complete ", _this3.completedRoutes);
          }, function (error) {
            console.log(error);
          });
        }
      }, {
        key: "getUserRoutes",
        value: function getUserRoutes() {
          var _this4 = this;

          // this.loaderService.createLoader("Loading...");
          // this.routingService.getUserRoute().subscribe(
          //   result => {
          // console.log("Get User Schedule", result);
          // this.skeletonBoolean = false;
          // this.userRoutes = result;
          this.getCompletedSchedule();
          this.sapApiService.getUserRouteShell().subscribe( //this.routingService.getModeUserRoute().subscribe(
          function (result) {
            console.log("Route Shell", result);
            _this4.routeShellRoutes = result;
            _this4.userRoutes = result;

            if (_this4.routeShellRoutes.length != 0) {
              var _iteratorNormalCompletion2 = true;
              var _didIteratorError2 = false;
              var _iteratorError2 = undefined;

              try {
                for (var _iterator2 = _this4.routeShellRoutes[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                  var routeShell = _step2.value;
                  console.log("Route ", routeShell.routeId); // for (let userRoute of this.userRoutes) {
                  //   if (routeShell.routeId === userRoute.routeId) {
                  //     userRoute.deliveryCount = routeShell.deliveryCount;
                  //     userRoute.routeId = routeShell.routeId;
                  //     userRoute.aceCount = routeShell.aceCount;
                  //     userRoute.cynchCount = routeShell.cynchCount;
                  //     userRoute.items = routeShell.items;
                  //     userRoute.totalRemainingCyls = routeShell.totalRemainingCyls;
                  //   }
                  // }

                  var toDepotRoute = void 0;

                  if (localStorage.getItem("current_route")) {
                    var currentRoute = JSON.parse(localStorage.getItem("current_route")); // for (let routeShell of this.routeShellRoutes) {

                    var _iteratorNormalCompletion3 = true;
                    var _didIteratorError3 = false;
                    var _iteratorError3 = undefined;

                    try {
                      for (var _iterator3 = _this4.userRoutes[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                        var userRoutes = _step3.value;

                        if (userRoutes.routeId === currentRoute.routeId) {
                          userRoutes.deliveryCount = currentRoute.deliveryCount;
                          userRoutes.routeId = currentRoute.routeId;
                          userRoutes.aceCount = currentRoute.aceCount;
                          userRoutes.cynchCount = currentRoute.cynchCount;
                          userRoutes.items = currentRoute.items; // userRoutes.totalRemainingCyls = currentRoute.totalRemainingCyls;

                          toDepotRoute = userRoutes;
                        }
                      } // }

                    } catch (err) {
                      _didIteratorError3 = true;
                      _iteratorError3 = err;
                    } finally {
                      try {
                        if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                          _iterator3.return();
                        }
                      } finally {
                        if (_didIteratorError3) {
                          throw _iteratorError3;
                        }
                      }
                    }

                    _this4.openRouteDetails(toDepotRoute);

                    localStorage.removeItem("current_route");
                    localStorage.setItem("navigate", JSON.stringify(true));
                  }
                }
              } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
              } finally {
                try {
                  if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                    _iterator2.return();
                  }
                } finally {
                  if (_didIteratorError2) {
                    throw _iteratorError2;
                  }
                }
              }

              _this4.skeletonBoolean = false; // this.loaderService.dismissLoader();
            } else {
              // this.loaderService.dismissLoader();
              _this4.skeletonBoolean = false;
            }
          }, function (error) {
            console.log("Error", error);
            _this4.skeletonBoolean = false; // this.loaderService.dismissLoader();
          }); // },
          // error => {
          //   console.log("Error", error);
          //   this.skeletonBoolean = false;
          //   this.loaderService.dismissLoader();
          // });
        }
      }, {
        key: "getUserDesiredSchedule",
        value: function getUserDesiredSchedule() {
          return new Promise(function (resolve, reject) {
            var _this5 = this;

            this.scheduleService.getSchedules("user").subscribe(function (result) {
              console.log("Get User Desired Schedule", result);
              _this5.userDesiredSchedules = result;
              resolve(true);
            }, function (error) {
              console.log("Error", error);
              resolve(false);
            });
          }.bind(this));
        }
      }, {
        key: "getCompletedSchedule",
        value: function getCompletedSchedule() {
          this.completeSchedules = new Array();
          this.uncompleteSchedules = new Array();
          var today = new Date(Date.now());
          today.setHours(0, 0, 0, 0);
          var _iteratorNormalCompletion4 = true;
          var _didIteratorError4 = false;
          var _iteratorError4 = undefined;

          try {
            for (var _iterator4 = this.userRoutes[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
              var schedule = _step4.value;
              var sched = new Date(schedule.deliveryDate); // console.log(sched.getDate());

              console.log("time" + today.getTime() + " t " + sched.getTime());

              if (sched < today) {
                this.completeSchedules.push(schedule); // this.userRoutes.pop();
              } else {
                this.uncompleteSchedules.push(schedule);
              } // console.log("Completed: ", this.completeSchedules);

            }
          } catch (err) {
            _didIteratorError4 = true;
            _iteratorError4 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion4 && _iterator4.return != null) {
                _iterator4.return();
              }
            } finally {
              if (_didIteratorError4) {
                throw _iteratorError4;
              }
            }
          }

          console.log("Completed Schedule", this.uncompleteSchedules);
        }
      }, {
        key: "submitSelectedSchedule",
        value: function submitSelectedSchedule() {
          var _this6 = this;

          this.loaderService.createLoader("Submitting...");
          var desiredSchedule = {
            scheduleId: []
          };
          var _iteratorNormalCompletion5 = true;
          var _didIteratorError5 = false;
          var _iteratorError5 = undefined;

          try {
            for (var _iterator5 = this.allSchedules[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
              var schedule = _step5.value;

              if (schedule.checked) {
                desiredSchedule.scheduleId.push(schedule.scheduleId);
              }
            }
          } catch (err) {
            _didIteratorError5 = true;
            _iteratorError5 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion5 && _iterator5.return != null) {
                _iterator5.return();
              }
            } finally {
              if (_didIteratorError5) {
                throw _iteratorError5;
              }
            }
          }

          this.scheduleService.sendDesiredSchedule(desiredSchedule).subscribe(function (result) {
            console.log("Schedule sent", result);
            var _iteratorNormalCompletion6 = true;
            var _didIteratorError6 = false;
            var _iteratorError6 = undefined;

            try {
              for (var _iterator6 = _this6.allSchedules[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                var schedule = _step6.value;
                schedule.checked = false;
              }
            } catch (err) {
              _didIteratorError6 = true;
              _iteratorError6 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion6 && _iterator6.return != null) {
                  _iterator6.return();
                }
              } finally {
                if (_didIteratorError6) {
                  throw _iteratorError6;
                }
              }
            }

            _this6.getAllSchedule();

            _this6.loaderService.dismissLoader();

            _this6.toastService.presentToast("Successfully scheduled a Shyft");
          }, function (error) {
            console.log("Failed", error);

            _this6.loaderService.dismissLoader();

            _this6.toastService.presentToast("Failed to schedule a Shyft");
          });
        }
      }, {
        key: "checkIfChecked",
        value: function checkIfChecked() {
          var _iteratorNormalCompletion7 = true;
          var _didIteratorError7 = false;
          var _iteratorError7 = undefined;

          try {
            for (var _iterator7 = this.allSchedules[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
              var schedule = _step7.value;

              if (schedule.checked) {
                this.checkboxChecker = true;
                console.log(true);
                return;
              } else {
                this.checkboxChecker = false;
              }
            }
          } catch (err) {
            _didIteratorError7 = true;
            _iteratorError7 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion7 && _iterator7.return != null) {
                _iterator7.return();
              }
            } finally {
              if (_didIteratorError7) {
                throw _iteratorError7;
              }
            }
          }
        }
      }, {
        key: "clearSelectedSchedule",
        value: function clearSelectedSchedule() {
          var _iteratorNormalCompletion8 = true;
          var _didIteratorError8 = false;
          var _iteratorError8 = undefined;

          try {
            for (var _iterator8 = this.allSchedules[Symbol.iterator](), _step8; !(_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done); _iteratorNormalCompletion8 = true) {
              var schedule = _step8.value;

              if (schedule.checked) {
                schedule.checked = false;
              }
            }
          } catch (err) {
            _didIteratorError8 = true;
            _iteratorError8 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion8 && _iterator8.return != null) {
                _iterator8.return();
              }
            } finally {
              if (_didIteratorError8) {
                throw _iteratorError8;
              }
            }
          }

          this.checkboxChecker = false;
        }
      }, {
        key: "openRouteDetails",
        value: function openRouteDetails(route) {
          var navigationExtras = {
            state: {
              route: route
            }
          };
          this.router.navigate(["/tabs/routes/".concat(route.routeId)], navigationExtras);
        } // // for calendar

      }, {
        key: "checkDates",
        value: function checkDates() {
          var curr = new Date();
          var first = curr.getDate() - curr.getDay() + 1;
          console.log(curr.getDate());
          console.log(curr.getDay());
          var last = first + 6;
          var thisDay = new Date(Date.now());
          this.weekdates = new Array();
          this.events = new Array();
          var event = false;
          this.today = thisDay.getDate();
          console.log('this is today: ' + thisDay); // let firstDay = new Date(curr.setDate(first)).toLocaleDateString();
          // let lastDay = new Date(curr.setDate(last)).toLocaleDateString();
          // console.log(this.today.toString());
          // console.log(curr.getDay().toLocaleString());
          // this.firstDay = new Date(curr.setDate(first)).toLocaleDateString();
          // console.log(firstDay);
          // console.log(lastDay);

          for (var firstD = first; firstD <= last; firstD++) {
            var day = new Date(curr.setDate(firstD)).toLocaleDateString(); // this.weekdates.push(day);

            var events = {
              date: "",
              event: false
            };
            events.date = day;
            var _iteratorNormalCompletion9 = true;
            var _didIteratorError9 = false;
            var _iteratorError9 = undefined;

            try {
              for (var _iterator9 = this.allSchedules[Symbol.iterator](), _step9; !(_iteratorNormalCompletion9 = (_step9 = _iterator9.next()).done); _iteratorNormalCompletion9 = true) {
                var schedule = _step9.value;
                var sched = schedule.deliveryDate.split("-");
                var month = day.split("/");

                if (sched[2] == firstD && sched[1] == month[0]) {
                  event = true;
                  break;
                } else {
                  event = false;
                }
              } // this.events.push(event);

            } catch (err) {
              _didIteratorError9 = true;
              _iteratorError9 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion9 && _iterator9.return != null) {
                  _iterator9.return();
                }
              } finally {
                if (_didIteratorError9) {
                  throw _iteratorError9;
                }
              }
            }

            events.event = event;
            console.log(events);
            this.weekdates.push(events);
          }

          console.log(this.weekdates);
        } // ionCalendar() {
        //   this.date = new Date();
        //   this.eventList = new Array();
        //   this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        //   this.getDaysOfMonth();
        // }
        // getDaysOfMonth() {
        //   this.daysInThisMonth = new Array();
        //   this.daysInLastMonth = new Array();
        //   this.daysInNextMonth = new Array();
        //   this.currentMonth = this.monthNames[this.date.getMonth()];
        //   this.currentYear = this.date.getFullYear();
        //   if(this.date.getMonth() === new Date().getMonth()) {
        //     this.currentDate = new Date().getDate();
        //   } else {
        //     this.currentDate = 999;
        //   }
        //   var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
        //   var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
        //   for(var i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++){
        //     this.daysInLastMonth.push(i);
        //   }
        //   var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
        //   for(var i = 0; i < thisNumOfDays; i++){
        //     this.daysInThisMonth.push(i+1);
        //   }
        //   var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
        //   var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
        //   for (var i = 0; i < (6-lastDayThisMonth); i++) {
        //     this.daysInNextMonth.push(i+1);
        //   }
        //   var totalDays = this.daysInLastMonth.length+this.daysInThisMonth.length+this.daysInNextMonth.length;
        //   if(totalDays<36) {
        //     for(var i = (7-lastDayThisMonth); i < ((7-lastDayThisMonth)+7); i++){
        //       this.daysInNextMonth.push(i);
        //     }
        //   }
        // }
        // goToLastMonth() {
        //   this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
        //   this.getDaysOfMonth();
        // }
        // goToNextMonth() {
        //   this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
        //   this.getDaysOfMonth();
        // }
        // saveEvent() {
        //   this.calendar.createEvent(this.event.title, this. event.location, this.event.message, new Date(this.event.startDate), new Date(this.event.endDate)).then(
        //     (msg) => {
        //       console.log("event saved");
        //     },
        //     (err) => {
        //       console.log(err);
        //     }
        //   );
        // }
        // loadEventThisMonth() {
        //   // this.eventList = new Array();
        //   var startDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        //   var endDate = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0);
        //   this.calendar.listEventsInRange(startDate, endDate).then(
        //     (msg) => {
        //       msg.forEach(item => {
        //         this.eventList.push(item);
        //       });
        //     },
        //     (err) => {
        //       console.log(err);
        //     }
        //   );
        // }
        // checkEvent(day) {
        //   var hasEvent = false;
        //   var thisDate1 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 00:00:00";
        //   var thisDate2 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 23:59:59";
        //   this.eventList.forEach(event => {
        //     if(((event.startDate >= thisDate1) && (event.startDate <= thisDate2)) || ((event.endDate >= thisDate1) && (event.endDate <= thisDate2))) {
        //       hasEvent = true;
        //     }
        //   });
        //   return hasEvent;
        // }
        // selectDate(day){
        //   this.isSelected = false;
        //   this.selectedEvent = new Array();
        //   var thisDate1 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 00:00:00";
        //   var thisDate2 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 23:59:59";
        //   this.eventList.forEach(event => {
        //     if(((event.startDate >= thisDate1) && (event.startDate <= thisDate2)) || ((event.endDate >= thisDate1) && (event.endDate <= thisDate2))) {
        //       this.isSelected = true;
        //       this.selectedEvent.push(event);
        //     }
        //   });
        // }
        // //end of calendar

      }, {
        key: "getTotalQuantity",
        value: function getTotalQuantity(items) {
          var total = 0;
          items.forEach(function (item) {
            total += item.quantityDelivered;
          });
          return total;
        }
      }, {
        key: "goToGigsDone",
        value: function goToGigsDone(route) {
          var navigationExtras = {
            state: {
              route: route
            }
          };
          this.router.navigate(["/tabs/routes/".concat(route.routeId, "/route-done")], navigationExtras);
        }
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ionViewDidEnter();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }]);

      return RoutesPage;
    }();

    RoutesPage.ctorParameters = function () {
      return [{
        type: src_shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_3__["ScheduleService"]
      }, {
        type: src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"]
      }, {
        type: src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_5__["RoutingService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"]
      }, {
        type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"]
      }, {
        type: src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_8__["SapApiService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["DomController"]
      }];
    };

    RoutesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-routes',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./routes.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/routes/routes.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./routes.component.scss */
      "./src/app/routes/routes.component.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_3__["ScheduleService"], src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"], src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_5__["RoutingService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"], src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"], src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_8__["SapApiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["DomController"]])], RoutesPage);
    /***/
  },

  /***/
  "./src/app/routes/routes.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/routes/routes.module.ts ***!
    \*****************************************/

  /*! exports provided: RoutesModule */

  /***/
  function srcAppRoutesRoutesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RoutesModule", function () {
      return RoutesModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/shared/shared.module */
    "./src/shared/shared.module.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _routes_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./routes.component */
    "./src/app/routes/routes.component.ts");

    var RoutesModule = function RoutesModule() {
      _classCallCheck(this, RoutesModule);
    };

    RoutesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild([{
        path: '',
        component: _routes_component__WEBPACK_IMPORTED_MODULE_7__["RoutesPage"]
      }])],
      declarations: [_routes_component__WEBPACK_IMPORTED_MODULE_7__["RoutesPage"]]
    })], RoutesModule);
    /***/
  }
}]);
//# sourceMappingURL=routes-routes-module-es5.js.map