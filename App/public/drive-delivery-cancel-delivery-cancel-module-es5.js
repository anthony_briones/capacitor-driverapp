function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["drive-delivery-cancel-delivery-cancel-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-cancel/delivery-cancel.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-cancel/delivery-cancel.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDriveDeliveryCancelDeliveryCancelPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>End Delivery</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content class=\"ion-padding\">\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-row>\n    <ion-col size=\"12\">\n      <p class=\"subtitle-text\">Reason:</p>\n      <ion-input class=\"content-text\" placeholder=\"Why can't you continue\"\n      [(ngModel)]=\"nonDeliveryReason\"></ion-input>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col size=\"12\">\n      <div class=\"ion-padding\">\n        <button class=\"endDelivery\" (click)=\"nextDelivery()\">End Delivery</button>\n      </div>\n    </ion-col>\n\n    <!-- <ion-col size=\"12\">\n      <div class=\"ion-padding\">\n        <button class=\"returnButton\" (click)=\"returnToDeliveryConfirmation()\">Go Back</button>\n      </div>\n    </ion-col> -->\n  </ion-row>\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/drive/delivery-cancel/delivery-cancel.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/drive/delivery-cancel/delivery-cancel.module.ts ***!
    \*****************************************************************/

  /*! exports provided: DeliveryCancelModule */

  /***/
  function srcAppDriveDeliveryCancelDeliveryCancelModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeliveryCancelModule", function () {
      return DeliveryCancelModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _delivery_cancel_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./delivery-cancel.page */
    "./src/app/drive/delivery-cancel/delivery-cancel.page.ts");

    var routes = [{
      path: '',
      component: _delivery_cancel_page__WEBPACK_IMPORTED_MODULE_6__["DeliveryCancelPage"]
    }];

    var DeliveryCancelModule = function DeliveryCancelModule() {
      _classCallCheck(this, DeliveryCancelModule);
    };

    DeliveryCancelModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild(routes)],
      declarations: [_delivery_cancel_page__WEBPACK_IMPORTED_MODULE_6__["DeliveryCancelPage"]]
    })], DeliveryCancelModule);
    /***/
  },

  /***/
  "./src/app/drive/delivery-cancel/delivery-cancel.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/drive/delivery-cancel/delivery-cancel.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppDriveDeliveryCancelDeliveryCancelPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\n.content-text {\n  padding: 0;\n  font-size: 1rem;\n  font-family: \"Montserrat-Regular\";\n}\n\n.end-text {\n  padding: 0 0 0.2rem 0;\n  margin: 0;\n  font-size: 0.8rem;\n  color: #4b4b4b;\n  font-family: \"Montserrat-SemiBold\";\n}\n\nbutton {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n}\n\n.returnButton {\n  background: #003A58;\n}\n\n.endDelivery {\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036));\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%);\n}\n\nion-input {\n  margin-top: 1rem;\n  margin-left: auto;\n  margin-right: auto;\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL2RyaXZlL2RlbGl2ZXJ5LWNhbmNlbC9kZWxpdmVyeS1jYW5jZWwucGFnZS5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXBwL2RyaXZlL2RlbGl2ZXJ5LWNhbmNlbC9kZWxpdmVyeS1jYW5jZWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU9BO0VBQ0kseUJBQUE7RUFDQSxnREFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkVsQmM7RUZtQmQsV0FBQTtFQUNBLGNFaEJJO0VGaUJKLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSwrQkFBQTtFQUFBLHdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxvQ0FBQTtVQUFBLDhCQUFBO0FDTko7O0FEU0E7RUFDSSxtQ0FBQTtVQUFBLGtDQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtVQUFBLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx3QkFBQTtVQUFBLHVCQUFBO0FDTko7O0FEU0E7RUFDSSxZQUFBO0FDTko7O0FFaERBO0VBQ0ksVUFBQTtFQUNBLGVBQUE7RUFDQSxpQ0FBQTtBRm1ESjs7QUVoREE7RUFDSSxxQkFBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxrQ0FBQTtBRm1ESjs7QUVoREE7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUZtREo7O0FFaERBO0VBQ0ksbUJBQUE7QUZtREo7O0FFaERBO0VBQ0ksNkZEckJnQjtFQ3FCaEIsb0VEckJnQjtBRHdFcEI7O0FFaERBO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBRm1ESiIsImZpbGUiOiJzcmMvYXBwL2RyaXZlL2RlbGl2ZXJ5LWNhbmNlbC9kZWxpdmVyeS1jYW5jZWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2ZvbnRzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMnO1xuXG4vLyAuaXMtdmFsaWQge1xuLy8gICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWVcbi8vIH1cblxuLmlzLWludmFsaWQge1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNFRTQwMzY7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDNcbn1cblxuLmlzLWludmFsaWQtc2VsZWN0IHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6ICR3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgaGVpZ2h0OiAycmVtO1xufVxuXG4uYmctY29sb3ItMXtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgICBkaXNwbGF5OiBmbGV4IWltcG9ydGFudDtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXIhaW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5mbGV4LWFsaWduLWNlbnRlciB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgICBvcGFjaXR5OiAwLjU7XG59XG5cblxuXG5cbiIsIi5pcy1pbnZhbGlkIHtcbiAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICBib3JkZXI6IDFweCBzb2xpZCAjNUVCMDAzO1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWFsaWduLXJpZ2h0IHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgb3BhY2l0eTogMC41O1xufVxuXG4uY29udGVudC10ZXh0IHtcbiAgcGFkZGluZzogMDtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LVJlZ3VsYXJcIjtcbn1cblxuLmVuZC10ZXh0IHtcbiAgcGFkZGluZzogMCAwIDAuMnJlbSAwO1xuICBtYXJnaW46IDA7XG4gIGZvbnQtc2l6ZTogMC44cmVtO1xuICBjb2xvcjogIzRiNGI0YjtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xufVxuXG5idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gIGhlaWdodDogM3JlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuLnJldHVybkJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbi5lbmREZWxpdmVyeSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpO1xufVxuXG5pb24taW5wdXQge1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICBkaXNwbGF5OiBibG9jaztcbn0iLCJcbi8vIGNvbG9yc1xuLy8gJHByaW1hcnktY29sb3ItMTogIzAwQTNDODtcbiRwcmltYXJ5LWNvbG9yLTE6ICMwMDNBNTg7XG4kcHJpbWFyeS1jb2xvci0yOiAjRkFBRjQwO1xuJHByaW1hcnktY29sb3ItMzogI0Y0NzczQjtcblxuJHdoaXRlOiAjZmZmZmZmO1xuXG4vLyAkYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsIHJnYmEoMjU1LDI1NSwyNTUsMC41MCkgMCUsIHJnYmEoMCwwLDAsMC41MCkgMTAwJSk7XG4kYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpOyIsIkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy9jb21tb24uc2Nzcyc7XG5AaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MnO1xuXG4uY29udGVudC10ZXh0e1xuICAgIHBhZGRpbmc6IDA7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1SZWd1bGFyJztcbn1cblxuLmVuZC10ZXh0e1xuICAgIHBhZGRpbmc6IDAgMCAuMnJlbSAwO1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LXNpemU6IC44cmVtO1xuICAgIGNvbG9yOiAjNGI0YjRiO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG59XG5cbmJ1dHRvbntcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDEuMTI1cmVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICAgIGhlaWdodDogM3JlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnXG59XG5cbi5yZXR1cm5CdXR0b257XG4gICAgYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuLmVuZERlbGl2ZXJ5e1xuICAgIGJhY2tncm91bmQtaW1hZ2U6ICRidXR0b24tZ3JhZGllbnQtMTtcbn1cblxuaW9uLWlucHV0e1xuICAgIG1hcmdpbi10b3A6IDFyZW07XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/drive/delivery-cancel/delivery-cancel.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/drive/delivery-cancel/delivery-cancel.page.ts ***!
    \***************************************************************/

  /*! exports provided: DeliveryCancelPage */

  /***/
  function srcAppDriveDeliveryCancelDeliveryCancelPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeliveryCancelPage", function () {
      return DeliveryCancelPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/shared/services/sap-api.service */
    "./src/shared/services/sap-api.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/shared/services/loader.service */
    "./src/shared/services/loader.service.ts");
    /* harmony import */


    var src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/shared/services/toast.service */
    "./src/shared/services/toast.service.ts");

    var DeliveryCancelPage =
    /*#__PURE__*/
    function () {
      function DeliveryCancelPage(router, sapApiService, nav, loaderService, modalController, toastService) {
        _classCallCheck(this, DeliveryCancelPage);

        this.router = router;
        this.sapApiService = sapApiService;
        this.nav = nav;
        this.loaderService = loaderService;
        this.modalController = modalController;
        this.toastService = toastService;
        this.deliveries = [];
        this.nonDeliveryReason = "";
      }

      _createClass(DeliveryCancelPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.currentDelivery = JSON.parse(localStorage.getItem("current_delivery"));
          this.stopSequenceNum = parseInt(localStorage.getItem("stop_sequence"));
          this.stopsLength = parseInt(localStorage.getItem("stops_length"));
          this.currentRoute = JSON.parse(localStorage.getItem("current_route_started")); // this.currentRoute.startTime = "10:00";

          this.getTankType();
        }
      }, {
        key: "getTankType",
        value: function getTankType() {
          if (this.currentDelivery.orderSource.toUpperCase() === "CYNCH") {
            this.isCynchOrder = true;
          } else {
            this.isCynchOrder = false;
          }
        }
      }, {
        key: "returnToDeliveryConfirmation",
        value: function returnToDeliveryConfirmation() {
          this.router.navigate(["/tabs/drive/".concat(this.currentRoute.routeId, "/confirmation")]);
        }
      }, {
        key: "nextDelivery",
        value: function nextDelivery() {
          if (this.nonDeliveryReason == "") {
            this.toastService.errorToast("Please provide a reason to end Delivery");
          } else {
            this.stopSequenceNum = this.stopSequenceNum + 1;

            if (this.stopsLength < this.stopSequenceNum) {
              this.endDelivery();
            } else {
              this.endDelivery();
            }
          }
        }
      }, {
        key: "endDelivery",
        value: function endDelivery() {
          var _this = this;

          this.loaderService.createLoader("Ending Delivery...");
          var editedDelivery = JSON.parse(JSON.stringify(this.currentDelivery));
          console.log("Current Delivery", editedDelivery);
          console.log("Current Route", this.currentRoute);
          editedDelivery.startTime = localStorage.getItem("start_time");
          editedDelivery.endTime = this.sapApiService.getConfirmationDateTime();
          editedDelivery.deliveryStatus.deliveryStatusId = "006"; // let tankDelivered: number = 0;
          // let tankRemaining: number = this.currentRoute.totalRemainingCyls;
          // editedDelivery.totalRemainingCyls = tankRemaining;

          if (localStorage.getItem("mileage")) {
            editedDelivery.mileage = parseFloat(localStorage.getItem("mileage"));
          } else {
            //Edited only due to testing please return to 0 when making a build
            editedDelivery.mileage = 27; // editedDelivery.mileage = 0;
          }

          editedDelivery.nonDeliveryReason = this.nonDeliveryReason;
          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = editedDelivery.items[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var item = _step.value;
              item.quantityDelivered = 0;
              item.quantityReturned = 0;
            }
          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }

          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = editedDelivery.services[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var service = _step2.value;
              service.quantityDelivered = 0;
              service.quantityReturned = 0;
            }
          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                _iterator2.return();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }

          this.sapApiService.updateDelivery(editedDelivery, this.currentRoute).subscribe(function (result) {
            console.log("Update Delivery", result); // this.currentRoute.totalRemainingCyls = tankRemaining;

            localStorage.setItem("current_route_started", JSON.stringify(_this.currentRoute));
            localStorage.setItem("stop_sequence", _this.stopSequenceNum.toString());
            _this.stopsLength = result.stopsLength;
            localStorage.setItem("stops_length", _this.stopsLength.toString());
            var location = {
              latitude: 0,
              longitude: 0
            };
            location.latitude = editedDelivery.latitude;
            location.longitude = editedDelivery.longitude;
            localStorage.setItem("previous_loc", JSON.stringify(location));

            if (_this.stopsLength < _this.stopSequenceNum) {
              // let routeId = localStorage.getItem("route_id");
              // localStorage.removeItem("previous_loc");
              _this.router.navigate(["/tabs/drive/".concat(_this.currentRoute.routeId, "/delivery-summary")]);
            } else {
              // let location = {
              //   latitude: 0,
              //   longitude: 0
              // }
              // location.latitude = editedDelivery.latitude;
              // location.longitude = editedDelivery.longitude;
              // localStorage.setItem("previous_loc", JSON.stringify(location));
              localStorage.removeItem("start_time");
              localStorage.removeItem('signage');

              _this.router.navigate(["/tabs/drive"]);
            } // this.router.navigate([`/tabs/drive`]);


            _this.loaderService.dismissLoader();
          }, function (error) {
            console.log("Error", error);

            _this.loaderService.dismissLoader();
          });
        }
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ngOnInit();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }]);

      return DeliveryCancelPage;
    }();

    DeliveryCancelPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_3__["SapApiService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }, {
        type: src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }, {
        type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_6__["ToastService"]
      }];
    };

    DeliveryCancelPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-delivery-cancel',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./delivery-cancel.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-cancel/delivery-cancel.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./delivery-cancel.page.scss */
      "./src/app/drive/delivery-cancel/delivery-cancel.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_3__["SapApiService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_6__["ToastService"]])], DeliveryCancelPage);
    /***/
  }
}]);
//# sourceMappingURL=drive-delivery-cancel-delivery-cancel-module-es5.js.map