function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["settings-help-help-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/help/help.page.html":
  /*!************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/settings/help/help.page.html ***!
    \************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSettingsHelpHelpPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <!-- <ion-buttons slot=\"start\"> -->\n      <ion-back-button class=\"float-left\"></ion-back-button>\n    <!-- </ion-buttons> -->\n    <ion-title size=\"large\">Help</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-grid>\n    <ion-row class=\"ion-padding\">\n      <ion-col class=\"ion-padding help\">\n        Please contact your Operations Manager directly, for any questions or issues. Otherwise, please contact\n        Customer Service at \n        <a href=\"tel: 8885252899\"> 888-525-2899</a>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/settings/help/help.module.ts":
  /*!**********************************************!*\
    !*** ./src/app/settings/help/help.module.ts ***!
    \**********************************************/

  /*! exports provided: HelpPageModule */

  /***/
  function srcAppSettingsHelpHelpModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HelpPageModule", function () {
      return HelpPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _help_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./help.page */
    "./src/app/settings/help/help.page.ts");

    var routes = [{
      path: '',
      component: _help_page__WEBPACK_IMPORTED_MODULE_6__["HelpPage"]
    }];

    var HelpPageModule = function HelpPageModule() {
      _classCallCheck(this, HelpPageModule);
    };

    HelpPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"].forRoot({
        animated: true
      }), _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_help_page__WEBPACK_IMPORTED_MODULE_6__["HelpPage"]]
    })], HelpPageModule);
    /***/
  },

  /***/
  "./src/app/settings/help/help.page.scss":
  /*!**********************************************!*\
    !*** ./src/app/settings/help/help.page.scss ***!
    \**********************************************/

  /*! exports provided: default */

  /***/
  function srcAppSettingsHelpHelpPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-title {\n  color: #003A58;\n  font-family: \"Montserrat-Bold\";\n  padding-top: 1rem;\n}\n\nion-buttons {\n  padding-bottom: 3rem;\n}\n\n.help {\n  color: #003A58;\n  font-size: 1rem;\n  font-family: \"Montserrat-SemiBold\";\n  border-radius: 0.625rem;\n  border: 1px solid #003A58;\n}\n\n.float-left {\n  float: left;\n  margin-bottom: 3rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL3NldHRpbmdzL2hlbHAvaGVscC5wYWdlLnNjc3MiLCIvVXNlcnMvYW50aG9ueS5icmlvbmVzL0RvY3VtZW50cy9DYXBEcml2ZXIvc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hcHAvc2V0dGluZ3MvaGVscC9oZWxwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFPQTtFQUNJLHlCQUFBO0VBQ0EsZ0RBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkFBQTtBQ05KOztBRFNBO0VBQ0kseUJFbEJjO0VGbUJkLFdBQUE7RUFDQSxjRWhCSTtFRmlCSixtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtBQ05KOztBRFNBO0VBQ0ksK0JBQUE7RUFBQSx3QkFBQTtFQUNBLGdDQUFBO0VBQ0Esb0NBQUE7VUFBQSw4QkFBQTtBQ05KOztBRFNBO0VBQ0ksbUNBQUE7VUFBQSxrQ0FBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7VUFBQSx5QkFBQTtBQ05KOztBRFNBO0VBQ0ksd0JBQUE7VUFBQSx1QkFBQTtBQ05KOztBRFNBO0VBQ0ksWUFBQTtBQ05KOztBRS9DQTtFQUNJLGNERmM7RUNHZCw4QkFBQTtFQUNBLGlCQUFBO0FGa0RKOztBRS9DQTtFQUVJLG9CQUFBO0FGaURKOztBRTlDQTtFQUVJLGNEZGM7RUNlZCxlQUFBO0VBQ0Esa0NBQUE7RUFDQSx1QkFBQTtFQUNBLHlCQUFBO0FGZ0RKOztBRTdDQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtBRmdESiIsImZpbGUiOiJzcmMvYXBwL3NldHRpbmdzL2hlbHAvaGVscC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvZm9udHMnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcyc7XG5cbi8vIC5pcy12YWxpZCB7XG4vLyAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZVxuLy8gfVxuXG4uaXMtaW52YWxpZCB7XG4gICAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwM1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogJHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xe1xuICAgIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICAgIGRpc3BsYXk6IGZsZXghaW1wb3J0YW50O1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlciFpbXBvcnRhbnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuXG5cblxuIiwiLmlzLWludmFsaWQge1xuICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDM7XG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDFyZW07XG4gIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTEge1xuICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbmlvbi10aXRsZSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgcGFkZGluZy10b3A6IDFyZW07XG59XG5cbmlvbi1idXR0b25zIHtcbiAgcGFkZGluZy1ib3R0b206IDNyZW07XG59XG5cbi5oZWxwIHtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xuICBib3JkZXItcmFkaXVzOiAwLjYyNXJlbTtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwM0E1ODtcbn1cblxuLmZsb2F0LWxlZnQge1xuICBmbG9hdDogbGVmdDtcbiAgbWFyZ2luLWJvdHRvbTogM3JlbTtcbn0iLCJcbi8vIGNvbG9yc1xuLy8gJHByaW1hcnktY29sb3ItMTogIzAwQTNDODtcbiRwcmltYXJ5LWNvbG9yLTE6ICMwMDNBNTg7XG4kcHJpbWFyeS1jb2xvci0yOiAjRkFBRjQwO1xuJHByaW1hcnktY29sb3ItMzogI0Y0NzczQjtcblxuJHdoaXRlOiAjZmZmZmZmO1xuXG4vLyAkYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsIHJnYmEoMjU1LDI1NSwyNTUsMC41MCkgMCUsIHJnYmEoMCwwLDAsMC41MCkgMTAwJSk7XG4kYnV0dG9uLWdyYWRpZW50LTE6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpOyIsIkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy9jb21tb24uc2Nzcyc7XG5AaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzLnNjc3MnO1xuXG5cbmlvbi10aXRsZSB7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIHBhZGRpbmctdG9wOiAxcmVtO1xufVxuXG5pb24tYnV0dG9ucyB7XG4gICAgLy8gbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgICBwYWRkaW5nLWJvdHRvbTogM3JlbTtcbn1cblxuLmhlbHAge1xuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG4gICAgYm9yZGVyLXJhZGl1czogMC42MjVyZW07XG4gICAgYm9yZGVyOiAxcHggc29saWQgJHByaW1hcnktY29sb3ItMTtcbn1cblxuLmZsb2F0LWxlZnR7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWFyZ2luLWJvdHRvbTogM3JlbTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/settings/help/help.page.ts":
  /*!********************************************!*\
    !*** ./src/app/settings/help/help.page.ts ***!
    \********************************************/

  /*! exports provided: HelpPage */

  /***/
  function srcAppSettingsHelpHelpPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HelpPage", function () {
      return HelpPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var HelpPage =
    /*#__PURE__*/
    function () {
      function HelpPage() {
        _classCallCheck(this, HelpPage);
      }

      _createClass(HelpPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ngOnInit();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }]);

      return HelpPage;
    }();

    HelpPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-help',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./help.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/help/help.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./help.page.scss */
      "./src/app/settings/help/help.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], HelpPage);
    /***/
  }
}]);
//# sourceMappingURL=settings-help-help-module-es5.js.map