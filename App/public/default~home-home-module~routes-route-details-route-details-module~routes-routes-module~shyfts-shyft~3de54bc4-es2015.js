(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~home-home-module~routes-route-details-route-details-module~routes-routes-module~shyfts-shyft~3de54bc4"],{

/***/ "./src/shared/pipes/convert24hrto12hr.pipe.ts":
/*!****************************************************!*\
  !*** ./src/shared/pipes/convert24hrto12hr.pipe.ts ***!
  \****************************************************/
/*! exports provided: Convert24hrto12hrPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Convert24hrto12hrPipe", function() { return Convert24hrto12hrPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let Convert24hrto12hrPipe = class Convert24hrto12hrPipe {
    transform(time) {
        let hour = (time.split(':'))[0];
        let min = (time.split(':'))[1];
        let part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
        min = (min + '').length == 1 ? `0${min}` : min;
        hour = hour > 12 ? hour - 12 : hour;
        // hour = parseInt(hour, 10);
        hour = (hour + '').length == 1 ? `${hour}` : hour;
        return `${hour}:${min} ${part}`;
    }
};
Convert24hrto12hrPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'convert24hrto12hr'
    })
], Convert24hrto12hrPipe);



/***/ }),

/***/ "./src/shared/pipes/custom-date.pipe.ts":
/*!**********************************************!*\
  !*** ./src/shared/pipes/custom-date.pipe.ts ***!
  \**********************************************/
/*! exports provided: CustomDatePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomDatePipe", function() { return CustomDatePipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


const ordinals = ['th', 'st', 'nd', 'rd'];
let CustomDatePipe = class CustomDatePipe {
    transform(value, args) {
        let newValue;
        let valueArray = value.split('-');
        let year = valueArray[0];
        let month = valueArray[1];
        let day = valueArray[2];
        let today = new Date(Date.now());
        // today.setUTCFullYear(parseInt(valueArray[0]));
        // today.setUTCMonth(parseInt(valueArray[1]) - 1);
        // today.setUTCDate(parseInt(valueArray[2]));
        // if(valueArray[3]) today.setUTCHours(parseInt(valueArray[3]));
        // if(valueArray[4]) today.setUTCMinutes(parseInt(valueArray[4]));
        // if(valueArray[5]) today.setUTCSeconds(parseInt(valueArray[5]));
        // console.log("valueArray", valueArray);
        // console.log("today", today);
        // let year = today.getFullYear();
        // let month = today.getMonth() + 1;
        // let day = today.getDate();
        let deliveryDate = new Date(`${month}/${day}/${year}`);
        if (args === "shortTime") {
            // let hour = valueArray[3];
            // let minute = valueArray[4];
            // newValue = hour + ":" + minute;
            let hour = parseInt(valueArray[3]);
            let min = valueArray[4];
            let part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
            min = (min + '').length == 1 ? `0${min}` : min;
            hour = hour > 12 ? hour - 12 : hour;
            let hours = (hour + '').length == 1 ? `${hour}` : hour;
            return `${hours}:${min} ${part}`;
        }
        if (args === "shortWeekDay") {
            newValue = deliveryDate.toLocaleString('default', { weekday: 'short' });
        }
        if (args === "day") {
            newValue = day;
        }
        if (args === "longWeekDay") {
            newValue = deliveryDate.toLocaleString('default', { weekday: 'long' });
        }
        if (args === "deliveryDate") {
            let v = (parseInt(day)) % 100;
            // let v = (day % 100);
            let ordinalDay = (parseInt(day) > 9 ? day : parseInt(day)) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);
            // let ordinalDay = (day > 9 ? day : day) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);
            let monthString = deliveryDate.toLocaleString('default', { month: 'long' });
            newValue = `${monthString} ${ordinalDay} ${year}`;
        }
        if (args === "longDate") {
            let monthString = deliveryDate.toLocaleString('default', { month: 'long' });
            newValue = `${monthString} ${day}, ${year}`;
        }
        return newValue;
    }
};
CustomDatePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'customDate'
    })
], CustomDatePipe);



/***/ }),

/***/ "./src/shared/pipes/custom-utcdate.pipe.ts":
/*!*************************************************!*\
  !*** ./src/shared/pipes/custom-utcdate.pipe.ts ***!
  \*************************************************/
/*! exports provided: CustomUTCDatePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomUTCDatePipe", function() { return CustomUTCDatePipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


const ordinals = ['th', 'st', 'nd', 'rd'];
let CustomUTCDatePipe = class CustomUTCDatePipe {
    transform(value, args) {
        let newValue;
        let valueArray = value.split('-');
        // let year = valueArray[0];
        // let month = valueArray[1];
        // let day = valueArray[2];
        let today = new Date(Date.now());
        today.setUTCFullYear(parseInt(valueArray[0]));
        today.setUTCMonth(parseInt(valueArray[1]) - 1);
        today.setUTCDate(parseInt(valueArray[2]));
        if (valueArray[3])
            today.setUTCHours(parseInt(valueArray[3]));
        if (valueArray[4])
            today.setUTCMinutes(parseInt(valueArray[4]));
        if (valueArray[5])
            today.setUTCSeconds(parseInt(valueArray[5]));
        // console.log("valueArray", valueArray);
        // console.log("today", today);
        let year = today.getFullYear();
        let month = today.getMonth() + 1;
        let day = today.getDate();
        let deliveryDate = new Date(`${month}/${day}/${year}`);
        if (args === "shortTime") {
            // let hour = valueArray[3];
            // let minute = valueArray[4];
            // newValue = hour + ":" + minute;
            let hour = today.getHours();
            let min = today.getMinutes();
            let part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
            let minute = (min + '').length == 1 ? `0${min}` : min;
            hour = hour > 12 ? hour - 12 : hour;
            let hours = (hour + '').length == 1 ? `${hour}` : hour;
            return `${hours}:${minute} ${part}`;
        }
        if (args === "shortWeekDay") {
            newValue = deliveryDate.toLocaleString('default', { weekday: 'short' });
        }
        if (args === "day") {
            newValue = day;
        }
        if (args === "longWeekDay") {
            newValue = deliveryDate.toLocaleString('default', { weekday: 'long' });
        }
        if (args === "deliveryDate") {
            // let v = (parseInt(day)) % 100;
            let v = (day % 100);
            // let ordinalDay = (parseInt(day) > 9 ? day : parseInt(day)) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);
            let ordinalDay = (day > 9 ? day : day) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);
            let monthString = deliveryDate.toLocaleString('default', { month: 'long' });
            newValue = `${monthString} ${ordinalDay} ${year}`;
        }
        if (args === "longDate") {
            let monthString = deliveryDate.toLocaleString('default', { month: 'long' });
            newValue = `${monthString} ${day}, ${year}`;
        }
        return newValue;
    }
};
CustomUTCDatePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'customUtcdate'
    })
], CustomUTCDatePipe);



/***/ }),

/***/ "./src/shared/services/routing.service.ts":
/*!************************************************!*\
  !*** ./src/shared/services/routing.service.ts ***!
  \************************************************/
/*! exports provided: RoutingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutingService", function() { return RoutingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let RoutingService = class RoutingService {
    constructor(http) {
        this.http = http;
    }
    activeRoute() {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.activeRouteURL, this.getHeaders());
    }
    // getUserRoute() {
    //     return this.http.get<Route[]>(environment.baseUrl + environment.services.bookingURL + `?mode=user`, this.getHeaders());
    // }
    routeCheckIn(routeId, checkInTime) {
        let checkIn = {
            routeId: routeId,
            checkInTime: checkInTime
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.checkInRouteURL, checkIn, this.getHeaders());
    }
    getModeUserRoute() {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeURL + `?mode=user`, this.getHeaders());
    }
    getUserRoute() {
        let apiMode = {
            mode: "routeShell"
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.sapApiURL, apiMode, this.getHeaders());
    }
    getUserRouteDetails(routeId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeDetailURL + `?routeId=${routeId}`, this.getHeaders());
    }
    cancelRoute(routeId) {
        let body = {
            routeId: routeId
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeCancelURL, body, this.getHeaders());
    }
    getHeaders() {
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-type': 'application/json'
            })
        };
        return httpOptions;
    }
};
RoutingService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
RoutingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], RoutingService);



/***/ }),

/***/ "./src/shared/services/schedule.service.ts":
/*!*************************************************!*\
  !*** ./src/shared/services/schedule.service.ts ***!
  \*************************************************/
/*! exports provided: ScheduleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScheduleService", function() { return ScheduleService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let ScheduleService = class ScheduleService {
    constructor(http) {
        this.http = http;
    }
    getSchedules(mode) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.scheduleURL + `?mode=${mode}`, this.getHeaders());
    }
    // getUserSchedule() {
    //     return this.http.get<Schedule[]>(environment.baseUrl + environment.services.userScheduleURL, this.getHeaders());    
    // }
    // sendDesiredSchedule(schedule) {
    //     return this.http.post<any>(environment.baseUrl + environment.services.scheduleURL, schedule, this.getHeaders());
    // }
    sendDesiredSchedule(schedule) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.scheduleURL, schedule, this.getHeaders());
    }
    // cancelSchedule(route) {
    //     let scheduleDetail = {
    //         mode: "desired",
    //         scheduleId: route.scheduleId,
    //         udsId: route.udsId
    //     }
    //     return this.http.post<any>(environment.baseUrl + environment.services.scheduleURL, schedule, this.getHeaders());
    // }
    // cancelSchedule(booking) {
    //     let scheduleDetail = {
    //         mode: "desired",
    //         scheduleId: booking.scheduleId,
    //         udsId: booking.udsId
    //     }
    //     return this.http.delete<any>(environment.baseUrl + environment.services.scheduleDetailURl +
    //         `?mode=${scheduleDetail.mode}&scheduleId=${scheduleDetail.scheduleId}&udsId=${scheduleDetail.udsId}`, this.getHeaders())
    // }
    cancelSchedule(schedule) {
        let scheduleToCancel = {
            scheduleId: schedule.scheduleId
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.scheduleCancelURL, scheduleToCancel, this.getHeaders());
    }
    getHeaders() {
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-type': 'application/json'
            })
        };
        return httpOptions;
    }
};
ScheduleService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
ScheduleService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], ScheduleService);



/***/ }),

/***/ "./src/shared/shared.module.ts":
/*!*************************************!*\
  !*** ./src/shared/shared.module.ts ***!
  \*************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pipes/custom-date.pipe */ "./src/shared/pipes/custom-date.pipe.ts");
/* harmony import */ var _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pipes/convert24hrto12hr.pipe */ "./src/shared/pipes/convert24hrto12hr.pipe.ts");
/* harmony import */ var _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pipes/custom-utcdate.pipe */ "./src/shared/pipes/custom-utcdate.pipe.ts");





let SharedModule = class SharedModule {
};
SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__["CustomDatePipe"],
            _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__["Convert24hrto12hrPipe"],
            _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__["CustomUTCDatePipe"],
        ],
        entryComponents: [],
        imports: [],
        exports: [
            _pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__["CustomDatePipe"],
            _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__["Convert24hrto12hrPipe"],
            _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__["CustomUTCDatePipe"],
        ],
        providers: [],
    })
], SharedModule);



/***/ })

}]);
//# sourceMappingURL=default~home-home-module~routes-route-details-route-details-module~routes-routes-module~shyfts-shyft~3de54bc4-es2015.js.map