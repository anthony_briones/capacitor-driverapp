(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["routes-route-done-route-done-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/routes/route-done/route-done.page.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/routes/route-done/route-done.page.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Overview</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content ion-padding>\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <!-- Static -->\n  <!-- <ion-item lines=\"none\">\n    <ion-row>\n      <ion-col size=\"12\" class=\"delivery-type\">\n        <ion-icon name=\"md-home\"></ion-icon>\n        RESIDENTIAL DELIVERY\n      </ion-col>\n\n      <ion-col size=\"2\" class=\"delivery-card\">\n        <div class=\"delivery-count\">\n          1\n        </div>\n        <div class=\"delivery-total\">\n          of 5\n        </div>\n      </ion-col>\n\n      <ion-col size=\"7\" class=\"delivery-details\">\n        <div class=\"delivery-name\">\n          James Howlett\n        </div>\n        <div class=\"delivery-address\">\n          1097 Arbor Drive\n        </div>\n        <div class=\"delivery-address\">\n          Phoenixville, PA 19460\n        </div>\n      </ion-col>\n\n      <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n        <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n        <ion-text> 4</ion-text>\n      </ion-col>\n\n    </ion-row>\n  </ion-item> -->\n\n  <!-- Static -->\n  <!-- <ion-item lines=\"none\">\n    <ion-row>\n      <ion-col size=\"12\" class=\"delivery-type\">\n        <ion-icon name=\"md-home\"></ion-icon>\n        RESIDENTIAL DELIVERY\n      </ion-col>\n\n      <ion-col size=\"2\" class=\"delivery-card\">\n        <div class=\"delivery-count\">\n          2\n        </div>\n        <div class=\"delivery-total\">\n          of 5\n        </div>\n      </ion-col>\n\n      <ion-col size=\"7\" class=\"delivery-details\">\n        <div class=\"delivery-name\">\n          James Smith\n        </div>\n        <div class=\"delivery-address\">\n          1097 Arbor Drive\n        </div>\n        <div class=\"delivery-address\">\n          Phoenixville, PA 19460\n        </div>\n      </ion-col>\n\n      <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n        <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n        <ion-text> 2</ion-text>\n      </ion-col>\n\n    </ion-row>\n  </ion-item> -->\n\n  <!-- Dynamic -->\n  <ion-item lines=\"none\" *ngFor=\"let delivery of deliveries; let i = index\">\n    <ion-row>\n      <ion-col size=\"12\" class=\"delivery-type\">\n        <ion-icon name=\"md-home\"></ion-icon>\n        {{getOrderSource(delivery.orderSource)}}\n      </ion-col>\n      <ion-col size=\"2\" class=\"delivery-card\">\n        <div class=\"delivery-count\">\n          {{i+1}}\n        </div>\n        <div class=\"delivery-total\">\n          of {{deliveries.length}}\n        </div>\n      </ion-col>\n      <ion-col size=\"7\" class=\"delivery-details\">\n        <div class=\"delivery-address\">\n          {{delivery.street}}\n        </div>\n        <div class=\"delivery-address\">\n          {{delivery.city}}, {{delivery.state}} {{delivery.zipcode}}\n        </div>\n      </ion-col>\n\n      <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n        <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n        <ion-text>{{getTotalQuantity(delivery.items)}}</ion-text>\n      </ion-col>\n\n    </ion-row>\n  </ion-item>\n\n  <!-- Skeleton -->\n  <ng-container *ngIf=\"showDeliveries\">\n    <ion-item lines=\"none\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"delivery-type\">\n          <!-- <ion-icon name=\"md-home\"></ion-icon> -->\n          <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"2\" class=\"\">\n            <ion-skeleton-text class=\"\" animated class=\"h-100\"></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"7\" class=\"delivery-details\">\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n          <div class=\"delivery-address\">\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </div>\n        </ion-col>\n\n        <!-- <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n          <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n          <ion-text><ion-skeleton-text animated></ion-skeleton-text></ion-text>\n        </ion-col> -->\n\n      </ion-row>\n\n    </ion-item>\n\n    <ion-item lines=\"none\">\n        <ion-row>\n          <ion-col size=\"12\" class=\"delivery-type\">\n            <!-- <ion-icon name=\"md-home\"></ion-icon> -->\n            <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n          </ion-col>\n          <ion-col size=\"2\" class=\"\">\n              <ion-skeleton-text class=\"\" animated class=\"h-100\"></ion-skeleton-text>\n          </ion-col>\n          <ion-col size=\"7\" class=\"delivery-details\">\n            <div class=\"delivery-address\">\n              <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n            </div>\n            <div class=\"delivery-address\">\n              <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n            </div>\n          </ion-col>\n  \n          <!-- <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n            <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n            <ion-text><ion-skeleton-text animated></ion-skeleton-text></ion-text>\n          </ion-col> -->\n  \n        </ion-row>\n  \n      </ion-item>\n\n      <ion-item lines=\"none\">\n          <ion-row>\n            <ion-col size=\"12\" class=\"delivery-type\">\n              <!-- <ion-icon name=\"md-home\"></ion-icon> -->\n              <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n            </ion-col>\n            <ion-col size=\"2\" class=\"\">\n                <ion-skeleton-text class=\"\" animated class=\"h-100\"></ion-skeleton-text>\n            </ion-col>\n            <ion-col size=\"7\" class=\"delivery-details\">\n              <div class=\"delivery-address\">\n                <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n              </div>\n              <div class=\"delivery-address\">\n                <ion-skeleton-text class=\"delivery-skeleton\" animated></ion-skeleton-text>\n              </div>\n            </ion-col>\n    \n            <!-- <ion-col size=\"3\" class=\"delivery-tanks vertical-align horizontal-align\">\n              <ion-img src=\"../assets/icon/propane-tank-graphic.svg\"></ion-img>\n              <ion-text><ion-skeleton-text animated></ion-skeleton-text></ion-text>\n            </ion-col> -->\n    \n          </ion-row>\n    \n        </ion-item>\n\n  </ng-container>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/routes/route-done/route-done.module.ts":
/*!********************************************************!*\
  !*** ./src/app/routes/route-done/route-done.module.ts ***!
  \********************************************************/
/*! exports provided: RouteDonePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouteDonePageModule", function() { return RouteDonePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _route_done_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./route-done.page */ "./src/app/routes/route-done/route-done.page.ts");







const routes = [
    {
        path: '',
        component: _route_done_page__WEBPACK_IMPORTED_MODULE_6__["RouteDonePage"]
    }
];
let RouteDonePageModule = class RouteDonePageModule {
};
RouteDonePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_route_done_page__WEBPACK_IMPORTED_MODULE_6__["RouteDonePage"]]
    })
], RouteDonePageModule);



/***/ }),

/***/ "./src/app/routes/route-done/route-done.page.scss":
/*!********************************************************!*\
  !*** ./src/app/routes/route-done/route-done.page.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-title {\n  color: #003A58;\n}\n\nion-item {\n  margin: 1rem 0;\n}\n\n.delivery-type {\n  padding-top: 1rem;\n  padding-bottom: 1rem;\n  font-family: \"Montserrat-Bold\";\n  font-size: 0.75rem;\n  color: #003A58;\n  letter-spacing: 0;\n}\n\n.delivery-skeleton {\n  width: 10rem;\n}\n\n.delivery-card {\n  border: 1px solid #003A58;\n  border-radius: 4px;\n  padding: 0 10px;\n}\n\n.delivery-card .delivery-count {\n  font-family: \"Montserrat-Bold\";\n  font-size: 2.5rem;\n  color: #003A58;\n  letter-spacing: 0;\n  text-align: center;\n}\n\n.delivery-card .delivery-total {\n  font-family: \"Montserrat-Regular\";\n  font-size: 1rem;\n  color: #003A58;\n  letter-spacing: 0;\n  text-align: center;\n}\n\n.delivery-details {\n  padding-left: 1rem;\n  padding-right: 1rem;\n}\n\n.delivery-details .delivery-name {\n  font-family: \"Roboto-Bold\";\n  font-size: 1rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.5rem;\n}\n\n.delivery-details .delivery-address {\n  font-family: \"Roboto-Regular\";\n  font-size: 0.875rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.3125rem;\n}\n\n.delivery-tanks ion-img {\n  position: relative;\n  display: inline-block;\n}\n\n.delivery-tanks ion-text {\n  position: absolute;\n  text-align: center;\n  font-family: \"Montserrat-Bold\";\n  font-size: 1.5rem;\n  color: #003A58;\n  letter-spacing: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL3JvdXRlcy9yb3V0ZS1kb25lL3JvdXRlLWRvbmUucGFnZS5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXBwL3JvdXRlcy9yb3V0ZS1kb25lL3JvdXRlLWRvbmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU9BO0VBQ0kseUJBQUE7RUFDQSxnREFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkVsQmM7RUZtQmQsV0FBQTtFQUNBLGNFaEJJO0VGaUJKLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSwrQkFBQTtFQUFBLHdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxvQ0FBQTtVQUFBLDhCQUFBO0FDTko7O0FEU0E7RUFDSSxtQ0FBQTtVQUFBLGtDQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtVQUFBLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx3QkFBQTtVQUFBLHVCQUFBO0FDTko7O0FEU0E7RUFDSSxZQUFBO0FDTko7O0FFaERBO0VBQ0ksY0REYztBRG9EbEI7O0FFaERBO0VBQ0ksY0FBQTtBRm1ESjs7QUVoREE7RUFDSSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLGNEYmM7RUNjZCxpQkFBQTtBRm1ESjs7QUVoREE7RUFDSSxZQUFBO0FGbURKOztBRWhEQTtFQUVJLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FGa0RKOztBRWhESTtFQUNJLDhCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjRDlCVTtFQytCVixpQkFBQTtFQUNBLGtCQUFBO0FGa0RSOztBRS9DSTtFQUNJLGlDQUFBO0VBQ0EsZUFBQTtFQUNBLGNEdENVO0VDdUNWLGlCQUFBO0VBQ0Esa0JBQUE7QUZpRFI7O0FFN0NBO0VBRUksa0JBQUE7RUFDQSxtQkFBQTtBRitDSjs7QUU3Q0k7RUFDSSwwQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBRitDUjs7QUU1Q0k7RUFDSSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUY4Q1I7O0FFeENJO0VBRUksa0JBQUE7RUFDQSxxQkFBQTtBRjBDUjs7QUV2Q0k7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsOEJBQUE7RUFDQSxpQkFBQTtFQUNBLGNEL0VVO0VDZ0ZWLGlCQUFBO0FGeUNSIiwiZmlsZSI6InNyYy9hcHAvcm91dGVzL3JvdXRlLWRvbmUvcm91dGUtZG9uZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvZm9udHMnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcyc7XG5cbi8vIC5pcy12YWxpZCB7XG4vLyAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZVxuLy8gfVxuXG4uaXMtaW52YWxpZCB7XG4gICAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwM1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogJHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xe1xuICAgIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICAgIGRpc3BsYXk6IGZsZXghaW1wb3J0YW50O1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlciFpbXBvcnRhbnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuXG5cblxuIiwiLmlzLWludmFsaWQge1xuICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDM7XG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDFyZW07XG4gIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTEge1xuICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbmlvbi10aXRsZSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xufVxuXG5pb24taXRlbSB7XG4gIG1hcmdpbjogMXJlbSAwO1xufVxuXG4uZGVsaXZlcnktdHlwZSB7XG4gIHBhZGRpbmctdG9wOiAxcmVtO1xuICBwYWRkaW5nLWJvdHRvbTogMXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xufVxuXG4uZGVsaXZlcnktc2tlbGV0b24ge1xuICB3aWR0aDogMTByZW07XG59XG5cbi5kZWxpdmVyeS1jYXJkIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwM0E1ODtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBwYWRkaW5nOiAwIDEwcHg7XG59XG4uZGVsaXZlcnktY2FyZCAuZGVsaXZlcnktY291bnQge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgZm9udC1zaXplOiAyLjVyZW07XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmRlbGl2ZXJ5LWNhcmQgLmRlbGl2ZXJ5LXRvdGFsIHtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1SZWd1bGFyXCI7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5kZWxpdmVyeS1kZXRhaWxzIHtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xuICBwYWRkaW5nLXJpZ2h0OiAxcmVtO1xufVxuLmRlbGl2ZXJ5LWRldGFpbHMgLmRlbGl2ZXJ5LW5hbWUge1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tQm9sZFwiO1xuICBmb250LXNpemU6IDFyZW07XG4gIGNvbG9yOiAjNDI0MjQyO1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgbGluZS1oZWlnaHQ6IDEuNXJlbTtcbn1cbi5kZWxpdmVyeS1kZXRhaWxzIC5kZWxpdmVyeS1hZGRyZXNzIHtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLVJlZ3VsYXJcIjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgY29sb3I6ICM0MjQyNDI7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4uZGVsaXZlcnktdGFua3MgaW9uLWltZyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuLmRlbGl2ZXJ5LXRhbmtzIGlvbi10ZXh0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBmb250LXNpemU6IDEuNXJlbTtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xufSIsIlxuLy8gY29sb3JzXG4vLyAkcHJpbWFyeS1jb2xvci0xOiAjMDBBM0M4O1xuJHByaW1hcnktY29sb3ItMTogIzAwM0E1ODtcbiRwcmltYXJ5LWNvbG9yLTI6ICNGQUFGNDA7XG4kcHJpbWFyeS1jb2xvci0zOiAjRjQ3NzNCO1xuXG4kd2hpdGU6ICNmZmZmZmY7XG5cbi8vICRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgcmdiYSgyNTUsMjU1LDI1NSwwLjUwKSAwJSwgcmdiYSgwLDAsMCwwLjUwKSAxMDAlKTtcbiRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KC0xODBkZWcsICNGRjg4NDAgMCUsICNFRTQwMzYgMTAwJSk7IiwiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2NvbW1vbi5zY3NzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XG5cbmlvbi10aXRsZXtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIG1hcmdpbjogMXJlbSAwO1xufVxuXG4uZGVsaXZlcnktdHlwZSB7XG4gICAgcGFkZGluZy10b3A6IDFyZW07XG4gICAgcGFkZGluZy1ib3R0b206IDFyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbn1cblxuLmRlbGl2ZXJ5LXNrZWxldG9ue1xuICAgIHdpZHRoOiAxMHJlbTtcbn1cblxuLmRlbGl2ZXJ5LWNhcmQge1xuXG4gICAgYm9yZGVyOiAxcHggc29saWQgJHByaW1hcnktY29sb3ItMTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgcGFkZGluZzogMCAxMHB4O1xuXG4gICAgLmRlbGl2ZXJ5LWNvdW50e1xuICAgICAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgICAgIGZvbnQtc2l6ZTogMi41cmVtO1xuICAgICAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG5cbiAgICAuZGVsaXZlcnktdG90YWwge1xuICAgICAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtUmVndWxhcic7XG4gICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxufVxuXG4uZGVsaXZlcnktZGV0YWlscyB7XG5cbiAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gICAgcGFkZGluZy1yaWdodDogMXJlbTtcbiAgICBcbiAgICAuZGVsaXZlcnktbmFtZSB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDFyZW07XG4gICAgICAgIGNvbG9yOiAjNDI0MjQyO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuNXJlbTtcbiAgICB9XG5cbiAgICAuZGVsaXZlcnktYWRkcmVzcyB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLVJlZ3VsYXInO1xuICAgICAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgICAgICBjb2xvcjogIzQyNDI0MjtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG4gICAgfVxuXG59XG5cbi5kZWxpdmVyeS10YW5rcyB7XG4gICAgaW9uLWltZyB7XG4gICAgICAgIC8vIHdpZHRoOiA1MCU7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIH1cblxuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XG4gICAgICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICB9XG5cbiAgIFxuXG5cbn0iXX0= */");

/***/ }),

/***/ "./src/app/routes/route-done/route-done.page.ts":
/*!******************************************************!*\
  !*** ./src/app/routes/route-done/route-done.page.ts ***!
  \******************************************************/
/*! exports provided: RouteDonePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouteDonePage", function() { return RouteDonePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/shared/services/routing.service */ "./src/shared/services/routing.service.ts");




let RouteDonePage = class RouteDonePage {
    constructor(route, router, routingService) {
        this.route = route;
        this.router = router;
        this.routingService = routingService;
        this.deliveries = [];
        this.showDeliveries = true;
        this.routeId = this.route.snapshot.paramMap.get('id');
    }
    ;
    ngOnInit() {
        console.log(this.routeId);
        this.getRoutes();
    }
    getRoutes() {
        this.routingService.getUserRouteDetails(this.routeId).subscribe(result => {
            console.log("Done Results: ", result);
            this.showDeliveries = false;
            this.deliveries = result.deliveries;
        }, error => {
        });
    }
    // Show Total Quantity for delivery
    getTotalQuantity(items) {
        let total = 0;
        items.forEach(item => {
            total += item.quantityDelivered;
        });
        return total;
    }
    getOrderSource(orderSource) {
        orderSource = orderSource.toUpperCase();
        switch (orderSource) {
            case "CYNCH":
                return "RESIDENTIAL DELIVERY";
            case "ACE":
                return "COMMERCIAL DELIVERY";
            default:
                return "RESIDENTIAL DELIVERY";
        }
    }
    doRefresh(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log('Begin async operation');
            this.ngOnInit();
            event.target.complete();
        });
    }
};
RouteDonePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_3__["RoutingService"] }
];
RouteDonePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-route-done',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./route-done.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/routes/route-done/route-done.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./route-done.page.scss */ "./src/app/routes/route-done/route-done.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_shared_services_routing_service__WEBPACK_IMPORTED_MODULE_3__["RoutingService"]])
], RouteDonePage);



/***/ }),

/***/ "./src/shared/services/routing.service.ts":
/*!************************************************!*\
  !*** ./src/shared/services/routing.service.ts ***!
  \************************************************/
/*! exports provided: RoutingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutingService", function() { return RoutingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let RoutingService = class RoutingService {
    constructor(http) {
        this.http = http;
    }
    activeRoute() {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.activeRouteURL, this.getHeaders());
    }
    // getUserRoute() {
    //     return this.http.get<Route[]>(environment.baseUrl + environment.services.bookingURL + `?mode=user`, this.getHeaders());
    // }
    routeCheckIn(routeId, checkInTime) {
        let checkIn = {
            routeId: routeId,
            checkInTime: checkInTime
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.checkInRouteURL, checkIn, this.getHeaders());
    }
    getModeUserRoute() {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeURL + `?mode=user`, this.getHeaders());
    }
    getUserRoute() {
        let apiMode = {
            mode: "routeShell"
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.sapApiURL, apiMode, this.getHeaders());
    }
    getUserRouteDetails(routeId) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeDetailURL + `?routeId=${routeId}`, this.getHeaders());
    }
    cancelRoute(routeId) {
        let body = {
            routeId: routeId
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.routeCancelURL, body, this.getHeaders());
    }
    getHeaders() {
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-type': 'application/json'
            })
        };
        return httpOptions;
    }
};
RoutingService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
RoutingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], RoutingService);



/***/ })

}]);
//# sourceMappingURL=routes-route-done-route-done-module-es2015.js.map