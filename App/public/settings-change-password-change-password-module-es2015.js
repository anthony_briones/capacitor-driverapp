(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["settings-change-password-change-password-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/change-password/change-password.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/settings/change-password/change-password.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <!-- <ion-buttons slot=\"start\"> -->\n      <ion-back-button class=\"float-left\"></ion-back-button>\n    <!-- </ion-buttons> -->\n    <ion-title>Change Password</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <!-- <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher> -->\n  <ion-grid>\n    <form [formGroup]=\"accountForm\" (submit)=\"checkPassword()\">\n      <ion-row>\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"oldPassword\">\n              Old Password\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input *ngIf=\"!showOld\" type=\"password\" formControlName=\"oldPassword\" id=\"oldPassword\"\n            [ngClass]=\"{ 'is-invalid': (fc.oldPassword.dirty || fc.oldPassword.touched || submitted) && fc.oldPassword.errors, 'is-valid': (fc.oldPassword.dirty || fc.oldPassword.touched) && !fc.oldPassword.errors }\">\n          <input *ngIf=\"showOld\" type=\"text\" formControlName=\"oldPassword\" id=\"oldPassword\"\n            [ngClass]=\"{ 'is-invalid': (fc.oldPassword.dirty || fc.oldPassword.touched || submitted) && fc.oldPassword.errors, 'is-valid': (fc.oldPassword.dirty || fc.oldPassword.touched) && !fc.oldPassword.errors }\">\n          <button ion-button clear color=\"dark\" type=\"button\" class=\"eye-btn\" (click)=\"showOld = !showOld\" item-right>\n            <ion-icon name=\"md-eye\"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"newPassword\">\n              New Password\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input *ngIf=\"!showNew\" type=\"password\" formControlName=\"newPassword\" id=\"newPassword\"\n            [ngClass]=\"{ 'is-invalid': (fc.newPassword.dirty || fc.newPassword.touched || submitted) && fc.newPassword.errors, 'is-valid': (fc.newPassword.dirty || fc.newPassword.touched) && !fc.newPassword.errors }\">\n          <input *ngIf=\"showNew\" type=\"text\" formControlName=\"newPassword\" id=\"newPassword\"\n            [ngClass]=\"{ 'is-invalid': (fc.newPassword.dirty || fc.newPassword.touched || submitted) && fc.newPassword.errors, 'is-valid': (fc.newPassword.dirty || fc.newPassword.touched) && !fc.newPassword.errors }\">\n          <button ion-button clear color=\"dark\" type=\"button\" class=\"eye-btn\" (click)=\"showNew = !showNew\" item-right>\n            <ion-icon name=\"md-eye\"></ion-icon>\n          </button>\n        </ion-col>\n\n        <ion-col class=\"invalid-text\"size=\"12\" *ngIf=\"fc.newPassword.errors && (fc.newPassword.dirty || fc.newPassword.touched)\">\n          <label *ngIf=\"fc.newPassword.errors?.required\">\n            New Password is required\n          </label>\n          <label *ngIf=\"!fc.newPassword.errors?.required\">\n            New Password must contain the following:\n          </label>\n          <ul class=\"invalid-text\">\n            <li *ngIf=\"fc.newPassword.errors?.hasCapitalCase && (fc.newPassword.dirty || fc.newPassword.touched)\">\n              An upper case character.\n            </li>\n            <li *ngIf=\"fc.newPassword.errors?.hasSmallCase && (fc.newPassword.dirty || fc.newPassword.touched)\">\n              A lower case character.\n            </li>\n            <li *ngIf=\"fc.newPassword.errors?.hasNumber && (fc.newPassword.dirty || fc.newPassword.touched)\">\n              A number. <br>\n            </li>\n            <li *ngIf=\"fc.newPassword.errors?.minlength && (fc.newPassword.dirty || fc.newPassword.touched)\">\n              Minimum of 8 characters.\n            </li>\n            <li *ngIf=\"fc.newPassword.errors?.maxlength && (fc.newPassword.dirty || fc.newPassword.touched)\">\n              Maximum of 20 characters.\n            </li>\n          </ul>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n          <div class=\"account-label ion-text-uppercase\">\n            <label for=\"confirmPassword\">\n              Confirm Password\n            </label>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\" class=\"account-input\">\n          <input *ngIf=\"!showConfirm\" type=\"password\" formControlName=\"confirmPassword\" id=\"confirmPassword\"\n            [ngClass]=\"{ 'is-invalid': (fc.confirmPassword.dirty || fc.confirmPassword.touched || submitted) && fc.confirmPassword.errors, 'is-valid': (fc.confirmPassword.dirty || fc.confirmPassword.touched) && !fc.confirmPassword.errors }\">\n          <input *ngIf=\"showConfirm\" type=\"text\" formControlName=\"confirmPassword\" id=\"confirmPassword\"\n            [ngClass]=\"{ 'is-invalid': (fc.confirmPassword.dirty || fc.confirmPassword.touched || submitted) && fc.confirmPassword.errors, 'is-valid': (fc.confirmPassword.dirty || fc.confirmPassword.touched) && !fc.confirmPassword.errors }\">\n          <button ion-button clear color=\"dark\" type=\"button\" class=\"eye-btn\" (click)=\"showConfirm = !showConfirm\"\n            item-right>\n            <ion-icon name=\"md-eye\"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size=\"12\" class=\" shadow ion-padding-top\">\n          <button class=\"btn shadow-lg save-password-btn save-btn\" icon-only round ion-button type=\"submit\">\n            Save Changes\n          </button>\n        </ion-col>\n      </ion-row>\n    </form>\n  </ion-grid>\n\n\n</ion-content>");

/***/ }),

/***/ "./src/app/settings/change-password/change-password.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/settings/change-password/change-password.module.ts ***!
  \********************************************************************/
/*! exports provided: ChangePasswordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordPageModule", function() { return ChangePasswordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _change_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./change-password.page */ "./src/app/settings/change-password/change-password.page.ts");







const routes = [
    {
        path: '',
        component: _change_password_page__WEBPACK_IMPORTED_MODULE_6__["ChangePasswordPage"]
    }
];
let ChangePasswordPageModule = class ChangePasswordPageModule {
};
ChangePasswordPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"].forRoot({
                animated: true
            }),
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_change_password_page__WEBPACK_IMPORTED_MODULE_6__["ChangePasswordPage"]]
    })
], ChangePasswordPageModule);



/***/ }),

/***/ "./src/app/settings/change-password/change-password.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/settings/change-password/change-password.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-title {\n  color: #003A58;\n  font-family: \"Montserrat-Bold\";\n  padding-top: 1rem;\n}\n\n.invalid-text {\n  color: #EE4036;\n}\n\nion-grid {\n  max-width: 350px;\n}\n\n.float-left {\n  float: left;\n  margin-bottom: 3rem;\n}\n\n.account-label {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: left;\n          justify-content: left;\n  -webkit-box-align: left;\n          align-items: left;\n  height: 100%;\n  width: 100%;\n  color: #727272;\n  font-size: 0.75rem;\n  padding-top: 2rem;\n  font-family: \"Roboto-Medium\";\n}\n\n.account-input {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  height: 100%;\n  width: 100%;\n  color: #424242;\n  padding: 1rem 0 0 0;\n  font-family: \"Roboto-Bold\";\n  border-bottom: 0.001rem solid #a0a0a0;\n}\n\ninput {\n  background-color: transparent;\n  border: 0;\n  font-size: 1rem;\n  width: 100%;\n  font-weight: bold;\n  padding-bottom: 0.5rem;\n}\n\ninput[type=password] {\n  -webkit-text-stroke-width: 0.18rem;\n  letter-spacing: 0.18rem;\n}\n\n.settings-button {\n  padding-top: 1.5rem;\n}\n\n.save-password-btn {\n  background: #F4773B 100% !important;\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036)) !important;\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%) !important;\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n  line-height: 1.3125rem;\n}\n\n.eye-btn {\n  background-color: white;\n  color: #003A58;\n  font-size: 1rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL3NldHRpbmdzL2NoYW5nZS1wYXNzd29yZC9jaGFuZ2UtcGFzc3dvcmQucGFnZS5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXBwL3NldHRpbmdzL2NoYW5nZS1wYXNzd29yZC9jaGFuZ2UtcGFzc3dvcmQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU9BO0VBQ0kseUJBQUE7RUFDQSxnREFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkVsQmM7RUZtQmQsV0FBQTtFQUNBLGNFaEJJO0VGaUJKLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSwrQkFBQTtFQUFBLHdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxvQ0FBQTtVQUFBLDhCQUFBO0FDTko7O0FEU0E7RUFDSSxtQ0FBQTtVQUFBLGtDQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtVQUFBLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx3QkFBQTtVQUFBLHVCQUFBO0FDTko7O0FEU0E7RUFDSSxZQUFBO0FDTko7O0FFL0NBO0VBQ0ksY0RGYztFQ0dkLDhCQUFBO0VBQ0EsaUJBQUE7QUZrREo7O0FFL0NBO0VBQ0ksY0FBQTtBRmtESjs7QUUvQ0E7RUFDSSxnQkFBQTtBRmtESjs7QUUvQ0E7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7QUZrREo7O0FFOUNBO0VBQ0ksb0JBQUE7RUFBQSxhQUFBO0VBQ0Esc0JBQUE7VUFBQSxxQkFBQTtFQUNBLHVCQUFBO1VBQUEsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7QUZpREo7O0FFOUNBO0VBQ0ksb0JBQUE7RUFBQSxhQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLDBCQUFBO0VBQ0EscUNBQUE7QUZpREo7O0FFOUNBO0VBQ0ksNkJBQUE7RUFDQSxTQUFBO0VBRUEsZUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0FGZ0RKOztBRTVDQTtFQUNJLGtDQUFBO0VBQ0EsdUJBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksbUJBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksbUNBQUE7RUFDQSx3R0FBQTtFQUFBLCtFQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0VBQ0Esc0JBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksdUJBQUE7RUFDQSxjRC9FYztFQ2dGZCxlQUFBO0FGK0NKIiwiZmlsZSI6InNyYy9hcHAvc2V0dGluZ3MvY2hhbmdlLXBhc3N3b3JkL2NoYW5nZS1wYXNzd29yZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvZm9udHMnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcyc7XG5cbi8vIC5pcy12YWxpZCB7XG4vLyAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZVxuLy8gfVxuXG4uaXMtaW52YWxpZCB7XG4gICAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwM1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogJHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xe1xuICAgIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICAgIGRpc3BsYXk6IGZsZXghaW1wb3J0YW50O1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlciFpbXBvcnRhbnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuXG5cblxuIiwiLmlzLWludmFsaWQge1xuICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDM7XG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDFyZW07XG4gIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTEge1xuICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbmlvbi10aXRsZSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgcGFkZGluZy10b3A6IDFyZW07XG59XG5cbi5pbnZhbGlkLXRleHQge1xuICBjb2xvcjogI0VFNDAzNjtcbn1cblxuaW9uLWdyaWQge1xuICBtYXgtd2lkdGg6IDM1MHB4O1xufVxuXG4uZmxvYXQtbGVmdCB7XG4gIGZsb2F0OiBsZWZ0O1xuICBtYXJnaW4tYm90dG9tOiAzcmVtO1xufVxuXG4uYWNjb3VudC1sYWJlbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogbGVmdDtcbiAgYWxpZ24taXRlbXM6IGxlZnQ7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiAjNzI3MjcyO1xuICBmb250LXNpemU6IDAuNzVyZW07XG4gIHBhZGRpbmctdG9wOiAycmVtO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tTWVkaXVtXCI7XG59XG5cbi5hY2NvdW50LWlucHV0IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiAjNDI0MjQyO1xuICBwYWRkaW5nOiAxcmVtIDAgMCAwO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tQm9sZFwiO1xuICBib3JkZXItYm90dG9tOiAwLjAwMXJlbSBzb2xpZCAjYTBhMGEwO1xufVxuXG5pbnB1dCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBib3JkZXI6IDA7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nLWJvdHRvbTogMC41cmVtO1xufVxuXG5pbnB1dFt0eXBlPXBhc3N3b3JkXSB7XG4gIC13ZWJraXQtdGV4dC1zdHJva2Utd2lkdGg6IDAuMThyZW07XG4gIGxldHRlci1zcGFjaW5nOiAwLjE4cmVtO1xufVxuXG4uc2V0dGluZ3MtYnV0dG9uIHtcbiAgcGFkZGluZy10b3A6IDEuNXJlbTtcbn1cblxuLnNhdmUtcGFzc3dvcmQtYnRuIHtcbiAgYmFja2dyb3VuZDogI0Y0NzczQiAxMDAlICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXNpemU6IDEuMTI1cmVtO1xuICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgaGVpZ2h0OiAzcmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4uZXllLWJ0biB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1zaXplOiAxcmVtO1xufSIsIlxuLy8gY29sb3JzXG4vLyAkcHJpbWFyeS1jb2xvci0xOiAjMDBBM0M4O1xuJHByaW1hcnktY29sb3ItMTogIzAwM0E1ODtcbiRwcmltYXJ5LWNvbG9yLTI6ICNGQUFGNDA7XG4kcHJpbWFyeS1jb2xvci0zOiAjRjQ3NzNCO1xuXG4kd2hpdGU6ICNmZmZmZmY7XG5cbi8vICRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgcmdiYSgyNTUsMjU1LDI1NSwwLjUwKSAwJSwgcmdiYSgwLDAsMCwwLjUwKSAxMDAlKTtcbiRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KC0xODBkZWcsICNGRjg4NDAgMCUsICNFRTQwMzYgMTAwJSk7IiwiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2NvbW1vbi5zY3NzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XG5cblxuaW9uLXRpdGxlIHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgcGFkZGluZy10b3A6IDFyZW07XG59XG5cbi5pbnZhbGlkLXRleHR7XG4gICAgY29sb3I6ICNFRTQwMzZcbn1cblxuaW9uLWdyaWQge1xuICAgIG1heC13aWR0aDogMzUwcHg7XG59XG5cbi5mbG9hdC1sZWZ0e1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG1hcmdpbi1ib3R0b206IDNyZW07XG59XG5cblxuLmFjY291bnQtbGFiZWx7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGxlZnQ7XG4gICAgYWxpZ24taXRlbXM6IGxlZnQ7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAjNzI3MjcyO1xuICAgIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgICBwYWRkaW5nLXRvcDogMnJlbTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xufVxuXG4uYWNjb3VudC1pbnB1dHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAjNDI0MjQyO1xuICAgIHBhZGRpbmc6IDFyZW0gMCAwIDA7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tQm9sZCc7XG4gICAgYm9yZGVyLWJvdHRvbTogMC4wMDFyZW0gc29saWQgI2EwYTBhMDtcbn1cblxuaW5wdXR7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyOiAwO1xuICAgIC8vIGJvcmRlci1ib3R0b206IDAuMDAxcmVtIHNvbGlkIHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMC41cmVtO1xufVxuXG5cbmlucHV0W3R5cGU9XCJwYXNzd29yZFwiXXtcbiAgICAtd2Via2l0LXRleHQtc3Ryb2tlLXdpZHRoOiAwLjE4cmVtO1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjE4cmVtO1xufVxuXG4uc2V0dGluZ3MtYnV0dG9ue1xuICAgIHBhZGRpbmctdG9wOiAxLjVyZW07XG59XG5cbi5zYXZlLXBhc3N3b3JkLWJ0biB7XG4gICAgYmFja2dyb3VuZDogI0Y0NzczQiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogJGJ1dHRvbi1ncmFkaWVudC0xICFpbXBvcnRhbnQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgICBoZWlnaHQ6IDNyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4uZXllLWJ0biB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIFxufSJdfQ== */");

/***/ }),

/***/ "./src/app/settings/change-password/change-password.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/settings/change-password/change-password.page.ts ***!
  \******************************************************************/
/*! exports provided: ChangePasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordPage", function() { return ChangePasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_shared_services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/shared/services/user.service */ "./src/shared/services/user.service.ts");
/* harmony import */ var src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/shared/services/toast.service */ "./src/shared/services/toast.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/shared/services/loader.service */ "./src/shared/services/loader.service.ts");
/* harmony import */ var src_shared_helpers_custom_validator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/shared/helpers/custom-validator */ "./src/shared/helpers/custom-validator.ts");









let ChangePasswordPage = class ChangePasswordPage {
    constructor(formBuilder, userService, toastr, router, location, loader) {
        this.formBuilder = formBuilder;
        this.userService = userService;
        this.toastr = toastr;
        this.router = router;
        this.location = location;
        this.loader = loader;
        this.showOld = false;
        this.showNew = false;
        this.showConfirm = false;
    }
    ngOnInit() {
        this.createAccountForm();
    }
    passwordValidator(fieldControl) {
        if (fieldControl.value) {
            const same = this.accountForm.controls.oldPassword.value === fieldControl.value;
            // return same ? null : { notSame: true };
            if (same) {
                return { notSame: true };
            }
            else {
                return null;
            }
        }
    }
    createAccountForm() {
        this.accountForm = this.formBuilder.group({
            oldPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            // newPassword: ['', Validators.required],
            // confirmPassword: ['', Validators.required],
            newPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    // 1. Password Field is Required
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    // 2. Minimum 8 characters
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(8),
                    // 3. Maximum 20 characters
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(20),
                    // 4. check whether the entered password has upper case letter
                    src_shared_helpers_custom_validator__WEBPACK_IMPORTED_MODULE_8__["CustomValidators"].patternValidator(/[A-Z]/, { hasCapitalCase: true }),
                    // 5. check whether the entered password has a lower-case letter
                    src_shared_helpers_custom_validator__WEBPACK_IMPORTED_MODULE_8__["CustomValidators"].patternValidator(/[a-z]/, { hasSmallCase: true }),
                    // 6. check whether the entered password has a number
                    src_shared_helpers_custom_validator__WEBPACK_IMPORTED_MODULE_8__["CustomValidators"].patternValidator(/\d/, { hasNumber: true }),
                ])],
            confirmPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
        });
    }
    checkPassword() {
        this.setValues();
        if (this.newPassword != this.confirmPassword) {
            this.toastr.presentToast("Confirm Password incorrect");
            console.log("changing error");
        }
        else {
            this.changePassword();
            console.log("change success");
        }
    }
    setValues() {
        this.oldPassword = this.fc.oldPassword.value;
        this.newPassword = this.fc.newPassword.value;
        this.confirmPassword = this.fc.confirmPassword.value;
        console.log("old", this.oldPassword);
        console.log("new", this.newPassword);
        console.log("confirm", this.confirmPassword);
    }
    changePassword() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            // this.setValues();
            this.markFormGroupTouched(this.accountForm);
            if (this.fc.oldPassword.value === this.fc.newPassword.value && (this.fc.oldPassword.value !== '' || this.fc.newPassword.value !== '')) {
                this.toastr.errorToast('Old and New passwords can not be same.');
                return;
            }
            if (this.fc.confirmPassword.value !== this.fc.newPassword.value) {
                this.toastr.errorToast('New password and confirm new password must be the same.');
                return;
            }
            if (!this.fc.oldPassword.value || !this.fc.newPassword.value || !this.fc.confirmPassword.value) {
                this.toastr.errorToast('Please fill in all required fields.');
                return;
            }
            if (this.accountForm.invalid) {
                this.toastr.presentToast("Please complete the required fields");
                return;
            }
            this.loader.createLoader("Changing Password");
            yield this.userService.changePassword(this.oldPassword, this.newPassword, this.confirmPassword).subscribe(result => {
                console.log("Success", result.message);
                this.toastr.presentToast("Password succesfully changed");
                this.loader.dismissLoader();
                this.location.back();
            }, error => {
                this.loader.dismissLoader();
                this.toastr.presentToast(error.error.error);
                console.log("Error", error);
            });
        });
    }
    get fc() { return this.accountForm.controls; }
    markFormGroupTouched(formGroup) {
        Object.values(formGroup.controls).forEach(control => {
            control.markAsTouched();
        });
    }
    doRefresh(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log('Begin async operation');
            this.ngOnInit();
            event.target.complete();
        });
    }
};
ChangePasswordPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: src_shared_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_4__["ToastService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"] },
    { type: src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_7__["LoaderService"] }
];
ChangePasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-change-password',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./change-password.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/change-password/change-password.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./change-password.page.scss */ "./src/app/settings/change-password/change-password.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        src_shared_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"],
        src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_4__["ToastService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
        _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"],
        src_shared_services_loader_service__WEBPACK_IMPORTED_MODULE_7__["LoaderService"]])
], ChangePasswordPage);



/***/ }),

/***/ "./src/shared/helpers/custom-validator.ts":
/*!************************************************!*\
  !*** ./src/shared/helpers/custom-validator.ts ***!
  \************************************************/
/*! exports provided: CustomValidators */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomValidators", function() { return CustomValidators; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class CustomValidators {
    static patternValidator(regex, error) {
        return (control) => {
            if (!control.value) {
                // if control is empty return no error
                return null;
            }
            // test the value of the control against the regexp supplied
            const valid = regex.test(control.value);
            // if true, return no error (no error), else return error passed in the second parameter
            return valid ? null : error;
        };
    }
    static passwordMatchValidator(control) {
        const password = control.get('password').value; // get password from our password form control
        const confirmPassword = control.get('confirmPassword').value; // get password from our confirmPassword form control
        // compare is the password math
        if (password !== confirmPassword) {
            // if they don't match, set an error in our confirmPassword form control
            control.get('confirmPassword').setErrors({ NoPassswordMatch: true });
        }
    }
}


/***/ })

}]);
//# sourceMappingURL=settings-change-password-change-password-module-es2015.js.map