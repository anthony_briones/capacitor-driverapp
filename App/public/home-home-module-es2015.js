(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title [size]=\"titleSize\">\n      Home\n    </ion-title>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [scrollEvents]=\"true\" (ionScroll)=\"onContentScroll($event)\">\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <div *ngIf=\"currentRoute\" @fadein>\n    <ion-card class=\"next-gig-panel\">\n      <ion-card-header>\n        <ion-item-divider mode=\"ios\" class=\"next-gig-header\">\n          <ion-label class=\"header-font\">\n            Your Current Route\n          </ion-label>\n        </ion-item-divider>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-row class=\"ion-padding\">\n          <ion-col size=\"9\" class=\"next-gig-date ion-no-padding\">\n            {{currentRoute.deliveryDate | customDate:'longWeekDay'}},\n            {{currentRoute.deliveryDate | customDate:'deliveryDate'}}\n          </ion-col>\n\n          <ion-col size=\"3\" class=\"next-gig-time ion-no-padding\">\n            <i class=\"fa fa-clock-o\"></i>\n            {{currentRoute.startTime | convert24hrto12hr}}\n          </ion-col>\n\n          <ion-col size=\"12\" class=\"next-gig-delivery ion-no-padding\">\n            {{currentDelivery?.stopsLength}} Deliveries\n          </ion-col>\n\n          <!-- <ion-col size=\"6\" class=\"next-gig-details vertical-align\">\n                            <ion-icon class=\"icon-delivery\" src=\"/assets/icon/icon-home-delivery.svg\"> </ion-icon> 4 Residential\n                          </ion-col>\n                          <ion-col size=\"6\" class=\"next-gig-details vertical-align\">\n                              <ion-icon class=\"icon-delivery\" src=\"/assets/icon/icon-commercial-delivery.svg\">testing </ion-icon>  1 Commercial\n                          </ion-col> -->\n\n          <ion-col size=\"1\" class=\"ion-no-padding vertical-align\">\n            <ion-img class=\"tank-img\" src=\"/assets/icon/icon-tank-cynch-white.svg\"></ion-img>\n          </ion-col>\n          <ion-col size=\"6\" class=\"ion-no-padding vertical-align\">\n            <ion-label class=\"other-number\">{{cynchCount}}</ion-label>\n            <!-- <ion-label *ngIf=\"!isCynchOrder\" class=\"other-number\">0</ion-label> -->\n            <ion-label class=\"other-tanks\">Cynch tanks</ion-label>\n          </ion-col>\n          <ion-col size=\"1\" class=\"ion-no-padding vertical-align\">\n            <ion-img class=\"tank-img\" src=\"/assets/icon/icon-tank-amerigas-white.svg\"></ion-img>\n          </ion-col>\n          <ion-col size=\"4\" class=\"ion-no-padding vertical-align\">\n\n            <ion-label class=\"other-number\">{{aceCount}}</ion-label>\n            <!-- <ion-label *ngIf=\"isCynchOrder\" class=\"other-number\">0</ion-label> -->\n            <ion-label class=\"other-tanks\">ACE tanks</ion-label>\n          </ion-col>\n\n\n          <ion-col size=\"12\" class=\"login-button shadow ion-no-padding\">\n            <button class=\"btn shadow-lg vertical-align \" round ion-button (click)=\"openDeliveryPage()\">\n              <ion-icon class=\"navigate-icon\" slot=\"start\" src=\"/assets/icon/icon-navigation.svg\"></ion-icon>\n              Continue Delivery\n            </button>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n\n    </ion-card>\n\n  </div>\n\n  <div *ngIf=\"!currentRoute\" @fadein>\n    <div *ngIf=\"routes.length != 0\">\n      <div *ngFor=\"let route of routes\">\n        <ion-card class=\"next-gig-panel\" *ngIf=\"route.deliveryCount != 0 && route.deliveryDate === (dateNow | date:'yyyy-MM-dd')\">\n          <ion-card-header>\n            <ion-item-divider mode=\"ios\" class=\"next-gig-header\">\n              <ion-label class=\"header-font\">\n                Your Next Route\n              </ion-label>\n            </ion-item-divider>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-row class=\"ion-padding\">\n              <ion-col size=\"9\" class=\"next-gig-date ion-no-padding\">\n                {{route.deliveryDate | customDate:'longWeekDay'}},\n                {{route.deliveryDate | customDate:'deliveryDate'}}\n              </ion-col>\n\n              <ion-col size=\"3\" class=\"next-gig-time ion-no-padding\">\n                <i class=\"fa fa-clock-o\"></i>\n                {{route.startTime | convert24hrto12hr}}\n              </ion-col>\n\n              <ion-col size=\"12\" class=\"next-gig-delivery ion-no-padding\">\n                {{route.deliveryCount}} Deliveries\n              </ion-col>\n\n              <!-- <ion-col size=\"6\" class=\"next-gig-details vertical-align\">\n                            <ion-icon class=\"icon-delivery\" src=\"/assets/icon/icon-home-delivery.svg\"> </ion-icon> 4 Residential\n                          </ion-col>\n                          <ion-col size=\"6\" class=\"next-gig-details vertical-align\">\n                              <ion-icon class=\"icon-delivery\" src=\"/assets/icon/icon-commercial-delivery.svg\">testing </ion-icon>  1 Commercial\n                          </ion-col> -->\n\n              <ion-col size=\"1\" class=\"ion-no-padding vertical-align\">\n                <ion-img class=\"tank-img\" src=\"/assets/icon/icon-tank-cynch-white.svg\"></ion-img>\n              </ion-col>\n              <ion-col size=\"6\" class=\"ion-no-padding vertical-align\">\n                <ion-label class=\"other-number\">{{route.cynchCount}}</ion-label>\n                <ion-label class=\"other-tanks\">Cynch tanks</ion-label>\n              </ion-col>\n              <ion-col size=\"1\" class=\"ion-no-padding vertical-align\">\n                <ion-img class=\"tank-img\" src=\"/assets/icon/icon-tank-amerigas-white.svg\"></ion-img>\n              </ion-col>\n              <ion-col size=\"4\" class=\"ion-no-padding vertical-align\">\n\n                <ion-label class=\"other-number\">{{route.aceCount}}</ion-label>\n                <ion-label class=\"other-tanks\">ACE tanks</ion-label>\n              </ion-col>\n\n\n              <ion-col size=\"12\" class=\"login-button shadow ion-no-padding\">\n                <button class=\"btn shadow-lg vertical-align \" round ion-button (click)=\"openMapApp(route)\">\n                  <ion-icon class=\"navigate-icon\" slot=\"start\" src=\"/assets/icon/icon-navigation.svg\"></ion-icon>\n                  Go To Depot\n                </button>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n\n        </ion-card>\n      </div>\n    </div>\n\n    <ion-card class=\"next-gig-panel-noDelivery\" *ngIf=\"routes.length != 0 && routes.deliveryCount === 0\">\n      <div *ngIf=\"routes[0].deliveryCount === 0\">\n        <ion-card-header mode=\"ios\">\n          <ion-item-divider mode=\"ios\" class=\"next-gig-header-noDelivery\">\n            <div class=\"header-font-noDelivery\">\n              Your Next Shyft\n            </div>\n          </ion-item-divider>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-row>\n            <ion-col size=\"8\" class=\"next-gig-date-noDelivery\">\n              {{routes[0].deliveryDate | customDate:'longWeekDay'}},\n              <br> {{routes[0].deliveryDate | customDate:'deliveryDate'}}\n            </ion-col>\n\n            <ion-col size=\"4\" class=\"next-gig-time-noDelivery\">\n              <i class=\"fa fa-clock-o\"></i>\n              {{routes[0].startTime | convert24hrto12hr}}\n            </ion-col>\n\n          </ion-row>\n        </ion-card-content>\n      </div>\n    </ion-card>\n\n  </div>\n\n  <!-- skeleton for currentRoute -->\n  <div *ngIf=\"showskeleton\" @fadein>\n    <div *ngIf=\"routes.length == 0\">\n      <div>\n        <ion-card class=\"next-gig-panel\">\n          <ion-card-header>\n            <ion-item-divider mode=\"ios\" class=\"next-gig-header\">\n              <ion-label class=\"header-font-skeleton\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </ion-label>\n            </ion-item-divider>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-row class=\"ion-padding\">\n\n              <ion-col size=\"2\" class=\"\">\n                <!-- <ion-img class=\"tank-img\" src=\"/assets/icon/icon-tank-cynch-white.svg\"></ion-img> -->\n                <ion-skeleton-text class=\"tank-img-skeleton\" animated></ion-skeleton-text>\n              </ion-col>\n\n              <ion-col size=\"8\" class=\"\">\n                <div>\n                  <ion-skeleton-text class=\"w-75\" animated></ion-skeleton-text>\n                </div>\n                <div>\n                  <ion-skeleton-text class=\"w-50\" animated></ion-skeleton-text>\n                </div>\n              </ion-col>\n\n              <ion-col size=\"5\" class=\"\">\n              </ion-col>\n\n              <ion-col size=\"12\" class=\"ion-no-padding\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </ion-col>\n\n              <ion-col size=\"12\" class=\"ion-no-padding\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </ion-col>\n\n              <ion-col size=\"12\" class=\"ion-no-padding\">\n                <ion-skeleton-text animated class=\"w-75\"></ion-skeleton-text>\n              </ion-col>\n\n            </ion-row>\n          </ion-card-content>\n\n        </ion-card>\n      </div>\n    </div>\n\n  </div>\n\n  <!-- <ion-card class=\"rating-panel\" *ngIf=\"!showskeleton\" @fadein>\n    <ion-card-header>\n      <ion-item-divider mode=\"ios\" class=\"divider-rating\">\n        <ion-label class=\"rating-header\">\n          Keep It Up!\n        </ion-label>\n      </ion-item-divider>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-row class=\"rating-info\">\n        <ion-col size=\"12\" class=\"rating-label\">\n          <ion-label>\n            Rating Level Promotion\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"3\" class=\"rating-img\">\n          image goes here\n        </ion-col>\n        <ion-col size=\"9\" class=\"rating-details\">\n          <ion-label>\n            You have been rated excellently over your last 5 gigs. You've earned 25 reward points!\n          </ion-label>\n\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card> -->\n\n  <ion-card class=\"reminder-panel\" *ngIf=\"!showskeleton\" @fadein>\n    <ion-card-header>\n      <ion-item-divider mode=\"ios\" class=\"divider-reminder\">\n        <ion-label class=\"reminder-header\">\n          Welcome to Cynch Driver App\n        </ion-label>\n      </ion-item-divider>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-row class=\"reminder-info\">\n        <ion-col size=\"12\" class=\"reminder-label\">\n          <ion-label>\n            Hi, {{currentUser.firstname}}\n          </ion-label>\n        </ion-col>\n\n        <ion-col size=\"12\" class=\"reminder-details\">\n          <ion-label>\n            Thanks for signing up to drive for Cynch. Enjoy the flexibility and freedom to drive whenever you want!\n          </ion-label>\n\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n\n  <!-- skeleton for rating -->\n  <ion-card class=\"rating-panel\" *ngIf=\"showskeleton\">\n    <ion-card-header>\n      <ion-item-divider mode=\"ios\" class=\"divider-rating\">\n        <ion-label class=\"rating-header-skeletion\">\n            <ion-skeleton-text animated></ion-skeleton-text>\n        </ion-label>\n      </ion-item-divider>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-row class=\"rating-info\">\n        <ion-col size=\"12\" class=\"rating-label\">\n          <ion-label>\n              <ion-skeleton-text animated></ion-skeleton-text>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"3\" class=\"rating-img\">\n            <ion-skeleton-text class=\"h-100\" animated></ion-skeleton-text>\n        </ion-col>\n        <ion-col size=\"9\" class=\"rating-details\">\n          <ion-label>\n              <ion-skeleton-text animated></ion-skeleton-text>\n          </ion-label>\n\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n\n  <!-- skeleton for reminder -->\n  <!-- <ion-card class=\"reminder-panel\"  *ngIf=\"showskeleton\">\n    <ion-card-header>\n      <ion-item-divider mode=\"ios\" class=\"divider-reminder\">\n        <ion-label class=\"reminder-header-skeleton\">\n            <ion-skeleton-text animated></ion-skeleton-text>\n        </ion-label>\n      </ion-item-divider>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-row class=\"reminder-info\">\n        <ion-col size=\"12\" class=\"reminder-label\">\n          <ion-label>\n              <ion-skeleton-text animated></ion-skeleton-text>\n          </ion-label>\n        </ion-col>\n\n        <ion-col size=\"12\" class=\"reminder-details\">\n          <ion-label>\n              <ion-skeleton-text animated></ion-skeleton-text>\n          </ion-label>\n\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card> -->\n\n</ion-content>");

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/shared/shared.module.ts");








let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                {
                    path: '',
                    component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                },
            ])
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-title {\n  color: #003A58;\n  font-family: \"Montserrat-Bold\";\n}\n\nion-toolbar {\n  margin-top: 1rem !important;\n  font-family: \"Montserrat-Bold\";\n}\n\nion-content {\n  --background: white;\n}\n\n.next-gig-panel {\n  background-color: #003A58;\n  margin: 1rem;\n}\n\n.next-gig-panel-noDelivery {\n  background-color: #FFFFFF;\n  margin: 1rem;\n}\n\n.divider-rating {\n  background-color: white;\n  border-bottom: 1px solid #F2F2F2;\n}\n\n.divider-reminder {\n  background-color: white;\n  border-bottom: 1px solid #F2F2F2;\n}\n\n.next-gig-header {\n  background-color: #003A58;\n  border-bottom: 1px solid #1A4E69;\n}\n\n.next-gig-header-noDelivery {\n  background-color: #FFFFFF;\n  border-bottom: 1px solid #F2F2F2;\n}\n\n.header-font {\n  color: #FAAF40;\n  font-size: 1rem;\n  font-family: \"Montserrat-Bold\";\n}\n\n.header-font-skeleton {\n  width: 10rem;\n}\n\n.header-font-noDelivery {\n  color: #003A58;\n  font-size: 1rem;\n  font-family: \"Montserrat-Bold\";\n}\n\n.next-gig-date {\n  font-size: 0.875rem;\n  color: white;\n  font-family: \"Roboto-Bold\";\n  line-height: 1.3125rem;\n}\n\n.next-gig-time {\n  font-size: 0.875rem;\n  color: white;\n  font-family: \"Roboto-Bold\";\n  line-height: 1.3125rem;\n}\n\n.next-gig-date-noDelivery {\n  font-size: 0.875rem;\n  padding-left: 1rem;\n  padding-top: 0.625rem;\n  color: #003A58;\n  font-family: \"Roboto-Bold\";\n  line-height: 1.3125rem;\n}\n\n.next-gig-time-noDelivery {\n  font-size: 0.875rem;\n  padding-right: 1rem;\n  padding-top: 0.625rem;\n  color: #003A58;\n  font-family: \"Roboto-Bold\";\n  line-height: 1.3125rem;\n}\n\n.next-gig-delivery {\n  font-size: 1.75rem;\n  color: white;\n  font-weight: bolder;\n  font-family: \"Montserrat-Bold\";\n}\n\n.next-gig-details {\n  padding: 0.625rem 1rem;\n  color: white;\n  font-family: \"Roboto-Medium\";\n  font-size: 0.875rem;\n  line-height: 1.3125rem;\n}\n\n.login-button {\n  padding-top: 1rem;\n}\n\nbutton {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n  background: #F4773B 100%;\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036));\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%);\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\nion-card-header,\nion-card-content {\n  padding: 0;\n}\n\n.rating-panel {\n  background-color: white;\n  margin: 1rem;\n}\n\n.rating-header {\n  color: gray;\n  font-size: 0.875rem;\n  font-family: \"Roboto-Black\";\n  line-height: 1.3125rem;\n}\n\n.rating-header-skeletion {\n  color: gray;\n  font-size: 0.875rem;\n  font-family: \"Roboto-Black\";\n  line-height: 1.3125rem;\n  width: 75%;\n}\n\n.rating-label {\n  color: #004D75;\n  font-size: 1.25rem;\n  padding: 0;\n  font-family: \"Montserrat-Bold\";\n  letter-spacing: 0;\n}\n\n.rating-img {\n  padding: 0.75rem;\n}\n\n.rating-details {\n  color: #424242;\n  font-size: 0.875rem;\n  padding: 0.75rem;\n  font-family: \"Roboto-Regular\";\n}\n\n.rating-info {\n  padding: 0.625rem 1rem 1rem 1rem;\n}\n\n.reminder-panel {\n  background-color: white;\n  margin: 1rem;\n}\n\n.reminder-header {\n  color: gray;\n  font-size: 0.875rem;\n  font-family: \"Roboto-Black\";\n  line-height: 1.3125rem;\n}\n\n.reminder-header-skeleton {\n  color: gray;\n  font-size: 0.875rem;\n  font-family: \"Roboto-Black\";\n  line-height: 1.3125rem;\n  width: 75%;\n}\n\n.reminder-label {\n  color: #004D75;\n  font-size: 1.25rem;\n  padding: 0;\n  font-family: \"Montserrat-Bold\";\n  letter-spacing: 0;\n}\n\n.reminder-details {\n  color: #424242;\n  font-size: 0.875rem;\n  padding: 0.625rem 0;\n  line-height: 1.3125rem;\n}\n\n.reminder-info {\n  padding: 0.625rem 1rem 1rem 1rem;\n}\n\n.clock-img {\n  width: 1rem !important;\n}\n\nion-icon.navigate-icon {\n  height: 20px;\n  width: 20px;\n  padding-right: 5px;\n}\n\nion-icon.icon-delivery {\n  height: 20px;\n  width: 20px;\n  padding-right: 5px;\n  color: red;\n}\n\n.icon-delivery {\n  color: red !important;\n}\n\n.tank-img {\n  height: 100%;\n}\n\n.tank-img-skeleton {\n  background-size: 1.5rem 1.5rem;\n  height: 2rem;\n  background-image: -webkit-gradient(linear, left top, right top, from(#004C73), color-stop(20%, #005580), color-stop(40%, #004C73), to(#004C73));\n  background-image: linear-gradient(to right, #004C73 0%, #005580 20%, #004C73 40%, #004C73 100%);\n}\n\n.other-tanks {\n  font-size: 0.875rem;\n  color: white;\n  letter-spacing: 0;\n  font-family: \"Roboto-Medium\";\n}\n\n.other-number {\n  font-size: 1.25rem;\n  color: #ffffff;\n  font-family: \"Montserrat-Bold\";\n  padding-right: 5px;\n  padding-left: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCIvVXNlcnMvYW50aG9ueS5icmlvbmVzL0RvY3VtZW50cy9DYXBEcml2ZXIvc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFPQTtFQUNJLHlCQUFBO0VBQ0EsZ0RBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkFBQTtBQ05KOztBRFNBO0VBQ0kseUJFbEJjO0VGbUJkLFdBQUE7RUFDQSxjRWhCSTtFRmlCSixtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtBQ05KOztBRFNBO0VBQ0ksK0JBQUE7RUFBQSx3QkFBQTtFQUNBLGdDQUFBO0VBQ0Esb0NBQUE7VUFBQSw4QkFBQTtBQ05KOztBRFNBO0VBQ0ksbUNBQUE7VUFBQSxrQ0FBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7VUFBQSx5QkFBQTtBQ05KOztBRFNBO0VBQ0ksd0JBQUE7VUFBQSx1QkFBQTtBQ05KOztBRFNBO0VBQ0ksWUFBQTtBQ05KOztBRTVDQTtFQUNJLGNETGM7RUNPZCw4QkFBQTtBRjhDSjs7QUUzQ0E7RUFDSSwyQkFBQTtFQUNBLDhCQUFBO0FGOENKOztBRTNDQTtFQUVJLG1CQUFBO0FGNkNKOztBRXpDQTtFQUNJLHlCRHRCYztFQ3VCZCxZQUFBO0FGNENKOztBRXhDQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtBRjJDSjs7QUV4Q0E7RUFDSSx1QkFBQTtFQUNBLGdDQUFBO0FGMkNKOztBRXhDQTtFQUNJLHVCQUFBO0VBQ0EsZ0NBQUE7QUYyQ0o7O0FFeENBO0VBQ0kseUJEM0NjO0VDNENkLGdDQUFBO0FGMkNKOztBRXhDQTtFQUNJLHlCQUFBO0VBQ0EsZ0NBQUE7QUYyQ0o7O0FFeENBO0VBQ0ksY0RwRGM7RUNxRGQsZUFBQTtFQUVBLDhCQUFBO0FGMENKOztBRXZDQTtFQUNJLFlBQUE7QUYwQ0o7O0FFdkNBO0VBQ0ksY0RoRWM7RUNpRWQsZUFBQTtFQUNBLDhCQUFBO0FGMENKOztBRXJDQTtFQUNJLG1CQUFBO0VBR0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0Esc0JBQUE7QUZzQ0o7O0FFbkNBO0VBQ0ksbUJBQUE7RUFHQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxzQkFBQTtBRm9DSjs7QUVqQ0E7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxjRDdGYztFQzhGZCwwQkFBQTtFQUNBLHNCQUFBO0FGb0NKOztBRWpDQTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNEdEdjO0VDdUdkLDBCQUFBO0VBQ0Esc0JBQUE7QUZvQ0o7O0FFakNBO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFFQSw4QkFBQTtBRm1DSjs7QUVoQ0E7RUFDSSxzQkFBQTtFQUNBLFlBQUE7RUFDQSw0QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUZtQ0o7O0FFaENBO0VBRUksaUJBQUE7QUZrQ0o7O0FFL0JBO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUVBLDhCQUFBO0VBQ0Esd0JBQUE7RUFDQSw2RkRsSWdCO0VDa0loQixvRURsSWdCO0VDbUloQix3QkFBQTtVQUFBLHVCQUFBO0FGaUNKOztBRTlCQTs7RUFFSSxVQUFBO0FGaUNKOztBRTlCQTtFQUNJLHVCQUFBO0VBQ0EsWUFBQTtBRmlDSjs7QUU5QkE7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0FGaUNKOztBRTlCQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSxVQUFBO0FGaUNKOztBRTlCQTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSw4QkFBQTtFQUNBLGlCQUFBO0FGaUNKOztBRTlCQTtFQUNJLGdCQUFBO0FGaUNKOztBRTlCQTtFQUNJLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsNkJBQUE7QUZpQ0o7O0FFOUJBO0VBQ0ksZ0NBQUE7QUZpQ0o7O0FFN0JBO0VBQ0ksdUJBQUE7RUFDQSxZQUFBO0FGZ0NKOztBRTdCQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7QUZnQ0o7O0FFN0JBO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLFVBQUE7QUZnQ0o7O0FFN0JBO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLDhCQUFBO0VBQ0EsaUJBQUE7QUZnQ0o7O0FFN0JBO0VBQ0ksY0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBRmdDSjs7QUU3QkE7RUFDSSxnQ0FBQTtBRmdDSjs7QUU3QkE7RUFDSSxzQkFBQTtBRmdDSjs7QUU1Qkk7RUFHSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FGNkJSOztBRTFCSTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FGNEJSOztBRXZCQTtFQUNJLHFCQUFBO0FGMEJKOztBRXRCQTtFQUNJLFlBQUE7QUZ5Qko7O0FFcEJBO0VBQ0ksOEJBQUE7RUFDQSxZQUFBO0VBQ0EsK0lBQUE7RUFBQSwrRkFBQTtBRnVCSjs7QUVuQkE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLDRCQUFBO0FGc0JKOztBRWpCQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBRm9CSiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvZm9udHMnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcyc7XG5cbi8vIC5pcy12YWxpZCB7XG4vLyAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZVxuLy8gfVxuXG4uaXMtaW52YWxpZCB7XG4gICAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwM1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogJHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xe1xuICAgIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICAgIGRpc3BsYXk6IGZsZXghaW1wb3J0YW50O1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlciFpbXBvcnRhbnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuXG5cblxuIiwiLmlzLWludmFsaWQge1xuICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDM7XG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDFyZW07XG4gIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTEge1xuICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbmlvbi10aXRsZSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuaW9uLXRvb2xiYXIge1xuICBtYXJnaW4tdG9wOiAxcmVtICFpbXBvcnRhbnQ7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbi5uZXh0LWdpZy1wYW5lbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIG1hcmdpbjogMXJlbTtcbn1cblxuLm5leHQtZ2lnLXBhbmVsLW5vRGVsaXZlcnkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGRkZGO1xuICBtYXJnaW46IDFyZW07XG59XG5cbi5kaXZpZGVyLXJhdGluZyB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI0YyRjJGMjtcbn1cblxuLmRpdmlkZXItcmVtaW5kZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNGMkYyRjI7XG59XG5cbi5uZXh0LWdpZy1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzFBNEU2OTtcbn1cblxuLm5leHQtZ2lnLWhlYWRlci1ub0RlbGl2ZXJ5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNGMkYyRjI7XG59XG5cbi5oZWFkZXItZm9udCB7XG4gIGNvbG9yOiAjRkFBRjQwO1xuICBmb250LXNpemU6IDFyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xufVxuXG4uaGVhZGVyLWZvbnQtc2tlbGV0b24ge1xuICB3aWR0aDogMTByZW07XG59XG5cbi5oZWFkZXItZm9udC1ub0RlbGl2ZXJ5IHtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG59XG5cbi5uZXh0LWdpZy1kYXRlIHtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tQm9sZFwiO1xuICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4ubmV4dC1naWctdGltZSB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLm5leHQtZ2lnLWRhdGUtbm9EZWxpdmVyeSB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIHBhZGRpbmctbGVmdDogMXJlbTtcbiAgcGFkZGluZy10b3A6IDAuNjI1cmVtO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJvbGRcIjtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLm5leHQtZ2lnLXRpbWUtbm9EZWxpdmVyeSB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XG4gIHBhZGRpbmctdG9wOiAwLjYyNXJlbTtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1Cb2xkXCI7XG4gIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5uZXh0LWdpZy1kZWxpdmVyeSB7XG4gIGZvbnQtc2l6ZTogMS43NXJlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuLm5leHQtZ2lnLWRldGFpbHMge1xuICBwYWRkaW5nOiAwLjYyNXJlbSAxcmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1NZWRpdW1cIjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLmxvZ2luLWJ1dHRvbiB7XG4gIHBhZGRpbmctdG9wOiAxcmVtO1xufVxuXG5idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gIGhlaWdodDogM3JlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgYmFja2dyb3VuZDogI0Y0NzczQiAxMDAlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoLTE4MGRlZywgI0ZGODg0MCAwJSwgI0VFNDAzNiAxMDAlKTtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbmlvbi1jYXJkLWhlYWRlcixcbmlvbi1jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAwO1xufVxuXG4ucmF0aW5nLXBhbmVsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIG1hcmdpbjogMXJlbTtcbn1cblxuLnJhdGluZy1oZWFkZXIge1xuICBjb2xvcjogZ3JheTtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJsYWNrXCI7XG4gIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5yYXRpbmctaGVhZGVyLXNrZWxldGlvbiB7XG4gIGNvbG9yOiBncmF5O1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tQmxhY2tcIjtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbiAgd2lkdGg6IDc1JTtcbn1cblxuLnJhdGluZy1sYWJlbCB7XG4gIGNvbG9yOiAjMDA0RDc1O1xuICBmb250LXNpemU6IDEuMjVyZW07XG4gIHBhZGRpbmc6IDA7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBsZXR0ZXItc3BhY2luZzogMDtcbn1cblxuLnJhdGluZy1pbWcge1xuICBwYWRkaW5nOiAwLjc1cmVtO1xufVxuXG4ucmF0aW5nLWRldGFpbHMge1xuICBjb2xvcjogIzQyNDI0MjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgcGFkZGluZzogMC43NXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLVJlZ3VsYXJcIjtcbn1cblxuLnJhdGluZy1pbmZvIHtcbiAgcGFkZGluZzogMC42MjVyZW0gMXJlbSAxcmVtIDFyZW07XG59XG5cbi5yZW1pbmRlci1wYW5lbCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBtYXJnaW46IDFyZW07XG59XG5cbi5yZW1pbmRlci1oZWFkZXIge1xuICBjb2xvcjogZ3JheTtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJsYWNrXCI7XG4gIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5yZW1pbmRlci1oZWFkZXItc2tlbGV0b24ge1xuICBjb2xvcjogZ3JheTtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLUJsYWNrXCI7XG4gIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG4gIHdpZHRoOiA3NSU7XG59XG5cbi5yZW1pbmRlci1sYWJlbCB7XG4gIGNvbG9yOiAjMDA0RDc1O1xuICBmb250LXNpemU6IDEuMjVyZW07XG4gIHBhZGRpbmc6IDA7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBsZXR0ZXItc3BhY2luZzogMDtcbn1cblxuLnJlbWluZGVyLWRldGFpbHMge1xuICBjb2xvcjogIzQyNDI0MjtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgcGFkZGluZzogMC42MjVyZW0gMDtcbiAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLnJlbWluZGVyLWluZm8ge1xuICBwYWRkaW5nOiAwLjYyNXJlbSAxcmVtIDFyZW0gMXJlbTtcbn1cblxuLmNsb2NrLWltZyB7XG4gIHdpZHRoOiAxcmVtICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1pY29uLm5hdmlnYXRlLWljb24ge1xuICBoZWlnaHQ6IDIwcHg7XG4gIHdpZHRoOiAyMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG59XG5pb24taWNvbi5pY29uLWRlbGl2ZXJ5IHtcbiAgaGVpZ2h0OiAyMHB4O1xuICB3aWR0aDogMjBweDtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xuICBjb2xvcjogcmVkO1xufVxuXG4uaWNvbi1kZWxpdmVyeSB7XG4gIGNvbG9yOiByZWQgIWltcG9ydGFudDtcbn1cblxuLnRhbmstaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4udGFuay1pbWctc2tlbGV0b24ge1xuICBiYWNrZ3JvdW5kLXNpemU6IDEuNXJlbSAxLjVyZW07XG4gIGhlaWdodDogMnJlbTtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMDA0QzczIDAlLCAjMDA1NTgwIDIwJSwgIzAwNEM3MyA0MCUsICMwMDRDNzMgMTAwJSk7XG59XG5cbi5vdGhlci10YW5rcyB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1NZWRpdW1cIjtcbn1cblxuLm90aGVyLW51bWJlciB7XG4gIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIHBhZGRpbmctbGVmdDogOHB4O1xufSIsIlxuLy8gY29sb3JzXG4vLyAkcHJpbWFyeS1jb2xvci0xOiAjMDBBM0M4O1xuJHByaW1hcnktY29sb3ItMTogIzAwM0E1ODtcbiRwcmltYXJ5LWNvbG9yLTI6ICNGQUFGNDA7XG4kcHJpbWFyeS1jb2xvci0zOiAjRjQ3NzNCO1xuXG4kd2hpdGU6ICNmZmZmZmY7XG5cbi8vICRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgcmdiYSgyNTUsMjU1LDI1NSwwLjUwKSAwJSwgcmdiYSgwLDAsMCwwLjUwKSAxMDAlKTtcbiRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KC0xODBkZWcsICNGRjg4NDAgMCUsICNFRTQwMzYgMTAwJSk7IiwiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2NvbW1vbi5zY3NzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XG5cbi8vIC5iZy1jb2xvcntcbi8vICAgICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG4vLyB9XG5cbmlvbi10aXRsZXtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICAvLyBzaXplOiAzcmVtICFpbXBvcnRhbnQ7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xufVxuXG5pb24tdG9vbGJhciB7XG4gICAgbWFyZ2luLXRvcDogMXJlbSAhaW1wb3J0YW50O1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbn1cblxuaW9uLWNvbnRlbnR7XG4gICAgLy8gLS1iYWNrZ3JvdW5kOiAjRDlEOUQ5O1xuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XG59XG5cblxuLm5leHQtZ2lnLXBhbmVse1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgbWFyZ2luOiAxcmVtO1xuICAgIFxufVxuXG4ubmV4dC1naWctcGFuZWwtbm9EZWxpdmVyeSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgICBtYXJnaW46IDFyZW07XG59XG5cbi5kaXZpZGVyLXJhdGluZyB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNGMkYyRjI7XG59XG5cbi5kaXZpZGVyLXJlbWluZGVyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI0YyRjJGMjtcbn1cblxuLm5leHQtZ2lnLWhlYWRlcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMUE0RTY5O1xufVxuXG4ubmV4dC1naWctaGVhZGVyLW5vRGVsaXZlcnkge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNGMkYyRjI7XG59XG5cbi5oZWFkZXItZm9udHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMjtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgLy8gZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG59XG5cbi5oZWFkZXItZm9udC1za2VsZXRvbntcbiAgICB3aWR0aDogMTByZW07XG59XG5cbi5oZWFkZXItZm9udC1ub0RlbGl2ZXJ5IHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xufVxuXG5cblxuLm5leHQtZ2lnLWRhdGV7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICAvLyBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gICAgLy8gcGFkZGluZy10b3A6IDAuNjI1cmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1Cb2xkJztcbiAgICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4ubmV4dC1naWctdGltZXtcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIC8vIHBhZGRpbmctcmlnaHQ6IDFyZW07XG4gICAgLy8gcGFkZGluZy10b3A6IDAuNjI1cmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1Cb2xkJztcbiAgICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4ubmV4dC1naWctZGF0ZS1ub0RlbGl2ZXJ5e1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xuICAgIHBhZGRpbmctdG9wOiAwLjYyNXJlbTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1Cb2xkJztcbiAgICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4ubmV4dC1naWctdGltZS1ub0RlbGl2ZXJ5e1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgcGFkZGluZy1yaWdodDogMXJlbTtcbiAgICBwYWRkaW5nLXRvcDogMC42MjVyZW07XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tQm9sZCc7XG4gICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLm5leHQtZ2lnLWRlbGl2ZXJ5e1xuICAgIGZvbnQtc2l6ZTogMS43NXJlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICAvLyBwYWRkaW5nOiAwIDFyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xufVxuXG4ubmV4dC1naWctZGV0YWlsc3tcbiAgICBwYWRkaW5nOiAwLjYyNXJlbSAxcmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLmxvZ2luLWJ1dHRvbntcbiAgICAvLyBwYWRkaW5nOiAxLjI1cmVtIDFyZW07XG4gICAgcGFkZGluZy10b3A6IDFyZW07XG59XG5cbmJ1dHRvbnsgIFxuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAvLyBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBiYWNrZ3JvdW5kOiAjRjQ3NzNCIDEwMCU7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogJGJ1dHRvbi1ncmFkaWVudC0xO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG5pb24tY2FyZC1oZWFkZXIsXG5pb24tY2FyZC1jb250ZW50e1xuICAgIHBhZGRpbmc6IDA7XG59XG5cbi5yYXRpbmctcGFuZWx7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgbWFyZ2luOiAxcmVtO1xufVxuXG4ucmF0aW5nLWhlYWRlcntcbiAgICBjb2xvcjogZ3JheTtcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJsYWNrJztcbiAgICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufVxuXG4ucmF0aW5nLWhlYWRlci1za2VsZXRpb257XG4gICAgY29sb3I6IGdyYXk7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1CbGFjayc7XG4gICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbiAgICB3aWR0aDogNzUlO1xufVxuXG4ucmF0aW5nLWxhYmVse1xuICAgIGNvbG9yOiAjMDA0RDc1O1xuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgICBwYWRkaW5nOiAwO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbn1cblxuLnJhdGluZy1pbWd7XG4gICAgcGFkZGluZzogMC43NXJlbTtcbn1cblxuLnJhdGluZy1kZXRhaWxze1xuICAgIGNvbG9yOiAjNDI0MjQyO1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgcGFkZGluZzogMC43NXJlbTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1SZWd1bGFyJztcbn1cblxuLnJhdGluZy1pbmZve1xuICAgIHBhZGRpbmc6IDAuNjI1cmVtIDFyZW0gMXJlbSAxcmVtO1xufVxuXG5cbi5yZW1pbmRlci1wYW5lbHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBtYXJnaW46IDFyZW07XG59XG5cbi5yZW1pbmRlci1oZWFkZXJ7XG4gICAgY29sb3I6IGdyYXk7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1CbGFjayc7XG4gICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn1cblxuLnJlbWluZGVyLWhlYWRlci1za2VsZXRvbntcbiAgICBjb2xvcjogZ3JheTtcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLUJsYWNrJztcbiAgICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xuICAgIHdpZHRoOiA3NSVcbn1cblxuLnJlbWluZGVyLWxhYmVse1xuICAgIGNvbG9yOiAjMDA0RDc1O1xuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgICBwYWRkaW5nOiAwO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbn1cblxuLnJlbWluZGVyLWRldGFpbHN7XG4gICAgY29sb3I6ICM0MjQyNDI7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBwYWRkaW5nOiAwLjYyNXJlbSAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjMxMjVyZW07XG59XG5cbi5yZW1pbmRlci1pbmZve1xuICAgIHBhZGRpbmc6IDAuNjI1cmVtIDFyZW0gMXJlbSAxcmVtO1xufVxuXG4uY2xvY2staW1nIHtcbiAgICB3aWR0aDogMXJlbSFpbXBvcnRhbnQ7XG59XG5cbmlvbi1pY29uIHtcbiAgICAmLm5hdmlnYXRlLWljb24ge1xuICAgICAgICAvLyBwYWRkaW5nOiA1cHggNXB4IDAgMDtcbiAgICAgICAgLy8gbWFyZ2luOiA1cHggNXB4IDAgMDs7XG4gICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICB9XG5cbiAgICAmLmljb24tZGVsaXZlcnkge1xuICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gICAgICAgIGNvbG9yOiByZWQ7XG5cbiAgICB9XG59XG5cbi5pY29uLWRlbGl2ZXJ5IHtcbiAgICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XG59XG5cblxuLnRhbmstaW1nIHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgLy8gd2lkdGg6IDEwMCU7XG4gICAgLy8gbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG4udGFuay1pbWctc2tlbGV0b257XG4gICAgYmFja2dyb3VuZC1zaXplOiAxLjVyZW0gMS41cmVtO1xuICAgIGhlaWdodDogMnJlbTtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMwMDRDNzMgMCUsICMwMDU1ODAgMjAlLCAjMDA0QzczIDQwJSwgIzAwNEM3MyAxMDAlKTtcbn1cblxuXG4ub3RoZXItdGFua3N7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tTWVkaXVtJztcbiAgICAvLyBtYXJnaW46IGF1dG87XG4gICAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbn1cblxuLm90aGVyLW51bWJlcntcbiAgICBmb250LXNpemU6IDEuMjVyZW07XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDhweDtcbn1cblxuXG4iXX0= */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/schedule.service */ "./src/shared/services/schedule.service.ts");
/* harmony import */ var _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/loader.service */ "./src/shared/services/loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _shared_services_routing_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/services/routing.service */ "./src/shared/services/routing.service.ts");
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ "./node_modules/@ionic-native/launch-navigator/ngx/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../shared/services/sap-api.service */ "./src/shared/services/sap-api.service.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");











let HomePage = class HomePage {
    constructor(scheduleService, loaderService, navCtrl, router, routingService, launchNavigator, geolocation, sapApiService, domCtrl) {
        this.scheduleService = scheduleService;
        this.loaderService = loaderService;
        this.navCtrl = navCtrl;
        this.router = router;
        this.routingService = routingService;
        this.launchNavigator = launchNavigator;
        this.geolocation = geolocation;
        this.sapApiService = sapApiService;
        this.domCtrl = domCtrl;
        this.routes = [];
        this.titleSize = "large";
        this.dateNow = new Date();
        this.showskeleton = true;
    }
    ngOnInit() {
        console.log(this.dateNow.getDate());
    }
    ionViewDidEnter() {
        this.showskeleton = true;
        this.currentRoute = null;
        this.getUserRoute();
        this.currentUser = JSON.parse(localStorage.getItem("current_user"));
        console.log(this.currentUser);
        // this.getLocation();
        this.dateNow.setHours(23, 59, 59);
        // window.location.reload();
        // this.navCtrl.pop();
        // console.log("popping page");
    }
    getUserRoute() {
        // if (localStorage.getItem("current_route_started")) {
        //   this.showskeleton = false;
        //   this.currentRoute = JSON.parse(localStorage.getItem("current_route_started"));
        // } else 
        // this.loaderService.createLoader("Loading...");
        this.routes = [];
        this.routingService.activeRoute().subscribe(result => {
            console.log("active route: ", result);
            if (Object.keys(result).length != 0) {
                localStorage.setItem("stop_sequence", result.stopSequenceNum);
                localStorage.setItem("route_id", result.routeId);
                localStorage.setItem("shyft_toggle", "true");
                // console.log("aceCount", this.aceCount);
                this.routingService.getUserRouteDetails(result.routeId).subscribe(currentRoute => {
                    console.log("current", currentRoute);
                    // currentRoute.totalRemainingCyls = result.totalRemainingCyls
                    localStorage.setItem("current_route_started", JSON.stringify(currentRoute));
                    this.showskeleton = false;
                    this.currentRoute = currentRoute;
                    //test
                    if (result.isRouteComplete) {
                        result.stopSequenceNum = result.stopSequenceNum - 1;
                    }
                    this.sapApiService.getRouteDetails(this.currentRoute.sapRouteId, result.stopSequenceNum).subscribe(currentDelivery => {
                        console.log("Resultsssss", currentDelivery);
                        this.aceCount = currentDelivery.aceCount;
                        this.cynchCount = currentDelivery.cynchCount;
                        // currentDelivery = currentDelivery.delivery
                        if (currentDelivery.stopSequenceNum === 1) {
                            let location = {
                                latitude: 0,
                                longitude: 0
                            };
                            for (let storage of this.currentUser.msa.plants[0].storages) {
                                if (storage.isCylinderLoc) {
                                    location.latitude = storage.latitude;
                                    location.longitude = storage.longitude;
                                }
                            }
                            localStorage.setItem("previous_loc", JSON.stringify(location));
                            // this.showskeleton = false;
                        }
                        else {
                            if (result.isRouteComplete) {
                                currentDelivery.stopSequenceNum = currentDelivery.stopSequenceNum + 1;
                            }
                            this.sapApiService.getRouteDetails(this.currentRoute.sapRouteId, currentDelivery.stopSequenceNum - 1).subscribe(previousDelivery => {
                                // previousDelivery = previousDelivery.delivery
                                let location = {
                                    latitude: 0,
                                    longitude: 0
                                };
                                location.latitude = previousDelivery.latitude;
                                location.longitude = previousDelivery.longitude;
                                localStorage.setItem("previous_loc", JSON.stringify(location));
                                //Go to Summary Page
                                if (result.isRouteComplete) {
                                    this.router.navigate([`/tabs/drive/${this.currentRoute.routeId}/delivery-summary`]);
                                }
                                // this.showskeleton = false;
                            }, error => {
                                console.log(error);
                                this.showskeleton = false;
                            });
                        }
                        //To Resolve Timing Issue - Route shell Here
                        this.getRouteShell();
                        this.currentDelivery = currentDelivery;
                        this.getTankQuantity();
                    }, error => {
                        console.log(error);
                    });
                    // //Go to Summary Page
                    // if (result.isRouteComplete) {
                    //   this.router.navigate([`/tabs/drive/${this.currentRoute.routeId}/delivery-summary`]);
                    // }
                }, err => {
                    console.log(err);
                });
            }
            else {
                localStorage.removeItem("current_delivery");
                localStorage.removeItem("stop_sequence");
                localStorage.removeItem("route_id");
                localStorage.removeItem("stops_length");
                localStorage.removeItem("mileage");
                localStorage.removeItem("current_route_started");
                localStorage.removeItem("material_code");
                this.getRouteShell();
            }
        }, error => {
            console.log(error);
        });
        // this.sapApiService.getUserRouteShell().subscribe(result => {
        //   if (result.length === 0) {
        //     this.routingService.getUserRoute().subscribe(
        //       result => {
        //         this.showskeleton = false;
        //         for (let res of result) {
        //           let sched = new Date(res.deliveryDate)
        //           // console.log("sced", sched);
        //           // console.log("day", this.dateNow);
        //           // console.log(sched > this.dateNow);       
        //           if (res.routeStatus.routeStatusId.toString() === "001" && sched > this.dateNow) {
        //             this.routes.push(res);
        //           }
        //         }
        //         console.log("test", this.routes);
        //         console.log("Success", result);
        //       },
        //       error => {
        //         this.showskeleton = false;
        //         console.log("Error", error);
        //       }
        //     );
        //   } else {
        //     console.log("Result", result);
        //     this.showskeleton = false;
        //     this.routes = result
        //     // this.loaderService.dismissLoader();
        //   }
        // },
        //   error => {
        //     console.log("Error", error);
        //     this.showskeleton = false;
        //     // this.loaderService.dismissLoader();
        //   });
    }
    getRouteShell() {
        this.sapApiService.getUserRouteShell().subscribe(result => {
            if (result.length === 0) {
                this.routingService.getUserRoute().subscribe(result => {
                    this.showskeleton = false;
                    for (let res of result) {
                        let sched = new Date(res.deliveryDate);
                        // console.log("sced", sched);
                        // console.log("day", this.dateNow);
                        // console.log(sched > this.dateNow);       
                        if (res.routeStatus.routeStatusId.toString() === "001" && sched > this.dateNow) {
                            this.routes.push(res);
                        }
                    }
                    console.log("test", this.routes);
                    console.log("Success", result);
                }, error => {
                    this.showskeleton = false;
                    console.log("Error", error);
                });
            }
            else {
                console.log("Result", result);
                this.showskeleton = false;
                this.routes = result;
                // this.loaderService.dismissLoader();
            }
        }, error => {
            console.log("Error", error);
            this.showskeleton = false;
            // this.loaderService.dismissLoader();
        });
    }
    getTankQuantity() {
        if (this.currentDelivery.orderSource.toUpperCase() === "CYNCH") {
            this.isCynchOrder = true;
        }
        else {
            this.isCynchOrder = false;
        }
        let quantity = 0;
        for (let item of this.currentDelivery.items) {
            quantity += item.quantity;
        }
        this.tankQuantity = quantity;
        console.log("Quantity", this.tankQuantity);
    }
    openScheduleDetails(route) {
        let navigationExtras = {
            state: {
                route: route
            }
        };
        this.router.navigate(['/tabs/routes'], navigationExtras);
    }
    openRouteDetails(route) {
        localStorage.setItem("current_route", JSON.stringify(route));
        this.router.navigate([`/tabs/routes`]);
    }
    getLocation() {
        return new Promise(function (resolve, reject) {
            this.geolocation.getCurrentPosition().then((resp) => {
                this.latitude = resp.coords.latitude;
                this.longitude = resp.coords.longitude;
                console.log("Current Location", this.latitude);
                console.log("Current Location", this.longitude);
                resolve(true);
            }).catch((error) => {
                console.log('Error getting location', error);
                resolve(false);
            });
        }.bind(this));
    }
    openMapApp(route) {
        this.openRouteDetails(route);
    }
    openDeliveryPage() {
        this.router.navigate([`/tabs/drive`]);
    }
    onContentScroll(ev) {
        this.domCtrl.write(() => {
            // this.updateHeader(ev);
            // console.log(ev.detail.scrollTop);
            if (ev.detail.scrollTop > 50) {
                this.titleSize = undefined;
            }
            else {
                this.titleSize = "large";
            }
        });
    }
    doRefresh(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log('Begin async operation');
            this.ionViewDidEnter();
            event.target.complete();
        });
    }
};
HomePage.ctorParameters = () => [
    { type: _shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_2__["ScheduleService"] },
    { type: _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _shared_services_routing_service__WEBPACK_IMPORTED_MODULE_6__["RoutingService"] },
    { type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_7__["LaunchNavigator"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"] },
    { type: _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_9__["SapApiService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["DomController"] }
];
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["trigger"])('fadein', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["style"])({ opacity: 0 })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["transition"])('void => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["style"])({ opacity: 0 }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["animate"])('1000ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["style"])({ opacity: 1 }))
                ])
            ]),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["trigger"])('fadein-1', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["style"])({ opacity: 0 })),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["transition"])('void => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["style"])({ opacity: 0 }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["animate"])('3000ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["style"])({ opacity: 1 }))
                ])
            ]),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["trigger"])('slidelefttitle', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["transition"])('void => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["style"])({ opacity: 0, transform: 'translateX(150%)' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["animate"])('900ms 300ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["style"])({ transform: 'translateX(0%)', opacity: 1 }))
                ])
            ]),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["trigger"])('sliderighttitle', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["transition"])('void => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["style"])({ opacity: 0, transform: 'translateX(-150%)' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["animate"])('900ms 300ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_10__["style"])({ transform: 'translateX(0%)', opacity: 1 }))
                ])
            ])
        ],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_2__["ScheduleService"],
        _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _shared_services_routing_service__WEBPACK_IMPORTED_MODULE_6__["RoutingService"],
        _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_7__["LaunchNavigator"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"],
        _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_9__["SapApiService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["DomController"]])
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map