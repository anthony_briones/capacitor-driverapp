function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["drive-delivery-detail-delivery-detail-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-detail/delivery-detail.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-detail/delivery-detail.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDriveDeliveryDetailDeliveryDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Delivery</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-row>\n    <ion-col size=\"5\" class=\"card-delivery ion-no-padding\">\n      <div class=\"card-label\">\n        Delivery\n      </div>\n      <div class=\"vertical-align horizontal-align\">\n        <ion-label class=\"card-number-delivery\">\n          {{currentDelivery.stopSequenceNum}}\n        </ion-label>\n        <ion-label class=\"card-total-delivery\">\n          of {{currentDelivery.stopsLength}}\n        </ion-label>\n      </div>\n\n    </ion-col>\n    <ion-col size=\"7\" class=\"ion-no-padding\">\n\n      <div *ngIf=\"isCynchOrder\" class=\"delivery-type vertical-align\">\n        <ion-icon name=\"md-home\"></ion-icon> RESIDENTIAL\n      </div>\n      <div *ngIf=\"!isCynchOrder\" class=\"delivery-type vertical-align\">\n        <ion-icon name=\"md-home\"></ion-icon> COMMERCIAL\n      </div>\n      <div class=\"delivery-name\">\n        {{currentDelivery.customerName}}\n      </div>\n      <div class=\"delivery-address\">\n        {{currentDelivery.street}}, {{currentDelivery.city}}, {{currentDelivery.state}} {{currentDelivery.zipcode}}\n      </div>\n      <!-- <div class=\"delivery-address\">\n            Delivery Id {{deliveries[currentDelivery].deliveryId}}\n          </div>\n          <div class=\"delivery-button ion-padding\">\n            <button class=\"btn\" (click)=\"openDeliveryDetails(1)\">Details & Delivery</button>\n          </div> -->\n    </ion-col>\n\n    <ng-container *ngIf=\"isCynchOrder\">\n      <ion-col size=\"5\" class=\"ion-padding-start tank-label vertical-align\">\n        <ion-icon src=\"assets/icon/propane-tank-graphic.svg\"></ion-icon>\n        {{tankQuantity}}\n      </ion-col>\n    </ng-container>\n    <ng-container *ngIf=\"!isCynchOrder\">\n      <ion-col size=\"5\" class=\"ion-padding-start tank-label vertical-align\">\n        <ion-icon src=\"assets/icon/propane-tank-graphic.svg\"></ion-icon>\n        {{tankQuantity}}\n      </ion-col>\n    </ng-container>\n\n    <ion-col size=\"7\" class=\"ion-padding-start tank-details vertical-align\">\n      <button class=\"btn vertical-align horizontal-align\" (click)=\"openMapApp()\">\n        <ion-icon src=\"assets/icon/icon-navigation.svg\"></ion-icon> NAVIGATE\n      </button>\n\n      <!-- <button class=\"btn\" (click)=\"openMapApp()\">Navigate</button> -->\n    </ion-col>\n    <!-- <ion-col size=\"4\" class=\"card-delivery\">\n\n    <!-- <ion-col size=\"4\" class=\"card-delivery\">\n        <div class=\"card-label\">\n          Delivery\n        </div>\n        <div class=\"card-number-delivery\">\n          1\n        </div>\n        <div class=\"card-total-delivery\">\n          of 5\n        </div>\n      </ion-col>\n      <ion-col size=\"8\">\n        <div class=\"delivery-type\">\n          RESIDENTIAL DELIVERY\n        </div>\n        <div class=\"delivery-name\">\n          James Howlett\n        </div>\n        <div class=\"delivery-address\">\n          1097 Arbor Drive Pheonixville, PA 19460\n        </div>\n        <div class=\"delivery-button\">\n            <button class=\"btn\" (click)=\"openMapApp()\">Navigate</button>\n        </div>\n      </ion-col> -->\n\n    <ion-col size=\"7\" class=\"delivery-label-details ion-padding-top vertical-align\">\n      <div>\n        DELIVERY DETAILS\n      </div>\n    </ion-col>\n    <ion-col *ngIf=\"!isInEdit && !isCynchOrder\" size=\"5\" class=\"vertical-align right ion-padding-top\">\n      <ion-button class=\"edit-btn2\" (click)=\"isInEdit = !isInEdit\">\n        Edit\n      </ion-button>\n    </ion-col>\n    <ion-col *ngIf=\"isInEdit\" size=\"5\" class=\"vertical-align right ion-padding-top\">\n      <ion-button class=\"edit-btn2\" (click)=\"isInEdit = !isInEdit\">\n        Save Changes\n      </ion-button>\n    </ion-col>\n\n  </ion-row>\n\n  <!-- <ng-container *ngIf=\"currentDelivery.orderSource == 'ACE'\"> -->\n  <ion-row *ngFor=\"let item of currentDelivery.items; let i = index\">\n    <ion-col *ngIf=\"item.materialCode == materialCode[0].materialCode\" size=\"12\" class=\"delivery-label\">\n      <div>\n        TOTAL TANKS FOR EXCHANGE\n      </div>\n    </ion-col>\n    <ion-col *ngIf=\"item.materialCode == materialCode[0].materialCode\" size=\"8\" class=\"details-tank vertical-align\">\n      <div *ngIf=\"!isInEdit\">\n        {{item.quantityDelivered}}\n      </div>\n      <div *ngIf=\"isInEdit\" class=\"edit-selected\">\n        <input class=\"\" (click)=\"openModal(item.quantityDelivered, i, false)\" value=\"{{item.quantityDelivered}}\">\n      </div>\n    </ion-col>\n\n    <ion-col *ngIf=\"item.materialCode == materialCode[1].materialCode && spare\" size=\"12\" class=\"delivery-label\">\n      <div>\n        TOTAL TANKS FOR SPARE\n      </div>\n    </ion-col>\n    <ion-col *ngIf=\"item.materialCode == materialCode[1].materialCode && spare\" size=\"8\" class=\"details-tank vertical-align\">\n      <div *ngIf=\"!isInEdit\">\n        {{item.quantityDelivered}}\n      </div>\n      <div *ngIf=\"isInEdit\" class=\"edit-selected\">\n        <input class=\"\" (click)=\"openModal(item.quantityDelivered, i, false)\" value=\"{{item.quantityDelivered}}\">\n      </div>\n    </ion-col>\n\n    <!-- <ion-col *ngIf=\"!isCynchOrder && item.materialCode == 'CYL-204'\" size=\"12\" class=\"delivery-label\">\n      <div>\n        TOTAL TANKS RETURNED\n      </div>\n    </ion-col>\n    <ion-col *ngIf=\"!isCynchOrder && item.materialCode == 'CYL-204'\" size=\"8\" class=\"details-tank vertical-align\">\n      <div *ngIf=\"!isInEdit\">\n        {{item?.quantityReturned}}\n      </div>\n      <div *ngIf=\"isInEdit\" class=\"edit-selected\">\n        <input class=\"\" (click)=\"openModal(item.quantityReturned, i, true)\" value=\"{{item.quantityReturned}}\">\n      </div>\n    </ion-col> -->\n\n    <!-- <ion-col *ngIf=\"isCynchOrder\" size=\"4\">\n      <button class=\"btn edit-btn vertical-align horizontal-align\" (click)=\"openModal(item.quantity, i)\">\n        <ion-icon name=\"md-create\"></ion-icon> Edit Qty\n      </button>\n    </ion-col> -->\n  </ion-row>\n\n  <ion-row *ngIf=\"!spare && isInEdit\"> \n    <ion-col size=\"12\" class=\"delivery-label\">\n      <div>\n        TOTAL TANKS FOR SPARE\n      </div>\n    </ion-col>\n    <ion-col size=\"8\" class=\"details-tank vertical-align\">\n      <div *ngIf=\"isInEdit\" class=\"edit-selected\">\n        <input class=\"\" (click)=\"openModal(0, 3, false)\" value=\"0\">\n      </div>\n    </ion-col>\n  </ion-row>\n\n  <ion-row *ngIf=\"!isCynchOrder && tanksReturned\">\n    <ion-col size=\"12\" class=\"delivery-label\">\n      <div>\n        TOTAL TANKS RETURNED\n      </div>\n    </ion-col>\n    <ion-col size=\"8\" class=\"details-tank vertical-align\">\n      <div *ngIf=\"!isInEdit\">\n        {{tanksReturned}}\n      </div>\n      <div *ngIf=\"isInEdit\" class=\"edit-selected\">\n        <input class=\"\" (click)=\"openModal(tanksReturned, 0, true)\" value=\"{{tanksReturned}}\">\n      </div>\n    </ion-col>\n  </ion-row>\n  <!-- </ng-container> -->\n\n  <ion-row>\n    <ion-col size=\"12\" class=\"delivery-label\">\n      <div>\n        CUSTOMER NOTES\n      </div>\n    </ion-col>\n    <ion-col size=\"12\" class=\"details-other\">\n      <div *ngIf=\"currentDelivery.deliveryNotes\">\n        {{currentDelivery.deliveryNotes}}\n      </div>\n      <div *ngIf=\"!currentDelivery.deliveryNotes\">\n        None\n      </div>\n    </ion-col>\n    <!-- \n    <ion-col size=\"12\" class=\"delivery-label\">\n      <div>\n        LOCATION\n      </div>\n    </ion-col> -->\n    <!-- <ion-col size=\"12\" class=\"details-other\">\n      <div>\n        Next to Garage\n      </div>\n    </ion-col> -->\n\n    <ion-col size=\"12\" class=\"delivery-label\">\n      <div>\n        INTERNAL NOTES\n      </div>\n    </ion-col>\n    <ion-col size=\"12\" class=\"details-other\">\n      <div>\n        None\n      </div>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"ion-no-padding ion-padding-top\">\n      <button class=\"btn confirm-delivery\" (click)=\"confirmDelivery()\">\n        CONFIRM DELIVERY\n      </button>\n    </ion-col>\n\n    <ion-col *ngIf=\"!isCynchOrder || spareOnly\" size=\"12\" class=\"ion-no-padding ion-padding-top\">\n      <button class=\"btn end-delivery disable\" disabled (click)=\"goToCancelDelivery()\">\n        END DELIVERY\n      </button>\n    </ion-col>\n\n    <ion-col *ngIf=\"isCynchOrder && !spareOnly\" size=\"6\" class=\"no-tanks\" (click)=\"noTankLeftOutConfirmation()\">\n      <button class=\"btn no-tanks-btn\">\n        No Tank Left Out\n      </button>\n    </ion-col>\n\n    <ion-col *ngIf=\"isCynchOrder && !spareOnly\" size=\"6\" class=\"no-tanks\">\n      <button class=\"btn end-delivery disable\" disabled (click)=\"goToCancelDelivery()\">\n        END DELIVERY\n      </button>\n    </ion-col>\n\n    <!-- <ion-col size=\"12\" class=\"ion-no-padding ion-padding-top\">\n      <button class=\"btn confirm-delivery\" (click)=\"confirmDelivery()\">\n        CONFIRM DELIVERY\n      </button>\n    </ion-col> -->\n\n\n    <ion-col size=\"6\" *ngIf=\"!currentDelivery.customerPhone.length == 0\" class=\"ion-padding-top\" (click)=\"useCall('customer')\">\n      <button class=\"customer-col vertical-align horizontal-align\">\n        <ion-img src=\"assets/icon/icon-phone.svg\"></ion-img>\n      </button>\n    </ion-col>\n\n    <ion-col size=\"6\" *ngIf=\"currentDelivery.customerPhone.length == 0\" class=\"ion-padding-top disable\">\n      <button class=\"customer-col vertical-align horizontal-align\">\n        <ion-img src=\"assets/icon/icon-phone.svg\"></ion-img>\n      </button>\n    </ion-col>\n\n    <ion-col size=\"6\" class=\"ion-padding-top\" (click)=\"useCall('office')\">\n      <button class=\"office-col vertical-align horizontal-align\">\n        <ion-img src=\"assets/icon/icon-phone.svg\"></ion-img>\n      </button>\n    </ion-col>\n\n    <ion-col size=\"6\" *ngIf=\"!currentDelivery.customerPhone.length == 0\" (click)=\"useCall('customer')\">\n      <ion-label class=\"customer-label vertical-align horizontal-align\">\n        CALL CUSTOMER\n      </ion-label>\n    </ion-col>\n    <ion-col size=\"6\" *ngIf=\"currentDelivery.customerPhone.length == 0\">\n      <ion-label class=\"customer-label vertical-align horizontal-align disable\">\n        CALL CUSTOMER\n      </ion-label>\n    </ion-col>\n\n    <ion-col size=\"6\" (click)=\"useCall('office')\">\n      <ion-label class=\"office-label vertical-align horizontal-align\">\n        CALL OFFICE\n      </ion-label>\n    </ion-col>\n\n  </ion-row>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/drive/delivery-detail/delivery-detail.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/drive/delivery-detail/delivery-detail.module.ts ***!
    \*****************************************************************/

  /*! exports provided: DeliveryDetailPageModule */

  /***/
  function srcAppDriveDeliveryDetailDeliveryDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeliveryDetailPageModule", function () {
      return DeliveryDetailPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _delivery_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./delivery-detail.page */
    "./src/app/drive/delivery-detail/delivery-detail.page.ts");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../shared/shared.module */
    "./src/shared/shared.module.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js"); // import { DeliveryEditPageModule } from '../../drive/delivery-edit/delivery-edit.module';


    var routes = [{
      path: '',
      component: _delivery_detail_page__WEBPACK_IMPORTED_MODULE_6__["DeliveryDetailPage"]
    }];

    var DeliveryDetailPageModule = function DeliveryDetailPageModule() {
      _classCallCheck(this, DeliveryDetailPageModule);
    };

    DeliveryDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], // DeliveryEditPageModule,
      _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      // exports: [DeliveryEditPage],
      declarations: [_delivery_detail_page__WEBPACK_IMPORTED_MODULE_6__["DeliveryDetailPage"]]
    })], DeliveryDetailPageModule);
    /***/
  },

  /***/
  "./src/app/drive/delivery-detail/delivery-detail.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/drive/delivery-detail/delivery-detail.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppDriveDeliveryDetailDeliveryDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-title {\n  color: #003A58;\n  font-size: 1rem;\n  font-family: \"Montserrat-Bold\";\n  letter-spacing: 0;\n}\n\n.edit-selected {\n  border-radius: 0.375rem;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\n.no-padding {\n  padding: 0 !important;\n}\n\n.card-delivery {\n  border: 1px solid gray;\n  text-align: center;\n  padding: 0.75rem;\n}\n\n.card-label {\n  text-transform: uppercase;\n  color: #003A58;\n  font-size: 0.875rem;\n  font-family: \"Montserrat-Bold\";\n}\n\n.card-number-delivery {\n  color: #003A58;\n  font-size: 3.5rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.card-total-delivery {\n  color: #003A58;\n  font-size: 1.75rem;\n  font-family: \"Montserrat-Bold\";\n  margin-left: 0.625rem;\n}\n\nion-icon {\n  padding-right: 5px;\n}\n\n.delivery-type {\n  font-family: \"Montserrat-SemiBold\";\n  font-size: 0.875rem;\n  color: #003A58;\n  letter-spacing: 0;\n  padding: 0 1rem;\n}\n\n.delivery-name {\n  font-family: \"Montserrat-Bold\";\n  font-size: 1.125rem;\n  color: #003A58;\n  letter-spacing: 0;\n  line-height: 1.6875rem;\n  padding: 0.875rem 0 0 1rem;\n}\n\n.delivery-address {\n  font-family: \"Roboto-Regular\";\n  font-size: 1.125rem;\n  color: #424242;\n  letter-spacing: 0;\n  line-height: 1.5rem;\n  padding-left: 1rem;\n}\n\nbutton {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n  background: #003A58;\n}\n\n.delivery-button {\n  padding: 0 1rem;\n}\n\n.edit-btn2 {\n  font-size: 0.875rem;\n  color: white;\n  background-color: #003A58;\n  --background: $primary-color-1;\n  border-radius: 0.375rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.delivery-label {\n  color: #424242;\n  font-family: \"Montserrat-SemiBold\";\n  font-size: 0.875rem;\n  margin-top: 1rem;\n  padding: 0;\n}\n\n.delivery-label-details {\n  color: #003A58;\n  font-family: \"Montserrat-Bold\";\n  font-size: 1rem;\n  margin-top: 1rem;\n  padding: 0;\n}\n\n.tank-label {\n  color: #003A58;\n  font-size: 1rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.details-tank {\n  font-size: 1.125rem;\n  font-family: \"Roboto-Regular\";\n  color: #424242;\n  padding: 0;\n}\n\n.details-other {\n  font-size: 1rem;\n  font-family: \"Roboto-Regular\";\n  color: #424242;\n  padding: 0;\n}\n\n.confirm-delivery {\n  background: #F4773B 100% !important;\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036)) !important;\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%) !important;\n  margin-top: 1rem;\n  font-size: 1rem;\n}\n\n.no-tanks {\n  padding: 1rem 0.625rem 0 0;\n}\n\n.no-tanks-btn {\n  color: #003A58;\n  margin-top: 1rem;\n  background-color: #E8E8E8;\n  font-size: 0.875rem;\n  font-family: \"Montserrat-Bold\";\n}\n\n.end-delivery {\n  color: #003A58;\n  margin-top: 1rem;\n  background-color: #E8E8E8;\n  font-size: 0.875rem;\n  font-family: \"Montserrat-Bold\";\n}\n\n.edit-btn {\n  color: #003A58;\n  background-color: white;\n  font-size: 0.875rem;\n  font-family: \"Montserrat-Regular\";\n  border: 1px solid #003A58;\n}\n\n.customer-col {\n  margin: 2px auto;\n  height: 50px;\n  width: 50px;\n  background-color: #F4773B;\n  border-radius: 30px;\n}\n\n.office-col {\n  margin: 2px auto;\n  height: 50px;\n  width: 50px;\n  background-color: #003A58;\n  border-radius: 30px;\n}\n\n.customer-label {\n  color: #F4773B;\n  font-size: 1rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.office-label {\n  color: #003A58;\n  font-size: 1rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.right {\n  -webkit-box-pack: end !important;\n          justify-content: flex-end !important;\n}\n\n.no-border {\n  border: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL2RyaXZlL2RlbGl2ZXJ5LWRldGFpbC9kZWxpdmVyeS1kZXRhaWwucGFnZS5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXBwL2RyaXZlL2RlbGl2ZXJ5LWRldGFpbC9kZWxpdmVyeS1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU9BO0VBQ0kseUJBQUE7RUFDQSxnREFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkVsQmM7RUZtQmQsV0FBQTtFQUNBLGNFaEJJO0VGaUJKLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSwrQkFBQTtFQUFBLHdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxvQ0FBQTtVQUFBLDhCQUFBO0FDTko7O0FEU0E7RUFDSSxtQ0FBQTtVQUFBLGtDQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtVQUFBLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx3QkFBQTtVQUFBLHVCQUFBO0FDTko7O0FEU0E7RUFDSSxZQUFBO0FDTko7O0FFaERBO0VBQ0ksY0REYztFQ0VkLGVBQUE7RUFDQSw4QkFBQTtFQUNBLGlCQUFBO0FGbURKOztBRWhEQTtFQUNJLHVCQUFBO0FGbURKOztBRTVDQTtFQUNJLFlBQUE7QUYrQ0o7O0FFNUNBO0VBQ0kscUJBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksc0JBQUE7RUFFQSxrQkFBQTtFQUNBLGdCQUFBO0FGOENKOztBRTNDQTtFQUNJLHlCQUFBO0VBQ0EsY0RoQ2M7RUNpQ2QsbUJBQUE7RUFDQSw4QkFBQTtBRjhDSjs7QUUzQ0E7RUFDSSxjRHRDYztFQ3VDZCxpQkFBQTtFQUNBLGtDQUFBO0FGOENKOztBRTFDQTtFQUNJLGNEN0NjO0VDOENkLGtCQUFBO0VBQ0EsOEJBQUE7RUFDQSxxQkFBQTtBRjZDSjs7QUV6Q0E7RUFDSSxrQkFBQTtBRjRDSjs7QUV6Q0E7RUFDSSxrQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsY0QzRGM7RUM0RGQsaUJBQUE7RUFDQSxlQUFBO0FGNENKOztBRXpDQTtFQUNJLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjRG5FYztFQ29FZCxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsMEJBQUE7QUY0Q0o7O0FFekNBO0VBQ0ksNkJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUY0Q0o7O0FFekNBO0VBRUksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUVBLDhCQUFBO0VBQ0EsbUJEM0ZjO0FEcUlsQjs7QUV0Q0E7RUFDSSxlQUFBO0FGeUNKOztBRXRDQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLHlCRHRHYztFQ3VHZCw4QkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0NBQUE7QUZ5Q0o7O0FFdENBO0VBQ0ksY0FBQTtFQUNBLGtDQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7QUZ5Q0o7O0FFdENBO0VBQ0ksY0RySGM7RUNzSGQsOEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0FGeUNKOztBRXJDQTtFQUNJLGNEOUhjO0VDK0hkLGVBQUE7RUFDQSxrQ0FBQTtBRndDSjs7QUVyQ0E7RUFDSSxtQkFBQTtFQUNBLDZCQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7QUZ3Q0o7O0FFckNBO0VBQ0ksZUFBQTtFQUNBLDZCQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7QUZ3Q0o7O0FFckNBO0VBQ0ksbUNBQUE7RUFDQSx3R0FBQTtFQUFBLCtFQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FGd0NKOztBRXJDQTtFQUNJLDBCQUFBO0FGd0NKOztBRXJDQTtFQUNJLGNEN0pjO0VDOEpkLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FGd0NKOztBRXBDQTtFQUNJLGNEdEtjO0VDdUtkLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FGdUNKOztBRXBDQTtFQUNJLGNEOUtjO0VDK0tkLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQ0FBQTtFQUNBLHlCQUFBO0FGdUNKOztBRW5DQTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSx5QkR4TGM7RUN5TGQsbUJBQUE7QUZzQ0o7O0FFbkNBO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHlCRGxNYztFQ21NZCxtQkFBQTtBRnNDSjs7QUVuQ0E7RUFDSSxjRHJNYztFQ3NNZCxlQUFBO0VBQ0Esa0NBQUE7QUZzQ0o7O0FFbkNBO0VBQ0ksY0Q3TWM7RUM4TWQsZUFBQTtFQUNBLGtDQUFBO0FGc0NKOztBRW5DQTtFQUNJLGdDQUFBO1VBQUEsb0NBQUE7QUZzQ0o7O0FFbkNDO0VBQ0ksWUFBQTtBRnNDTCIsImZpbGUiOiJzcmMvYXBwL2RyaXZlL2RlbGl2ZXJ5LWRldGFpbC9kZWxpdmVyeS1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2ZvbnRzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMnO1xuXG4vLyAuaXMtdmFsaWQge1xuLy8gICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWVcbi8vIH1cblxuLmlzLWludmFsaWQge1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNFRTQwMzY7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDNcbn1cblxuLmlzLWludmFsaWQtc2VsZWN0IHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6ICR3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgaGVpZ2h0OiAycmVtO1xufVxuXG4uYmctY29sb3ItMXtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgICBkaXNwbGF5OiBmbGV4IWltcG9ydGFudDtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXIhaW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5mbGV4LWFsaWduLWNlbnRlciB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgICBvcGFjaXR5OiAwLjU7XG59XG5cblxuXG5cbiIsIi5pcy1pbnZhbGlkIHtcbiAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICBib3JkZXI6IDFweCBzb2xpZCAjNUVCMDAzO1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWFsaWduLXJpZ2h0IHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgb3BhY2l0eTogMC41O1xufVxuXG5pb24tdGl0bGUge1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG59XG5cbi5lZGl0LXNlbGVjdGVkIHtcbiAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG59XG5cbi5kaXNhYmxlIHtcbiAgb3BhY2l0eTogMC41O1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbn1cblxuLmNhcmQtZGVsaXZlcnkge1xuICBib3JkZXI6IDFweCBzb2xpZCBncmF5O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDAuNzVyZW07XG59XG5cbi5jYXJkLWxhYmVsIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgY29sb3I6ICMwMDNBNTg7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xufVxuXG4uY2FyZC1udW1iZXItZGVsaXZlcnkge1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1zaXplOiAzLjVyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbn1cblxuLmNhcmQtdG90YWwtZGVsaXZlcnkge1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1zaXplOiAxLjc1cmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgbWFyZ2luLWxlZnQ6IDAuNjI1cmVtO1xufVxuXG5pb24taWNvbiB7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbn1cblxuLmRlbGl2ZXJ5LXR5cGUge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LVNlbWlCb2xkXCI7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgcGFkZGluZzogMCAxcmVtO1xufVxuXG4uZGVsaXZlcnktbmFtZSB7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBmb250LXNpemU6IDEuMTI1cmVtO1xuICBjb2xvcjogIzAwM0E1ODtcbiAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjY4NzVyZW07XG4gIHBhZGRpbmc6IDAuODc1cmVtIDAgMCAxcmVtO1xufVxuXG4uZGVsaXZlcnktYWRkcmVzcyB7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1SZWd1bGFyXCI7XG4gIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gIGNvbG9yOiAjNDI0MjQyO1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgbGluZS1oZWlnaHQ6IDEuNXJlbTtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xufVxuXG5idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gIGhlaWdodDogM3JlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuLmRlbGl2ZXJ5LWJ1dHRvbiB7XG4gIHBhZGRpbmc6IDAgMXJlbTtcbn1cblxuLmVkaXQtYnRuMiB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbiAgLS1iYWNrZ3JvdW5kOiAkcHJpbWFyeS1jb2xvci0xO1xuICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xufVxuXG4uZGVsaXZlcnktbGFiZWwge1xuICBjb2xvcjogIzQyNDI0MjtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICBwYWRkaW5nOiAwO1xufVxuXG4uZGVsaXZlcnktbGFiZWwtZGV0YWlscyB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICBwYWRkaW5nOiAwO1xufVxuXG4udGFuay1sYWJlbCB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LXNpemU6IDFyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbn1cblxuLmRldGFpbHMtdGFuayB7XG4gIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1SZWd1bGFyXCI7XG4gIGNvbG9yOiAjNDI0MjQyO1xuICBwYWRkaW5nOiAwO1xufVxuXG4uZGV0YWlscy1vdGhlciB7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLVJlZ3VsYXJcIjtcbiAgY29sb3I6ICM0MjQyNDI7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5jb25maXJtLWRlbGl2ZXJ5IHtcbiAgYmFja2dyb3VuZDogI0Y0NzczQiAxMDAlICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIGZvbnQtc2l6ZTogMXJlbTtcbn1cblxuLm5vLXRhbmtzIHtcbiAgcGFkZGluZzogMXJlbSAwLjYyNXJlbSAwIDA7XG59XG5cbi5uby10YW5rcy1idG4ge1xuICBjb2xvcjogIzAwM0E1ODtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0U4RThFODtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG59XG5cbi5lbmQtZGVsaXZlcnkge1xuICBjb2xvcjogIzAwM0E1ODtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0U4RThFODtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG59XG5cbi5lZGl0LWJ0biB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1SZWd1bGFyXCI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDNBNTg7XG59XG5cbi5jdXN0b21lci1jb2wge1xuICBtYXJnaW46IDJweCBhdXRvO1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjQ3NzNCO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuXG4ub2ZmaWNlLWNvbCB7XG4gIG1hcmdpbjogMnB4IGF1dG87XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG59XG5cbi5jdXN0b21lci1sYWJlbCB7XG4gIGNvbG9yOiAjRjQ3NzNCO1xuICBmb250LXNpemU6IDFyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbn1cblxuLm9mZmljZS1sYWJlbCB7XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBmb250LXNpemU6IDFyZW07XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbn1cblxuLnJpZ2h0IHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZCAhaW1wb3J0YW50O1xufVxuXG4ubm8tYm9yZGVyIHtcbiAgYm9yZGVyOiBub25lO1xufSIsIlxuLy8gY29sb3JzXG4vLyAkcHJpbWFyeS1jb2xvci0xOiAjMDBBM0M4O1xuJHByaW1hcnktY29sb3ItMTogIzAwM0E1ODtcbiRwcmltYXJ5LWNvbG9yLTI6ICNGQUFGNDA7XG4kcHJpbWFyeS1jb2xvci0zOiAjRjQ3NzNCO1xuXG4kd2hpdGU6ICNmZmZmZmY7XG5cbi8vICRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgcmdiYSgyNTUsMjU1LDI1NSwwLjUwKSAwJSwgcmdiYSgwLDAsMCwwLjUwKSAxMDAlKTtcbiRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KC0xODBkZWcsICNGRjg4NDAgMCUsICNFRTQwMzYgMTAwJSk7IiwiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2NvbW1vbi5zY3NzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XG5cbmlvbi10aXRsZXtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xufVxuXG4uZWRpdC1zZWxlY3RlZHtcbiAgICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbn1cblxuaW9uLXJvdyB7XG4gICAgLy8gcGFkZGluZzogMC42MjVyZW0gMXJlbTtcbn1cblxuLmRpc2FibGUge1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuLm5vLXBhZGRpbmcge1xuICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbn1cblxuLmNhcmQtZGVsaXZlcnkge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyYXk7XG4gICAgLy8gbWFyZ2luOiAxcmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAwLjc1cmVtO1xufVxuXG4uY2FyZC1sYWJlbCB7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbn1cblxuLmNhcmQtbnVtYmVyLWRlbGl2ZXJ5IHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LXNpemU6IDMuNXJlbTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xuXG59XG5cbi5jYXJkLXRvdGFsLWRlbGl2ZXJ5IHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LXNpemU6IDEuNzVyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIG1hcmdpbi1sZWZ0OiAwLjYyNXJlbTtcbn1cblxuXG5pb24taWNvbiB7XG4gICAgcGFkZGluZy1yaWdodDogNXB4O1xufVxuXG4uZGVsaXZlcnktdHlwZXtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgcGFkZGluZzogMCAxcmVtO1xufVxuXG4uZGVsaXZlcnktbmFtZSB7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuNjg3NXJlbTtcbiAgICBwYWRkaW5nOiAwLjg3NXJlbSAwIDAgMXJlbTtcbn1cblxuLmRlbGl2ZXJ5LWFkZHJlc3Mge1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLVJlZ3VsYXInO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgY29sb3I6ICM0MjQyNDI7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuNXJlbTtcbiAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XG59XG5cbmJ1dHRvbntcbiAgXG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgICBoZWlnaHQ6IDNyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIC8vIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIGJhY2tncm91bmQ6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLy8gYmFja2dyb3VuZC1pbWFnZTogJGJ1dHRvbi1ncmFkaWVudC0xO1xufVxuXG4uZGVsaXZlcnktYnV0dG9uIHtcbiAgICBwYWRkaW5nOiAwIDFyZW07XG59XG5cbi5lZGl0LWJ0bjIge1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLS1iYWNrZ3JvdW5kOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG59XG5cbi5kZWxpdmVyeS1sYWJlbCB7XG4gICAgY29sb3I6ICM0MjQyNDI7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIG1hcmdpbi10b3A6IDFyZW07XG4gICAgcGFkZGluZzogMDtcbn1cblxuLmRlbGl2ZXJ5LWxhYmVsLWRldGFpbHMge1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICBwYWRkaW5nOiAwO1xufVxuXG5cbi50YW5rLWxhYmVsIHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbn1cblxuLmRldGFpbHMtdGFuayB7XG4gICAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1SZWd1bGFyJztcbiAgICBjb2xvcjogIzQyNDI0MjtcbiAgICBwYWRkaW5nOiAwO1xufVxuXG4uZGV0YWlscy1vdGhlciB7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLVJlZ3VsYXInO1xuICAgIGNvbG9yOiAjNDI0MjQyO1xuICAgIHBhZGRpbmc6IDA7XG59XG5cbi5jb25maXJtLWRlbGl2ZXJ5IHtcbiAgICBiYWNrZ3JvdW5kOiAjRjQ3NzNCIDEwMCUgIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiAkYnV0dG9uLWdyYWRpZW50LTEgIWltcG9ydGFudDtcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbn1cblxuLm5vLXRhbmtzIHtcbiAgICBwYWRkaW5nOiAxcmVtIDAuNjI1cmVtIDAgMDtcbn1cblxuLm5vLXRhbmtzLWJ0biB7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRThFOEU4O1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xuICAgIC8vIG1hcmdpbjogMXJlbSAwLjYyNXJlbSAwIDA7XG59XG5cbi5lbmQtZGVsaXZlcnl7XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRThFOEU4O1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xufVxuXG4uZWRpdC1idG4ge1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVJlZ3VsYXInO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICRwcmltYXJ5LWNvbG9yLTE7XG59XG5cblxuLmN1c3RvbWVyLWNvbCB7XG4gICAgbWFyZ2luOiAycHggYXV0bztcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgd2lkdGg6IDUwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMztcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuXG4ub2ZmaWNlLWNvbCB7XG4gICAgbWFyZ2luOiAycHggYXV0bztcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgd2lkdGg6IDUwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuXG4uY3VzdG9tZXItbGFiZWwge1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0zO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xufVxuXG4ub2ZmaWNlLWxhYmVsIHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbn1cblxuLnJpZ2h0IHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kICFpbXBvcnRhbnQ7XG4gfVxuXG4gLm5vLWJvcmRlciB7XG4gICAgIGJvcmRlcjogbm9uZTtcbiB9XG5cbiAiXX0= */";
    /***/
  },

  /***/
  "./src/app/drive/delivery-detail/delivery-detail.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/drive/delivery-detail/delivery-detail.page.ts ***!
    \***************************************************************/

  /*! exports provided: DeliveryDetailPage */

  /***/
  function srcAppDriveDeliveryDetailDeliveryDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeliveryDetailPage", function () {
      return DeliveryDetailPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _node_modules_ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../node_modules/@ionic-native/launch-navigator/ngx */
    "./node_modules/@ionic-native/launch-navigator/ngx/index.js");
    /* harmony import */


    var _node_modules_ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../node_modules/@ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../shared/services/loader.service */
    "./src/shared/services/loader.service.ts");
    /* harmony import */


    var _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../../node_modules/@angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _delivery_edit_delivery_edit_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../delivery-edit/delivery-edit.page */
    "./src/app/drive/delivery-edit/delivery-edit.page.ts");
    /* harmony import */


    var src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/shared/services/toast.service */
    "./src/shared/services/toast.service.ts");
    /* harmony import */


    var src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/shared/services/alert.service */
    "./src/shared/services/alert.service.ts");
    /* harmony import */


    var src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! src/shared/services/sap-api.service */
    "./src/shared/services/sap-api.service.ts");
    /* harmony import */


    var _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../../shared/services/lauch-navigator.service */
    "./src/shared/services/lauch-navigator.service.ts"); // import { CallNumber } from '@ionic-native/call-number/ngx';
    // import { __await } from 'tslib';


    var DeliveryDetailPage =
    /*#__PURE__*/
    function () {
      function DeliveryDetailPage(geolocation, launchNavigator, loaderService, router, modalController, // private callNumber: CallNumber,
      alertController, toastService, alertService, sapApiService, launchNavService) {
        _classCallCheck(this, DeliveryDetailPage);

        this.geolocation = geolocation;
        this.launchNavigator = launchNavigator;
        this.loaderService = loaderService;
        this.router = router;
        this.modalController = modalController;
        this.alertController = alertController;
        this.toastService = toastService;
        this.alertService = alertService;
        this.sapApiService = sapApiService;
        this.launchNavService = launchNavService;
        this.sampleLat = 18.649664500000002;
        this.sampleLong = 129.06789579999997;
      }

      _createClass(DeliveryDetailPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.getMaterialcode();
          console.log("1****");

          if (localStorage.getItem("current_delivery")) {
            this.currentDelivery = JSON.parse(localStorage.getItem("current_delivery"));
            this.stopsLength = parseInt(localStorage.getItem("stops_length"));
            this.stopSequenceNum = parseInt(localStorage.getItem("stop_sequence"));
            this.getTankQuantity();

            if (!this.isCynchOrder && this.currentDelivery.items.filter(function (x) {
              return x.materialCode == _this.materialCode[0].materialCode;
            })[0]) {
              this.tanksReturned = this.currentDelivery.items.filter(function (x) {
                return x.materialCode == _this.materialCode[0].materialCode;
              })[0].quantityReturned;
              console.log(this.tanksReturned);
            }
          }

          this.isInEdit = false;
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          console.log("2***");
          this.getTankType(); // this.getMaterialcode();
        }
      }, {
        key: "getLocation",
        value: function getLocation() {
          return new Promise(function (resolve, reject) {
            var _this2 = this;

            this.geolocation.getCurrentPosition().then(function (resp) {
              _this2.latitude = resp.coords.latitude;
              _this2.longitude = resp.coords.longitude;
              console.log("Current Location", _this2.latitude);
              console.log("Current Location", _this2.longitude);
              resolve(true);
            }).catch(function (error) {
              console.log('Error getting location', error);
              resolve(false);
            });
          }.bind(this));
        }
      }, {
        key: "openMapApp",
        value: function openMapApp() {
          var _this3 = this;

          this.getLocation().then(function (result) {
            if (result) {
              _this3.launchNavService.checkExistingSelectedMap().then(function (result) {
                if (result) {
                  _this3.launchNavService.getUserSelectedMap().then(function (preferredMap) {
                    console.log("Selected", preferredMap);

                    if (preferredMap === "this.launchNavigator.APP.APPLE_MAPS") {
                      var options = {
                        start: "".concat(_this3.latitude, ", ").concat(_this3.longitude),
                        app: _this3.launchNavigator.APP.APPLE_MAPS
                      };

                      _this3.launchNavigatorMapApp(options);
                    } else if (preferredMap === "this.launchNavigator.APP.WAZE") {
                      var _options = {
                        start: "".concat(_this3.latitude, ", ").concat(_this3.longitude),
                        app: _this3.launchNavigator.APP.WAZE
                      };

                      _this3.launchNavigatorMapApp(_options);
                    } else if (preferredMap === "this.launchNavigator.APP.GOOGLE_MAPS") {
                      var _options2 = {
                        start: "".concat(_this3.latitude, ", ").concat(_this3.longitude),
                        app: _this3.launchNavigator.APP.GOOGLE_MAPS
                      };

                      _this3.launchNavigatorMapApp(_options2);
                    } else {
                      var _options3 = {
                        start: "".concat(_this3.latitude, ", ").concat(_this3.longitude)
                      };

                      _this3.launchNavigatorMapApp(_options3);
                    }
                  });
                } else {
                  var options = {
                    start: "".concat(_this3.latitude, ", ").concat(_this3.longitude),
                    appSelection: {
                      rememberChoice: {
                        enabled: true
                      }
                    }
                  };

                  _this3.launchNavigatorMapApp(options);

                  console.log("None selected");
                }
              });
            }
          });
        }
      }, {
        key: "launchNavigatorMapApp",
        value: function launchNavigatorMapApp(options) {
          this.launchNavigator.navigate("".concat(this.currentDelivery.street, " ").concat(this.currentDelivery.city, ", ").concat(this.currentDelivery.state), options).then(function (success) {
            console.log("Success Return");
          }, function (error) {
            console.log("Error", error);
          });
        }
      }, {
        key: "getTankQuantity",
        value: function getTankQuantity() {
          if (this.currentDelivery.orderSource.toUpperCase() === "CYNCH") {
            this.isCynchOrder = true;
          } else {
            this.isCynchOrder = false;
          }

          var quantity = 0;
          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = this.currentDelivery.items[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var item = _step.value;
              quantity += item.quantity;
            }
          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }

          if (quantity > 1) {
            this.tankQuantity = quantity + " tanks";
          } else {
            this.tankQuantity = quantity + " tank";
          }

          console.log("Quantity", this.tankQuantity);
        }
      }, {
        key: "confirmDelivery",
        value: function confirmDelivery() {
          this.router.navigate(["/tabs/drive/".concat(this.currentDelivery.stopId, "/confirmation")]);
        }
      }, {
        key: "goToDeliveryEdit",
        value: function goToDeliveryEdit() {
          this.router.navigate(["/tabs/drive/".concat(this.currentDelivery.stopId, "/delivery-edit")]);
        }
      }, {
        key: "openModal",
        value: function openModal(qty, i, isReturn) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            var _this4 = this;

            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (isReturn) {
                      console.log("Index", this.currentDelivery.items.findIndex(function (x) {
                        return x.materialCode == _this4.materialCode[0].materialCode;
                      }));
                      i = this.currentDelivery.items.findIndex(function (x) {
                        return x.materialCode == _this4.materialCode[0].materialCode;
                      });
                    }

                    _context.next = 3;
                    return this.modalController.create({
                      component: _delivery_edit_delivery_edit_page__WEBPACK_IMPORTED_MODULE_7__["DeliveryEditPage"],
                      componentProps: {
                        'quantity': qty,
                        'index': i,
                        'isReturn': isReturn
                      },
                      cssClass: 'custom-modal'
                    });

                  case 3:
                    modal = _context.sent;
                    modal.onDidDismiss().then(function (detail) {
                      if (detail !== null) {
                        console.log('the Result:', detail.data);
                      }

                      console.log('exit');

                      if (localStorage.getItem("current_delivery")) {
                        _this4.currentDelivery = JSON.parse(localStorage.getItem("current_delivery"));

                        if (!_this4.isCynchOrder) {
                          _this4.tanksReturned = _this4.currentDelivery.items.filter(function (x) {
                            return x.materialCode == _this4.materialCode[0].materialCode;
                          })[0].quantityReturned;
                          console.log("Tanks Returned updated value", _this4.tanksReturned);
                        }

                        _this4.getTankQuantity();

                        _this4.getTankType();
                      }
                    });
                    _context.next = 7;
                    return modal.present();

                  case 7:
                    return _context.abrupt("return", _context.sent);

                  case 8:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        } // useCall(source: string) {
        //   switch (source) {
        //     case "customer":
        //       this.callNumber.callNumber(this.currentDelivery.customerPhone, true)
        //         .then(res => console.log('Launched dialer!', res))
        //         .catch(err => console.log('Error launching dialer', err));
        //       break;
        //     case "office":
        //       this.callNumber.callNumber("18885252899", true)
        //         .then(res => console.log('Launched dialer!', res))
        //         .catch(err => console.log('Error launching dialer', err));
        //       break;
        //     default:
        //       this.callNumber.callNumber("18885252899", true)
        //         .then(res => console.log('Launched dialer!', res))
        //         .catch(err => console.log('Error launching dialer', err));
        //       break;
        //   }
        // }

      }, {
        key: "noTankLeftOutConfirmation",
        value: function noTankLeftOutConfirmation() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var _this5 = this;

            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    this.alertService.createNTLOAlerts("Once you start the NTLO process you cannot go back", "Proceed", "Cancel", "Are you sure?").then(function (resp) {
                      if (resp) {
                        var message = "<p>Check to make sure you are at the correct location.</p>" + "<p>Are you at</p>" + "<p><h3>" + _this5.currentDelivery.street + ",</p><p>" + _this5.currentDelivery.state + " " + _this5.currentDelivery.zipcode + "?</h3></p>";
                        console.log(message);

                        _this5.alertService.createNTLOAlerts(message, "Yes, continue", "Wrong address", "NTLO").then(function (result) {
                          if (result) {
                            message = "Knock on the front door. If someone answers, ask if they\n                have an empty tank to bring out for you";

                            _this5.alertService.createNTLOAlerts(message, "No tank", "Customer is home", "NTLO").then(function (response) {
                              if (response) {
                                _this5.createNTLOCallAlert("Try calling the customer", "No Tank", "Tank found", "NTLO").then(function (result) {
                                  if (result) {
                                    message = "Look on the back porch and the sides of the house.";

                                    _this5.alertService.createNTLOAlerts(message, "No tank", "Tank Found", "NTLO").then(function (response) {
                                      if (response) {
                                        message = "Check that tank on the grill. If the tank is completely empty, exchange that tank\n                                      and tap Exchanged Tank From Grill. If it is not completely empty, tap NTLO and continue to the next delivery.";

                                        _this5.alertService.createNTLOAlerts(message, "NTLO", "Exchanged Tank From Grill", "NTLO").then(function (result) {
                                          if (result) {
                                            _this5.nextDelivery();
                                          }
                                        });
                                      }
                                    });
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    });

                  case 1:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "createNTLOCallAlert",
        value: function createNTLOCallAlert(message, confirmText, cancelText, title) {
          return new Promise(function (resolve, reject) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
            /*#__PURE__*/
            regeneratorRuntime.mark(function _callee3() {
              var _this6 = this;

              var choiceAlert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertController.create({
                        // message: "Once you start the NTLO process you cannot go back",
                        header: title,
                        message: message,
                        mode: "ios",
                        cssClass: "custom-alert",
                        buttons: [{
                          text: "Call: " + this.currentDelivery.customerPhone,
                          handler: function handler() {
                            _this6.useCall("customer");
                          }
                        }, {
                          text: confirmText,
                          handler: function handler() {
                            resolve(true);
                          }
                        }, {
                          text: cancelText,
                          cssClass: "alert-cancel",
                          handler: function handler() {
                            resolve(false);
                          }
                        }]
                      });

                    case 2:
                      choiceAlert = _context3.sent;
                      _context3.next = 5;
                      return choiceAlert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }.bind(this));
        }
      }, {
        key: "nextDelivery",
        value: function nextDelivery() {
          this.stopSequenceNum = this.stopSequenceNum + 1;

          if (this.stopsLength < this.stopSequenceNum) {
            this.updateDeliveryNTLO(); // this.clearFinishedDelivery();
          } else {
            this.updateDeliveryNTLO();
          }
        }
      }, {
        key: "updateDeliveryNTLO",
        value: function updateDeliveryNTLO() {
          var _this7 = this;

          this.currentRoute = JSON.parse(localStorage.getItem("current_route_started"));
          this.loaderService.createLoader("Submitting..."); // this.items = JSON.parse(localStorage.getItem("material_code"));

          var editedDelivery = JSON.parse(JSON.stringify(this.currentDelivery)); // let transferedItem: any
          // for (let item of editedDelivery.items) {
          //   if (item.materialCode === "CYL-204") {
          //     transferedItem = JSON.parse(JSON.stringify(item));
          //     transferedItem.transferType = "2";
          //   }
          // }
          // editedDelivery.items.push(transferedItem);

          console.log("Current Delivery", editedDelivery);
          editedDelivery.startTime = localStorage.getItem("start_time");
          editedDelivery.endTime = this.sapApiService.getConfirmationDateTime();
          editedDelivery.deliveryStatus.deliveryStatusId = "006";
          var tankDelivered = 0;
          var tankRemaining; // for (let item of editedDelivery.items) {
          //   tankDelivered += item.quantityDelivered;
          // }

          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = editedDelivery.items[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var item = _step2.value;
              item.quantityDelivered = 0;
              item.quantityReturned = 0;
            }
          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                _iterator2.return();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }

          var _iteratorNormalCompletion3 = true;
          var _didIteratorError3 = false;
          var _iteratorError3 = undefined;

          try {
            for (var _iterator3 = editedDelivery.services[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
              var service = _step3.value;
              service.quantityDelivered = 0;
              service.quantityReturned = 0;
            } // tankRemaining = this.currentRoute.totalRemainingCyls;
            // editedDelivery.totalRemainingCyls = tankRemaining;

          } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                _iterator3.return();
              }
            } finally {
              if (_didIteratorError3) {
                throw _iteratorError3;
              }
            }
          }

          if (localStorage.getItem("mileage")) {
            editedDelivery.mileage = parseFloat(localStorage.getItem("mileage"));
          } else {
            //Edited only due to testing please return to 0 when making a build
            editedDelivery.mileage = 27; // editedDelivery.mileage = 0;
          }

          editedDelivery.nonDeliveryReason = "130";
          this.sapApiService.updateDelivery(editedDelivery, this.currentRoute).subscribe(function (result) {
            console.log("Update Delivery", result); // this.currentRoute.totalRemainingCyls = tankRemaining;

            localStorage.setItem("current_route_started", JSON.stringify(_this7.currentRoute));
            localStorage.setItem("stop_sequence", _this7.stopSequenceNum.toString());
            _this7.stopsLength = result.stopsLength;
            localStorage.setItem("stops_length", _this7.stopsLength.toString()); // if (this.stopsLength < this.stopSequenceNum) {
            //   this.sapApiService.submitRoute(this.currentRoute.routeId, editedDelivery.endTime).subscribe(
            //     result => {
            //       console.log(result);
            //       this.clearFinishedDelivery();
            //     },
            //     error => {
            //       console.log("Error", error);
            //       this.loaderService.dismissLoader();
            //     }
            //   )
            // }

            var location = {
              latitude: 0,
              longitude: 0
            };
            location.latitude = editedDelivery.latitude;
            location.longitude = editedDelivery.longitude;
            localStorage.setItem("previous_loc", JSON.stringify(location));

            if (_this7.stopsLength < _this7.stopSequenceNum) {
              // let routeId = localStorage.getItem("route_id");
              // localStorage.removeItem("previous_loc");
              _this7.router.navigate(["/tabs/drive/".concat(_this7.currentRoute.routeId, "/delivery-summary")]);
            } else {
              // let location = {
              //   latitude: 0,
              //   longitude: 0
              // }
              // location.latitude = editedDelivery.latitude;
              // location.longitude = editedDelivery.longitude;
              // localStorage.setItem("previous_loc", JSON.stringify(location));
              localStorage.removeItem("start_time");

              _this7.router.navigate(["/tabs/drive"]);
            } // this.router.navigate([`/tabs/drive`]);


            _this7.loaderService.dismissLoader();
          }, function (error) {
            console.log("Error", error);

            _this7.loaderService.dismissLoader();
          });
        }
      }, {
        key: "goToCancelDelivery",
        value: function goToCancelDelivery() {
          this.currentRoute = JSON.parse(localStorage.getItem("current_route_started"));
          this.router.navigate(["/tabs/drive/".concat(this.currentRoute.routeId, "/delivery-cancel")]);
        }
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee4() {
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ngOnInit();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "getTankType",
        value: function getTankType() {
          console.log(this.currentDelivery);
          this.spareOnly = false;
          this.spare = false;
          var exchange = false;
          var spare = false;
          var _iteratorNormalCompletion4 = true;
          var _didIteratorError4 = false;
          var _iteratorError4 = undefined;

          try {
            for (var _iterator4 = this.currentDelivery.items[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
              var itemTank = _step4.value;

              if (itemTank.materialCode === this.materialCode[0].materialCode) {
                exchange = true;
                console.log('exchange', exchange);
              } else if (itemTank.materialCode === this.materialCode[1].materialCode) {
                spare = true;
                this.spare = true;
              }
            }
          } catch (err) {
            _didIteratorError4 = true;
            _iteratorError4 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion4 && _iterator4.return != null) {
                _iterator4.return();
              }
            } finally {
              if (_didIteratorError4) {
                throw _iteratorError4;
              }
            }
          }

          if (!exchange && spare) {
            this.spareOnly = true;
          }

          console.log('spare only', this.spareOnly);
        }
      }, {
        key: "getMaterialcode",
        value: function getMaterialcode() {
          this.materialCode = JSON.parse(localStorage.getItem("material_code"));
          console.log("Material Code", this.materialCode);
        }
      }]);

      return DeliveryDetailPage;
    }();

    DeliveryDetailPage.ctorParameters = function () {
      return [{
        type: _node_modules_ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_3__["Geolocation"]
      }, {
        type: _node_modules_ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_2__["LaunchNavigator"]
      }, {
        type: _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"]
      }, {
        type: _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
      }, {
        type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]
      }, {
        type: src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__["AlertService"]
      }, {
        type: src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_10__["SapApiService"]
      }, {
        type: _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_11__["LaunchNavigatorService"]
      }];
    };

    DeliveryDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-delivery-detail',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./delivery-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-detail/delivery-detail.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./delivery-detail.page.scss */
      "./src/app/drive/delivery-detail/delivery-detail.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_node_modules_ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_3__["Geolocation"], _node_modules_ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_2__["LaunchNavigator"], _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"], _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"], src_shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__["AlertService"], src_shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_10__["SapApiService"], _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_11__["LaunchNavigatorService"]])], DeliveryDetailPage);
    /***/
  },

  /***/
  "./src/shared/pipes/convert24hrto12hr.pipe.ts":
  /*!****************************************************!*\
    !*** ./src/shared/pipes/convert24hrto12hr.pipe.ts ***!
    \****************************************************/

  /*! exports provided: Convert24hrto12hrPipe */

  /***/
  function srcSharedPipesConvert24hrto12hrPipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Convert24hrto12hrPipe", function () {
      return Convert24hrto12hrPipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var Convert24hrto12hrPipe =
    /*#__PURE__*/
    function () {
      function Convert24hrto12hrPipe() {
        _classCallCheck(this, Convert24hrto12hrPipe);
      }

      _createClass(Convert24hrto12hrPipe, [{
        key: "transform",
        value: function transform(time) {
          var hour = time.split(':')[0];
          var min = time.split(':')[1];
          var part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
          min = (min + '').length == 1 ? "0".concat(min) : min;
          hour = hour > 12 ? hour - 12 : hour; // hour = parseInt(hour, 10);

          hour = (hour + '').length == 1 ? "".concat(hour) : hour;
          return "".concat(hour, ":").concat(min, " ").concat(part);
        }
      }]);

      return Convert24hrto12hrPipe;
    }();

    Convert24hrto12hrPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'convert24hrto12hr'
    })], Convert24hrto12hrPipe);
    /***/
  },

  /***/
  "./src/shared/pipes/custom-date.pipe.ts":
  /*!**********************************************!*\
    !*** ./src/shared/pipes/custom-date.pipe.ts ***!
    \**********************************************/

  /*! exports provided: CustomDatePipe */

  /***/
  function srcSharedPipesCustomDatePipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CustomDatePipe", function () {
      return CustomDatePipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var ordinals = ['th', 'st', 'nd', 'rd'];

    var CustomDatePipe =
    /*#__PURE__*/
    function () {
      function CustomDatePipe() {
        _classCallCheck(this, CustomDatePipe);
      }

      _createClass(CustomDatePipe, [{
        key: "transform",
        value: function transform(value, args) {
          var newValue;
          var valueArray = value.split('-');
          var year = valueArray[0];
          var month = valueArray[1];
          var day = valueArray[2];
          var today = new Date(Date.now()); // today.setUTCFullYear(parseInt(valueArray[0]));
          // today.setUTCMonth(parseInt(valueArray[1]) - 1);
          // today.setUTCDate(parseInt(valueArray[2]));
          // if(valueArray[3]) today.setUTCHours(parseInt(valueArray[3]));
          // if(valueArray[4]) today.setUTCMinutes(parseInt(valueArray[4]));
          // if(valueArray[5]) today.setUTCSeconds(parseInt(valueArray[5]));
          // console.log("valueArray", valueArray);
          // console.log("today", today);
          // let year = today.getFullYear();
          // let month = today.getMonth() + 1;
          // let day = today.getDate();

          var deliveryDate = new Date("".concat(month, "/").concat(day, "/").concat(year));

          if (args === "shortTime") {
            // let hour = valueArray[3];
            // let minute = valueArray[4];
            // newValue = hour + ":" + minute;
            var hour = parseInt(valueArray[3]);
            var min = valueArray[4];
            var part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
            min = (min + '').length == 1 ? "0".concat(min) : min;
            hour = hour > 12 ? hour - 12 : hour;
            var hours = (hour + '').length == 1 ? "".concat(hour) : hour;
            return "".concat(hours, ":").concat(min, " ").concat(part);
          }

          if (args === "shortWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'short'
            });
          }

          if (args === "day") {
            newValue = day;
          }

          if (args === "longWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'long'
            });
          }

          if (args === "deliveryDate") {
            var v = parseInt(day) % 100; // let v = (day % 100);

            var ordinalDay = (parseInt(day) > 9 ? day : parseInt(day)) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]); // let ordinalDay = (day > 9 ? day : day) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);

            var monthString = deliveryDate.toLocaleString('default', {
              month: 'long'
            });
            newValue = "".concat(monthString, " ").concat(ordinalDay, " ").concat(year);
          }

          if (args === "longDate") {
            var _monthString = deliveryDate.toLocaleString('default', {
              month: 'long'
            });

            newValue = "".concat(_monthString, " ").concat(day, ", ").concat(year);
          }

          return newValue;
        }
      }]);

      return CustomDatePipe;
    }();

    CustomDatePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'customDate'
    })], CustomDatePipe);
    /***/
  },

  /***/
  "./src/shared/pipes/custom-utcdate.pipe.ts":
  /*!*************************************************!*\
    !*** ./src/shared/pipes/custom-utcdate.pipe.ts ***!
    \*************************************************/

  /*! exports provided: CustomUTCDatePipe */

  /***/
  function srcSharedPipesCustomUtcdatePipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CustomUTCDatePipe", function () {
      return CustomUTCDatePipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var ordinals = ['th', 'st', 'nd', 'rd'];

    var CustomUTCDatePipe =
    /*#__PURE__*/
    function () {
      function CustomUTCDatePipe() {
        _classCallCheck(this, CustomUTCDatePipe);
      }

      _createClass(CustomUTCDatePipe, [{
        key: "transform",
        value: function transform(value, args) {
          var newValue;
          var valueArray = value.split('-'); // let year = valueArray[0];
          // let month = valueArray[1];
          // let day = valueArray[2];

          var today = new Date(Date.now());
          today.setUTCFullYear(parseInt(valueArray[0]));
          today.setUTCMonth(parseInt(valueArray[1]) - 1);
          today.setUTCDate(parseInt(valueArray[2]));
          if (valueArray[3]) today.setUTCHours(parseInt(valueArray[3]));
          if (valueArray[4]) today.setUTCMinutes(parseInt(valueArray[4]));
          if (valueArray[5]) today.setUTCSeconds(parseInt(valueArray[5])); // console.log("valueArray", valueArray);
          // console.log("today", today);

          var year = today.getFullYear();
          var month = today.getMonth() + 1;
          var day = today.getDate();
          var deliveryDate = new Date("".concat(month, "/").concat(day, "/").concat(year));

          if (args === "shortTime") {
            // let hour = valueArray[3];
            // let minute = valueArray[4];
            // newValue = hour + ":" + minute;
            var hour = today.getHours();
            var min = today.getMinutes();
            var part = hour == 12 ? 'PM' : hour > 12 ? 'PM' : 'AM';
            var minute = (min + '').length == 1 ? "0".concat(min) : min;
            hour = hour > 12 ? hour - 12 : hour;
            var hours = (hour + '').length == 1 ? "".concat(hour) : hour;
            return "".concat(hours, ":").concat(minute, " ").concat(part);
          }

          if (args === "shortWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'short'
            });
          }

          if (args === "day") {
            newValue = day;
          }

          if (args === "longWeekDay") {
            newValue = deliveryDate.toLocaleString('default', {
              weekday: 'long'
            });
          }

          if (args === "deliveryDate") {
            // let v = (parseInt(day)) % 100;
            var v = day % 100; // let ordinalDay = (parseInt(day) > 9 ? day : parseInt(day)) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);

            var ordinalDay = (day > 9 ? day : day) + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);
            var monthString = deliveryDate.toLocaleString('default', {
              month: 'long'
            });
            newValue = "".concat(monthString, " ").concat(ordinalDay, " ").concat(year);
          }

          if (args === "longDate") {
            var _monthString2 = deliveryDate.toLocaleString('default', {
              month: 'long'
            });

            newValue = "".concat(_monthString2, " ").concat(day, ", ").concat(year);
          }

          return newValue;
        }
      }]);

      return CustomUTCDatePipe;
    }();

    CustomUTCDatePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'customUtcdate'
    })], CustomUTCDatePipe);
    /***/
  },

  /***/
  "./src/shared/shared.module.ts":
  /*!*************************************!*\
    !*** ./src/shared/shared.module.ts ***!
    \*************************************/

  /*! exports provided: SharedModule */

  /***/
  function srcSharedSharedModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
      return SharedModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./pipes/custom-date.pipe */
    "./src/shared/pipes/custom-date.pipe.ts");
    /* harmony import */


    var _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./pipes/convert24hrto12hr.pipe */
    "./src/shared/pipes/convert24hrto12hr.pipe.ts");
    /* harmony import */


    var _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./pipes/custom-utcdate.pipe */
    "./src/shared/pipes/custom-utcdate.pipe.ts");

    var SharedModule = function SharedModule() {
      _classCallCheck(this, SharedModule);
    };

    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__["CustomDatePipe"], _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__["Convert24hrto12hrPipe"], _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__["CustomUTCDatePipe"]],
      entryComponents: [],
      imports: [],
      exports: [_pipes_custom_date_pipe__WEBPACK_IMPORTED_MODULE_2__["CustomDatePipe"], _pipes_convert24hrto12hr_pipe__WEBPACK_IMPORTED_MODULE_3__["Convert24hrto12hrPipe"], _pipes_custom_utcdate_pipe__WEBPACK_IMPORTED_MODULE_4__["CustomUTCDatePipe"]],
      providers: []
    })], SharedModule);
    /***/
  }
}]);
//# sourceMappingURL=drive-delivery-detail-delivery-detail-module-es5.js.map