function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["routes-route-details-route-details-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/routes/route-details/route-details.page.html":
  /*!****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/routes/route-details/route-details.page.html ***!
    \****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppRoutesRouteDetailsRouteDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Routes</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <div>\n    <ion-card>\n      <ion-card-content class=\"gig-detail mont\">\n        <ion-label class=\"gig-date\">\n          {{route.deliveryDate | customDate:'longWeekDay'}}, {{route?.deliveryDate | customDate:'deliveryDate'}}\n          <br>\n        </ion-label>\n        <ion-label class=\"gig-time\">\n          {{route.startTime | convert24hrto12hr}}\n          <!-- - {{route?.endTime | convert24hrto12hr}} -->\n        </ion-label>\n\n        <ion-row>\n          <ion-col class=\"flex-align-right vertical-align\" size=\"4\">\n            <img class=\"tank-icon\" src=\"../../../assets/icon/propane-tank-graphic.svg\">\n          </ion-col>\n          <ion-col class=\"flex-align-center vertical-align\" size=\"3\">\n            <ion-label class=\"total-number\">{{route.cynchCount + route.aceCount}}</ion-label>\n          </ion-col>\n          <ion-col class=\"vertical-align\" size=\"5\">\n            <span class=\"total-tank\">Total Tanks</span>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col size=\"1\" class=\"ion-no-padding vertical-align\">\n            <ion-img class=\"ace-img\" src=\"../../../assets/icon/icon-tank-cynch-white.svg\"></ion-img>\n          </ion-col>\n          <ion-col size=\"6\" class=\"ion-no-padding vertical-align\">\n            <ion-label class=\"other-number\">{{route.cynchCount}}</ion-label>\n            <ion-label class=\"other-tanks\">Cynch tanks</ion-label>\n          </ion-col>\n          <ion-col size=\"1\" class=\"ion-no-padding vertical-align\">\n            <ion-img class=\"ace-img\" src=\"../../../assets/icon/icon-tank-amerigas-white.svg\"></ion-img>\n          </ion-col>\n          <ion-col size=\"4\" class=\"ion-no-padding vertical-align\">\n\n            <ion-label class=\"other-number\">{{route.aceCount}}</ion-label>\n            <ion-label class=\"other-tanks\">ACE tanks</ion-label>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n\n    <div class=\"ion-padding ion-text-center\" *ngIf=\"!isAtDepot\">\n      <button (click)=\"checkGPSPermission()\" class=\"check-in-button vertical-align\">\n        <ion-icon class=\"check-in-icon\" src=\"../../../assets/icon/icon-checkin.svg\">\n        </ion-icon>\n        Check In at Depot\n      </button>\n    </div>\n\n    <div *ngIf=\"showNavigate\">\n      <div class=\"ion-padding ion-text-center\" *ngIf=\"!isAtDepot\">\n        <button (click)=\"launchNavigatorApp()\" class=\"navigate-btn vertical-align horizontal-align\">\n          <ion-icon class=\"check-in-icon\" src=\"../../../assets/icon/icon-navigation.svg\">\n          </ion-icon>\n          Navigate\n        </button>\n      </div>\n    </div>\n\n    <div *ngIf=\"isAtDepot\">\n      <ion-row>\n        <ion-col class=\"pre-tip\">\n          Pre-Tip Checklist\n        </ion-col>\n      </ion-row>\n\n      <form class=\"gig-detail-list\" [formGroup]=\"preTripForm\" (ngSubmit)=\"startGig(route)\">\n\n        <ion-row>\n          <ion-col size=\"2\" class=\"flex-align-center vertical-align ion-padding\" size=\"2\">\n            <ion-checkbox (ionChange)=\"toggleCheckbox()\" formControlName=\"shippingPaper\" id=\"shippingPaper\"></ion-checkbox>\n          </ion-col>\n          <ion-col size=\"10\" (click)=\"toggleCheckbox('shippingPaper')\">\n            <p>Do you have your shipping paper?</p>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col size=\"2\" class=\"flex-align-center vertical-align ion-padding\" size=\"2\">\n            <ion-checkbox (ionChange)=\"toggleCheckbox()\" formControlName=\"msdsSheet\" id=\"msdsSheet\"></ion-checkbox>\n          </ion-col>\n          <ion-col size=\"10\" (click)=\"toggleCheckbox('msdsSheet')\">\n            <p>Do you have an MSDS sheet?</p>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col size=\"2\" class=\"flex-align-center vertical-align ion-padding\" size=\"2\">\n            <ion-checkbox (ionChange)=\"toggleCheckbox()\" formControlName=\"tankSecured\" id=\"tankSecured\"></ion-checkbox>\n          </ion-col>\n          <ion-col size=\"10\" (click)=\"toggleCheckbox('tankSecured')\">\n            <p>Are your tanks loaded and secured?</p>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col size=\"2\" class=\"flex-align-center vertical-align ion-padding\" size=\"2\">\n            <ion-checkbox (ionChange)=\"toggleCheckbox()\" formControlName=\"insuranceCard\" id=\"insuranceCard\"></ion-checkbox>\n          </ion-col>\n          <ion-col size=\"10\" (click)=\"toggleCheckbox('insuranceCard')\">\n            <p>Do you have your registration and insurance card?</p>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col size=\"2\" class=\"flex-align-center vertical-align ion-padding\" size=\"2\">\n            <ion-checkbox (ionChange)=\"toggleCheckbox()\" formControlName=\"driversLicense\" id=\"driversLicense\"></ion-checkbox>\n          </ion-col>\n          <ion-col size=\"10\" (click)=\"toggleCheckbox('driversLicense')\">\n            <p>Do you have your driver's license?</p>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col size=\"2\" class=\"flex-align-center vertical-align ion-padding\" size=\"2\">\n            <ion-checkbox (ionChange)=\"toggleCheckbox()\" formControlName=\"phoneCharger\" id=\"phoneCharger\"></ion-checkbox>\n          </ion-col>\n          <ion-col size=\"10\" (click)=\"toggleCheckbox('phoneCharger')\">\n            <p>Do you have your cellphone charger?</p>\n          </ion-col>\n        </ion-row>\n\n        <ion-row *ngIf=\"isButtonActive\">\n          <ion-col class=\"button-padding\">\n            <button class=\"primary-button start-gig\" type=\"submit\"> Start Route </button>\n          </ion-col>\n        </ion-row>\n\n        <ion-row *ngIf=\"!isButtonActive\">\n          <ion-col class=\"button-padding\">\n            <button class=\"primary-button start-gig\" style=\"opacity: .5;\" type=\"button\"> Start Route </button>\n          </ion-col>\n        </ion-row>\n      </form>\n    </div>\n  </div>\n\n\n\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/routes/route-details/route-details.module.ts":
  /*!**************************************************************!*\
    !*** ./src/app/routes/route-details/route-details.module.ts ***!
    \**************************************************************/

  /*! exports provided: RouteDetailsPageModule */

  /***/
  function srcAppRoutesRouteDetailsRouteDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RouteDetailsPageModule", function () {
      return RouteDetailsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _route_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./route-details.page */
    "./src/app/routes/route-details/route-details.page.ts");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../shared/shared.module */
    "./src/shared/shared.module.ts");

    var routes = [{
      path: '',
      component: _route_details_page__WEBPACK_IMPORTED_MODULE_6__["RouteDetailsPage"]
    }];

    var RouteDetailsPageModule = function RouteDetailsPageModule() {
      _classCallCheck(this, RouteDetailsPageModule);
    };

    RouteDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_route_details_page__WEBPACK_IMPORTED_MODULE_6__["RouteDetailsPage"]]
    })], RouteDetailsPageModule);
    /***/
  },

  /***/
  "./src/app/routes/route-details/route-details.page.scss":
  /*!**************************************************************!*\
    !*** ./src/app/routes/route-details/route-details.page.scss ***!
    \**************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppRoutesRouteDetailsRouteDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-title {\n  color: #003A58;\n}\n\nion-content {\n  --background: #fafafa;\n}\n\n.pre-tip {\n  font-size: 1rem;\n  color: #003A58;\n  padding: 1rem;\n  font-weight: bold;\n}\n\n.gig-detail-list {\n  background-color: white;\n}\n\n.gig-detail {\n  background-color: #003A58;\n  color: #ffffff;\n  text-align: center;\n}\n\n.gig-date, .gig-time {\n  font-size: 1rem;\n  font-family: \"Montserrat-Bold\";\n}\n\n.tank-icon {\n  height: 3.75rem;\n  width: 3.75rem;\n  display: -webkit-box;\n  display: flex;\n}\n\n.total-number {\n  font-size: 3.5rem;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.total-tank {\n  font-size: 1rem;\n  color: #FAAF40;\n  letter-spacing: 0;\n  font-family: \"Montserrat-Bold\";\n}\n\n.other-tanks {\n  font-size: 0.875rem;\n  color: white;\n  letter-spacing: 0;\n  font-family: \"Roboto-Medium\";\n  padding-right: 10px;\n}\n\n.other-number {\n  font-size: 1.25rem;\n  color: #ffffff;\n  font-family: \"Montserrat-Bold\";\n  padding-right: 5px;\n  padding-left: 8px;\n}\n\nion-checkbox {\n  --background-checked: #F4773B;\n  --border-color-checked: #F4773B;\n}\n\n.start-gig {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-weight: bolder;\n  background: #F4773B 100%;\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036));\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%);\n}\n\n.submit-button {\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036));\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%);\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n}\n\n.button-padding {\n  padding: 0 1rem;\n}\n\n.cancel-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 0.2rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 3rem;\n  font-family: \"Montserrat-Bold\";\n}\n\n.backgound-white {\n  background-color: #ffffff;\n}\n\n.shyft-date {\n  font-size: 1rem;\n  font-family: \"Montserrat-Bold\";\n  color: #003A58;\n}\n\n.shyft-date-padding {\n  padding: 0 1rem 0 1rem;\n}\n\n.subtitle-text {\n  padding: 0 0 0.2rem 0;\n  margin: 0;\n  font-size: 0.8rem;\n  color: #4b4b4b;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.content-text {\n  padding: 0;\n  margin: 0;\n  font-size: 1rem;\n  font-family: \"Montserrat-Regular\";\n}\n\n.check-in-button {\n  background-color: #ffffff;\n  border: 1px #003A58 solid;\n  width: 100%;\n  height: 3rem;\n  font-size: 1rem;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  font-family: \"Montserrat-SemiBold\";\n  color: #003A58;\n}\n\n.navigate-btn {\n  color: white;\n  border: solid 1px #003A58;\n  font-family: \"Montserrat-SemiBold\";\n  background-color: #003A58;\n  width: 100%;\n  height: 3rem;\n  font-size: 1rem;\n}\n\n.ace-img {\n  height: 100%;\n}\n\n.check-in-icon {\n  margin-right: 0.5rem;\n  height: 1.2rem;\n  width: 1.2rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL3JvdXRlcy9yb3V0ZS1kZXRhaWxzL3JvdXRlLWRldGFpbHMucGFnZS5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXBwL3JvdXRlcy9yb3V0ZS1kZXRhaWxzL3JvdXRlLWRldGFpbHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU9BO0VBQ0kseUJBQUE7RUFDQSxnREFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkVsQmM7RUZtQmQsV0FBQTtFQUNBLGNFaEJJO0VGaUJKLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSwrQkFBQTtFQUFBLHdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxvQ0FBQTtVQUFBLDhCQUFBO0FDTko7O0FEU0E7RUFDSSxtQ0FBQTtVQUFBLGtDQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtVQUFBLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx3QkFBQTtVQUFBLHVCQUFBO0FDTko7O0FEU0E7RUFDSSxZQUFBO0FDTko7O0FFaERBO0VBQ0ksY0REYztBRG9EbEI7O0FFaERBO0VBQ0kscUJBQUE7QUZtREo7O0FFaERBO0VBQ0ksZUFBQTtFQUNBLGNEVmM7RUNXZCxhQUFBO0VBQ0EsaUJBQUE7QUZtREo7O0FFaERBO0VBQ0ksdUJBQUE7QUZtREo7O0FFaERBO0VBQ0kseUJEcEJjO0VDcUJkLGNEakJJO0VDa0JKLGtCQUFBO0FGbURKOztBRWhEQTtFQUNJLGVBQUE7RUFDQSw4QkFBQTtBRm1ESjs7QUUvQ0E7RUFDSSxlQUFBO0VBQ0EsY0FBQTtFQUNBLG9CQUFBO0VBQUEsYUFBQTtBRmtESjs7QUUvQ0E7RUFDSSxpQkFBQTtFQUNBLGtDQUFBO0FGa0RKOztBRTdDQTtFQUNJLGVBQUE7RUFDQSxjRDdDYztFQzhDZCxpQkFBQTtFQUNBLDhCQUFBO0FGZ0RKOztBRTdDQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7RUFFQSxtQkFBQTtBRitDSjs7QUU1Q0E7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUYrQ0o7O0FFNUNBO0VBQ0ksNkJBQUE7RUFDQSwrQkFBQTtBRitDSjs7QUU1Q0E7RUFFSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSx3QkFBQTtFQUNBLDZGRDNFZ0I7RUMyRWhCLG9FRDNFZ0I7QUR5SHBCOztBRXpDQTtFQUNJLDZGRGpGZ0I7RUNpRmhCLG9FRGpGZ0I7RUNrRmhCLFdBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUY0Q0o7O0FFekNBO0VBQ0ksZUFBQTtBRjRDSjs7QUV6Q0E7RUFDSSx5QkRyR2M7RUNzR2QsV0FBQTtFQUNBLGNEbkdJO0VDb0dKLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FGNENKOztBRXpDQTtFQUNJLHlCRDVHSTtBRHdKUjs7QUV6Q0E7RUFDSSxlQUFBO0VBQ0EsOEJBQUE7RUFDQSxjRHRIYztBRGtLbEI7O0FFekNBO0VBQ0ksc0JBQUE7QUY0Q0o7O0FFekNBO0VBQ0kscUJBQUE7RUFDQSxTQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0NBQUE7QUY0Q0o7O0FFekNBO0VBQ0ksVUFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0VBQ0EsaUNBQUE7QUY0Q0o7O0FFekNBO0VBQ0kseUJBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLG9CQUFBO0VBQUEsYUFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSxrQ0FBQTtFQUNBLGNEckpjO0FEaU1sQjs7QUV6Q0E7RUFDSSxZQUFBO0VBQ0EseUJBQUE7RUFFQSxrQ0FBQTtFQUNBLHlCRDdKYztFQzhKZCxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUYyQ0o7O0FFeENBO0VBQ0ksWUFBQTtBRjJDSjs7QUV2Q0E7RUFDSSxvQkFBQTtFQUNBLGNBQUE7RUFDQSxhQUFBO0FGMENKIiwiZmlsZSI6InNyYy9hcHAvcm91dGVzL3JvdXRlLWRldGFpbHMvcm91dGUtZGV0YWlscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvZm9udHMnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcyc7XG5cbi8vIC5pcy12YWxpZCB7XG4vLyAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZVxuLy8gfVxuXG4uaXMtaW52YWxpZCB7XG4gICAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwM1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBjb2xvcjogJHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xe1xuICAgIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICAgIGRpc3BsYXk6IGZsZXghaW1wb3J0YW50O1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlciFpbXBvcnRhbnQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlciFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuXG5cblxuIiwiLmlzLWludmFsaWQge1xuICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJzcmMvYXNzZXRzL2ljb24vaWNvbi1oZWxwLnN2Z1wiKSBuby1yZXBlYXQ7XG59XG5cbi5pcy12YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDM7XG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNFRTQwMzY7XG59XG5cbi5wcmltYXJ5LWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDFyZW07XG4gIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTEge1xuICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmO1xufVxuXG4udmVydGljYWwtYWxpZ24ge1xuICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5ob3Jpem9udGFsLWFsaWduIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmRpc2FibGUge1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbmlvbi10aXRsZSB7XG4gIGNvbG9yOiAjMDAzQTU4O1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogI2ZhZmFmYTtcbn1cblxuLnByZS10aXAge1xuICBmb250LXNpemU6IDFyZW07XG4gIGNvbG9yOiAjMDAzQTU4O1xuICBwYWRkaW5nOiAxcmVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmdpZy1kZXRhaWwtbGlzdCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4uZ2lnLWRldGFpbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5naWctZGF0ZSwgLmdpZy10aW1lIHtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuLnRhbmstaWNvbiB7XG4gIGhlaWdodDogMy43NXJlbTtcbiAgd2lkdGg6IDMuNzVyZW07XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi50b3RhbC1udW1iZXIge1xuICBmb250LXNpemU6IDMuNXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xufVxuXG4udG90YWwtdGFuayB7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgY29sb3I6ICNGQUFGNDA7XG4gIGxldHRlci1zcGFjaW5nOiAwO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuLm90aGVyLXRhbmtzIHtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgY29sb3I6IHdoaXRlO1xuICBsZXR0ZXItc3BhY2luZzogMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLU1lZGl1bVwiO1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufVxuXG4ub3RoZXItbnVtYmVyIHtcbiAgZm9udC1zaXplOiAxLjI1cmVtO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgcGFkZGluZy1sZWZ0OiA4cHg7XG59XG5cbmlvbi1jaGVja2JveCB7XG4gIC0tYmFja2dyb3VuZC1jaGVja2VkOiAjRjQ3NzNCO1xuICAtLWJvcmRlci1jb2xvci1jaGVja2VkOiAjRjQ3NzNCO1xufVxuXG4uc3RhcnQtZ2lnIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICBoZWlnaHQ6IDNyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgYmFja2dyb3VuZDogI0Y0NzczQiAxMDAlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoLTE4MGRlZywgI0ZGODg0MCAwJSwgI0VFNDAzNiAxMDAlKTtcbn1cblxuLnN1Ym1pdC1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoLTE4MGRlZywgI0ZGODg0MCAwJSwgI0VFNDAzNiAxMDAlKTtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICBoZWlnaHQ6IDNyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmJ1dHRvbi1wYWRkaW5nIHtcbiAgcGFkZGluZzogMCAxcmVtO1xufVxuXG4uY2FuY2VsLWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDNBNTg7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMC4ycmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgaGVpZ2h0OiAzcmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbn1cblxuLmJhY2tnb3VuZC13aGl0ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59XG5cbi5zaHlmdC1kYXRlIHtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LUJvbGRcIjtcbiAgY29sb3I6ICMwMDNBNTg7XG59XG5cbi5zaHlmdC1kYXRlLXBhZGRpbmcge1xuICBwYWRkaW5nOiAwIDFyZW0gMCAxcmVtO1xufVxuXG4uc3VidGl0bGUtdGV4dCB7XG4gIHBhZGRpbmc6IDAgMCAwLjJyZW0gMDtcbiAgbWFyZ2luOiAwO1xuICBmb250LXNpemU6IDAuOHJlbTtcbiAgY29sb3I6ICM0YjRiNGI7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbn1cblxuLmNvbnRlbnQtdGV4dCB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LVJlZ3VsYXJcIjtcbn1cblxuLmNoZWNrLWluLWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlcjogMXB4ICMwMDNBNTggc29saWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDNyZW07XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbiAgY29sb3I6ICMwMDNBNTg7XG59XG5cbi5uYXZpZ2F0ZS1idG4ge1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlcjogc29saWQgMXB4ICMwMDNBNTg7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtU2VtaUJvbGRcIjtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogM3JlbTtcbiAgZm9udC1zaXplOiAxcmVtO1xufVxuXG4uYWNlLWltZyB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLmNoZWNrLWluLWljb24ge1xuICBtYXJnaW4tcmlnaHQ6IDAuNXJlbTtcbiAgaGVpZ2h0OiAxLjJyZW07XG4gIHdpZHRoOiAxLjJyZW07XG59IiwiXG4vLyBjb2xvcnNcbi8vICRwcmltYXJ5LWNvbG9yLTE6ICMwMEEzQzg7XG4kcHJpbWFyeS1jb2xvci0xOiAjMDAzQTU4O1xuJHByaW1hcnktY29sb3ItMjogI0ZBQUY0MDtcbiRwcmltYXJ5LWNvbG9yLTM6ICNGNDc3M0I7XG5cbiR3aGl0ZTogI2ZmZmZmZjtcblxuLy8gJGJ1dHRvbi1ncmFkaWVudC0xOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCByZ2JhKDI1NSwyNTUsMjU1LDAuNTApIDAlLCByZ2JhKDAsMCwwLDAuNTApIDEwMCUpO1xuJGJ1dHRvbi1ncmFkaWVudC0xOiBsaW5lYXItZ3JhZGllbnQoLTE4MGRlZywgI0ZGODg0MCAwJSwgI0VFNDAzNiAxMDAlKTsiLCJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzJztcblxuaW9uLXRpdGxle1xuICAgIGNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xufVxuXG5pb24tY29udGVudHtcbiAgICAtLWJhY2tncm91bmQ6ICNmYWZhZmE7XG59XG5cbi5wcmUtdGlwIHtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgcGFkZGluZzogMXJlbTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmdpZy1kZXRhaWwtbGlzdHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmdpZy1kZXRhaWwge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgY29sb3I6ICR3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5naWctZGF0ZSwgLmdpZy10aW1le1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgXG59XG5cbi50YW5rLWljb24ge1xuICAgIGhlaWdodDogMy43NXJlbTtcbiAgICB3aWR0aDogMy43NXJlbTtcbiAgICBkaXNwbGF5OiBmbGV4O1xufVxuXG4udG90YWwtbnVtYmVyIHtcbiAgICBmb250LXNpemU6IDMuNXJlbTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xufVxuXG5cblxuLnRvdGFsLXRhbmsge1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMjtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG59XG5cbi5vdGhlci10YW5rc3tcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xuICAgIC8vIG1hcmdpbjogYXV0bztcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufVxuXG4ub3RoZXItbnVtYmVye1xuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgcGFkZGluZy1yaWdodDogNXB4O1xuICAgIHBhZGRpbmctbGVmdDogOHB4O1xufVxuXG5pb24tY2hlY2tib3h7XG4gICAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6ICNGNDc3M0I7XG4gICAgLS1ib3JkZXItY29sb3ItY2hlY2tlZDogI0Y0NzczQjtcbn1cblxuLnN0YXJ0LWdpZ3tcblxuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGJhY2tncm91bmQ6ICNGNDc3M0IgMTAwJTtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiAkYnV0dG9uLWdyYWRpZW50LTE7XG59XG5cblxuXG4uc3VibWl0LWJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogJGJ1dHRvbi1ncmFkaWVudC0xO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmJ1dHRvbi1wYWRkaW5ne1xuICAgIHBhZGRpbmc6IDAgMXJlbTs7XG59XG5cbi5jYW5jZWwtYnV0dG9uIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAkd2hpdGU7XG4gICAgYm9yZGVyLXJhZGl1czogLjJyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBoZWlnaHQ6IDNyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LUJvbGQnO1xufVxuXG4uYmFja2dvdW5kLXdoaXRle1xuICAgIGJhY2tncm91bmQtY29sb3I6ICR3aGl0ZTtcbn1cblxuLnNoeWZ0LWRhdGV7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJztcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbn1cblxuLnNoeWZ0LWRhdGUtcGFkZGluZyB7XG4gICAgcGFkZGluZzogMCAxcmVtIDAgMXJlbTtcbn1cblxuLnN1YnRpdGxlLXRleHR7XG4gICAgcGFkZGluZzogMCAwIC4ycmVtIDA7XG4gICAgbWFyZ2luOiAwO1xuICAgIGZvbnQtc2l6ZTogLjhyZW07XG4gICAgY29sb3I6ICM0YjRiNGI7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbn1cblxuLmNvbnRlbnQtdGV4dHtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVJlZ3VsYXInO1xufVxuXG4uY2hlY2staW4tYnV0dG9ue1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgYm9yZGVyOiAxcHggJHByaW1hcnktY29sb3ItMSBzb2xpZDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDNyZW07XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0LVNlbWlCb2xkJztcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbn1cblxuLm5hdmlnYXRlLWJ0biB7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlcjogc29saWQgMXB4ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgLy8gZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtU2VtaUJvbGQnO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbn1cblxuLmFjZS1pbWcge1xuICAgIGhlaWdodDogMTAwJTtcbiAgICAvLyB3aWR0aDogMTAwJTtcbiAgICAvLyBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uY2hlY2staW4taWNvbntcbiAgICBtYXJnaW4tcmlnaHQ6IC41cmVtO1xuICAgIGhlaWdodDogMS4ycmVtO1xuICAgIHdpZHRoOiAxLjJyZW07XG59XG4iXX0= */";
    /***/
  },

  /***/
  "./src/app/routes/route-details/route-details.page.ts":
  /*!************************************************************!*\
    !*** ./src/app/routes/route-details/route-details.page.ts ***!
    \************************************************************/

  /*! exports provided: RouteDetailsPage */

  /***/
  function srcAppRoutesRouteDetailsRouteDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RouteDetailsPage", function () {
      return RouteDetailsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _shared_services_routing_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../shared/services/routing.service */
    "./src/shared/services/routing.service.ts");
    /* harmony import */


    var _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../shared/services/loader.service */
    "./src/shared/services/loader.service.ts");
    /* harmony import */


    var _shared_services_delivery_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../shared/services/delivery.service */
    "./src/shared/services/delivery.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../shared/services/schedule.service */
    "./src/shared/services/schedule.service.ts");
    /* harmony import */


    var _shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../shared/services/alert.service */
    "./src/shared/services/alert.service.ts");
    /* harmony import */


    var _shared_services_toast_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../shared/services/toast.service */
    "./src/shared/services/toast.service.ts");
    /* harmony import */


    var _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../../shared/services/sap-api.service */
    "./src/shared/services/sap-api.service.ts");
    /* harmony import */


    var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @ionic-native/launch-navigator/ngx */
    "./node_modules/@ionic-native/launch-navigator/ngx/index.js");
    /* harmony import */


    var _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../../shared/services/lauch-navigator.service */
    "./src/shared/services/lauch-navigator.service.ts");
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/ngx/index.js");
    /* harmony import */


    var _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @ionic-native/location-accuracy/ngx */
    "./node_modules/@ionic-native/location-accuracy/ngx/index.js");
    /* harmony import */


    var _capacitor_core__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @capacitor/core */
    "./node_modules/@capacitor/core/dist/esm/index.js"); // import { Geolocation } from '@ionic-native/geolocation/ngx';


    var Geolocation = _capacitor_core__WEBPACK_IMPORTED_MODULE_16__["Plugins"].Geolocation;

    var RouteDetailsPage =
    /*#__PURE__*/
    function () {
      function RouteDetailsPage(router, formBuilder, routingService, loaderService, deliveryService, platform, scheduleService, alertService, toastService, sapApiService, // private geolocation: Geolocation,
      launchNavigator, launchNavService, androidPermissions, locationAccuracy // private geofence: Geofence
      // private spherical: Spherical
      ) {
        _classCallCheck(this, RouteDetailsPage);

        this.router = router;
        this.formBuilder = formBuilder;
        this.routingService = routingService;
        this.loaderService = loaderService;
        this.deliveryService = deliveryService;
        this.platform = platform;
        this.scheduleService = scheduleService;
        this.alertService = alertService;
        this.toastService = toastService;
        this.sapApiService = sapApiService;
        this.launchNavigator = launchNavigator;
        this.launchNavService = launchNavService;
        this.androidPermissions = androidPermissions;
        this.locationAccuracy = locationAccuracy;
        this.hasDeliveryCount = true;
        this.isButtonActive = false;
        this.enableGigStart = false;
        this.showNavigate = false;
        this.isAppAvailable = false; // geofence.initialize().then(
        //   // resolved promise does not return a value
        //   () => console.log('Geofence Plugin Ready'),
        //   (err) => console.log(err)
        // )
      }

      _createClass(RouteDetailsPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.initializeForm();
          this.currentUser = JSON.parse(localStorage.getItem("current_user"));
          this.getRouteDetails(this.router.getCurrentNavigation().extras.state.route);
          this.isAtDepot = false;
          this.showNavigate = false;
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          var _this = this;

          // this.delay(1000).then(any => {
          if (localStorage.getItem("shyft_toggle")) {
            this.router.navigate(["/tabs/routes"]);
          } // });
          // this.addGeofence();


          this.launchNavService.checkAvailableApp().then(function (availableApp) {
            _this.availableApps = availableApp;
          });
        } // private addGeofence() {
        //   let depotLatitude;
        //   let depotLongitude;
        //   let testLat = 14.657809;
        //   let testLong = 121.05219;
        //   // for (let storage of this.currentUser.msa.plants[0].storages) {
        //   //   if (storage.isCylinderLoc) {
        //   //     depotLatitude = storage.latitude;
        //   //     depotLongitude = storage.longitude;
        //   //   }
        //   // }
        //   //options describing geofence
        //   let fence = {
        //     id: '69ca1b88-6fbe-4e80-a4d4-ff4d3748acdb', //any unique ID
        //     latitude: testLat, //center of geofence radius
        //     longitude: testLong,
        //     radius: 500, //radius to edge of geofence in meters
        //     transitionType: 1, //see 'Transition Types' below
        //     notification: { //notification settings
        //       id: 1, //any unique ID
        //       title: 'You are now ready to confirm a route', //notification title
        //       text: 'You have arrived in our Depot.', //notification body
        //       openAppOnClick: true //open app when notification is tapped
        //     }
        //   }
        //   this.geofence.addOrUpdate(fence).then(
        //     () => console.log('Geofence added'),
        //     (err) => console.log('Geofence failed to add')
        //   );
        // }
        // checkGeoFenceTransition() {
        //   this.getLocation().then(result => {
        //     if (result) {
        //       for (let storage of this.currentUser.msa.plants[0].storages) {
        //         if (storage.isCylinderLoc) {
        //           this.depotLatitude = storage.latitude;
        //           this.depotLongitude = storage.longitude;
        //         }
        //       }
        //       this.latitude = this.depotLatitude;
        //       this.longitude = this.depotLongitude;
        //       if (this.geofence.onTransitionReceived()) {
        //         this.toastService.presentToast("You're within the vicinity of the Depot");
        //         console.log("At Depot");
        //         this.enableGigStart = true;
        //       }
        //       else {
        //         console.log("far away from Depot");
        //         this.enableGigStart = false;
        //       }
        //     }
        //   },
        //   error => {
        //     console.log(error);
        //   });
        // }

      }, {
        key: "delay",
        value: function delay(ms) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return new Promise(function (resolve) {
                      return setTimeout(function () {
                        return resolve();
                      }, ms);
                    }).then(function () {
                      return console.log("fired");
                    });

                  case 2:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee);
          }));
        }
      }, {
        key: "initializeForm",
        value: function initializeForm() {
          this.preTripForm = this.formBuilder.group({
            shippingPaper: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            msdsSheet: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            tankSecured: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            insuranceCard: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            driversLicense: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            phoneCharger: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
          });
        }
      }, {
        key: "toggleCheckbox",
        value: function toggleCheckbox(property) {
          if (property) {
            switch (property) {
              case "shippingPaper":
                {
                  this.fc.shippingPaper.patchValue(!this.fc.shippingPaper.value);
                  break;
                }

              case "msdsSheet":
                {
                  this.fc.msdsSheet.patchValue(!this.fc.msdsSheet.value);
                  break;
                }

              case "tankSecured":
                {
                  this.fc.tankSecured.patchValue(!this.fc.tankSecured.value);
                  break;
                }

              case "insuranceCard":
                {
                  this.fc.insuranceCard.patchValue(!this.fc.insuranceCard.value);
                  break;
                }

              case "driversLicense":
                {
                  this.fc.driversLicense.patchValue(!this.fc.driversLicense.value);
                  break;
                }

              case "phoneCharger":
                {
                  this.fc.phoneCharger.patchValue(!this.fc.phoneCharger.value);
                  break;
                }

              default:
                {
                  break;
                }
            }
          }

          if (this.fc.shippingPaper.value == true && this.fc.msdsSheet.value == true && this.fc.tankSecured.value == true && this.fc.insuranceCard.value && this.fc.driversLicense.value == true && this.fc.phoneCharger.value) {
            this.isButtonActive = true;
          } else {
            this.isButtonActive = false;
          }
        }
      }, {
        key: "startGig",
        value: function startGig(route) {
          var _this2 = this;

          this.loaderService.createLoader("Starting route..."); // localStorage.setItem("stop_sequence", "1");
          // localStorage.setItem("current_route_started", JSON.stringify(route));
          // localStorage.setItem("route_id", route.routeId);
          // this.router.navigate([`/tabs/drive`]);
          // if (this.enableGigStart) {

          this.sapApiService.confirmRoute(route).subscribe(function (result) {
            console.log("SAP confirm result", result);
            localStorage.setItem("stop_sequence", "1");
            localStorage.setItem("current_route_started", JSON.stringify(route));
            localStorage.setItem("route_id", route.routeId);
            localStorage.setItem("shyft_toggle", "true");
            var location = {
              latitude: 0,
              longitude: 0
            };
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
              for (var _iterator = _this2.currentUser.msa.plants[0].storages[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var storage = _step.value;

                if (storage.isCylinderLoc) {
                  location.latitude = storage.latitude;
                  location.longitude = storage.longitude;
                }
              }
            } catch (err) {
              _didIteratorError = true;
              _iteratorError = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }
              } finally {
                if (_didIteratorError) {
                  throw _iteratorError;
                }
              }
            }

            localStorage.setItem("previous_loc", JSON.stringify(location));

            _this2.loaderService.dismissLoader();

            _this2.router.navigate(["/tabs/drive"]);
          }, function (error) {
            console.log("Error");
          }); //   this.deliveryService.getUserDeliveries(route.routeId).subscribe(
          //     result => {
          //       console.log("Delivery", result);
          //       localStorage.setItem("current_routeId", route.routeId.toString());
          //       localStorage.setItem("current_deliveries", JSON.stringify(result));
          //     },
          //     error => {
          //       console.log("Delivery Error", error)
          //     }
          //   )
          // }
          // else {
          //   this.toastService.presentToast("Please be within the 500 meters of the Depot.");
          // }
        }
      }, {
        key: "getRouteDetails",
        value: function getRouteDetails(route) {
          // if (route.deliveryCount != 0) {
          //   this.hasDeliveryCount = true;
          this.route = route;
          console.log("this is route", this.route);

          if (localStorage.getItem("navigate")) {
            localStorage.removeItem("navigate");
            this.launchNavigatorApp();
          } // } else {
          //   this.route = route;
          //   this.hasDeliveryCount = false;
          // }

        }
      }, {
        key: "cancelShyft",
        value: function cancelShyft() {
          var _this3 = this;

          this.alertService.createChoiceAlert("Are you sure you want to cancel your Shyft?").then(function (result) {
            if (result) {
              _this3.loaderService.createLoader("Loading...");

              _this3.routingService.cancelRoute(_this3.route.routeId).subscribe(function (result) {
                _this3.toastService.presentToast("Shyft cancelled");

                _this3.loaderService.dismissLoader();

                _this3.router.navigate(['tabs/gigs']);
              }, function (error) {
                _this3.toastService.presentToast("Failed to cancel Shyft");

                _this3.loaderService.dismissLoader();
              });
            }
          });
        }
      }, {
        key: "checkInAtDepot",
        value: function checkInAtDepot() {
          var _this4 = this;

          var depotLatitude;
          var depotLongitude;
          var distance = 0;
          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = this.currentUser.msa.plants[0].storages[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var storage = _step2.value;

              if (storage.isCylinderLoc) {
                depotLatitude = storage.latitude;
                depotLongitude = storage.longitude;
              }
            } // depotLatitude = 14.649901;
            // depotLongitude = 121.046870;
            // depotLatitude = 14.656804; 
            // depotLongitude = 121.056531;

          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                _iterator2.return();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }

          console.log("**GETTING LOCATION**"); // this.testLocation();

          this.getLocation().then(function (result) {
            console.log("**GET LOCATION:", result);

            if (result) {
              // this.toastService.presentToast("*get location true");
              distance = _this4.checkDistance(_this4.latitude, _this4.longitude, depotLatitude, depotLongitude);
              console.log("****Distance ", distance);

              if (distance <= 1000) {
                // this.loaderService.createLoader("Checking In...");
                var checkInTime = _this4.sapApiService.getConfirmationDateTime();

                _this4.routingService.routeCheckIn(_this4.route.routeId, checkInTime).subscribe(function (result) {
                  _this4.isAtDepot = true;

                  _this4.loaderService.dismissLoader();
                }, function (error) {
                  console.log("Error on getting checkin time", error);

                  _this4.loaderService.dismissLoader();
                });
              } else {
                _this4.loaderService.dismissLoader();

                _this4.toastService.presentToast("You have to be in the depot to check in!");

                _this4.showNavigate = true;
              }
            } else {
              console.log("NO LOCATION***", result);

              _this4.loaderService.dismissLoader();
            }
          }, function (error) {
            _this4.loaderService.dismissLoader(); // this.toastService.presentToast("*Get Location Error* ")


            console.log("Error", error);
          }); //end
          // let checkInTime = this.sapApiService.getConfirmationDateTime();
          // this.routingService.routeCheckIn(this.route.routeId, checkInTime).subscribe( result => {
          //   this.isAtDepot = true;
          //   this.loaderService.dismissLoader();
          // }, error => {
          //   console.log("Error on getting checkin time", error);
          //   this.loaderService.dismissLoader();
          // });
        } // async testLocation(){ 
        //   const coordinates = await Geolocation.getCurrentPosition({enableHighAccuracy: true});
        //   console.log("**CURRENT", coordinates);
        //   this.toastService.presentToast(coordinates.coords);
        // } 

      }, {
        key: "getLocation",
        value: function getLocation() {
          return new Promise(function (resolve, reject) {
            var _this5 = this;

            Geolocation.getCurrentPosition().then(function (resp) {
              _this5.latitude = resp.coords.latitude;
              _this5.longitude = resp.coords.longitude;
              console.log("Current Location", _this5.latitude);
              console.log("Current Location", _this5.longitude);
              resolve(true);
            }).catch(function (error) {
              if (error.code == 1) {
                _this5.toastService.presentToast("Please enable location services to continue");
              } else if (error.code == 2) {
                _this5.toastService.presentToast("Location Unavailable");
              } else if (error.code == 3) {
                _this5.toastService.presentToast("Request Timeout");
              }

              console.log('Error getting location', error.code);
              resolve(false);
            });
          }.bind(this));
        }
      }, {
        key: "checkDistance",
        value: function checkDistance(fromLat, fromLng, toLat, toLng) {
          // let fromCord = {
          //   lat: fromLat,
          //   lng: fromLng
          // }
          var fromCord = new google.maps.LatLng(fromLat, fromLng);
          var toCord = new google.maps.LatLng(toLat, toLng); // let toCord = {
          //   lat: toLat,
          //   lng: toLng
          // }

          console.log("from", fromCord);
          console.log("to", toCord); // let computedDistance = (google.maps.android.Spherical.computeDistanceBetween(fromCord, toCord) / 1000).toFixed(2);

          var computedDistance = google.maps.geometry.spherical.computeDistanceBetween(fromCord, toCord).toFixed(2);
          console.log("****Computed Distance", computedDistance); // return Spherical.computeDistanceBetween(fromCord, toCord);
          // return google.maps.geometry.spherical.computeDistanceBetween(fromCord, toCord);

          return parseInt(computedDistance);
        }
      }, {
        key: "launchNavigatorApp",
        value: function launchNavigatorApp() {
          var _this6 = this;

          this.getLocation().then(function (result) {
            if (result) {
              _this6.launchNavService.checkExistingSelectedMap().then(function (result) {
                if (result) {
                  _this6.launchNavService.getUserSelectedMap().then(function (preferredMap) {
                    console.log("Selected", preferredMap);

                    if (result === "this.launchNavigator.APP.GOOGLE_MAPS" || result === "google_maps") {
                      if (_this6.availableApps.google_maps) {
                        _this6.isAppAvailable = true;
                      }
                    } else if (result === "this.launchNavigator.APP.WAZE" || result === "waze") {
                      if (_this6.availableApps.waze) {
                        _this6.isAppAvailable = true;
                      }
                    } else if (result === "this.launchNavigator.APP.APPLE_MAPS" || result === "apple_maps") {
                      if (_this6.availableApps.apple_maps) {
                        _this6.isAppAvailable = true;
                      }
                    } else if (result === "this.launchNavigator.APP.CITYMAPPER" || result === "citymapper") {
                      if (_this6.availableApps.citymapper) {
                        _this6.isAppAvailable = true;
                      }
                    } else {
                      _this6.isAppAvailable = false;
                    }

                    if (_this6.isAppAvailable) {
                      if (preferredMap === "this.launchNavigator.APP.APPLE_MAPS") {
                        var options = {
                          start: "".concat(_this6.latitude, ", ").concat(_this6.longitude),
                          app: _this6.launchNavigator.APP.APPLE_MAPS
                        };

                        _this6.launchNavigatorMapApp(options);
                      } else if (preferredMap === "this.launchNavigator.APP.WAZE") {
                        var _options = {
                          start: "".concat(_this6.latitude, ", ").concat(_this6.longitude),
                          app: _this6.launchNavigator.APP.WAZE
                        };

                        _this6.launchNavigatorMapApp(_options);
                      } else if (preferredMap === "this.launchNavigator.APP.GOOGLE_MAPS") {
                        var _options2 = {
                          start: "".concat(_this6.latitude, ", ").concat(_this6.longitude),
                          app: _this6.launchNavigator.APP.GOOGLE_MAPS
                        };

                        _this6.launchNavigatorMapApp(_options2);
                      } else {
                        var _options3 = {
                          start: "".concat(_this6.latitude, ", ").concat(_this6.longitude),
                          appSelection: {
                            rememberChoice: {
                              enabled: true
                            }
                          }
                        };

                        _this6.launchNavigatorMapApp(_options3);
                      }
                    } else {
                      var _options4 = {
                        start: "".concat(_this6.latitude, ", ").concat(_this6.longitude),
                        appSelection: {
                          rememberChoice: {
                            enabled: true
                          }
                        }
                      };

                      _this6.launchNavigatorMapApp(_options4);
                    }
                  }, function (getUserError) {
                    // this.toastService.presentToast("Get User error*");
                    console.log("*Get User Error*", getUserError);
                  });
                } else {
                  var options = {
                    start: "".concat(_this6.latitude, ", ").concat(_this6.longitude)
                  };

                  _this6.launchNavigatorMapApp(options);

                  console.log("None selected");
                }
              }, function (checkSelectedMap) {
                // this.toastService.presentToast("Nav Error Check Selected Map*");
                console.log("*Nav Error Check Selected Map* ", checkSelectedMap);
              });
            }
          }, function (error) {
            // this.toastService.presentToast("Get Location Error*");
            console.log("*Get Location Error* ", error);
          });
        }
      }, {
        key: "launchNavigatorMapApp",
        value: function launchNavigatorMapApp(options) {
          var depotLatitude;
          var depotLongitude;
          var _iteratorNormalCompletion3 = true;
          var _didIteratorError3 = false;
          var _iteratorError3 = undefined;

          try {
            for (var _iterator3 = this.currentUser.msa.plants[0].storages[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
              var storage = _step3.value;

              if (storage.isCylinderLoc) {
                depotLatitude = storage.latitude;
                depotLongitude = storage.longitude;
              }
            }
          } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                _iterator3.return();
              }
            } finally {
              if (_didIteratorError3) {
                throw _iteratorError3;
              }
            }
          }

          this.launchNavigator.navigate("".concat(depotLatitude, ", ").concat(depotLongitude), options).then(function (success) {
            console.log("Success Return", success);
          }, function (error) {
            console.log("Error", error);
          });
        }
      }, {
        key: "checkInRoute",
        value: function checkInRoute() {}
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ionViewDidEnter();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "checkGPSPermission",
        value: function checkGPSPermission() {
          var _this7 = this;

          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function (result) {
            if (result.hasPermission) {
              //If hvaing permission show 'Turn on GPS' dialogue
              // this.toastService.presentToast("Has Permission");
              _this7.askToTurnOnGPS();
            } else {
              //If not having permission ask for permission
              // this.toastService.presentToast("not having permission");
              _this7.requestGPSPermissions();
            }
          }, function (err) {
            alert(err);
          });
        }
      }, {
        key: "requestGPSPermissions",
        value: function requestGPSPermissions() {
          var _this8 = this;

          this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
              console.log("4"); // this.toastService.presentToast("4");
            } else {
              _this8.androidPermissions.requestPermission(_this8.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function () {
                // call method to turn on GPS
                _this8.askToTurnOnGPS();
              }, function (error) {
                // this.toastService.presentToast("Please enable location services to continue");
                console.log("Request permission error" + error); // alert('request permission Error requesting location permission' + error)
              });
            }
          });
        }
      }, {
        key: "askToTurnOnGPS",
        value: function askToTurnOnGPS() {
          var _this9 = this;

          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
            _this9.loaderService.createLoader("Checking In...");

            console.log("CHECKING IN**");

            _this9.checkInAtDepot();
          }, function (error) {
            _this9.toastService.presentToast("Please enable location services to continue");

            console.log('Error requesting location permissions ' + JSON.stringify(error));
          });
        }
      }, {
        key: "fc",
        get: function get() {
          return this.preTripForm.controls;
        }
      }]);

      return RouteDetailsPage;
    }();

    RouteDetailsPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]
      }, {
        type: _shared_services_routing_service__WEBPACK_IMPORTED_MODULE_4__["RoutingService"]
      }, {
        type: _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"]
      }, {
        type: _shared_services_delivery_service__WEBPACK_IMPORTED_MODULE_6__["DeliveryService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
      }, {
        type: _shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_8__["ScheduleService"]
      }, {
        type: _shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__["AlertService"]
      }, {
        type: _shared_services_toast_service__WEBPACK_IMPORTED_MODULE_10__["ToastService"]
      }, {
        type: _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_11__["SapApiService"]
      }, {
        type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_12__["LaunchNavigator"]
      }, {
        type: _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_13__["LaunchNavigatorService"]
      }, {
        type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_14__["AndroidPermissions"]
      }, {
        type: _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_15__["LocationAccuracy"]
      }];
    };

    RouteDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-route-details',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./route-details.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/routes/route-details/route-details.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./route-details.page.scss */
      "./src/app/routes/route-details/route-details.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _shared_services_routing_service__WEBPACK_IMPORTED_MODULE_4__["RoutingService"], _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"], _shared_services_delivery_service__WEBPACK_IMPORTED_MODULE_6__["DeliveryService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"], _shared_services_schedule_service__WEBPACK_IMPORTED_MODULE_8__["ScheduleService"], _shared_services_alert_service__WEBPACK_IMPORTED_MODULE_9__["AlertService"], _shared_services_toast_service__WEBPACK_IMPORTED_MODULE_10__["ToastService"], _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_11__["SapApiService"], _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_12__["LaunchNavigator"], _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_13__["LaunchNavigatorService"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_14__["AndroidPermissions"], _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_15__["LocationAccuracy"] // private geofence: Geofence
    // private spherical: Spherical
    ])], RouteDetailsPage);
    /***/
  },

  /***/
  "./src/shared/services/delivery.service.ts":
  /*!*************************************************!*\
    !*** ./src/shared/services/delivery.service.ts ***!
    \*************************************************/

  /*! exports provided: DeliveryService */

  /***/
  function srcSharedServicesDeliveryServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeliveryService", function () {
      return DeliveryService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var DeliveryService =
    /*#__PURE__*/
    function () {
      function DeliveryService(http) {
        _classCallCheck(this, DeliveryService);

        this.http = http;
      }

      _createClass(DeliveryService, [{
        key: "getUserDeliveries",
        value: function getUserDeliveries(routeId) {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.deliveryURL + "?mode=booking&routeId=".concat(routeId), this.getHeaders());
        }
      }, {
        key: "getUserBookingDetails",
        value: function getUserBookingDetails(routeId) {
          return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].services.userBookingDetailsURL + "?routeId=".concat(routeId), this.getHeaders());
        }
      }, {
        key: "getHeaders",
        value: function getHeaders() {
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
              'Authorization': 'Bearer ' + localStorage.getItem('token'),
              'Content-type': 'application/json'
            })
          };
          return httpOptions;
        }
      }]);

      return DeliveryService;
    }();

    DeliveryService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }];
    };

    DeliveryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])], DeliveryService);
    /***/
  }
}]);
//# sourceMappingURL=routes-route-details-route-details-module-es5.js.map