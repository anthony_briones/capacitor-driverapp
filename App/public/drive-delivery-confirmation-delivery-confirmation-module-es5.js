function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["drive-delivery-confirmation-delivery-confirmation-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-confirmation/delivery-confirmation.page.html":
  /*!*******************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-confirmation/delivery-confirmation.page.html ***!
    \*******************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDriveDeliveryConfirmationDeliveryConfirmationPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Delivery Confirmation</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-grid *ngIf=\"isCynchOrder\">\n    <ion-row *ngIf=\"!image\">\n      <ion-col size=\"12\">\n        <div (click)=\"attachPhoto()\" class=\"ion-padding add-photo-div ion-text-center\">\n          <ion-icon class=\"camera-icon\" name=\"camera\"></ion-icon>\n          <p>Add Photo Confirmation</p>\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf=\"image\">\n      <ion-col size=\"12\" class=\"vertical-align horizontal-align\"> \n        <div (click)=\"attachPhoto()\" class=\"photo-size ion-text-center vertical-align horizontal-align\">\n          <img class=\"photo-size vertical-align horizontal-align\" src=\"{{image}}\">\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"ion-padding\">\n      <ion-col size=\"12\">\n        <p class=\"subtitle-text\">NOTES</p>\n        <ion-input class=\"content-text\" placeholder=\"Add any special notes about the delivery here\"></ion-input>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n  <ion-grid *ngIf=\"!isCynchOrder\">\n    <ion-row class=\"ion-padding\">\n      <ion-col size=\"12\">\n        <p class=\"subtitle-text\">NOTES</p>\n        <ion-input class=\"content-text\" placeholder=\"Add any special notes about the delivery here\"></ion-input>\n      </ion-col>\n    </ion-row>\n\n    <!-- <ion-row class=\"ion-padding\">\n      <ion-col size=\"12\">\n        <p class=\"subtitle-text\">JOB CODE</p>\n        <ion-input class=\"content-text\" placeholder=\"Enter Code\"></ion-input>\n      </ion-col>\n    </ion-row> -->\n\n    <ion-row class=\"ion-padding\">\n      <ion-col size=\"12\">\n        <p class=\"subtitle-text\">PO NUMBER</p>\n        <ion-input class=\"content-text\" placeholder=\"Enter PO\"></ion-input>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"ion-padding\">\n      <ion-col size=\"12\" *ngIf=\"!signatureImage\">\n        <p class=\"subtitle-text\">CUSTOMER SIGNATURE</p>\n        <ion-input class=\"content-text signage\" placeholder=\"Tap to sign\" (click)=\"openSignatureModel()\"></ion-input>\n      </ion-col>\n      <ion-col size=\"12\" *ngIf=\"signatureImage\">\n        <ion-img [src]=\"signatureImage\"></ion-img>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n\n  <!-- <div class=\"ion-padding\">\n    <button (click)=\"attachPhoto()\"> Attach Photo</button>\n  </div> -->\n\n  <div class=\"ion-padding\">\n    <button class=\"finalizeDelivery\" (click)=\"nextDelivery()\"> Finalize Delivery</button>\n  </div>\n\n  <!-- <div class=\"ion-padding\">\n    <button class=\"cancelDelivery\" (click)=\"goToCancelDelivery()\">End Delivery</button>\n  </div> -->\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/drive/delivery-confirmation/delivery-confirmation.module.ts":
  /*!*****************************************************************************!*\
    !*** ./src/app/drive/delivery-confirmation/delivery-confirmation.module.ts ***!
    \*****************************************************************************/

  /*! exports provided: DeliveryConfirmationPageModule */

  /***/
  function srcAppDriveDeliveryConfirmationDeliveryConfirmationModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeliveryConfirmationPageModule", function () {
      return DeliveryConfirmationPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _delivery_confirmation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./delivery-confirmation.page */
    "./src/app/drive/delivery-confirmation/delivery-confirmation.page.ts");

    var routes = [{
      path: '',
      component: _delivery_confirmation_page__WEBPACK_IMPORTED_MODULE_6__["DeliveryConfirmationPage"]
    }];

    var DeliveryConfirmationPageModule = function DeliveryConfirmationPageModule() {
      _classCallCheck(this, DeliveryConfirmationPageModule);
    };

    DeliveryConfirmationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_delivery_confirmation_page__WEBPACK_IMPORTED_MODULE_6__["DeliveryConfirmationPage"]]
    })], DeliveryConfirmationPageModule);
    /***/
  },

  /***/
  "./src/app/drive/delivery-confirmation/delivery-confirmation.page.scss":
  /*!*****************************************************************************!*\
    !*** ./src/app/drive/delivery-confirmation/delivery-confirmation.page.scss ***!
    \*****************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppDriveDeliveryConfirmationDeliveryConfirmationPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nbutton {\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n}\n\n.cancelDelivery {\n  background: #003A58;\n}\n\n.finalizeDelivery {\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036));\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%);\n}\n\n.add-photo-div {\n  margin-top: 1rem;\n  margin-left: auto;\n  margin-right: auto;\n  display: block;\n  height: 9.375rem;\n  width: 15.625rem;\n  border: 1px solid #003A58;\n}\n\n.photo-div {\n  height: 9.375rem;\n  width: 100%;\n}\n\n.photo-size {\n  margin-top: 1rem;\n  margin-left: auto;\n  margin-right: auto;\n  width: 75%;\n  height: auto;\n}\n\n.camera-icon {\n  height: 5rem;\n  width: 5rem;\n}\n\n.subtitle-text {\n  padding: 0 0 0.2rem 0;\n  margin: 0;\n  font-size: 0.8rem;\n  color: #4b4b4b;\n  font-family: \"Montserrat-SemiBold\";\n}\n\n.content-text {\n  padding: 0;\n  margin: 0;\n  font-size: 0.875rem;\n  font-family: \"Montserrat-Regular\";\n}\n\n.signage {\n  border: 1px solid #E3E3E5;\n  text-align: center;\n  font-size: 1.5rem;\n  height: 100px;\n  box-shadow: 1px 1px #E9E9E9;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL2RyaXZlL2RlbGl2ZXJ5LWNvbmZpcm1hdGlvbi9kZWxpdmVyeS1jb25maXJtYXRpb24ucGFnZS5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2NzcyIsIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXBwL2RyaXZlL2RlbGl2ZXJ5LWNvbmZpcm1hdGlvbi9kZWxpdmVyeS1jb25maXJtYXRpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU9BO0VBQ0kseUJBQUE7RUFDQSxnREFBQTtBQ05KOztBRFNBO0VBQ0kseUJBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkVsQmM7RUZtQmQsV0FBQTtFQUNBLGNFaEJJO0VGaUJKLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSwrQkFBQTtFQUFBLHdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxvQ0FBQTtVQUFBLDhCQUFBO0FDTko7O0FEU0E7RUFDSSxtQ0FBQTtVQUFBLGtDQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtVQUFBLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx3QkFBQTtVQUFBLHVCQUFBO0FDTko7O0FEU0E7RUFDSSxZQUFBO0FDTko7O0FFL0NBO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FGa0RKOztBRS9DQTtFQUNJLG1CQUFBO0FGa0RKOztBRS9DQTtFQUNJLDZGRFJnQjtFQ1FoQixvRURSZ0I7QUQwRHBCOztBRS9DQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7QUZrREo7O0FFL0NBO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0FGa0RKOztBRS9DQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0FGa0RKOztBRTlDQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0FGaURKOztBRTlDQTtFQUNJLHFCQUFBO0VBQ0EsU0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGtDQUFBO0FGaURKOztBRTlDQTtFQUNJLFVBQUE7RUFDQSxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQ0FBQTtBRmlESjs7QUU5Q0E7RUFDSSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsMkJBQUE7QUZpREoiLCJmaWxlIjoic3JjL2FwcC9kcml2ZS9kZWxpdmVyeS1jb25maXJtYXRpb24vZGVsaXZlcnktY29uZmlybWF0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy9mb250cyc7XG5AaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvdmFyaWFibGVzJztcblxuLy8gLmlzLXZhbGlkIHtcbi8vICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlXG4vLyB9XG5cbi5pcy1pbnZhbGlkIHtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjRUU0MDM2O1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcInNyYy9hc3NldHMvaWNvbi9pY29uLWhlbHAuc3ZnXCIpIG5vLXJlcGVhdDtcbn1cblxuLmlzLXZhbGlkIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjNUVCMDAzXG59XG5cbi5pcy1pbnZhbGlkLXNlbGVjdCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0VFNDAzNjtcbn1cblxuLnByaW1hcnktYnV0dG9uIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvci0xO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiAkd2hpdGU7XG4gICAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGhlaWdodDogMnJlbTtcbn1cblxuLmJnLWNvbG9yLTF7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4O1xufVxuXG5pb24tdG9vbGJhcntcbiAgICAtLWJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi52ZXJ0aWNhbC1hbGlnbiB7XG4gICAgZGlzcGxheTogZmxleCFpbXBvcnRhbnQ7XG4gICAgYWxpZ24tY29udGVudDogY2VudGVyIWltcG9ydGFudDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyIWltcG9ydGFudDtcbn1cblxuLmhvcml6b250YWwtYWxpZ24ge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWFsaWduLXJpZ2h0IHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uZmxleC1hbGlnbi1jZW50ZXIge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZGlzYWJsZSB7XG4gICAgb3BhY2l0eTogMC41O1xufVxuXG5cblxuXG4iLCIuaXMtaW52YWxpZCB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNFRTQwMzY7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcInNyYy9hc3NldHMvaWNvbi9pY29uLWhlbHAuc3ZnXCIpIG5vLXJlcGVhdDtcbn1cblxuLmlzLXZhbGlkIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzVFQjAwMztcbn1cblxuLmlzLWludmFsaWQtc2VsZWN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgI0VFNDAzNjtcbn1cblxuLnByaW1hcnktYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwM0E1ODtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgaGVpZ2h0OiAycmVtO1xufVxuXG4uYmctY29sb3ItMSB7XG4gIC0tYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuaW9uLXRvb2xiYXIge1xuICAtLWJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi52ZXJ0aWNhbC1hbGlnbiB7XG4gIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmhvcml6b250YWwtYWxpZ24ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1hbGlnbi1yaWdodCB7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5mbGV4LWFsaWduLWNlbnRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZGlzYWJsZSB7XG4gIG9wYWNpdHk6IDAuNTtcbn1cblxuYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xuICBoZWlnaHQ6IDNyZW07XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG59XG5cbi5jYW5jZWxEZWxpdmVyeSB7XG4gIGJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbi5maW5hbGl6ZURlbGl2ZXJ5IHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KC0xODBkZWcsICNGRjg4NDAgMCUsICNFRTQwMzYgMTAwJSk7XG59XG5cbi5hZGQtcGhvdG8tZGl2IHtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgZGlzcGxheTogYmxvY2s7XG4gIGhlaWdodDogOS4zNzVyZW07XG4gIHdpZHRoOiAxNS42MjVyZW07XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDNBNTg7XG59XG5cbi5waG90by1kaXYge1xuICBoZWlnaHQ6IDkuMzc1cmVtO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLnBob3RvLXNpemUge1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICB3aWR0aDogNzUlO1xuICBoZWlnaHQ6IGF1dG87XG59XG5cbi5jYW1lcmEtaWNvbiB7XG4gIGhlaWdodDogNXJlbTtcbiAgd2lkdGg6IDVyZW07XG59XG5cbi5zdWJ0aXRsZS10ZXh0IHtcbiAgcGFkZGluZzogMCAwIDAuMnJlbSAwO1xuICBtYXJnaW46IDA7XG4gIGZvbnQtc2l6ZTogMC44cmVtO1xuICBjb2xvcjogIzRiNGI0YjtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xufVxuXG4uY29udGVudC10ZXh0IHtcbiAgcGFkZGluZzogMDtcbiAgbWFyZ2luOiAwO1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0LVJlZ3VsYXJcIjtcbn1cblxuLnNpZ25hZ2Uge1xuICBib3JkZXI6IDFweCBzb2xpZCAjRTNFM0U1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBib3gtc2hhZG93OiAxcHggMXB4ICNFOUU5RTk7XG59IiwiXG4vLyBjb2xvcnNcbi8vICRwcmltYXJ5LWNvbG9yLTE6ICMwMEEzQzg7XG4kcHJpbWFyeS1jb2xvci0xOiAjMDAzQTU4O1xuJHByaW1hcnktY29sb3ItMjogI0ZBQUY0MDtcbiRwcmltYXJ5LWNvbG9yLTM6ICNGNDc3M0I7XG5cbiR3aGl0ZTogI2ZmZmZmZjtcblxuLy8gJGJ1dHRvbi1ncmFkaWVudC0xOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCByZ2JhKDI1NSwyNTUsMjU1LDAuNTApIDAlLCByZ2JhKDAsMCwwLDAuNTApIDEwMCUpO1xuJGJ1dHRvbi1ncmFkaWVudC0xOiBsaW5lYXItZ3JhZGllbnQoLTE4MGRlZywgI0ZGODg0MCAwJSwgI0VFNDAzNiAxMDAlKTsiLCJAaW1wb3J0ICdzcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MnO1xuQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzJztcblxuXG5idXR0b257XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgICBoZWlnaHQ6IDNyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1Cb2xkJ1xufVxuXG4uY2FuY2VsRGVsaXZlcnl7XG4gICAgYmFja2dyb3VuZDogIzAwM0E1ODtcbn1cblxuLmZpbmFsaXplRGVsaXZlcnl7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogJGJ1dHRvbi1ncmFkaWVudC0xO1xufVxuXG4uYWRkLXBob3RvLWRpdntcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBoZWlnaHQ6IDkuMzc1cmVtO1xuICAgIHdpZHRoOiAxNS42MjVyZW07XG4gICAgYm9yZGVyOiAxcHggc29saWQgJHByaW1hcnktY29sb3ItMTtcbn1cblxuLnBob3RvLWRpdntcbiAgICBoZWlnaHQ6IDkuMzc1cmVtO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4ucGhvdG8tc2l6ZSB7XG4gICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IDc1JTtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgLy8gZGlzcGxheTogYmxvY2s7XG59XG5cbi5jYW1lcmEtaWNvbntcbiAgICBoZWlnaHQ6IDVyZW07XG4gICAgd2lkdGg6IDVyZW07XG59XG5cbi5zdWJ0aXRsZS10ZXh0e1xuICAgIHBhZGRpbmc6IDAgMCAuMnJlbSAwO1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LXNpemU6IC44cmVtO1xuICAgIGNvbG9yOiAjNGI0YjRiO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG59XG5cbi5jb250ZW50LXRleHR7XG4gICAgcGFkZGluZzogMDtcbiAgICBtYXJnaW46IDA7XG4gICAgZm9udC1zaXplOiAuODc1cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1SZWd1bGFyJztcbn1cblxuLnNpZ25hZ2Uge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFM0UzRTU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgYm94LXNoYWRvdzogMXB4IDFweCAjRTlFOUU5O1xuXG59Il19 */";
    /***/
  },

  /***/
  "./src/app/drive/delivery-confirmation/delivery-confirmation.page.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/drive/delivery-confirmation/delivery-confirmation.page.ts ***!
    \***************************************************************************/

  /*! exports provided: DeliveryConfirmationPage */

  /***/
  function srcAppDriveDeliveryConfirmationDeliveryConfirmationPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeliveryConfirmationPage", function () {
      return DeliveryConfirmationPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../node_modules/@angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _node_modules_ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../node_modules/@ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/ngx/index.js");
    /* harmony import */


    var _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../shared/services/sap-api.service */
    "./src/shared/services/sap-api.service.ts");
    /* harmony import */


    var _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../../node_modules/@ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../shared/services/loader.service */
    "./src/shared/services/loader.service.ts");
    /* harmony import */


    var _signature_signature_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../signature/signature.page */
    "./src/app/drive/signature/signature.page.ts");
    /* harmony import */


    var src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/shared/services/toast.service */
    "./src/shared/services/toast.service.ts");

    var DeliveryConfirmationPage =
    /*#__PURE__*/
    function () {
      // @Input signatureImage: string;
      function DeliveryConfirmationPage(router, camera, sapApiService, nav, loaderService, modalController, toastService // private navParams: NavParams,
      ) {
        _classCallCheck(this, DeliveryConfirmationPage);

        this.router = router;
        this.camera = camera;
        this.sapApiService = sapApiService;
        this.nav = nav;
        this.loaderService = loaderService;
        this.modalController = modalController;
        this.toastService = toastService;
        this.deliveries = []; // this.signatureImage = this.navParams.get('signatureImage');
      }

      _createClass(DeliveryConfirmationPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.currentDelivery = JSON.parse(localStorage.getItem("current_delivery"));
          this.stopSequenceNum = parseInt(localStorage.getItem("stop_sequence"));
          this.stopsLength = parseInt(localStorage.getItem("stops_length"));
          this.currentRoute = JSON.parse(localStorage.getItem("current_route_started")); // this.currentRoute.startTime = "10:00";

          this.getTankType();
          console.log("Current Delivery 2", this.currentDelivery);
          console.log("Current Route 2", this.currentRoute);
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          console.log("checking storage"); // this.signatureImage = this.navParams.get('signatureImage');

          if (localStorage.getItem("signage")) {
            this.signatureImage = localStorage.getItem("signage");
            console.log("you had signed", this.signatureImage);
          }
        }
      }, {
        key: "nextDelivery",
        value: function nextDelivery() {
          // if (this.image) {
          this.stopSequenceNum = this.stopSequenceNum + 1;

          if (this.stopsLength < this.stopSequenceNum) {
            this.updateDelivery(); // this.clearFinishedDelivery();
          } else {
            this.updateDelivery();
          } // } else {
          //   if (this.isCynchOrder) {
          //     this.toastService.presentToast("Image is required.");
          //   } else {
          //     this.toastService.presentToast("Signature is required");
          //   }
          // }

        }
      }, {
        key: "showSummary",
        value: function showSummary() {
          var routeId = localStorage.getItem("route_id");
          this.router.navigate(["/tabs/drive/".concat(routeId, "/delivery-summary")]);
        }
      }, {
        key: "clearFinishedDelivery",
        value: function clearFinishedDelivery() {
          localStorage.removeItem("current_delivery");
          localStorage.removeItem("stop_sequence");
          localStorage.removeItem("route_id");
          localStorage.removeItem("stops_length");
          localStorage.removeItem("mileage");
          localStorage.removeItem("current_route_started");
          localStorage.removeItem("material_code");
          localStorage.removeItem("start_time");
          localStorage.removeItem("signage");
        }
      }, {
        key: "attachPhoto",
        value: function attachPhoto() {
          var _this = this;

          this.image = "";
          var options = {
            quality: 100,
            targetWidth: 900,
            targetHeight: 600,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
            allowEdit: true,
            sourceType: 1
          };
          this.camera.getPicture(options).then(function (imageData) {
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            _this.image = base64Image;
            console.log(base64Image);
          }, function (err) {// Handle error
          });
        }
      }, {
        key: "updateDelivery",
        value: function updateDelivery() {
          var _this2 = this;

          // this.items = JSON.parse(localStorage.getItem("material_code"));
          this.loaderService.createLoader("Submitting...");
          var editedDelivery = JSON.parse(JSON.stringify(this.currentDelivery)); // let transferedItem: any
          // for (let item of editedDelivery.items) {
          //   if (item.materialCode === "CYL-204") {
          //     transferedItem = JSON.parse(JSON.stringify(item));
          //     transferedItem.transferType = "2";
          //   }
          // }
          // editedDelivery.items.push(transferedItem);

          console.log("Current Delivery", editedDelivery);
          console.log("Current Route", this.currentRoute);
          editedDelivery.startTime = localStorage.getItem("start_time");
          editedDelivery.endTime = this.sapApiService.getConfirmationDateTime();
          editedDelivery.deliveryStatus.deliveryStatusId = "001";
          var tankDelivered = 0;
          var tankRemaining;
          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = editedDelivery.items[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var item = _step.value;
              tankDelivered += item.quantityDelivered;
            } // tankRemaining = editedDelivery.totalRemainingCyls - tankDelivered;
            // editedDelivery.totalRemainingCyls = tankRemaining;

          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }

          if (localStorage.getItem("mileage")) {
            editedDelivery.mileage = parseFloat(localStorage.getItem("mileage"));
          } else {
            //Edited only due to testing please return to 0 when making a build
            editedDelivery.mileage = 27; // editedDelivery.mileage = 0;
          }

          if (this.image) {
            this.sapApiService.uploadPhoto(this.currentDelivery.sapOrderId, this.image).subscribe(function (result) {
              console.log(result.message);

              _this2.sapApiService.updateDelivery(editedDelivery, _this2.currentRoute).subscribe(function (result) {
                console.log("Update Delivery", result); // this.currentRoute.totalRemainingCyls = tankRemaining;

                localStorage.setItem("current_route_started", JSON.stringify(_this2.currentRoute));
                localStorage.setItem("stop_sequence", _this2.stopSequenceNum.toString());
                _this2.stopsLength = result.stopsLength;
                localStorage.setItem("stops_length", _this2.stopsLength.toString()); // if (this.stopsLength < this.stopSequenceNum) {
                //   this.sapApiService.submitRoute(this.currentRoute.routeId, editedDelivery.endTime).subscribe(
                //     result => {
                //       console.log(result);
                //       this.clearFinishedDelivery();
                //     },
                //     error => {
                //       console.log("Error", error);
                //       this.loaderService.dismissLoader();
                //     }
                //   )
                // }

                var location = {
                  latitude: 0,
                  longitude: 0
                };
                location.latitude = editedDelivery.latitude;
                location.longitude = editedDelivery.longitude;
                localStorage.setItem("previous_loc", JSON.stringify(location));

                if (_this2.stopsLength < _this2.stopSequenceNum) {
                  // let routeId = localStorage.getItem("route_id");
                  // localStorage.removeItem("previous_loc");
                  _this2.router.navigate(["/tabs/drive/".concat(_this2.currentRoute.routeId, "/delivery-summary")]);
                } else {
                  localStorage.removeItem("start_time");
                  localStorage.removeItem('signage'); // let location = {
                  //   latitude: 0,
                  //   longitude: 0
                  // }
                  // location.latitude = editedDelivery.latitude;
                  // location.longitude = editedDelivery.longitude;
                  // localStorage.setItem("previous_loc", JSON.stringify(location));

                  _this2.router.navigate(["/tabs/drive"]);
                } // this.router.navigate([`/tabs/drive`]);


                _this2.loaderService.dismissLoader();
              }, function (error) {
                console.log("Error", error);

                _this2.loaderService.dismissLoader();
              });
            }, function (error) {
              console.log(error.message);
            });
          } else {
            this.sapApiService.updateDelivery(editedDelivery, this.currentRoute).subscribe(function (result) {
              console.log("Update Delivery", result); // this.currentRoute.totalRemainingCyls = tankRemaining;

              localStorage.setItem("current_route_started", JSON.stringify(_this2.currentRoute));
              localStorage.setItem("stop_sequence", _this2.stopSequenceNum.toString());
              _this2.stopsLength = result.stopsLength;
              localStorage.setItem("stops_length", _this2.stopsLength.toString()); // if (this.stopsLength < this.stopSequenceNum) {
              //   this.sapApiService.submitRoute(this.currentRoute.routeId, editedDelivery.endTime).subscribe(
              //     result => {
              //       console.log(result);
              //       this.clearFinishedDelivery();
              //     },
              //     error => {
              //       console.log("Error", error);
              //       this.loaderService.dismissLoader();
              //     }
              //   )
              // }

              var location = {
                latitude: 0,
                longitude: 0
              };
              location.latitude = editedDelivery.latitude;
              location.longitude = editedDelivery.longitude;
              localStorage.setItem("previous_loc", JSON.stringify(location));

              if (_this2.stopsLength < _this2.stopSequenceNum) {
                // let routeId = localStorage.getItem("route_id");
                // localStorage.removeItem("previous_loc");
                _this2.router.navigate(["/tabs/drive/".concat(_this2.currentRoute.routeId, "/delivery-summary")]);
              } else {
                localStorage.removeItem("start_time");
                localStorage.removeItem('signage'); // let location = {
                //   latitude: 0,
                //   longitude: 0
                // }
                // location.latitude = editedDelivery.latitude;
                // location.longitude = editedDelivery.longitude;
                // localStorage.setItem("previous_loc", JSON.stringify(location));

                _this2.router.navigate(["/tabs/drive"]);
              } // this.router.navigate([`/tabs/drive`]);


              _this2.loaderService.dismissLoader();
            }, function (error) {
              console.log("Error", error);

              _this2.loaderService.dismissLoader();
            });
          }
        }
      }, {
        key: "getTankType",
        value: function getTankType() {
          if (this.currentDelivery.orderSource.toUpperCase() === "CYNCH") {
            this.isCynchOrder = true;
          } else {
            this.isCynchOrder = false;
          }
        } // async openSignatureModel() {
        //   const modal = await this.modalController.create({
        //     component: SignaturePage
        //     // cssClass: 'custom-modal'
        //   });
        //   return await modal.present();
        // }

      }, {
        key: "openSignatureModel",
        value: function openSignatureModel() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            var _this3 = this;

            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.modalController.create({
                      component: _signature_signature_page__WEBPACK_IMPORTED_MODULE_7__["SignaturePage"]
                    });

                  case 2:
                    modal = _context.sent;
                    modal.onDidDismiss().then(function (detail) {
                      if (detail !== null) {
                        _this3.signatureImage = detail.data;
                        _this3.image = detail.data;
                      } // console.log("this is image modal");

                    });
                    _context.next = 6;
                    return modal.present();

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "goToCancelDelivery",
        value: function goToCancelDelivery() {
          this.router.navigate(["/tabs/drive/".concat(this.currentRoute.routeId, "/delivery-cancel")]);
        }
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ionViewDidEnter();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }]);

      return DeliveryConfirmationPage;
    }();

    DeliveryConfirmationPage.ctorParameters = function () {
      return [{
        type: _node_modules_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _node_modules_ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"]
      }, {
        type: _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_4__["SapApiService"]
      }, {
        type: _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
      }, {
        type: _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]
      }, {
        type: _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]
      }, {
        type: src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]
      }];
    };

    DeliveryConfirmationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-delivery-confirmation',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./delivery-confirmation.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/drive/delivery-confirmation/delivery-confirmation.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./delivery-confirmation.page.scss */
      "./src/app/drive/delivery-confirmation/delivery-confirmation.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_node_modules_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _node_modules_ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"], _shared_services_sap_api_service__WEBPACK_IMPORTED_MODULE_4__["SapApiService"], _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _shared_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"], _node_modules_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"], src_shared_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"] // private navParams: NavParams,
    ])], DeliveryConfirmationPage);
    /***/
  }
}]);
//# sourceMappingURL=drive-delivery-confirmation-delivery-confirmation-module-es5.js.map