function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["settings-navigation-settings-navigation-settings-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/navigation-settings/navigation-settings.page.html":
  /*!******************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/settings/navigation-settings/navigation-settings.page.html ***!
    \******************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSettingsNavigationSettingsNavigationSettingsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <!-- <ion-buttons slot=\"start\"> -->\n      <ion-back-button class=\"float-left\"></ion-back-button>\n      <!-- </ion-buttons> -->\n    <!-- </ion-buttons> -->\n    <ion-title size=\"large\">Navigation</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-list>\n    <ion-list-header class=\"navigation-header\">\n      <ion-label>\n        Select your default navigation app.\n      </ion-label>\n    </ion-list-header>\n\n    <ion-item lines=\"none\" *ngIf=\"availableApps?.waze\" (click)=\"setDefaultNavigationApp('waze')\">\n      <ion-row class=\"navigation-row\">\n        <ion-col size=\"2\">\n            <img src=\"../../../assets/icon/icon-waze@3x.png\">\n        </ion-col>\n        <ion-col class=\"navigation-label\" size=\"10\">\n          Waze\n        </ion-col>\n      </ion-row>\n      <ion-checkbox mode=\"ios\" slot=\"end\" [(ngModel)]=\"isWaze\">\n        </ion-checkbox>\n    </ion-item>\n\n    <ion-item lines=\"none\" *ngIf=\"availableApps?.google_maps\" (click)=\"setDefaultNavigationApp('google_maps')\">\n      <ion-row class=\"navigation-row\">\n        <ion-col size=\"2\">\n            <img src=\"../../../assets/icon/icon-google-maps@3x.png\">\n        </ion-col>\n        <ion-col class=\"navigation-label\" size=\"10\">\n          Google Maps\n        </ion-col>\n      </ion-row>\n      <ion-checkbox mode=\"ios\" slot=\"end\" [(ngModel)]=\"isGoogleMaps\">\n        </ion-checkbox>\n    </ion-item>\n\n    <ion-item lines=\"none\" *ngIf=\"availableApps?.apple_maps\" (click)=\"setDefaultNavigationApp('apple_maps')\">\n      <ion-row class=\"navigation-row\">\n        <ion-col size=\"2\">\n            <img src=\"../../../assets/icon/icon-apple-maps@3x.png\">\n        </ion-col>\n        <ion-col class=\"navigation-label\" size=\"10\">\n          Apple Maps\n        </ion-col>\n      </ion-row>\n      <ion-checkbox mode=\"ios\" slot=\"end\" [(ngModel)]=\"isAppleMaps\">\n        </ion-checkbox>\n    </ion-item>\n\n    <!-- <ion-item lines=\"none\" *ngIf=\"availableApps?.citymapper\" (click)=\"setDefaultNavigationApp('citymapper')\">\n      <ion-row class=\"navigation-row\">\n        <ion-col size=\"2\">\n            <img src=\"../../../assets/icon/icon-tank-amerigas-white@3x.png@3x.png\">\n        </ion-col>\n        <ion-col class=\"navigation-label\" size=\"10\">\n          CityMapper\n        </ion-col>\n      </ion-row>\n      <ion-checkbox mode=\"ios\" slot=\"end\" [(ngModel)]=\"isCityMapper\">\n        </ion-checkbox>\n    </ion-item> -->\n\n  </ion-list>\n\n  <ion-row>\n      <ion-col size=\"12\" class=\"settings-button shadow\">\n        <button class=\"btn shadow-lg save-btn\" icon-only round ion-button (click)=\"saveSelectedMap()\">\n          Save Changes\n        </button>\n      </ion-col>\n    </ion-row>\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/settings/navigation-settings/navigation-settings.module.ts":
  /*!****************************************************************************!*\
    !*** ./src/app/settings/navigation-settings/navigation-settings.module.ts ***!
    \****************************************************************************/

  /*! exports provided: NavigationSettingsPageModule */

  /***/
  function srcAppSettingsNavigationSettingsNavigationSettingsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NavigationSettingsPageModule", function () {
      return NavigationSettingsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _navigation_settings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./navigation-settings.page */
    "./src/app/settings/navigation-settings/navigation-settings.page.ts");

    var routes = [{
      path: '',
      component: _navigation_settings_page__WEBPACK_IMPORTED_MODULE_6__["NavigationSettingsPage"]
    }];

    var NavigationSettingsPageModule = function NavigationSettingsPageModule() {
      _classCallCheck(this, NavigationSettingsPageModule);
    };

    NavigationSettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_navigation_settings_page__WEBPACK_IMPORTED_MODULE_6__["NavigationSettingsPage"]]
    })], NavigationSettingsPageModule);
    /***/
  },

  /***/
  "./src/app/settings/navigation-settings/navigation-settings.page.scss":
  /*!****************************************************************************!*\
    !*** ./src/app/settings/navigation-settings/navigation-settings.page.scss ***!
    \****************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSettingsNavigationSettingsNavigationSettingsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".is-invalid {\n  border: 2px solid #EE4036;\n  background-image: url('icon-help.svg') no-repeat;\n}\n\n.is-valid {\n  border: 1px solid #5EB003;\n}\n\n.is-invalid-select {\n  border: 1px solid #EE4036;\n}\n\n.primary-button {\n  background-color: #003A58;\n  width: 100%;\n  color: #ffffff;\n  border-radius: 1rem;\n  text-align: center;\n  font-size: 1rem;\n  height: 2rem;\n}\n\n.bg-color-1 {\n  --background: #003A58;\n}\n\nion-toolbar {\n  --background: #ffffff;\n}\n\n.vertical-align {\n  display: -webkit-box !important;\n  display: flex !important;\n  align-content: center !important;\n  -webkit-box-align: center !important;\n          align-items: center !important;\n}\n\n.horizontal-align {\n  -webkit-box-pack: center !important;\n          justify-content: center !important;\n}\n\n.flex-align-right {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n}\n\n.flex-align-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.disable {\n  opacity: 0.5;\n}\n\nion-title {\n  color: #003A58;\n  font-family: \"Montserrat-Bold\";\n  padding-top: 1rem;\n}\n\nion-buttons {\n  padding-bottom: 3rem;\n}\n\n.float-left {\n  float: left;\n  margin-bottom: 3rem;\n}\n\n.navigation-row {\n  width: 100%;\n}\n\n.navigation-header {\n  background: #F2F2F2;\n}\n\n.navigation-label {\n  margin-top: 0.5rem;\n  margin-bottom: 0.5rem;\n  font-family: \"Montserrat-SemiBold\";\n  font-size: 18px;\n}\n\n.item-icon {\n  width: 1.5rem;\n  height: 1.5rem;\n}\n\n.save-btn {\n  background: #F4773B 100% !important;\n  background-image: -webkit-gradient(linear, left top, left bottom, from(#FF8840), to(#EE4036)) !important;\n  background-image: linear-gradient(-180deg, #FF8840 0%, #EE4036 100%) !important;\n  width: 100%;\n  font-size: 1.125rem;\n  border-radius: 0.375rem;\n  height: 3rem;\n  color: white;\n  font-family: \"Montserrat-Bold\";\n  line-height: 1.3125rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbnRob255LmJyaW9uZXMvRG9jdW1lbnRzL0NhcERyaXZlci9zcmMvYXNzZXRzL3Njc3MvY29tbW9uLnNjc3MiLCJzcmMvYXBwL3NldHRpbmdzL25hdmlnYXRpb24tc2V0dGluZ3MvbmF2aWdhdGlvbi1zZXR0aW5ncy5wYWdlLnNjc3MiLCIvVXNlcnMvYW50aG9ueS5icmlvbmVzL0RvY3VtZW50cy9DYXBEcml2ZXIvc3JjL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlcy5zY3NzIiwiL1VzZXJzL2FudGhvbnkuYnJpb25lcy9Eb2N1bWVudHMvQ2FwRHJpdmVyL3NyYy9hcHAvc2V0dGluZ3MvbmF2aWdhdGlvbi1zZXR0aW5ncy9uYXZpZ2F0aW9uLXNldHRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFPQTtFQUNJLHlCQUFBO0VBQ0EsZ0RBQUE7QUNOSjs7QURTQTtFQUNJLHlCQUFBO0FDTko7O0FEU0E7RUFDSSx5QkFBQTtBQ05KOztBRFNBO0VBQ0kseUJFbEJjO0VGbUJkLFdBQUE7RUFDQSxjRWhCSTtFRmlCSixtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUNOSjs7QURTQTtFQUNJLHFCQUFBO0FDTko7O0FEU0E7RUFDSSxxQkFBQTtBQ05KOztBRFNBO0VBQ0ksK0JBQUE7RUFBQSx3QkFBQTtFQUNBLGdDQUFBO0VBQ0Esb0NBQUE7VUFBQSw4QkFBQTtBQ05KOztBRFNBO0VBQ0ksbUNBQUE7VUFBQSxrQ0FBQTtBQ05KOztBRFNBO0VBQ0kscUJBQUE7VUFBQSx5QkFBQTtBQ05KOztBRFNBO0VBQ0ksd0JBQUE7VUFBQSx1QkFBQTtBQ05KOztBRFNBO0VBQ0ksWUFBQTtBQ05KOztBRS9DQTtFQUNJLGNERmM7RUNHZCw4QkFBQTtFQUNBLGlCQUFBO0FGa0RKOztBRS9DQTtFQUVJLG9CQUFBO0FGaURKOztBRTlDQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtBRmlESjs7QUU5Q0E7RUFDSSxXQUFBO0FGaURKOztBRTlDQTtFQUNJLG1CQUFBO0FGaURKOztBRTlDQTtFQUNJLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQ0FBQTtFQUNBLGVBQUE7QUZpREo7O0FFOUNBO0VBQ0ksYUFBQTtFQUNBLGNBQUE7QUZpREo7O0FFOUNBO0VBQ0ksbUNBQUE7RUFDQSx3R0FBQTtFQUFBLCtFQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0VBQ0Esc0JBQUE7QUZpREoiLCJmaWxlIjoic3JjL2FwcC9zZXR0aW5ncy9uYXZpZ2F0aW9uLXNldHRpbmdzL25hdmlnYXRpb24tc2V0dGluZ3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2ZvbnRzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMnO1xuXG4vLyAuaXMtdmFsaWQge1xuLy8gICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGJsdWVcbi8vIH1cblxuLmlzLWludmFsaWQge1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNFRTQwMzY7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM1RUIwMDNcbn1cblxuLmlzLWludmFsaWQtc2VsZWN0IHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5LWNvbG9yLTE7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6ICR3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgaGVpZ2h0OiAycmVtO1xufVxuXG4uYmctY29sb3ItMXtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDNBNTg7XG59XG5cbmlvbi10b29sYmFye1xuICAgIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgICBkaXNwbGF5OiBmbGV4IWltcG9ydGFudDtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXIhaW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmZsZXgtYWxpZ24tcmlnaHQge1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5mbGV4LWFsaWduLWNlbnRlciB7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgICBvcGFjaXR5OiAwLjU7XG59XG5cblxuXG5cbiIsIi5pcy1pbnZhbGlkIHtcbiAgYm9yZGVyOiAycHggc29saWQgI0VFNDAzNjtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwic3JjL2Fzc2V0cy9pY29uL2ljb24taGVscC5zdmdcIikgbm8tcmVwZWF0O1xufVxuXG4uaXMtdmFsaWQge1xuICBib3JkZXI6IDFweCBzb2xpZCAjNUVCMDAzO1xufVxuXG4uaXMtaW52YWxpZC1zZWxlY3Qge1xuICBib3JkZXI6IDFweCBzb2xpZCAjRUU0MDM2O1xufVxuXG4ucHJpbWFyeS1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAzQTU4O1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBoZWlnaHQ6IDJyZW07XG59XG5cbi5iZy1jb2xvci0xIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMDAzQTU4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnZlcnRpY2FsLWFsaWduIHtcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlciAhaW1wb3J0YW50O1xufVxuXG4uaG9yaXpvbnRhbC1hbGlnbiB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWFsaWduLXJpZ2h0IHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmZsZXgtYWxpZ24tY2VudGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5kaXNhYmxlIHtcbiAgb3BhY2l0eTogMC41O1xufVxuXG5pb24tdGl0bGUge1xuICBjb2xvcjogIzAwM0E1ODtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1Cb2xkXCI7XG4gIHBhZGRpbmctdG9wOiAxcmVtO1xufVxuXG5pb24tYnV0dG9ucyB7XG4gIHBhZGRpbmctYm90dG9tOiAzcmVtO1xufVxuXG4uZmxvYXQtbGVmdCB7XG4gIGZsb2F0OiBsZWZ0O1xuICBtYXJnaW4tYm90dG9tOiAzcmVtO1xufVxuXG4ubmF2aWdhdGlvbi1yb3cge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm5hdmlnYXRpb24taGVhZGVyIHtcbiAgYmFja2dyb3VuZDogI0YyRjJGMjtcbn1cblxuLm5hdmlnYXRpb24tbGFiZWwge1xuICBtYXJnaW4tdG9wOiAwLjVyZW07XG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdC1TZW1pQm9sZFwiO1xuICBmb250LXNpemU6IDE4cHg7XG59XG5cbi5pdGVtLWljb24ge1xuICB3aWR0aDogMS41cmVtO1xuICBoZWlnaHQ6IDEuNXJlbTtcbn1cblxuLnNhdmUtYnRuIHtcbiAgYmFja2dyb3VuZDogI0Y0NzczQiAxMDAlICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgtMTgwZGVnLCAjRkY4ODQwIDAlLCAjRUU0MDM2IDEwMCUpICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXNpemU6IDEuMTI1cmVtO1xuICBib3JkZXItcmFkaXVzOiAwLjM3NXJlbTtcbiAgaGVpZ2h0OiAzcmVtO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXQtQm9sZFwiO1xuICBsaW5lLWhlaWdodDogMS4zMTI1cmVtO1xufSIsIlxuLy8gY29sb3JzXG4vLyAkcHJpbWFyeS1jb2xvci0xOiAjMDBBM0M4O1xuJHByaW1hcnktY29sb3ItMTogIzAwM0E1ODtcbiRwcmltYXJ5LWNvbG9yLTI6ICNGQUFGNDA7XG4kcHJpbWFyeS1jb2xvci0zOiAjRjQ3NzNCO1xuXG4kd2hpdGU6ICNmZmZmZmY7XG5cbi8vICRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgcmdiYSgyNTUsMjU1LDI1NSwwLjUwKSAwJSwgcmdiYSgwLDAsMCwwLjUwKSAxMDAlKTtcbiRidXR0b24tZ3JhZGllbnQtMTogbGluZWFyLWdyYWRpZW50KC0xODBkZWcsICNGRjg4NDAgMCUsICNFRTQwMzYgMTAwJSk7IiwiQGltcG9ydCAnc3JjL2Fzc2V0cy9zY3NzL2NvbW1vbi5zY3NzJztcbkBpbXBvcnQgJ3NyYy9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XG5cblxuaW9uLXRpdGxlIHtcbiAgICBjb2xvcjogJHByaW1hcnktY29sb3ItMTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgcGFkZGluZy10b3A6IDFyZW07XG59XG5cbmlvbi1idXR0b25zIHtcbiAgICAvLyBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICAgIHBhZGRpbmctYm90dG9tOiAzcmVtO1xufVxuXG4uZmxvYXQtbGVmdHtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBtYXJnaW4tYm90dG9tOiAzcmVtO1xufVxuXG4ubmF2aWdhdGlvbi1yb3cge1xuICAgIHdpZHRoOiAxMDAlXG59XG5cbi5uYXZpZ2F0aW9uLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogI0YyRjJGMjtcbn1cblxuLm5hdmlnYXRpb24tbGFiZWwge1xuICAgIG1hcmdpbi10b3A6IC41cmVtO1xuICAgIG1hcmdpbi1ib3R0b206IC41cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdC1TZW1pQm9sZCc7XG4gICAgZm9udC1zaXplOiAxOHB4O1xufVxuXG4uaXRlbS1pY29uIHtcbiAgICB3aWR0aDogMS41cmVtO1xuICAgIGhlaWdodDogMS41cmVtO1xufVxuXG4uc2F2ZS1idG4ge1xuICAgIGJhY2tncm91bmQ6ICNGNDc3M0IgMTAwJSAhaW1wb3J0YW50O1xuICAgIGJhY2tncm91bmQtaW1hZ2U6ICRidXR0b24tZ3JhZGllbnQtMSAhaW1wb3J0YW50O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMS4xMjVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XG4gICAgaGVpZ2h0OiAzcmVtO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQtQm9sZCc7XG4gICAgbGluZS1oZWlnaHQ6IDEuMzEyNXJlbTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/settings/navigation-settings/navigation-settings.page.ts":
  /*!**************************************************************************!*\
    !*** ./src/app/settings/navigation-settings/navigation-settings.page.ts ***!
    \**************************************************************************/

  /*! exports provided: NavigationSettingsPage */

  /***/
  function srcAppSettingsNavigationSettingsNavigationSettingsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NavigationSettingsPage", function () {
      return NavigationSettingsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../shared/services/lauch-navigator.service */
    "./src/shared/services/lauch-navigator.service.ts");
    /* harmony import */


    var _shared_services_toast_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../shared/services/toast.service */
    "./src/shared/services/toast.service.ts");

    var NavigationSettingsPage =
    /*#__PURE__*/
    function () {
      function NavigationSettingsPage(launchNavService, toastService) {
        var _this = this;

        _classCallCheck(this, NavigationSettingsPage);

        this.launchNavService = launchNavService;
        this.toastService = toastService;
        this.isGoogleMaps = false;
        this.isAppleMaps = false;
        this.isWaze = false;
        this.isCityMapper = false;
        this.launchNavService.checkAvailableApp().then(function (availableApp) {
          _this.availableApps = availableApp;
        });
        console.log("available APPS**", this.availableApps);
      }

      _createClass(NavigationSettingsPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this2 = this;

          this.isGoogleMaps = false;
          this.isAppleMaps = false;
          this.isWaze = false; // this.isCityMapper = false;

          this.launchNavService.getUserSelectedMap().then(function (result) {
            console.log("***RES", result);

            if (result === "this.launchNavigator.APP.GOOGLE_MAPS" || result === "google_maps") {
              _this2.isGoogleMaps = true;
            } else if (result === "this.launchNavigator.APP.WAZE" || result === "waze") {
              _this2.isWaze = true;
            } else if (result === "this.launchNavigator.APP.APPLE_MAPS" || result === "apple_maps") {
              _this2.isAppleMaps = true; // } else if ( result === "this.launchNavigator.APP.CITYMAPPER" || result === "citymapper") {
              //   this.isCityMapper = true;
            }
          });
        }
      }, {
        key: "setDefaultNavigationApp",
        value: function setDefaultNavigationApp(app) {
          // console.log("Selected App", app);
          // this.launchNavService.setUserSelectedMap(app);
          // this.toastService.presentToast("Success");
          console.log("App", app);

          if (app === "waze") {
            this.isGoogleMaps = false;
            this.isAppleMaps = false;
            this.isWaze = true;
          } else if (app === "google_maps") {
            this.isGoogleMaps = true;
            this.isAppleMaps = false;
            this.isWaze = false;
          } else if (app === "apple_maps") {
            this.isGoogleMaps = false;
            this.isAppleMaps = true;
            this.isWaze = false; // } else if (app === "citymapper") {
            //   this.isGoogleMaps = false;
            //   this.isAppleMaps = false;
            //   this.isWaze = false;
            //   this.isCityMapper = true;
          }

          console.log("Waze", this.isWaze);
          console.log("Google", this.isGoogleMaps);
          console.log("Apple", this.isAppleMaps);
        }
      }, {
        key: "saveSelectedMap",
        value: function saveSelectedMap() {
          if (this.isWaze) {
            this.launchNavService.setUserSelectedMap("waze");
            this.toastService.presentToast("Saved Successfully");
          } else if (this.isGoogleMaps) {
            this.launchNavService.setUserSelectedMap("google_maps");
            this.toastService.presentToast("Saved Successfully");
          } else if (this.isAppleMaps) {
            this.launchNavService.setUserSelectedMap("apple_maps");
            this.toastService.presentToast("Saved Successfully"); // } else if (this.isCityMapper) {
            //   this.launchNavService.setUserSelectedMap("citymapper");
            //   this.toastService.presentToast("Saved Successfully");
          }
        }
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    console.log('Begin async operation');
                    this.ngOnInit();
                    event.target.complete();

                  case 3:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }]);

      return NavigationSettingsPage;
    }();

    NavigationSettingsPage.ctorParameters = function () {
      return [{
        type: _shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_2__["LaunchNavigatorService"]
      }, {
        type: _shared_services_toast_service__WEBPACK_IMPORTED_MODULE_3__["ToastService"]
      }];
    };

    NavigationSettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-navigation-settings',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./navigation-settings.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/navigation-settings/navigation-settings.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./navigation-settings.page.scss */
      "./src/app/settings/navigation-settings/navigation-settings.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_lauch_navigator_service__WEBPACK_IMPORTED_MODULE_2__["LaunchNavigatorService"], _shared_services_toast_service__WEBPACK_IMPORTED_MODULE_3__["ToastService"]])], NavigationSettingsPage);
    /***/
  }
}]);
//# sourceMappingURL=settings-navigation-settings-navigation-settings-module-es5.js.map